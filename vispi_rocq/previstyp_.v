Require Import ssreflect. 
Require Import ZArith Psatz. 
Require Export Names visfun stacks picalc.

Inductive preVisTyp : Visfun -> stack -> proc -> Prop :=
| previstyp_nil : preVisTyp vis0 stack_nil proc_nil
| previstyp_O_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (V : Vis) (s1 s2 : stack) (sO : stackO) (P : proc),
    (s s1) = In (stack_in p sO) -> (s s2) = Out sO ->
    let Vb := add_V V (srv b) in
    let B1 := add_vis (names (cnt p)) Vb (add_vis (names (srv b)) Vb vis0) in
    let B2 := add_vis cur V vis0 in
    V (srv a) -> preVisTyp B1 s1 P
    -> preVisTyp B2 s2 (proc_out_ser a n b p P)
| previstyp_O_CON : forall (p : Ncon) (n : Nfo) (b : Nser) (V : Vis) (P : proc),
    let Vb := add_V V (srv b) in
    let B1 := add_vis (names (cnt p)) Vb vis0 in
    let B2 := add_vis cur V vis0 in
    V (cnt p) -> preVisTyp B1 stack_nil P
    -> preVisTyp B2 (stack1 p) (proc_out_con p n b P)
| previstyp_O_LOC : forall (h: Nref) (n : Nfo), preVisTyp vis0 stack_nil (proc_out_loc h n)
| previstyp_I_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (V : Vis) (P : proc),
    let Vbp := add_V (add_V V (srv b)) (cnt p) in
    let B1 := add_vis cur Vbp vis0 in
    let B2 := add_vis (names (srv a)) V vis0 in
    preVisTyp B1 (stack1 p) P
    -> preVisTyp B2 stack_nil (proc_in_ser a n b p P)
| previstyp_I_CON : forall (p q : Ncon) (Hpq : p<> q) (n : Nfo) (b : Nser) (V : Vis) (P : proc),
    let Vb := add_V V (srv b) in
    let B1 := add_vis cur Vb vis0 in
    let B2 := add_vis (names (cnt p)) V vis0 in
    preVisTyp B1 (stack1 q) P
    -> preVisTyp B2 (stack2 p q Hpq) (proc_in_con p n b P)
| previstyp_I_LOC : forall (h : Nref) (n : Nfo) (V : Vis) (s : stack) (P : proc),
    let B := add_vis cur V vis0 in
    preVisTyp B s P -> preVisTyp B s (proc_in_loc h n P)
| previstyp_RES_SER : forall (a : Nser) (B : Visfun) (s : stack) (P : proc),
    preVisTyp B s P -> preVisTyp (trans_minus_vis (srv a) B) s (proc_nu_ser a P)
| previstyp_RES_CON1 : forall (p : Ncon) (B : Visfun) (s1 : stack) (s2 : stack) (P : proc),
    insert_stack_p p s1 s2 -> preVisTyp B s1 P -> preVisTyp (trans_minus_vis (cnt p) B) s2 (proc_nu_con p P)
| previstyp_RES_CON2 : forall (p : Ncon) (B : Visfun) (s : stack) (P : proc),
    not_in_stack p s -> preVisTyp B s P -> preVisTyp (trans_minus_vis (cnt p) B) s (proc_nu_con p P)
| previstyp_RES_LOC : forall (h : Nref) (B : Visfun) (s : stack) (P : proc),
    preVisTyp B s P -> preVisTyp B s (proc_nu_loc h P)
| previstyp_PAR : forall (B1 B2 : Visfun) (s s1 s2 : stack) (P1 P2 : proc),
    dom_disj (dom B1) (dom B2) -> inter s1 s2 s ->
    let B := union_vis B1 B2 in
    preVisTyp B1 s1 P1 -> preVisTyp B2 s2 P2 -> preVisTyp B s (proc_par P1 P2)
| previstyp_ITE : forall (x n : Nfo) (B : Visfun) (s : stack) (P1 P2 : proc),
    preVisTyp B s P1 -> preVisTyp B s P2 -> preVisTyp B s (proc_ite x n P1 P2)
| previstyp_FwC : forall (p q : Ncon) (Hpq : p<> q) (V : Vis),
    let B := add_vis (names (cnt p)) V vis0 in
    let s := stack2 p q Hpq in
    V (cnt q) -> preVisTyp B s (proc_fwd_con p q)
| previstyp_FwF : forall (a b : Nser) (V : Vis) ,
    let B := add_vis (names (srv a)) V vis0 in
    V (srv b) -> preVisTyp B stack_nil (proc_fwd_ser a b).

Definition is_fresh_nser (a : Nser) (B : Visfun) (s : stack) (P : proc) : Prop:=
  ~(dom B (names (srv a))) /\ (forall (x : Names_cur), ~(B x (srv a))) /\ notin_proc_ser a P.

Lemma exists_fresh_nser : forall (B : Visfun) (s : stack) (P : proc), exists (a : Nser), is_fresh_nser a B s P.
Proof.
  move=> B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_ser P) as [nP HP]. exists (give_nser (max nP (max ndomB ncodomB))).
  split.
  - have Hk : max nP (max ndomB ncodomB) >= ndomB by lia. apply (proj1 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB ncodomB) >= ncodomB by lia. intro x. apply (proj1 (HcodomB _ Hk x)).
    + have Hk : max nP (max ndomB ncodomB) >= nP by lia. apply (HP _ Hk).
Qed.

Definition is_fresh_ncon (p : Ncon) (B : Visfun) (s : stack) (P : proc) : Prop:=
  ~(dom B (names (cnt p))) /\ (forall (x : Names_cur), ~(B x (cnt p))) /\ notin_proc_con p P /\ not_in_stack p s.

Lemma exists_fresh_ncon : forall (B : Visfun) (s : stack) (P : proc), exists (p : Ncon), is_fresh_ncon p B s P.
Proof.
  intros B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_con P) as [nP HP]. destruct (stack_fin s) as [ns Hs]. exists (give_ncon (max nP (max ndomB (max ncodomB ns)))).
  split.
  - have Hk : max nP (max ndomB (max ncodomB ns)) >= ndomB by lia. apply (proj2 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB (max ncodomB ns)) >= ncodomB by lia. intro x. apply (proj2 (HcodomB _ Hk x)).
    + split.
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= nP by lia. apply (HP _ Hk).
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= ns by lia. apply (Hs _ Hk).
Qed.

Lemma dom_toplevel : forall (B : Visfun) (s : stack) (P : proc), preVisTyp B s P -> (forall (x : Names), dom B (names x) <-> in_toplevel_proc x P).
Proof.
  move=> B s P Htyp. induction Htyp.
  1-4,7 : move=> x; split=> [Hd | Htop]; [by destruct Hd | easy]. 
  - move=> x. split=> [Hd | Htop]. destruct Hd. easy. inversion H. by apply in_ser_toplevel.
    inversion Htop. right. by rewrite H1.
  - move=> x. split=> [Hd | Htop]. destruct Hd. easy. inversion H. by apply in_con_toplevel.
    inversion Htop. right. by rewrite H1.
  - move=> x. split=> [Hd | Htop]. destruct Hd. apply nu_ser_toplevel. intro. by rewrite H1 in H0.
    by apply IHHtyp.
    inversion Htop. split. by apply IHHtyp. intro. inversion H4. by rewrite H6 in H2.
  - move=> x; split=> [Hd | Htop];[ destruct Hd;  apply nu_con_toplevel|]. intro. by rewrite H2 in H1.
    by apply IHHtyp.
    inversion Htop; split;[ by apply IHHtyp|]. intro. inversion H5. by rewrite H7 in H3.
  - move=> x. split=> [Hd | Htop]. destruct Hd. apply nu_con_toplevel. intro. by rewrite H2 in H1.
    by apply IHHtyp.
    inversion Htop. split. by apply IHHtyp. intro. inversion H5. by rewrite H7 in H3.
  - move=> x; split=> [Hd | Htop] ;[ apply nu_loc_toplevel | inversion Htop]. all : by apply IHHtyp.
  - move=> x. split=> [Hd | Htop].
    destruct Hd. apply par_toplevel1. by apply IHHtyp1. apply par_toplevel2. by apply IHHtyp2.
    inversion Htop. left. by apply IHHtyp1. right. by apply IHHtyp2.
  - move=> y. split=> [Hd | Htop].
    apply ite_toplevel. by apply IHHtyp1. by apply IHHtyp2.
    inversion Htop. by apply IHHtyp1.
  - move=> x. split=> [Hd | Htop]. apply fwd_con_toplevel. destruct Hd. easy. by inversion H0.
    inversion Htop. right. by rewrite H2.
  - move=> x. split=> [Hd | Htop]. apply fwd_ser_toplevel. destruct Hd. easy. by inversion H0.
    inversion Htop. right. by rewrite H2.
Qed.

Lemma preWeakening_aux : forall (n: nat) (P : proc), (size P) <= n -> forall (B1 B2 : Visfun) (s : stack), preVisTyp B1 s P -> Visfun_weak B1 B2 -> preVisTyp B2 s P.
Proof.
  induction n=> P HsP B1 B2 s Htyp Hweak.
  -  inversion HsP. destruct P; try easy. inversion Htyp.
     rewrite (eq_sym H) in Hweak. rewrite (Visfun_weak_empty _ Hweak).
     apply previstyp_nil.
  - inversion HsP. 2: apply (IHn P H0 B1 B2 s Htyp Hweak). destruct P; try easy; inversion H0.
    + inversion Htyp.
      rewrite (eq_sym H5) in Hweak.
      have H_ : ~(dom vis0 cur) by easy. destruct (Visfun_weak_add vis0 B2 cur V H_ Hweak) as [B4 [HB2 HB4]].
      rewrite (Visfun_weak_empty _ HB4) in HB2. rewrite HB2.
      apply (previstyp_O_SER _ _ _ _ _ s1 s sO); try easy.
      apply (proj2 Hweak). by right.
      have HVb0 : forall (x : Names), Vb0 x -> (fun x : Names => B2 cur x \/ x = srv n2) x.
      move=> x HVb0. destruct HVb0 as [HV | HV]. have HB3 : B3 cur x. simpl. right. easy.
      left. rewrite H5 in HB3. apply (proj2 Hweak). by rewrite H5. by right.
      have HPn : size P <= n by lia.
      apply (IHn P HPn B0 _ _ H11). apply (weak_add _ _ (names (cnt n3)) _ _).
      apply (weak_add _ _ (names (srv n2)) _ _). apply Visfun_weak_refl. apply HVb0. apply HVb0.
    + inversion Htyp. rewrite (eq_sym H3) in Hweak.
      have H_ : ~(dom vis0 cur) by easy. destruct (Visfun_weak_add vis0 B2 cur V H_ Hweak) as [B4 [HB2 HB4]].
      rewrite (Visfun_weak_empty B4 HB4) in HB2. 
      rewrite HB2. apply previstyp_O_CON.
      apply (proj2 Hweak). right. by split.
      have HPn : size P <= n by lia. apply (IHn P HPn B0 _ _ H8).
      apply (weak_add _ _ (names (cnt n0)) Vb _). apply Visfun_weak_refl.
      move=> x HVb. destruct HVb as [HV | HV]. left. apply (proj2 Hweak). right. by split.
      right. by rewrite H6 in HV.
    + inversion Htyp.
      rewrite (eq_sym H) in Hweak. rewrite (Visfun_weak_empty B2 Hweak).
      apply previstyp_O_LOC.
    + inversion Htyp. rewrite (eq_sym H2) in Hweak.
      have H_ : ~(dom vis0 (names (srv a))) by easy. destruct (Visfun_weak_add vis0 B2 (names (srv a)) V H_ Hweak) as [B5 [HB2 HB5]].
      rewrite (Visfun_weak_empty B5 HB5) in HB2. rewrite HB2.
      rewrite H. apply previstyp_I_SER. have HPn : size P <= n by lia. apply (IHn P HPn B0 _ _ H4).
      apply weak_add. easy.  rewrite (eq_sym H7). apply add_incl_V. rewrite (eq_sym H6). apply add_incl_V.
      rewrite (eq_sym H). move=> x HV. apply (proj2 Hweak). right. by split.
    + inversion Htyp. rewrite (eq_sym H2) in Hweak.
      have H_ : ~(dom vis0 (names (cnt p))) by easy. destruct (Visfun_weak_add vis0 B2 (names (cnt p)) V H_ Hweak) as [B5 [HB2 HB5]].
      rewrite (Visfun_weak_empty B5 HB5) in HB2. rewrite HB2.
      rewrite H. apply previstyp_I_CON. have HPn : size P <= n by lia. apply (IHn P HPn B0 _ _ H4).
      apply weak_add. easy. rewrite (eq_sym H6). apply add_incl_V. rewrite (eq_sym H).
      move=> x HV. apply (proj2 Hweak). right. by split.
    + inversion Htyp. rewrite (eq_sym H2) in Hweak.
      have H_ : ~(dom vis0 cur) by easy. destruct (Visfun_weak_add vis0 B2 cur V H_ Hweak) as [B3 [HB2 HB3]].
      rewrite (Visfun_weak_empty B3 HB3) in HB2. rewrite HB2.
      apply previstyp_I_LOC. have HPn : size P <= n by lia. apply (IHn P HPn B _ _ H4). apply weak_add. easy.
      move=> x HV. apply (proj2 Hweak cur x). right. by split.
    + destruct (exists_fresh_nser B2 s (proc_nu_ser n0 P)) as [a Ha].
      have Hn : (notfree_ser n0 (proc_nu_ser n0 P)). apply ser_notfree_nu_ser. by left.
      destruct (exists_alpha_convert_proc_ser n0 a (proc_nu_ser n0 P)) as [Q HQ].
      have HPQ : (proc_nu_ser n0 P) = Q. apply (eq_proc_ser _ _ n0 a);try assumption. by destruct Ha as [_ [_ H]]. 
      rewrite HPQ in Htyp. rewrite HPQ. inversion HQ. 2: easy.
      rewrite (eq_sym H5) in Htyp. inversion Htyp.
      rewrite (eq_sym H7) in Hweak. rewrite H6 in Hweak.
      have Hfa : is_fresh_vis (srv a) B2. destruct Ha as [Hda [HBa _]]. by split.
      destruct (Visfun_weak_trans_minus _ B2 _ Hfa Hweak) as [B3 [HB2 HB3]].  rewrite HB2.
      apply previstyp_RES_SER.
      have HsQ : size Q0 = n by rewrite (eq_sym (size_alpha_ser P Q0 n0 a H4)).
      have HsQ' : size Q0 <= n by lia. by apply (IHn Q0 HsQ' B B3 s).
    + destruct (exists_fresh_ncon B2 s (proc_nu_con n0 P)) as [p Hp].
      have Hn : (notfree_con n0 (proc_nu_con n0 P)). apply con_notfree_nu_con. by left.
      destruct (exists_alpha_convert_proc_con n0 p (proc_nu_con n0 P)) as [Q HQ].
      have HPQ : (proc_nu_con n0 P) = Q. apply (eq_proc_con _ _ n0 p);try assumption. by destruct Hp as [_ [_ [H _]]]. 
      rewrite HPQ in Htyp. rewrite HPQ. inversion HQ. 2: easy.
      rewrite (eq_sym H5) in Htyp. inversion Htyp.
      * rewrite (eq_sym H8) in Hweak. rewrite H6 in Hweak.
        have Hfp : is_fresh_vis (cnt p) B2. destruct Hp as [Hdp [HBp _]]. by split.
        destruct (Visfun_weak_trans_minus _ B2 _ Hfp Hweak) as [B3 [HB2 HB3]].  rewrite HB2.
        apply (previstyp_RES_CON1 _ _ s1). assumption.
        have HsQ : size Q0 = n by rewrite (eq_sym (size_alpha_con P Q0 n0 p H4)).
        have HsQ' : size Q0 <= n by lia. by apply (IHn Q0 HsQ' B B3 s1).
      * rewrite (eq_sym H8) in Hweak. rewrite H6 in Hweak.
        have Hfp : is_fresh_vis (cnt p) B2. destruct Hp as [Hdp [HBp _]]. by split.
        destruct (Visfun_weak_trans_minus _ B2 _ Hfp Hweak) as [B3 [HB2 HB3]].  rewrite HB2.
        apply previstyp_RES_CON2. assumption.
        have HsQ : size Q0 = n by rewrite (eq_sym (size_alpha_con P Q0 n0 p H4)).
        have HsQ' : size Q0 <= n by lia. by apply (IHn Q0 HsQ' B B3 s).
    + inversion Htyp. apply previstyp_RES_LOC.
      have HsP' : size P <= n by lia. by apply (IHn P HsP' B1).
    + inversion Htyp. rewrite (eq_sym H5) in Hweak. destruct (Visfun_weak_union _ _ _ Hweak) as [B4 [ B5 [HB2 [HB4 HB5]]]]. rewrite HB2. apply (previstyp_PAR _ _ s s1 s2). apply (dom_disj_equiv (dom B0) (dom B3)); try assumption. by apply (proj1 HB4). by apply (proj1 HB5). assumption.
      have HsP1 : size P1 <= n by lia. by apply (IHn _ HsP1 B0).
      have HsP2 : size P2 <= n by lia. by apply (IHn _ HsP2 B3).
    + inversion Htyp.  apply previstyp_ITE. 
      have HsP1 : size P1 <= n by lia. by apply (IHn _ HsP1 B1).
      have HsP2 : size P2 <= n by lia. by apply (IHn _ HsP2 B1).
    + inversion Htyp. have H_ : ~(dom vis0 (names (srv a))) by easy. rewrite (eq_sym H2) in Hweak.
      destruct (Visfun_weak_add vis0 B2 (names (srv a)) V H_ Hweak) as [B3 [HB2 HB3]].
      rewrite (Visfun_weak_empty B3 HB3) in HB2. rewrite HB2. rewrite H.
      apply previstyp_FwF. apply (proj2 Hweak). right. rewrite H. by split.
    + inversion Htyp.  have H_ : ~(dom vis0 (names (cnt p))) by easy. rewrite (eq_sym H2) in Hweak.
      destruct (Visfun_weak_add vis0 B2 (names (cnt p)) V H_ Hweak) as [B3 [HB2 HB3]].
      rewrite (Visfun_weak_empty B3 HB3) in HB2. rewrite HB2. rewrite H. rewrite (eq_sym H5).
      apply previstyp_FwC. apply (proj2 Hweak). right.  rewrite H5. rewrite H.  by split.
Qed.

Lemma preWeakening : forall (P : proc) (B1 B2 : Visfun) (s : stack), preVisTyp B1 s P -> Visfun_weak B1 B2 -> preVisTyp B2 s P.
Proof.
  move=> P. apply (preWeakening_aux (size P) P). lia.
Qed.
