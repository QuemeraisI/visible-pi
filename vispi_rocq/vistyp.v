Require Import ssreflect. 
Require Import ZArith Psatz. 
Require Export Names visfun stacks picalc visfun_trans.

Inductive VisTyp : Visfun_trans -> stack -> proc -> Prop :=
| vistyp_nil : forall (B : Visfun_trans), dom_eq (dom B) empty -> VisTyp B stack_nil proc_nil
| vistyp_O_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (B2 : Visfun_trans) (s1 s2 : stack) (sO : stackO) (P : proc),
    (a <> b) -> (s s1) = In (stack_in p sO) -> (s s2) = Out sO -> dom_eq (dom B2) (add_dom empty cur) ->
    is_fresh_vis (srv b) B2 -> is_fresh_vis (cnt p) B2 ->
    let Vb := add_V (B2 cur) (srv b) in
    let B1 := add2_vis_trans (names (cnt p)) (names (srv b)) Vb in
    (B2 cur (srv a)) -> VisTyp B1 s1 P
    -> VisTyp B2 s2 (proc_out_ser a n b p P)
| vistyp_O_CON : forall (p : Ncon) (n : Nfo) (b : Nser) (B2 : Visfun_trans) (P : proc), dom_eq (dom B2) (add_dom empty cur) -> is_fresh_vis (srv b) B2 ->
    let Vb := add_V (B2 cur) (srv b) in
    let B1 := add1_vis_trans (names (cnt p)) Vb in
    (B2 cur (cnt p)) -> VisTyp B1 stack_nil P
    -> VisTyp B2 (stack1 p) (proc_out_con p n b P)
| vistyp_O_LOC : forall (h: Nref) (n : Nfo) (B : Visfun_trans), dom_eq (dom B) empty -> VisTyp B stack_nil (proc_out_loc h n)
| vistyp_I_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (B2 : Visfun_trans) (P : proc),
    (a <> b)->  dom_eq (dom B2) (add_dom empty (names (srv a))) -> is_fresh_vis (srv b) B2 -> is_fresh_vis (cnt p) B2 ->
    let Vbp := add_V (add_V (B2 (names (srv a))) (srv b)) (cnt p) in
    let B1 := add1_vis_trans cur Vbp in
    VisTyp B1 (stack1 p) P
    -> VisTyp B2 stack_nil (proc_in_ser a n b p P)
| vistyp_I_CON : forall (p q : Ncon) (Hpq : p<> q) (n : Nfo) (b : Nser) (B2 : Visfun_trans) (P : proc),
    dom_eq (dom B2) (add_dom empty (names (cnt p))) -> is_fresh_vis (srv b) B2 ->
    let Vb := add_V (B2 (names (cnt p))) (srv b) in
    let B1 := add1_vis_trans cur Vb in
    VisTyp B1 (stack1 q) P
    -> VisTyp B2 (stack2 p q Hpq) (proc_in_con p n b P)
| vistyp_I_LOC : forall (h : Nref) (n : Nfo) (B : Visfun_trans) (s : stack) (P : proc),
    dom_eq (dom B) (add_dom empty cur) ->
    VisTyp B s P -> VisTyp B s (proc_in_loc h n P)
| vistyp_RES_SER : forall (a : Nser) (B1 B2 : Visfun_trans) (s : stack) (P : proc),
    Visfun_eq B2 (erase_vis_trans (srv a) B1) -> dom B1 (names (srv a)) ->
    VisTyp B1 s P -> VisTyp B2 s (proc_nu_ser a P)
| vistyp_RES_CON1 : forall (p : Ncon) (B1 B2 : Visfun_trans) (s1 : stack) (s2 : stack) (P : proc),
    Visfun_eq B2 (erase_vis_trans (cnt p) B1) -> dom B1 (names (cnt p)) ->
    insert_stack_p p s1 s2 -> VisTyp B1 s1 P -> VisTyp B2 s2 (proc_nu_con p P)
| vistyp_RES_CON2 : forall (p : Ncon) (B1 B2 : Visfun_trans) (s : stack) (P : proc),
    Visfun_eq B2 (erase_vis_trans (cnt p) B1) -> 
    not_in_stack p s -> VisTyp B1 s P -> VisTyp B2 s (proc_nu_con p P)
| vistyp_RES_LOC : forall (h : Nref) (B : Visfun_trans) (s : stack) (P : proc),
    VisTyp B s P -> VisTyp B s (proc_nu_loc h P)
| vistyp_PAR : forall (B1 B2 B3 : Visfun_trans) (s s1 s2 : stack) (P1 P2 : proc),
    dom_disj (dom B1) (dom B2) -> Visfun_eq B3 (union_vis B1 B2) -> inter s1 s2 s ->
    VisTyp B1 s1 P1 -> VisTyp B2 s2 P2 -> VisTyp B3 s (proc_par P1 P2)
| vistyp_ITE : forall (x n : Nfo) (B : Visfun_trans) (s : stack) (P1 P2 : proc),
    VisTyp B s P1 -> VisTyp B s P2 -> VisTyp B s (proc_ite x n P1 P2)
| vistyp_FwC : forall (p q : Ncon) (Hpq : p<> q) (B : Visfun_trans),
    dom_eq (dom B) (add_dom empty (names (cnt p))) ->
    let s := stack2 p q Hpq in
    (B (names (cnt p)) (cnt q)) -> VisTyp B s (proc_fwd_con p q)
| vistyp_FwF : forall (a b : Nser) (B : Visfun_trans) ,
    (a<> b)-> dom_eq (dom B) (add_dom empty (names (srv a))) ->
    (B (names (srv a)) (srv b)) -> VisTyp B stack_nil (proc_fwd_ser a b).

Definition is_fresh_nser (a : Nser) (B : Visfun) (s : stack) (P : proc) : Prop:=
  ~(dom B (names (srv a))) /\ (forall (x : Names_cur), ~(B x (srv a))) /\ notin_proc_ser a P.

Lemma exists_fresh_nser : forall (B : Visfun) (s : stack) (P : proc), exists (a : Nser), is_fresh_nser a B s P.
Proof.
  move=> B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_ser P) as [nP HP]. exists (give_nser (max nP (max ndomB ncodomB))).
  split.
  - have Hk : max nP (max ndomB ncodomB) >= ndomB by lia. apply (proj1 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB ncodomB) >= ncodomB by lia. intro x. apply (proj1 (HcodomB _ Hk x)).
    + have Hk : max nP (max ndomB ncodomB) >= nP by lia. apply (HP _ Hk).
Qed.

Definition is_fresh_ncon (p : Ncon) (B : Visfun) (s : stack) (P : proc) : Prop:=
  ~(dom B (names (cnt p))) /\ (forall (x : Names_cur), ~(B x (cnt p))) /\ notin_proc_con p P /\ not_in_stack p s.

Lemma exists_fresh_ncon : forall (B : Visfun) (s : stack) (P : proc), exists (p : Ncon), is_fresh_ncon p B s P.
Proof.
  intros B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_con P) as [nP HP]. destruct (stack_fin s) as [ns Hs]. exists (give_ncon (max nP (max ndomB (max ncodomB ns)))).
  split.
  - have Hk : max nP (max ndomB (max ncodomB ns)) >= ndomB by lia. apply (proj2 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB (max ncodomB ns)) >= ncodomB by lia. intro x. apply (proj2 (HcodomB _ Hk x)).
    + split.
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= nP by lia. apply (HP _ Hk).
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= ns by lia. apply (Hs _ Hk).
Qed.

Lemma con_in_stack : forall (p : Ncon) (s : stack) (B : Visfun_trans) (P : proc), VisTyp B s P -> not_in_stack p s -> ~ dom B (names (cnt p)).
  move=> p s B P Htyp. induction Htyp=> Hstack Hd.
  - have Hdempty : empty (names (cnt p)) by apply H. easy.
  - have Hdcur : (add_dom empty cur) (names (cnt p)) by apply H2. destruct Hdcur. easy. easy.
  - have Hdcur : (add_dom empty cur) (names (cnt p)) by apply H. destruct Hdcur. easy. easy.
  - have Hdempty : empty (names (cnt p)) by apply H. easy.
  - have Hda : (add_dom empty (names (srv a))) (names (cnt p)) by apply H0. destruct Hda.
    easy. easy.
  - have Hdp0 : (add_dom empty (names (cnt p0))) (names (cnt p)) by apply H. destruct Hdp0. easy.
    inversion H1. inversion Hstack. by symmetry in H3.
  - have Hdcur : (add_dom empty cur) (names (cnt p)) by apply H. destruct Hdcur. easy. easy.
  - have Hd1 : dom (erase_vis_trans (srv a) B1) (names (cnt p)) by apply H. destruct Hd1. apply (IHHtyp Hstack H1).
  - have Hder : dom (erase_vis_trans (cnt p0) B1) (names (cnt p)) by apply H. destruct Hder.
    move: H2. apply IHHtyp. apply (insert_not_in p0 p s1 s2); try assumption. intro. by rewrite H2 in H3.
  - have Hder : dom (erase_vis_trans (cnt p0) B1) (names (cnt p)) by apply H. destruct Hder.
    by apply IHHtyp.
  - by apply IHHtyp.
  - destruct (inter_notin1 p s1 s2 s H1 Hstack). have Hun : dom (union_vis B1 B2) (names (cnt p)) by apply H0.
    destruct Hun. by apply IHHtyp1. by apply IHHtyp2.
  - by apply IHHtyp1.
  - have Hdp0 : (add_dom empty (names (cnt p0))) (names (cnt p)) by apply H. destruct Hdp0.
    easy. inversion H1. inversion Hstack. by symmetry in H3.
  - have Hda : (add_dom empty (names (srv a))) (names (cnt p)) by apply H0. by destruct Hda.
Qed.

Lemma Weakening_aux : forall (n: nat) (P : proc), (size P) <= n -> forall (B1 B2 : Visfun_trans) (s : stack), VisTyp B1 s P -> Visfun_weak B1 B2 -> VisTyp B2 s P.
  induction n=> P HnP B1 B2 s Htyp Hweak.
  - inversion HnP. destruct P; try easy. inversion Htyp. apply vistyp_nil. apply (dom_eq_trans _ (dom B1)).
    apply dom_eq_sym. by apply Hweak. assumption.
  - inversion HnP. 2: apply (IHn P H0 B1 B2 s Htyp Hweak).
    destruct P; try easy.
    + destruct (exists_fresh_nser B2 s (proc_out_ser n0 n1 n2 n3 P)) as [b Hfb].
      destruct (exists_alpha_convert_proc_ser n2 b (proc_out_ser n0 n1 n2 n3 P)) as [Q_ HalphQ_].
      destruct (exists_fresh_ncon B2 s Q_) as [p Hfp].
      destruct (exists_alpha_convert_proc_con n3 p Q_) as [Q HalphQ].
      have HPQ : (proc_out_ser n0 n1 n2 n3 P) = Q.
      have HQQ_ : Q_ = Q. move: HalphQ. apply eq_proc_con.  by apply Hfp. apply (alpha_proc_ser_notfree_con n2 b _ (proc_out_ser n0 n1 n2 n3 P)). apply con_notfree_out_ser. by left. assumption.
      rewrite (eq_sym HQQ_). move: HalphQ_. apply eq_proc_ser. by apply Hfb. apply ser_notfree_out_ser.
      symmetry. by inversion Htyp.  by left. rewrite HPQ.
      inversion HalphQ_. rewrite (eq_sym H3) in HalphQ. 2,3 : rewrite (eq_sym H4) in HalphQ.
      4 : rewrite (eq_sym H5) in HalphQ.
      all : inversion HalphQ. all : rewrite HPQ in Htyp.
      1 : rewrite (eq_sym H11) in Htyp. 2,5 : rewrite (eq_sym H12) in Htyp. 3,6 : rewrite (eq_sym H13) in Htyp.
      4,7 : rewrite (eq_sym H14) in Htyp. 8 : rewrite (eq_sym H15) in Htyp.
      all : inversion Htyp. all : try easy.
      apply (vistyp_O_SER _ _ _ _ _ s1 _ sO); try assumption. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym.
      by apply Hweak. assumption. destruct Hfb. split. assumption. by destruct H33.
      destruct Hfp. split. assumption. by destruct H33.
      by apply Hweak. rewrite HPQ in H0. rewrite (eq_sym H12) in H0. inversion H0.
      have HnQ1 : size Q1 <= n by lia. apply (IHn Q1 HnQ1 B3 _). assumption.
      apply add2_trans_weak. apply add_incl_V. rewrite H25. move=> y HB1.  by apply Hweak.
    + destruct (exists_fresh_nser B2 s (proc_out_con n0 n1 n2 P)) as [b Hfb].
      destruct (exists_alpha_convert_proc_ser n2 b (proc_out_con n0 n1 n2 P)) as [Q HalphQ].
      have HPQ : proc_out_con n0 n1 n2 P = Q. apply (eq_proc_ser _ _ n2 b). apply Hfb. apply ser_notfree_out_con.
      by left. assumption.
      rewrite HPQ. inversion HalphQ. 2 : easy.
      rewrite HPQ in Htyp. rewrite (eq_sym H3) in Htyp. inversion Htyp.
      apply vistyp_O_CON. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. by apply Hweak. assumption.
      destruct Hfb. destruct H18. by split. by apply Hweak. rewrite HPQ in H0. rewrite (eq_sym H3) in H0.
      inversion H0. have HnQ0 : size Q0 <= n by lia. apply (IHn Q0 HnQ0 B3). assumption.
      apply add1_trans_weak. rewrite (eq_sym H9). apply add_incl_V. rewrite H11. move=> z HB. by apply Hweak.
    + inversion Htyp. apply vistyp_O_LOC. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak.
      assumption.
    + destruct (exists_fresh_nser B2 s (proc_in_ser n0 n1 n2 n3 P)) as [b Hfb].
      destruct (exists_alpha_convert_proc_ser n2 b (proc_in_ser n0 n1 n2 n3 P)) as [Q_ HalphQ_].
      destruct (exists_fresh_ncon B2 s Q_) as [p Hfp].
      destruct (exists_alpha_convert_proc_con n3 p Q_) as [Q HalphQ].
      have HPQ : (proc_in_ser n0 n1 n2 n3 P) = Q.
      have HQQ_ : Q_ = Q. move: HalphQ. apply eq_proc_con.  by apply Hfp. apply (alpha_proc_ser_notfree_con n2 b _ (proc_in_ser n0 n1 n2 n3 P)). apply con_notfree_in_ser. by left. assumption.
      rewrite (eq_sym HQQ_). move: HalphQ_. apply eq_proc_ser. by apply Hfb. apply ser_notfree_in_ser.
      symmetry. by inversion Htyp.  by left. rewrite HPQ.
      inversion HalphQ_. rewrite (eq_sym H3) in HalphQ. 2,3 : rewrite (eq_sym H4) in HalphQ.
      4 : rewrite (eq_sym H5) in HalphQ.
      all : inversion HalphQ. all : rewrite HPQ in Htyp.
      1 : rewrite (eq_sym H11) in Htyp. 2,5 : rewrite (eq_sym H12) in Htyp. 3,6 : rewrite (eq_sym H13) in Htyp.
      4,7 : rewrite (eq_sym H14) in Htyp. 8 : rewrite (eq_sym H15) in Htyp.
      all : inversion Htyp. all : try easy.
      apply vistyp_I_SER. intro. rewrite H29 in Hfb. by destruct Hfb.
      apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. by apply Hweak. assumption.
      split; by apply Hfb. split; by apply Hfp. rewrite HPQ in H0. rewrite (eq_sym H12) in H0.
      inversion H0. have HnQ1 : size Q1 <= n by lia. apply (IHn _ HnQ1 B3); try assumption.
      apply add1_trans_weak. rewrite (eq_sym H17). rewrite (eq_sym H19). rewrite (eq_sym H20). apply add_incl_V.
      apply add_incl_V. move=> y HBy. apply Hweak. by rewrite (eq_sym H22).
    + destruct (exists_fresh_nser B2 s (proc_in_con n0 n1 n2 P)) as [b Hfb].
      destruct (exists_alpha_convert_proc_ser n2 b (proc_in_con n0 n1 n2 P)) as [Q HalphQ].
      have HPQ : proc_in_con n0 n1 n2 P = Q. apply (eq_proc_ser _ _ n2 b). apply Hfb. apply ser_notfree_in_con.
      by left. assumption.
      rewrite HPQ. inversion HalphQ. 2 : easy.
      rewrite HPQ in Htyp. rewrite (eq_sym H3) in Htyp. inversion Htyp.
      apply vistyp_I_CON. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. by apply Hweak. assumption.
      split; by apply Hfb. rewrite HPQ in H0. rewrite (eq_sym H3) in H0. inversion H0.
      have HnQ0 : size Q0 <= n by lia. apply (IHn _ HnQ0 B3). assumption.
      apply add1_trans_weak. rewrite (eq_sym H7). rewrite (eq_sym H9). apply add_incl_V.
      move=> y HBy. apply Hweak. by rewrite H10 in HBy.
    + inversion Htyp. inversion H0. have HnP_ : size P <= n by lia.
      apply vistyp_I_LOC. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. by apply Hweak. assumption.
      by apply (IHn _ HnP_ B1).
    + destruct (exists_fresh_nser B2 s (proc_nu_ser n0 P)) as [a Hfa].
      destruct (exists_alpha_convert_proc_ser n0 a (proc_nu_ser n0 P)) as [Q HalphQ].
      have HPQ : (proc_nu_ser  n0 P) = Q. apply (eq_proc_ser _ _ n0 a). apply Hfa.
      apply ser_notfree_nu_ser. by left. assumption. rewrite HPQ in H0. rewrite HPQ in Htyp.
      inversion HalphQ. 2 : easy. rewrite (eq_sym H4) in H0. rewrite (eq_sym H4) in Htyp.
      inversion Htyp. rewrite HPQ. rewrite (eq_sym H4).
      have Hfa2 : is_fresh_vis (srv a) B2. split; by apply Hfa.
      have Hweak2 : Visfun_weak (erase_vis (srv a) B0) B2. by apply (Visfun_weak_eq B1 _ B2). 
      destruct (Visfun_weak_trans_minus B0 B2 (srv a) Hfa2 H10 Hweak2) as [B4 [HBeq HBweak]].
      apply (vistyp_RES_SER _ B4). assumption. by apply HBweak. inversion H0.
      have HnQ0 : size Q0 <= n by lia. by apply (IHn _ HnQ0 B0).
    + destruct (exists_fresh_ncon B2 s (proc_nu_con n0 P)) as [p Hfp].
      destruct (exists_alpha_convert_proc_con n0 p (proc_nu_con n0 P)) as [Q HalphQ].
      have HPQ : (proc_nu_con n0 P) = Q. apply (eq_proc_con _ _ n0 p). apply Hfp.
      apply con_notfree_nu_con. by left. assumption. rewrite HPQ in H0. rewrite HPQ in Htyp. rewrite HPQ.
      inversion HalphQ. 2: easy. rewrite (eq_sym H4) in H0. rewrite (eq_sym H4) in Htyp.
      have Hfp2 : is_fresh_vis (cnt p) B2. split; by apply Hfp.
      inversion H0. have HnQ0 : size Q0 <= n by lia. 
      inversion Htyp.
      *  have Hweak2 : Visfun_weak (erase_vis (cnt p) B0) B2. by apply (Visfun_weak_eq B1 _ B2).
         destruct (Visfun_weak_trans_minus B0 B2 (cnt p) Hfp2 H9 Hweak2) as [B4 [HBeq HBweak]].
         apply (vistyp_RES_CON1 _ B4 _ s1 s). assumption. by apply HBweak. assumption.
         by apply (IHn _ HnQ0 B0).
      * have Hnd : ~dom B0 (names (cnt p)) by apply (con_in_stack p s B0 Q0).
        have Hweak2 : Visfun_weak (erase_vis (cnt p) B0) B2. by apply (Visfun_weak_eq B1 _ B2).
        destruct (Visfun_weak_trans_minus_bis B0 B2 (cnt p) Hfp2 Hnd Hweak2) as [B4 [HBeq HBweak]].
        apply (vistyp_RES_CON2 _ B4). assumption. assumption. by apply (IHn _ HnQ0 B0).
    + inversion Htyp. inversion H0. have HnP_ : size P <= n by lia.
      apply vistyp_RES_LOC. by apply (IHn _ HnP_ B1).
    + inversion Htyp. destruct (Visfun_weak_union_trans B0 B3 B2 H2) as [B5 [B6 [Heq [Hweak1 Hweak2]]]]. by apply (Visfun_weak_eq B1 _ B2).
      inversion H0. have HnP1 : size P1 <= n by lia. have HnP2 : size P2 <= n by lia.
      apply (vistyp_PAR B5 B6 B2 s s1 s2). move=> x Hdx5 Hdx6.
      have Hdx0 : dom B0 x by apply Hweak1. have Hdx3 : dom B3 x by apply Hweak2. by apply (H2 x Hdx0 Hdx3).
      assumption. assumption.
      by apply (IHn _ HnP1 B0). by apply (IHn _ HnP2 B3).
    + inversion Htyp. inversion H0. have HnP1 : size P1 <= n by lia. have HnP2 : size P2 <= n by lia.
      apply vistyp_ITE. by apply (IHn _ HnP1 B1). by apply (IHn _ HnP2 B1).
    + inversion Htyp. apply vistyp_FwF.  assumption. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym.
      apply Hweak. assumption. by apply Hweak.
    + inversion Htyp. rewrite (eq_sym H1). apply vistyp_FwC.  apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym.
      apply Hweak. assumption. rewrite H1. by apply Hweak.
Qed.

Lemma Weakening : forall (P : proc) (B1 B2 : Visfun_trans) (s : stack), VisTyp B1 s P -> Visfun_weak B1 B2 -> VisTyp B2 s P.
Proof.
  move=> P. have HnP : size P <= size P by lia. apply (Weakening_aux (size P) P HnP).
Qed.

