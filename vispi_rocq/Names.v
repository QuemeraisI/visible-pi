Require Import ssreflect. 
Parameter Nser Ncon Nref Nfo : Type.
Axiom Nser_dec : forall (x y : Nser), (x=y) \/ (x<> y).
Axiom Ncon_dec : forall (x y : Ncon), (x=y) \/ (x <> y).


Inductive Names :=
| srv : Nser -> Names
| cnt : Ncon -> Names.

Inductive Names_cur :=
| cur : Names_cur
| names : Names -> Names_cur.

Parameter give_nser : nat -> Nser.
Axiom infinite_nser : forall (n1 n2 : nat), (give_nser n1 = give_nser n2 -> n1 = n2).
Axiom given_nser : forall (x : Nser), (forall (n : nat), (give_nser n <> x)) \/ (exists (n : nat), (give_nser n = x)).
Parameter give_ncon : nat -> Ncon.
Axiom infinite_ncon : forall (n1 n2 : nat), (give_ncon n1 = give_ncon n2 -> n1 = n2).
Axiom given_ncon : forall (x : Ncon), (forall (n : nat), (give_ncon n <> x)) \/ (exists (n : nat), (give_ncon n = x)).

Lemma Names_dec : forall (x y : Names), (x=y) \/ (x <> y).
Proof.
  move=> x y. destruct x as [x | x ]; destruct y as [y | y].
  2,3 : right;easy. 2 : case (Ncon_dec x y)=> Hxy. case (Nser_dec x y)=> Hxy.
  1,3 : left; by rewrite Hxy.
  all : right; move=> Hsab; by inversion Hsab.
Qed.

Lemma Names_cur_dec : forall (x y : Names_cur), (x=y) \/ (x <> y).
Proof.
  move=> x y. destruct x as [|x]; destruct y as [|y].
  left; easy. 1,2 : right;easy. case (Names_dec x y)=> Hxy.
  left. by rewrite Hxy.
  right; move=> Hnxy; by inversion Hnxy.
Qed.
