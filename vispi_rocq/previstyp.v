Require Import ssreflect. 
Require Import ZArith Psatz. 
Require Export Names visfun stacks picalc.

Inductive preVisTyp : Visfun -> stack -> proc -> Prop :=
| previstyp_nil : forall (B : Visfun), dom_eq (dom B) empty -> preVisTyp B stack_nil proc_nil
| previstyp_O_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (B2 : Visfun) (s1 s2 : stack) (sO : stackO) (P : proc),
    (s s1) = In (stack_in p sO) -> (s s2) = Out sO -> dom_eq (dom B2) (add_dom empty cur) ->
    let Vb := add_V (B2 cur) (srv b) in
    let B1 := add_vis (names (cnt p)) Vb (add_vis (names (srv b)) Vb vis0) in
    (B2 cur (srv a)) -> preVisTyp B1 s1 P
    -> preVisTyp B2 s2 (proc_out_ser a n b p P)
| previstyp_O_CON : forall (p : Ncon) (n : Nfo) (b : Nser) (B2 : Visfun) (P : proc), dom_eq (dom B2) (add_dom empty cur) ->
    let Vb := add_V (B2 cur) (srv b) in
    let B1 := add_vis (names (cnt p)) Vb vis0 in
    (B2 cur (cnt p)) -> preVisTyp B1 stack_nil P
    -> preVisTyp B2 (stack1 p) (proc_out_con p n b P)
| previstyp_O_LOC : forall (h: Nref) (n : Nfo) (B : Visfun), dom_eq (dom B) empty -> preVisTyp B stack_nil (proc_out_loc h n)
| previstyp_I_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (B2 : Visfun) (P : proc),
    dom_eq (dom B2) (add_dom empty (names (srv a))) ->
    let Vbp := add_V (add_V (B2 (names (srv a))) (srv b)) (cnt p) in
    let B1 := add_vis cur Vbp vis0 in
    preVisTyp B1 (stack1 p) P
    -> preVisTyp B2 stack_nil (proc_in_ser a n b p P)
| previstyp_I_CON : forall (p q : Ncon) (Hpq : p<> q) (n : Nfo) (b : Nser) (B2 : Visfun) (P : proc),
    dom_eq (dom B2) (add_dom empty (names (cnt p))) ->
    let Vb := add_V (B2 (names (cnt p))) (srv b) in
    let B1 := add_vis cur Vb vis0 in
    preVisTyp B1 (stack1 q) P
    -> preVisTyp B2 (stack2 p q Hpq) (proc_in_con p n b P)
| previstyp_I_LOC : forall (h : Nref) (n : Nfo) (B : Visfun) (s : stack) (P : proc),
    dom_eq (dom B) (add_dom empty cur) ->
    preVisTyp B s P -> preVisTyp B s (proc_in_loc h n P)
| previstyp_RES_SER : forall (a : Nser) (B1 B2 : Visfun) (s : stack) (P : proc),
    Visfun_eq B2 (trans_minus_vis (srv a) B1) ->
    preVisTyp B1 s P -> preVisTyp B2 s (proc_nu_ser a P)
| previstyp_RES_CON1 : forall (p : Ncon) (B1 B2 : Visfun) (s1 : stack) (s2 : stack) (P : proc),
    Visfun_eq B2 (trans_minus_vis (cnt p) B1) ->
    insert_stack_p p s1 s2 -> preVisTyp B1 s1 P -> preVisTyp B2 s2 (proc_nu_con p P)
| previstyp_RES_CON2 : forall (p : Ncon) (B1 B2 : Visfun) (s : stack) (P : proc),
    Visfun_eq B2 (trans_minus_vis (cnt p) B1) ->
    not_in_stack p s -> preVisTyp B1 s P -> preVisTyp B2 s (proc_nu_con p P)
| previstyp_RES_LOC : forall (h : Nref) (B : Visfun) (s : stack) (P : proc),
    preVisTyp B s P -> preVisTyp B s (proc_nu_loc h P)
| previstyp_PAR : forall (B1 B2 B3 : Visfun) (s s1 s2 : stack) (P1 P2 : proc),
    dom_disj (dom B1) (dom B2) -> Visfun_eq B3 (union_vis B1 B2) -> inter s1 s2 s ->
    preVisTyp B1 s1 P1 -> preVisTyp B2 s2 P2 -> preVisTyp B3 s (proc_par P1 P2)
| previstyp_ITE : forall (x n : Nfo) (B : Visfun) (s : stack) (P1 P2 : proc),
    preVisTyp B s P1 -> preVisTyp B s P2 -> preVisTyp B s (proc_ite x n P1 P2)
| previstyp_FwC : forall (p q : Ncon) (Hpq : p<> q) (B : Visfun),
    dom_eq (dom B) (add_dom empty (names (cnt p))) ->
    let s := stack2 p q Hpq in
    (B (names (cnt p)) (cnt q)) -> preVisTyp B s (proc_fwd_con p q)
| previstyp_FwF : forall (a b : Nser) (B : Visfun) ,
    dom_eq (dom B) (add_dom empty (names (srv a))) ->
    (B (names (srv a)) (srv b)) -> preVisTyp B stack_nil (proc_fwd_ser a b).

Definition is_fresh_nser (a : Nser) (B : Visfun) (s : stack) (P : proc) : Prop:=
  ~(dom B (names (srv a))) /\ (forall (x : Names_cur), ~(B x (srv a))) /\ notin_proc_ser a P.

Lemma exists_fresh_nser : forall (B : Visfun) (s : stack) (P : proc), exists (a : Nser), is_fresh_nser a B s P.
Proof.
  move=> B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_ser P) as [nP HP]. exists (give_nser (max nP (max ndomB ncodomB))).
  split.
  - have Hk : max nP (max ndomB ncodomB) >= ndomB by lia. apply (proj1 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB ncodomB) >= ncodomB by lia. intro x. apply (proj1 (HcodomB _ Hk x)).
    + have Hk : max nP (max ndomB ncodomB) >= nP by lia. apply (HP _ Hk).
Qed.

Definition is_fresh_ncon (p : Ncon) (B : Visfun) (s : stack) (P : proc) : Prop:=
  ~(dom B (names (cnt p))) /\ (forall (x : Names_cur), ~(B x (cnt p))) /\ notin_proc_con p P /\ not_in_stack p s.

Lemma exists_fresh_ncon : forall (B : Visfun) (s : stack) (P : proc), exists (p : Ncon), is_fresh_ncon p B s P.
Proof.
  intros B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_con P) as [nP HP]. destruct (stack_fin s) as [ns Hs]. exists (give_ncon (max nP (max ndomB (max ncodomB ns)))).
  split.
  - have Hk : max nP (max ndomB (max ncodomB ns)) >= ndomB by lia. apply (proj2 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB (max ncodomB ns)) >= ncodomB by lia. intro x. apply (proj2 (HcodomB _ Hk x)).
    + split.
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= nP by lia. apply (HP _ Hk).
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= ns by lia. apply (Hs _ Hk).
Qed.

Lemma dom_toplevel : forall (B : Visfun) (s : stack) (P : proc), preVisTyp B s P -> (forall (x : Names), dom B (names x) <-> in_toplevel_proc x P).
Proof.
  move=> B s P Htyp. induction Htyp.
  - move=> x. split=> [Hd | Htop]. by destruct (proj1 (H _) Hd). easy.
  - move=> x. split=> [Hd | Htop]. by destruct (proj1 (H1 _) Hd). easy.
  - move=> x. split=> [Hd | Htop]. by destruct (proj1 (H _) Hd). easy.
  - move=> x. split=> [Hd | Htop]. by destruct (proj1 (H _) Hd). easy.
  - move=> x. split=> [Hd | Htop]. destruct (proj1 (H _) Hd). easy. inversion H0. by apply in_ser_toplevel.
    inversion Htop. apply H. right. by rewrite H2.
  - move=> x. split=> [Hd | Htop]. destruct (proj1 (H _) Hd). easy. inversion H0. by apply in_con_toplevel.
    inversion Htop. apply H. right. by rewrite H2.
  - move=> x. split=> [Hd | Htop]. by destruct (proj1 (H _) Hd). easy.
  - move=> x. split=> [Hd | Htop]. destruct (proj1 ((proj1 H) _) Hd). apply nu_ser_toplevel. intro. by rewrite H2 in H1.
    by apply IHHtyp.
    inversion Htop. apply (proj1 H). split. by apply IHHtyp. intro. inversion H5. by rewrite H7 in H3.
  - move=> x; split=> [Hd | Htop];[ destruct (proj1 (proj1 H _) Hd);  apply nu_con_toplevel|]. intro. by rewrite H3 in H2.
    by apply IHHtyp.
    inversion Htop; apply (proj1 H); split;[ by apply IHHtyp|]. intro. inversion H6. by rewrite H8 in H4.
  - move=> x. split=> [Hd | Htop]. destruct (proj1 (proj1 H _) Hd). apply nu_con_toplevel. intro. by rewrite H3 in H2.
    by apply IHHtyp.
    inversion Htop. apply (proj1 H). split. by apply IHHtyp. intro. inversion H6. by rewrite H8 in H4.
  - move=> x; split=> [Hd | Htop] ;[ apply nu_loc_toplevel | inversion Htop]. all : by apply IHHtyp.
  - move=> x. split=> [Hd | Htop].
    destruct (proj1 (proj1 H0 _) Hd). apply par_toplevel1. by apply IHHtyp1. apply par_toplevel2. by apply IHHtyp2.
    inversion Htop. apply (proj1 H0). left. by apply IHHtyp1. apply (proj1 H0). right. by apply IHHtyp2.
  - move=> y. split=> [Hd | Htop].
    apply ite_toplevel. by apply IHHtyp1. by apply IHHtyp2.
    inversion Htop. by apply IHHtyp1.
  - move=> x. split=> [Hd | Htop]. apply fwd_con_toplevel. destruct (proj1 (H _) Hd). easy. by inversion H1.
    inversion Htop. apply H. right. by rewrite H3.
  - move=> x. split=> [Hd | Htop]. apply fwd_ser_toplevel. destruct (proj1 (H _) Hd). easy. by inversion H1.
    inversion Htop. apply H. right. by rewrite H3.
Qed.

Lemma previstyp_Visfun_eq : forall (B1 B2 : Visfun) (s : stack) (P : proc), preVisTyp B1 s P -> Visfun_eq B1 B2 -> preVisTyp B2 s P.
Proof.
  move=> B1 B2 s P Htyp Heq. move: B2 Heq. induction Htyp.
  - move=> B2 Heq. apply previstyp_nil. apply (dom_eq_trans _ (dom B)); try easy. apply dom_eq_sym. apply (proj1 Heq).
  - move=> B0 Heq. apply (previstyp_O_SER _ _ _ _ _ s1 _ sO); try easy.
    apply (dom_eq_trans _ (dom B2)); try easy. apply dom_eq_sym. by apply Heq.
    by apply Heq. apply IHHtyp. apply add_equiv_vis. apply add_equiv_V. apply Heq.
    apply add_equiv_vis. apply add_equiv_V. apply Heq. apply Visfun_eq_refl.
  - move=> B0 Heq. apply previstyp_O_CON. apply (dom_eq_trans _ (dom B2)); try easy. apply dom_eq_sym. apply Heq.
    by apply Heq. apply IHHtyp.
    apply add_equiv_vis. apply add_equiv_V. apply Heq. apply Visfun_eq_refl.
  - move=> B2 Heq. apply previstyp_O_LOC. apply (dom_eq_trans _ (dom B)); try easy. apply dom_eq_sym. apply Heq.
  - move=> B0 Heq. apply previstyp_I_SER. apply (dom_eq_trans _ (dom B2)); try easy. apply dom_eq_sym. apply Heq.
    apply IHHtyp. apply add_equiv_vis. apply add_equiv_V. apply add_equiv_V. apply Heq.
    apply Visfun_eq_refl.
  - move=> B0 Heq. apply previstyp_I_CON. apply (dom_eq_trans _ (dom B2)); try easy. apply dom_eq_sym. apply Heq.
    apply IHHtyp. apply add_equiv_vis. apply add_equiv_V. apply Heq. apply Visfun_eq_refl.
  - move=> B2 Heq. apply previstyp_I_LOC. apply (dom_eq_trans _ (dom B)); try easy. apply dom_eq_sym. apply Heq.
    by apply IHHtyp.
  - move=> B0 Heq. apply (previstyp_RES_SER _ B1). apply (Visfun_eq_trans _ B2); try assumption. by apply Visfun_eq_sym. assumption.
  - move=> B0 Heq. apply (previstyp_RES_CON1 _ B1 _ s1 s2). apply (Visfun_eq_trans _ B2); try assumption. by apply Visfun_eq_sym. all: assumption.
  - move=> B0 Heq. apply (previstyp_RES_CON2 _ B1). apply (Visfun_eq_trans _ B2); try assumption. by apply Visfun_eq_sym. all: assumption.
  - move=> B2 Heq. apply previstyp_RES_LOC. by apply IHHtyp.
  - move=> B0 Heq. apply (previstyp_PAR B1 B2 _ _ s1 s2); try assumption. apply (Visfun_eq_trans _ B3).
    by apply Visfun_eq_sym. assumption.
  - move=> B2 Heq. apply previstyp_ITE. by apply IHHtyp1. by apply IHHtyp2.
  - move=> B2 Heq. apply previstyp_FwC. apply (dom_eq_trans _ (dom B)); try assumption. apply dom_eq_sym. apply Heq. by apply Heq.
  - move=> B2 Heq. apply previstyp_FwF. apply (dom_eq_trans _ (dom B)); try assumption. apply dom_eq_sym. apply Heq. by apply Heq.
Qed.

Lemma preWeakening_aux : forall (n: nat) (P : proc), (size P) <= n -> forall (B1 B2 : Visfun) (s : stack), preVisTyp B1 s P -> Visfun_weak B1 B2 -> preVisTyp B2 s P.
Proof.
  induction n=> P HnP B1 B2 s Htyp Hweak.
  - inversion HnP. destruct P; try easy. inversion Htyp. apply previstyp_nil. apply (dom_eq_trans _ (dom B1)); try assumption. apply dom_eq_sym. apply Hweak.
  - inversion HnP. 2: by apply (IHn _ H0 B1). (*inversion Htyp.*) destruct P; try easy; inversion H0. 
    + inversion Htyp.
      apply (previstyp_O_SER _ _ _ _ _ s1 s sO); try assumption. apply (dom_eq_trans _ (dom B1)); try assumption.
      apply dom_eq_sym. apply Hweak.
      by apply Hweak. have HsP0 : size P0 <= n. rewrite H5. by lia.
      rewrite (eq_sym H5). apply (IHn _ HsP0 B3). by rewrite H5. apply weak_add. apply weak_add. apply Visfun_weak_refl.
      apply add_incl_V. rewrite H6. apply Hweak. apply add_incl_V. rewrite H6. apply Hweak.
    + inversion Htyp.
      apply previstyp_O_CON. apply (dom_eq_trans _ (dom B1)); try assumption.  apply dom_eq_sym. apply Hweak.
      by apply Hweak.
      rewrite (eq_sym H7) in H1. have HsP0 : size P0 <= n by lia.
      rewrite (eq_sym H7). apply (IHn _ HsP0 B3); try assumption. by rewrite H7.
      apply weak_add. apply Visfun_weak_refl.
      rewrite (eq_sym H3). apply add_incl_V. rewrite H4. apply Hweak.
    + inversion Htyp. apply previstyp_O_LOC. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak. assumption.
    + inversion Htyp. apply previstyp_I_SER. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak. assumption.
      have HsP : size P <=n by lia.
      apply (IHn _ HsP B3); try assumption. apply weak_add. apply Visfun_weak_refl.
      rewrite (eq_sym H6). rewrite (eq_sym H7). apply add_incl_V. apply add_incl_V. rewrite H3. rewrite H. apply Hweak.
    + inversion Htyp.
      apply previstyp_I_CON. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak. assumption.
      have HsP : size P <=n by lia.
      apply (IHn _ HsP B3); try assumption. apply weak_add. apply Visfun_weak_refl.
      rewrite (eq_sym H). rewrite (eq_sym H6). apply add_incl_V. rewrite H3. apply Hweak.
    + inversion Htyp.
      apply previstyp_I_LOC. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak. assumption.
      have HsP : size P <=n by lia. 
      apply (IHn _ HsP B1). assumption. assumption.
    + destruct (exists_fresh_nser B2 s (proc_nu_ser n0 P)) as [a Ha].
      have Hn : (notfree_ser n0 (proc_nu_ser n0 P)). apply ser_notfree_nu_ser. by left.
      destruct (exists_alpha_convert_proc_ser n0 a (proc_nu_ser n0 P)) as [Q HQ].
      have HPQ : (proc_nu_ser n0 P) = Q. apply (eq_proc_ser _ _ n0 a);try assumption. by destruct Ha as [_ [_ H_]].
      rewrite HPQ in Htyp. inversion HQ;[|easy].
      rewrite (eq_sym H5) in Htyp. inversion Htyp. destruct (Visfun_weak_trans_minus B0 B2 (srv a)) as [B4 [HB2 HB4]]. split; by destruct Ha as [Ha1 [Ha2 _]]. apply (Visfun_weak_eq B1 _ B2). assumption.
      apply Visfun_eq_refl. assumption.
      rewrite HPQ. rewrite (eq_sym H5). apply (previstyp_RES_SER _ B4 B2). assumption.
      rewrite HPQ in H0. rewrite (eq_sym H5) in H0. inversion H0. have HsQ : size Q0 <= n by lia.
      by apply (IHn _ HsQ B0).
    + destruct (exists_fresh_ncon B2 s (proc_nu_con n0 P)) as [p Hp].
      have Hn : (notfree_con n0 (proc_nu_con n0 P)). apply con_notfree_nu_con. by left.
      destruct (exists_alpha_convert_proc_con n0 p (proc_nu_con n0 P)) as [Q HQ].
      have HPQ : (proc_nu_con n0 P) = Q. apply (eq_proc_con _ _ n0 p);try assumption. by destruct Hp as [_ [_ [H_ _]]].
      rewrite HPQ in Htyp. inversion HQ;[|easy].
      rewrite (eq_sym H5) in Htyp. inversion Htyp.
      * destruct (Visfun_weak_trans_minus B0 B2 (cnt p)) as [B4 [HB2 HB4]]. split; by destruct Hp as [Hp1 [Hp2 _]]. apply (Visfun_weak_eq B1 _ B2). assumption.
        apply Visfun_eq_refl. assumption.
        rewrite HPQ. rewrite (eq_sym H5). apply (previstyp_RES_CON1 _ B4 B2 s1). assumption.
        assumption. rewrite HPQ in H0. rewrite (eq_sym H5) in H0. inversion H0.   have HsQ : size Q0 <= n by lia.
        by apply (IHn _ HsQ B0).
      * destruct (Visfun_weak_trans_minus B0 B2 (cnt p)) as [B4 [HB2 HB4]]. split; by destruct Hp as [Hp1 [Hp2 _]]. apply (Visfun_weak_eq B1 _ B2). assumption.
        apply Visfun_eq_refl. assumption.
        rewrite HPQ. rewrite (eq_sym H5). apply (previstyp_RES_CON2 _ B4 B2). assumption. assumption.
        rewrite HPQ in H0. rewrite (eq_sym H5) in H0. inversion H0.   have HsQ : size Q0 <= n by lia.
        by apply (IHn _ HsQ B0).
    + inversion Htyp. apply previstyp_RES_LOC. have HsP : size P <= n by lia.
      by apply (IHn P HsP B1).
    + inversion Htyp. destruct (Visfun_weak_union B0 B3 B2) as [B5 [B6 [HB2 [HB5 HB6]]]]. apply (Visfun_weak_eq B1 _ B2).
      assumption. apply Visfun_eq_refl. assumption.
      apply (previstyp_PAR B5 B6 _ _ s1 s2). apply (dom_disj_equiv (dom B0) (dom B3)); try assumption.
      apply HB5. apply HB6. 1,2: assumption.
      have HsP1 : size P1 <= n by lia. by apply (IHn P1 HsP1 B0).
      have HsP2 : size P2 <= n by lia. by apply (IHn P2 HsP2 B3).
    + inversion Htyp. apply previstyp_ITE. 
      have HsP1 : size P1 <= n by lia. by apply (IHn P1 HsP1 B1).
      have HsP2 : size P2 <= n by lia. by apply (IHn P2 HsP2 B1).
    + inversion Htyp. apply previstyp_FwF. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak.
      assumption. by apply Hweak.
    + inversion Htyp. rewrite (eq_sym H2). apply previstyp_FwC. apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Hweak.
      assumption. rewrite H2. by apply Hweak.
Qed.

Lemma preWeakening : forall (P : proc) (B1 B2 : Visfun) (s : stack), preVisTyp B1 s P -> Visfun_weak B1 B2 -> preVisTyp B2 s P.
Proof.
  move=> P. apply (preWeakening_aux (size P) P). lia.
Qed.
