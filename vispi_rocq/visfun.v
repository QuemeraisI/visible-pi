Require Import ssreflect.
Require Import ZArith Psatz.
Require Export Names.          

Section domains.
          
  Definition is_dom_fin (dom : Names_cur -> Prop) : Prop :=  exists (n : nat), forall (k :nat), k >= n -> ~(dom (names (srv (give_nser k)))) /\ ~(dom (names (cnt (give_ncon k)))).

  (* Relations and operations on domains *)
Definition empty : Names_cur -> Prop := fun (x : Names_cur) => False.
Lemma empty_fin : is_dom_fin empty.
Proof.
  exists 0. move=> k Hk. by split. 
Qed.

Definition dom_eq (dom1 dom2 : Names_cur -> Prop) : Prop := forall (x : Names_cur), (dom1 x) <-> (dom2 x).

Lemma dom_eq_sym : forall (dom1 dom2 : Names_cur -> Prop), (dom_eq dom1 dom2) -> (dom_eq dom2 dom1).
Proof.
  move=> dom1 dom2 Hd. by split; apply (Hd x). 
Qed.

Lemma dom_eq_refl : forall (dom1 : Names_cur -> Prop), (dom_eq dom1 dom1).
Proof.
  easy. 
Qed.

Lemma dom_eq_trans : forall (dom1 dom2 dom3 : Names_cur -> Prop), (dom_eq dom1 dom2) -> (dom_eq dom2 dom3) -> (dom_eq dom1 dom3).
Proof.
  move=> dom1 dom2 dom3 Hd12 Hd23. split=> [Hd | Hd].
  - apply Hd23. by apply Hd12. 
  - apply Hd12. by apply Hd23. 
Qed.

Definition dom_incl (dom1 dom2 : Names_cur -> Prop) : Prop := forall (x : Names_cur), (dom1 x) -> (dom2 x).

Lemma dom_incl_refl : forall (dom : Names_cur -> Prop), dom_incl dom dom.
Proof.
  easy.
Qed.

Lemma dom_incl_trans : forall (dom1 dom2 dom3 : Names_cur -> Prop), dom_incl dom1 dom2 -> dom_incl dom2 dom3 -> dom_incl dom1 dom3.
Proof.
  move=> dom1 dom2 dom3 H1 H2 x Hdom1. by apply H2; apply H1. 
Qed.

Definition dom_disj (dom1 dom2 : Names_cur -> Prop) : Prop := forall (x : Names_cur), ((dom1 x) -> ~(dom2 x)).

Lemma dom_disj_comm (dom1 dom2 : Names_cur -> Prop) : dom_disj dom1 dom2 -> dom_disj dom2 dom1.
Proof.
  move=> H x Hx1 Hx2. by apply (H x Hx2 Hx1).
Qed.

Lemma dom_disj_equiv (dom1 dom2 dom3 dom4 : Names_cur -> Prop) : dom_disj dom1 dom2 -> dom_eq dom1 dom3 -> dom_eq dom2 dom4 -> dom_disj dom3 dom4.
Proof.
  move=> Hdisj Hdeq1 Hdeq2 x Hd3 Hd4.
  have Hd1 : dom1 x by apply (proj2 (Hdeq1 x) Hd3). have Hd2 : dom2 x by apply (proj2 (Hdeq2 x) Hd4).
  by apply (Hdisj x Hd1 Hd2).
Qed.  

Definition dom_union (dom1 dom2 : Names_cur -> Prop) : Names_cur -> Prop := fun (x : Names_cur) => (dom1 x \/ dom2 x).

Lemma dom_union_comm : forall (dom1 dom2 : Names_cur -> Prop), dom_eq (dom_union dom1 dom2) (dom_union dom2 dom1).
Proof.
  move=> dom1 dom2 x. split=> Hd; destruct Hd; try by right. all : by left.
Qed.

Lemma dom_union_fin : forall (dom1 dom2 : Names_cur -> Prop), is_dom_fin dom1 -> is_dom_fin dom2 -> is_dom_fin (dom_union dom1 dom2).
Proof.
  move=> dom1 dom2 Hd1 Hd2. destruct Hd1 as [n1 Hn1]. destruct Hd2 as [n2 Hn2]. exists (max n1 n2). move=> k Hk.
  split=> Hd; destruct Hd as [Hd | Hd].
  all : have Hk1 : k >= n1 by lia. all : have Hk2 : k >= n2 by lia.
  apply ((proj1 (Hn1 k Hk1)) Hd).
  apply ((proj1 (Hn2 k Hk2)) Hd).
  apply ((proj2 (Hn1 k Hk1)) Hd).
  apply ((proj2 (Hn2 k Hk2)) Hd).
Qed.

Definition minus_dom (dom : Names_cur -> Prop) (x : Names_cur) : Names_cur -> Prop :=
  fun (y: Names_cur) => (dom y) /\ (x <> y).

Lemma minus_dom_fin : forall (dom :Names_cur -> Prop) (x : Names_cur), is_dom_fin dom -> is_dom_fin (minus_dom dom x).
Proof.
  move=> dom x Hd; destruct Hd as [n Hn]. exists n. move=> k Hk. split=> Hd; destruct Hd as [Hd _].
  apply ((proj1 (Hn k Hk)) Hd). apply ((proj2 (Hn k Hk)) Hd).
Qed.

Definition add_dom (dom : Names_cur -> Prop) (x : Names_cur) : Names_cur -> Prop :=
  fun (y : Names_cur) => (dom y) \/ (x = y).

Lemma add_dom_fin : forall (dom : Names_cur -> Prop) (x: Names_cur), is_dom_fin dom -> is_dom_fin (add_dom dom x).
Proof.
  move=> dom x Hd. destruct Hd as [n Hn].
  destruct x as[| x]. exists n. move=> k Hk. split=> Hd; destruct Hd as [Hd | Hd]; try easy.
  - by apply ((proj1 (Hn k Hk)) Hd).
  - by apply ((proj2 (Hn k Hk)) Hd). 
  - destruct x as [a | p].
    1 : destruct (given_nser a). 3 : destruct (given_ncon p).
    1, 3 : exists n; move=> k Hk; split=> [Hd | Hd]; destruct Hd as [Hd | Hd]. 
             1,5 : apply (proj1 (Hn k Hk) Hd).
             1,6 : inversion Hd as [Hx]; apply (H k (eq_sym Hx)).
             1,4 : apply (proj2 (Hn k Hk) Hd).
             1,2 : by inversion Hd.
             1,2 : destruct H as [m Hx]; exists (max (m+1) n); move=> k Hk_; split=> [Hd | Hd].
             all : destruct Hd as [Hd | Hd].
             all : have Hk : (k >= n) by lia.
             1,5 : apply (proj1 (Hn k Hk) Hd).
             1,3,4,6 : inversion Hd as [Hx2]; rewrite Hx2 in Hx.
             rewrite (infinite_nser _ _ Hx) in Hk_; by lia.
             rewrite (infinite_ncon _ _ Hx) in Hk_; by lia.
             all : by apply (proj2 (Hn k Hk) Hd).
Qed.

Definition alpha_convert_dom (x y : Names) (dom : Names_cur -> Prop) := fun (z : Names_cur) => ((z = (names y)) /\ dom (names x)) \/ ((z <> (names x)) /\ dom z).

Lemma alpha_convert_dom_fin : forall (x y : Names) (dom : Names_cur -> Prop), is_dom_fin dom -> is_dom_fin (alpha_convert_dom x y dom).
Proof.
  move=> x y dom Hdom. destruct Hdom as [n Hdom]. destruct y as [a | p].
  destruct (given_nser a ). 3: destruct (given_ncon p).
  1,3 : exists n; move=> k Hk; split=> [Hd | Hd]; destruct Hd as [Hd | Hd]; destruct Hd as [Hkx Hx].
          1,5,7 : inversion Hkx as [Hx2]; by apply (H k Hx2).
          1,4 : by apply (proj1 (Hdom k Hk) Hx).
          by inversion Hkx.
          1,2 : by apply (proj2 (Hdom k Hk) Hx).
          all : destruct H as [n1 Hn1]; exists (max (n1+1) n); move=> k Hk; split=> [Hd | Hd].
          all : destruct Hd as [[Hkx Hx] | [Hkx Hx]].
          1,3,5,7 : inversion Hkx as [Hx2]; rewrite (eq_sym Hx2) in Hn1.
          have Hkn1 : k = n1 by apply infinite_nser. 2 : have Hkn1 : k = n1 by apply infinite_ncon.
          1, 2 : lia.
          1,2,3,4 : have Hkn : k >= n by lia.
          1,3 : apply (proj1 (Hdom k Hkn) Hx).
          1,2 : apply (proj2 (Hdom k Hkn) Hx).
Qed.

Lemma alpha_add_dom_replace : forall (x y : Names) (dom : Names_cur -> Prop), ~(dom (names x)) -> dom_eq (alpha_convert_dom x y (add_dom dom (names x))) (add_dom dom (names y)).
Proof.
  move=> x y dom Hx. split=> Hd; destruct Hd as [Hd | Hd].
  - destruct Hd as [Hy Hd]. rewrite Hy. by right.
  - destruct Hd as [Hx0 Hd]. destruct Hd. by left. exfalso. by apply (Hx0 (eq_sym H)).
  - right. split. move=> Hx0. by rewrite Hx0 in Hd. by left.
  - left. split. easy. by right.
Qed.

Lemma alpha_add_dom_leave : forall (x y z : Names) (dom : Names_cur -> Prop), (z <> x) -> dom_eq (alpha_convert_dom x y (add_dom dom (names z))) (add_dom (alpha_convert_dom x y dom) (names z)).
Proof.
  move=> x y z dom Hz. split=> Hd.
  - destruct Hd as [[Hx0 Hd] | [Hx0 Hd]]. destruct Hd. left. by left. by inversion H.
    destruct Hd. left. right. easy.  by right.
  - destruct Hd as [Hd | Hd]. destruct Hd as [[Hx0 Hd] | [Hx0 Hd]]. left. split. easy. by left.
    right. split. easy. by left.
    right. split. intro. rewrite H in Hd. by inversion Hd. by right.
Qed.

Lemma alpha_union_dom : forall (x y : Names) (dom1 dom2 : Names_cur -> Prop), dom_eq (alpha_convert_dom x y (dom_union dom1 dom2)) (dom_union (alpha_convert_dom x y dom1) (alpha_convert_dom x y dom2)).
Proof.
  move=> x y dom1 dom2. split=> Hd.
  - destruct Hd as [[Hx Hd] | [Hx Hd]]. destruct Hd. left. by left. right. by left.
    destruct Hd. left. by right. right. by right.
  -  destruct Hd as [Hd | Hd]; destruct Hd as [[Hx Hd] | [Hx Hd]].
     1,3 : left; split. easy. by left. easy. by right.
     all : right; split. easy. by left. easy. by right.
Qed.

End domains.

Section view.
  
Definition is_V_fin (V : Names -> Prop) : Prop := exists (n: nat), forall (k:nat), k>= n -> ~(V (srv (give_nser k))) /\ ~(V (cnt (give_ncon k))).

Structure Vis := {
    V :> Names -> Prop;
    V_fin : is_V_fin V;
  }.

Definition incl_V (V1 V2 : Vis) := forall (x : Names), V1 x -> V2 x.

Definition empty_V_ := fun (x : Names) => False.
Lemma empty_V_fin : is_V_fin (empty_V_).
Proof.
  exists 0. easy.
Qed.

Definition empty_V := {| V := empty_V_; V_fin := empty_V_fin;|}.
Definition add_V_ (V : Names -> Prop) (y : Names) := fun (x : Names) => ( V x \/ x = y).
Lemma add_V_fin : forall (V : Names -> Prop) (y : Names), is_V_fin V -> is_V_fin (add_V_ V y).
Proof.
  move=> V y HV. destruct HV as [n HV]. destruct y as [a | p].
  2 : destruct (given_ncon p). destruct (given_nser a).
  1,3 : exists n; move=> k Hk. 1,2 : split=> HaddV; destruct HaddV as [HaddV | HaddV].
          1,5 : apply (proj1 (HV k Hk) HaddV).
          1,3,4,6: inversion HaddV as [Hx]; apply (H k Hx).
          1,2 : apply (proj2 (HV k Hk) HaddV).
          all : destruct H as [n1 Hx]; exists (max n (n1 +1)); move=> k Hk.
          all : split=> HaddV; destruct HaddV as [HaddV | HaddV].
          1, 3,5,7 : have Hkn : k >= n by lia.
          1,3 : apply (proj1 (HV k Hkn) HaddV).
          1,2 : apply (proj2 (HV k Hkn) HaddV).
          all : inversion HaddV as [Hx2]; rewrite (eq_sym Hx2) in Hx.
          have Hkn1 : n1 = k by apply (infinite_nser _ _ Hx). lia.
          have Hkn1 : n1 = k by apply (infinite_ncon _ _ Hx). lia. 
Qed.

Definition add_V (V : Vis) (y : Names) := {| V := add_V_ V y; V_fin := add_V_fin V y (V_fin V); |}.

Definition minus_V_ (V : Names -> Prop) (y : Names) := fun (x : Names) => (V x /\ x <> y).

Lemma minus_V_fin : forall (V : Names -> Prop) (y : Names), is_V_fin V -> is_V_fin (minus_V_ V y).
Proof.
  move=> V y HV. destruct HV as [n Hn]. destruct y as [a | p]; exists n; move=> k Hk.
  all : split=> HmV; destruct HmV as [HV _].
  1,3 : apply ((proj1 (Hn k Hk)) HV).
  all : apply ((proj2 (Hn k Hk)) HV).
Qed.

Definition minus_V (V : Vis) (y : Names) := {| V := minus_V_ V y; V_fin := minus_V_fin V y (V_fin V); |}.

Definition union_V_ (V1 : Names -> Prop) (V2 : Names -> Prop) := fun (x : Names) => (V1 x \/ V2 x).

Lemma union_V_fin : forall (V1 V2 : Names -> Prop), is_V_fin V1 -> is_V_fin V2 -> is_V_fin (union_V_ V1 V2).
Proof.
  move=> V1 V2 HV1 HV2. destruct HV1 as [n1 Hn1]. destruct HV2 as [n2 Hn2].
  exists (max n1 n2). move=> k Hk. split=> HV; destruct HV as [HV | HV].
  1,3 : have Hkn : k >= n1 by lia. 3,4: have Hkn : k >= n2 by lia.
  apply ((proj1 (Hn1 k Hkn)) HV).
  apply ((proj2 (Hn1 k Hkn)) HV).
  apply ((proj1 (Hn2 k Hkn)) HV).
  apply ((proj2 (Hn2 k Hkn)) HV).
Qed.

Definition union_V (V1 V2 : Vis) := {| V:= union_V_ V1 V2; V_fin := union_V_fin V1 V2 (V_fin V1) (V_fin V2);|}.

Definition alpha_convert_V_ (x y : Names) (V : Vis) := fun (z : Names) => (z <> x /\ V z) \/ (z=y /\ V x).

Lemma alpha_convert_V_fin : forall (x y : Names) (V1 : Vis), is_V_fin (alpha_convert_V_ x y V1).
Proof.
  move=> x y V. destruct (V_fin V) as [n HV]. destruct y as [b | p].
  2 : destruct (given_ncon p). destruct (given_nser b).
  1,3 : exists n; move=> k Hk.  1,2 : split=> Halph; destruct Halph as [[Hkx HVx] | [Hkx HVx]].
          1,5 : apply (proj1 (HV k Hk) HVx).
          1,3,4,6: inversion Hkx as [Hkx2]; apply (H k Hkx2).
          1,2 : apply (proj2 (HV k Hk) HVx).
          all: destruct H as [n1 Hn1]; exists (max n (n1 +1)); move=> k Hk; split=> Halph; destruct Halph as [[Hxk HVk] | [Hxk HVk]].
          1,3,5,7 : have Hkn : k >= n by lia.
          1,3 : by apply (proj1 (HV k Hkn) HVk).
          1,2 : by apply (proj2 (HV k Hkn) HVk).
          all :inversion Hxk as [Hkx]; rewrite (eq_sym Hkx) in Hn1.
          have Hkn1 : n1 = k by apply (infinite_nser _ _ Hn1). lia.
          have Hkn1 : n1 = k by apply (infinite_ncon _ _ Hn1). lia.
Qed.

Definition alpha_convert_V (x y : Names) (V : Vis) := {| V := alpha_convert_V_ x y V; V_fin := alpha_convert_V_fin x y V; |}.

Lemma alpha_add_V_replace : forall (x y : Names) (V : Vis), forall ( z : Names), alpha_convert_V x y (add_V V x) z <-> add_V (alpha_convert_V x y V) y z.
Proof.
  move=> x y V z. split=> [HV | HV]; destruct HV.
  - left. left. destruct H. destruct H0. by split. easy.
  - destruct H. by right.
  - destruct H. left. split. easy. destruct H. by left.
    right. destruct H. split. easy. by right.
  - right. split. assumption. by right.
Qed.

Lemma alpha_add_V_leave : forall (x y z: Names) (V : Vis), z <> x -> forall (t : Names), alpha_convert_V x y (add_V V z) t <-> add_V (alpha_convert_V x y V) z t.
Proof.
  move=> x y z V Hz t. split=> Halphadd; destruct Halphadd.  
    - destruct H as [Ht HV]. destruct HV. left. by left. by right.
    - destruct H as [Ht HV]. destruct HV. rewrite Ht. left. by right.
      exfalso. by apply (Hz (eq_sym H)).
    - destruct H.  destruct H as [Ht HV].  left. split. easy. by left. right. split. easy. left. easy.
    - left. split. by rewrite H. by right.
Qed.

Lemma add_incl_V : forall (t : Names) (V1 V2 : Vis), incl_V V1 V2 -> incl_V (add_V V1 t) (add_V V2 t).
Proof.
  move=> t V1 V2 HV x HV1. destruct HV1; [left;  by apply HV | by right].
Qed.

Lemma union_incl_V : forall (V1 V2 V3 V4 : Vis), incl_V V1 V2 -> incl_V V3 V4 -> incl_V (union_V V1 V3) (union_V V2 V4).
Proof.
  move=> V1 V2 V3 V4 Hincl1 Hincl2 x Hun.
  destruct Hun; [left|right]. by apply Hincl1. by apply Hincl2.
Qed.

Lemma add_equiv_V : forall (t : Names) (V1 V2 : Vis), (forall (x : Names), V1 x <-> V2 x) -> (forall (x : Names), add_V V1 t x <-> add_V V2 t x).
Proof.
  move=> t V1 V2 HV x; split=> HV_. 2 : apply (add_incl_V _ V2). apply (add_incl_V _ V1).
  1,3 :  move=> y; by apply HV. all : assumption.
Qed.


Lemma alpha_minus_V_replace : forall (x y : Names) (V : Vis), ~(V y) -> forall (t : Names), alpha_convert_V x y (minus_V V x) t <-> (minus_V V x) t.
Proof.
  intros x y V Hy t. split=> HV; destruct HV.
  1,2 : destruct H as [Hx HV];  by destruct HV.
  left. split. easy. by split.
Qed.

Lemma alpha_minus_V_leave : forall (x y z : Names) (V : Vis), (z <> x) -> (z <> y) -> forall (t : Names), alpha_convert_V x y (minus_V V z) t <-> minus_V (alpha_convert_V x y V) z t.
Proof.
  move=> x y z V Hzx Hzy t. split=> HV; destruct HV.
  1,2 : destruct H as [Hx HV].
  split. left. split. easy. by destruct HV.  by destruct HV. rewrite Hx. split. destruct HV. by right. easy.
  destruct H. left. split. easy.  split. easy. easy. right. split. easy. split. easy. easy.
Qed.

Lemma alpha_union_V : forall (x y : Names) (V1 V2 : Vis), forall (t : Names), alpha_convert_V x y (union_V V1 V2) t <-> union_V (alpha_convert_V x y V1) (alpha_convert_V x y V2) t.
Proof.
  move=> x y V1 V2 t. split=> HV.
  - destruct HV as [[Hx HV] | [Hx HV]].
    all: destruct HV.
    1,3 : left. 3,4 : right.
    1,3 : by left. all : by right.
  - destruct HV; destruct H.
    1,3 : left. 3,4 : right.
    all : split; try easy.
    1,3 : left; easy. all : right; easy.
Qed.

Lemma add_minus_V : forall (x y : Names) (V : Vis), (x <> y) -> (forall (t : Names), add_V (minus_V V x) y t <-> minus_V (add_V V y) x t).
Proof.
  move=> x y V Hxy t. split=> [H |H ]; destruct H; destruct H.
  - split; [|assumption]. by left.
  - split. by right. intro. by rewrite H in Hxy.
  - left. by split.
  - by right.
Qed.

End view.

Section visibility_function_aux.

Definition is_codom_fin (B : Names_cur -> Vis) : Prop :=  exists (n : nat), forall (k : nat), k >= n -> forall (x : Names_cur), ~(B x (srv (give_nser k))) /\ ~(B x (cnt (give_ncon k))).

Lemma minus_vis_B_aux : forall (x : Names_cur) (B : Names_cur -> Vis) (y : Names_cur), is_V_fin (fun (z : Names) => (B y z /\ (x <> y))).
Proof.
  move=> x B y. destruct (V_fin (B y)) as [n HBy]. exists n. move=> k Hk. split=> HBxy.
  apply ((proj1 (HBy k Hk)) (proj1 HBxy)).
  apply ((proj2 (HBy k Hk)) (proj1 HBxy)).
Qed.

Definition minus_vis_B (x : Names_cur) (dom : Names_cur -> Prop) (B: Names_cur-> Vis) : Names_cur -> Vis :=
  fun (y : Names_cur) => {| V := (fun (z : Names) => (B y z /\ x <> y)); V_fin := minus_vis_B_aux x B y; |}.

Lemma minus_codom_fin : forall (x : Names_cur) (dom : Names_cur -> Prop) (B : Names_cur -> Vis), is_codom_fin B -> is_codom_fin (minus_vis_B x dom B).
Proof.
  move=> x dom B HB. destruct HB as [n HB]. exists n. move=> k Hk y. split=> HmB; destruct HmB as [HmB Hxy].
  - apply ((proj1 (HB k Hk y)) HmB).
  - apply ((proj2 (HB k Hk y)) HmB).
Qed.

Lemma add_vis_B_aux : forall (x : Names_cur) (V : Vis) (y : Names_cur), is_V_fin (fun (z: Names) => V z /\ (x=y)).
Proof.
  move=> x V y. destruct (V_fin V) as [n HV]. exists n. move=> k Hk. split=> HVxy; destruct HVxy.
  - apply ((proj1 (HV k Hk)) H).
  - apply ((proj2 (HV k Hk)) H).
Qed. 

Definition add_vis_B (x : Names_cur) (V: Vis) (dom : Names_cur -> Prop) (B: Names_cur -> Vis) : Names_cur -> Vis :=
  fun (y : Names_cur) => union_V (B y) {| V:= (fun (z: Names) => V z /\ (x=y)); V_fin := add_vis_B_aux x V y; |}.

Lemma add_codom_fin :forall (x : Names_cur) (V: Vis) (dom : Names_cur -> Prop) (B : Names_cur -> Vis),
    is_codom_fin B -> is_codom_fin (add_vis_B x V dom B).
Proof.
  move=> x V dom B HB. destruct HB as [n1 HB]. destruct (V_fin V) as [n2 HV].
  exists (max n1 n2). move=> k Hk y. split=> HaddB; destruct HaddB.
  1,3 :have Hkn1 : (k >= n1) by lia. 3,4 : have Hkn2 : (k >= n2) by lia.
  apply ((proj1 (HB k Hkn1 y)) H).
  apply ((proj2 (HB k Hkn1 y)) H).
  apply ((proj1 (HV k Hkn2)) (proj1 H)).  
  apply ((proj2 (HV k Hkn2)) (proj1 H)).
Qed.



Definition vis_union_B (B1 B2 : Names_cur -> Vis) : Names_cur -> Vis := fun (x : Names_cur) => union_V (B1 x) (B2 x).                                                                        
Lemma union_codom_fin : forall (B1 B2 : Names_cur -> Vis), is_codom_fin B1 -> is_codom_fin B2 -> is_codom_fin (vis_union_B B1 B2).
Proof.
  move=> B1 B2 HB1 HB2. destruct HB1 as [n1 HB1]. destruct HB2 as [n2 HB2].
  exists (max n1 n2). move=> k Hk x. split=> Hunion; destruct Hunion.
  1,3 : have Hkn :k >=n1 by lia. 3,4 : have Hkn : k >= n2 by lia.
  apply ((proj1 (HB1 k Hkn x)) H).
  apply ((proj2 (HB1 k Hkn x)) H).
  apply ((proj1 (HB2 k Hkn x)) H).
  apply ((proj2 (HB2 k Hkn x)) H). 
Qed.

Lemma vis_rest_B_aux : forall (B : Names_cur -> Vis) (dom : Names_cur -> Prop) (x : Names_cur), is_V_fin (fun (y : Names) => (B x y /\ dom x)).
Proof.
  move=> B dom x. destruct (V_fin (B x)) as [n HBx]. exists n. move=> k Hk. split=> HB.
  apply ((proj1 (HBx k Hk)) (proj1 HB)).
  apply ((proj2 (HBx k Hk)) (proj1 HB)).
Qed.

Definition vis_rest_B (B : Names_cur -> Vis) (dom : Names_cur -> Prop) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V:= fun (y : Names) => (B x y /\ dom x); V_fin := vis_rest_B_aux B dom x ; |}.

Lemma rest_codom_fin : forall (B : Names_cur -> Vis) (dom : Names_cur -> Prop), is_codom_fin B -> is_codom_fin (vis_rest_B B dom).
Proof.
  move=> B dom HB. destruct HB as [n HB]. exists n. move=> k Hk x. split=> Hrest; destruct Hrest.
  - apply ((proj1 (HB k Hk x)) H).
  - apply ((proj2 (HB k Hk x)) H).
Qed.

Definition alpha_convert_B_aux1 (a b : Names) (B : Names_cur -> Vis) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V := alpha_convert_V a b (B x); V_fin := alpha_convert_V_fin a b (B x); |}.

Lemma alpha_convert_B_fin_aux2 : forall (a b : Names) (B : Names_cur -> Vis) (x : Names_cur), is_V_fin (fun (y : Names) => (x = (names b) /\ B (names a) y) \/ (x <> (names a) /\ B x y)).
Proof.
  move=> a b B x. destruct (V_fin (B (names a))) as [na HBa]. destruct (V_fin (B x)) as [nx HBx].
  exists (max na nx). move=> k Hk. split=> H; destruct H.
  all : have Hka : k >= na by lia. all :  have Hkx : k >= nx by lia.
  apply (proj1 (HBa k Hka) (proj2 H)).
  apply (proj1 (HBx k Hkx) (proj2 H)).
  apply (proj2 (HBa k Hka) (proj2 H)).
  apply (proj2 (HBx k Hkx) (proj2 H)).
Qed.
        
Definition alpha_convert_B_aux2 (a b : Names) (B : Names_cur -> Vis) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V := fun (y : Names) => (x = (names b) /\ B (names a) y) \/ (x <> (names a) /\ B x y);
                        V_fin := alpha_convert_B_fin_aux2 a b B x; |}.

Definition alpha_convert_B (a b : Names) (B : Names_cur -> Vis) : Names_cur -> Vis :=
  alpha_convert_B_aux2 a b (alpha_convert_B_aux1 a b B).


End visibility_function_aux.


Section visibility_function.
Structure Visfun := {
    dom : Names_cur -> Prop;
    B :> Names_cur -> Vis;
    _dom : forall (x : Names_cur) (y : Names), (B x y) -> dom x;
    dom_fin : is_dom_fin dom; 
    codom_fin : is_codom_fin B;
  }.

Definition Visfun_eq (B1 : Visfun) (B2 : Visfun) : Prop := (dom_eq (dom B1) (dom B2)) /\ (forall (x : Names_cur), forall (y: Names), B1 x y <-> B2 x y).

Axiom eq_visfun : forall (B1 B2 : Visfun), Visfun_eq B1 B2 -> B1 = B2.


Lemma minus_vis_dom (x : Names_cur) (B: Visfun) :
  let dom1 := minus_dom (dom B) x in
  let B1 := minus_vis_B x (dom B) B in
  forall x y, (B1 x y) -> dom1 x.
Proof.
  intros. split.
  - apply (_dom B x0 y). apply (proj1 H).
  - apply (proj2 H).
Qed.

Definition minus_vis (x : Names_cur) (B : Visfun) : Visfun :=
  {|
    dom := minus_dom (dom B) x;
    B := (minus_vis_B x (dom B) B);
    _dom := (minus_vis_dom x B);
    dom_fin := minus_dom_fin (dom B) x (dom_fin B);
    codom_fin := minus_codom_fin x (dom B) B (codom_fin B);
  |}.

Lemma add_vis_dom (x : Names_cur) (V: Vis) (B: Visfun) :
  let dom1 := add_dom (dom B) x in
  let B1 := add_vis_B x V (dom B) B in
  forall x y, (B1 x y) -> (dom1 x).

Proof.
  intros. case H => Hd.
  - left. apply (_dom B x0 y). apply Hd.
  - right. apply (proj2 Hd).
Qed.

Definition add_vis (x : Names_cur) (V: Vis) (B : Visfun) : Visfun:=
  {|
    dom := (add_dom (dom B) x);
    B := (add_vis_B x V  (dom B) B);
    _dom := (add_vis_dom x V B);
    dom_fin := add_dom_fin (dom B) x (dom_fin B);
    codom_fin := add_codom_fin x V (dom B) B (codom_fin B);
  |}.

Definition add_rec_vis (x : Names) (B : Visfun) : Visfun :=
  add_vis (names x) (add_V empty_V x) B.

Definition loc_trans_V (x : Names) ( B : Visfun) (y : Names_cur) :=
  fun (z : Names) => B y z \/ ((B y x \/ y = (names x)) /\ (B (names x) z \/ (exists t, B (names x) t /\ B (names t) z))).

Lemma loc_trans_B_aux : forall (x : Names) (B : Visfun) (y : Names_cur), is_V_fin (loc_trans_V x B y).
Proof.
  move=> x B y. destruct (codom_fin B) as [n Hn]. exists n.
  move=> k Hk. split=> Hl.
  - destruct Hl. apply (proj1 (Hn k Hk y) H).  destruct H.
    destruct H0. apply (proj1 (Hn k Hk _) H0). destruct H0. destruct H0. apply (proj1 (Hn k Hk _) H1).
  - destruct Hl. apply (proj2 (Hn k Hk y) H).  destruct H.
    destruct H0. apply (proj2 (Hn k Hk _) H0). destruct H0. destruct H0. apply (proj2 (Hn k Hk _) H1).
Qed.

Definition loc_trans_B (x : Names) (B: Visfun) : Names_cur -> Vis :=
  fun (y : Names_cur) => {| V := loc_trans_V x B y; V_fin := loc_trans_B_aux x B y;|}.

Lemma loc_trans_codom_fin  : forall (x : Names) (B : Visfun), is_codom_fin (loc_trans_B x B).
Proof.
  move=> x B. destruct (codom_fin B) as [n Hn]. exists n. move=> k Hk y. split=> HtrB; destruct HtrB as [H | [_ H]].
  apply ((proj1 (Hn k Hk _)) H).
  destruct H. apply ((proj1 (Hn k Hk _)) H).
  destruct H. destruct H. apply ((proj1 (Hn k Hk _)) H0).
  apply ((proj2 (Hn k Hk _)) H).
  destruct H. apply ((proj2 (Hn k Hk _)) H).
  destruct H. destruct H. apply ((proj2 (Hn k Hk _)) H0).
Qed.

Lemma trans_minus_vis_B_aux : forall (x : Names) (B: Names_cur -> Vis) (y : Names_cur), is_V_fin (fun (z: Names) => (B y z) /\ (y <> (names x)) /\ (z <> x)).
Proof.
  move=> x B y. destruct (V_fin (B y)) as [n HBy]. exists n. move=> k Hk. split=> HBtm.
  apply ((proj1 (HBy k Hk)) (proj1 HBtm)).
  apply ((proj2 (HBy k Hk)) (proj1 HBtm)).
Qed.

Definition trans_minus_vis_B (x : Names) (B: Visfun) : Names_cur -> Vis :=
  let B1 := loc_trans_B x B in
  fun (y : Names_cur) => {| V:= fun (z : Names) => (B1 y z) /\ (y <> (names x)) /\ (z <> x); V_fin := trans_minus_vis_B_aux x B1 y; |}.

Lemma trans_minus_codom_fin : forall (x : Names) (B : Visfun), is_codom_fin (trans_minus_vis_B x B).
Proof.
  move=> x B. destruct (loc_trans_codom_fin x B) as [n HtrB].
  exists n. move=> k Hk y. split=> HtrmB; destruct HtrmB.
  - apply ((proj1 (HtrB k Hk y)) H).
  - apply ((proj2 (HtrB k Hk y)) H).
Qed.

Lemma trans_minus_vis_dom (x : Names) (B: Visfun) :
  let dom1 := minus_dom (dom B) (names x) in
  let B1 := trans_minus_vis_B x B in
  forall x y, (B1 x y) -> (dom1 x).
Proof.
  intros. destruct H. split.
  - destruct H.
    + apply (_dom B x0 y H).
    + destruct H. destruct H0. destruct H. apply (_dom B x0 x  H). easy.
  - intro. apply (proj1 H0 (eq_sym H1)).
Qed.

Definition trans_minus_vis (x : Names) (B : Visfun) : Visfun:=
  {|
    dom := minus_dom (dom B) (names x);
    B:= (trans_minus_vis_B x B);
    _dom := (trans_minus_vis_dom x B);
    dom_fin := minus_dom_fin (dom B) (names x) (dom_fin B);
    codom_fin := trans_minus_codom_fin x B ;
  |}.

Lemma Visfun_eq_refl : forall (B : Visfun), Visfun_eq B B.
Proof.
  intros. split. apply (dom_eq_refl (dom B0)).
  intros. by split. 
Qed.

Lemma Visfun_eq_sym : forall (B1 B2 : Visfun), Visfun_eq B1 B2 -> Visfun_eq B2 B1.
Proof.
  move=> B1 B2 Heq. split.
  apply (dom_eq_sym (dom B1) (dom B2) (proj1 Heq)).
  move=> x y. split=> HB. by apply (proj2 ((proj2 Heq) x y)). by apply (proj1 ((proj2 Heq) x y)).
Qed.

Lemma Visfun_eq_trans : forall (B1 B2 B3 : Visfun), Visfun_eq B1 B2 -> Visfun_eq B2 B3 -> Visfun_eq B1 B3.
Proof.
  move=> B1 B2 B3 HB12 HB23. split.
  apply (dom_eq_trans (dom B1) (dom B2) (dom B3) (proj1 HB12) (proj1 HB23)).
  move=> x y. split=> HB.  apply (proj1 (proj2 HB23 x y) (proj1 (proj2 HB12 x y) HB)).
  apply (proj2 (proj2 HB12 x y) (proj2 (proj2 HB23 x y) HB)).
Qed.
  
Lemma vis_union_dom (B1 : Visfun) (B2 : Visfun) :
  let dom3 := dom_union (dom B1) (dom B2) in
  let B3 := vis_union_B B1 B2 in
  forall (x : Names_cur) (y : Names), (B3 x y) -> dom3 x.
Proof.
  intros. destruct H.
  - left. apply (_dom B1 x y H).
  - right. apply (_dom B2 x y H).
Qed.

Definition union_vis (B1 : Visfun) (B2 : Visfun) : Visfun :=
  {|
    dom := dom_union (dom B1) (dom B2);
    B := vis_union_B B1 B2;
    _dom := vis_union_dom B1 B2;
    dom_fin := dom_union_fin (dom B1) (dom B2) (dom_fin B1) (dom_fin B2);
    codom_fin := union_codom_fin B1 B2 (codom_fin B1) (codom_fin B2);
  |}.

Lemma union_vis_comm : forall (B1 B2 : Visfun), Visfun_eq (union_vis B1 B2) (union_vis B2 B1).
Proof.
  move=> B1 B2. split. apply dom_union_comm.
  move=> x y. split=> H; destruct H; try by right. all : by left.
Qed.


Lemma vis_rest_dom (B : Visfun) (dom1 : Names_cur -> Prop) :
  let B1 := vis_rest_B B dom1 in
  forall (x : Names_cur) (y : Names), (B1 x y) -> dom1 x.
Proof.
  move=> B1 x y HB1. by destruct  HB1. 
Qed.

Definition rest_vis (B : Visfun) (d : Names_cur -> Prop) (dfin : is_dom_fin d) : Visfun :=
  {|
    dom := d;
    B := vis_rest_B B d;
    _dom := vis_rest_dom B d;
    dom_fin := dfin;
    codom_fin := rest_codom_fin B d (codom_fin B);
  |}.

Lemma rest_union_vis : forall (B1 B2 : Visfun), dom_disj (dom B1) (dom B2) -> Visfun_eq (rest_vis (union_vis B1 B2) (dom B1) (dom_fin B1))  B1.
Proof.
  move=> B1 B2 Hdis. split. by apply dom_eq_refl.
  move=> x y. split=> HB.
  - destruct HB as [[Hu1 | Hu2] HB]. apply Hu1. exfalso. apply (Hdis x HB (_dom _ _ _ Hu2)).
  - split. by left. by apply (_dom B1 x y HB).
Qed.

Lemma alpha_convert_aux_dom (a b : Names) (B : Visfun) :
  let dom1 := alpha_convert_dom a b (dom B) in
  let B1 := alpha_convert_B a b B in
  forall (x : Names_cur) (y : Names), (B1 x y) -> dom1 x.
Proof.
  move=> dom1 B1 x y HB. simpl in HB. destruct HB as [[Hx Halph] | [Hx Halph]].
  - destruct Halph as [[Hy HBy] | [Hy HBa]].
    + have Ha : dom B (names a) by apply (_dom B (names a) y HBy). by left.
    + have Ha : dom B (names a) by apply (_dom B (names a) a HBa). by left.
  - destruct Halph as [[Hy HBy] | [Hy HBa]].
    +  have Hx1 : dom B x by apply (_dom B x y HBy). by right.
    +  have Hx1 : dom B x by apply (_dom B x a HBa). by right.
Qed.


Lemma alpha_convert_codom_fin (a b : Names) (B : Visfun) : is_codom_fin (alpha_convert_B a b B).
Proof.
  destruct (codom_fin B) as [n HB]. destruct b as [b | p].
  2 : destruct (given_ncon p). destruct (given_nser b).
  1,3 : exists n; move=> k Hk x; split=> Halpha; destruct Halpha as [[Hxb Halph] | [Hxa Halph]].
          1-8 : destruct Halph as [[Hka HBak] | [Hka HBak]].
          1,3,9,11 : apply (proj1 (HB k Hk _) HBak).
          3,5,9,11 : apply (proj2 (HB k Hk _) HBak).
          1-8 : inversion Hka as [Hkb]; by apply (H k Hkb).
          all : destruct H as [nb Hnb]; exists (max n (nb +1)); move=> k Hk x; split=> Halpha; destruct Halpha as [[Hxb Halph] | [Hxa Halph]].
          all : have Hkn : k >= n by lia.
          all : destruct Halph as [[Hka HBak] | [Hka HBak]].
          1,3,9,11 : apply (proj1 (HB k Hkn _) HBak).
          3,5,9,11 : apply (proj2 (HB k Hkn _) HBak).
          all : inversion Hka as [Hkb]; rewrite (eq_sym Hkb) in Hnb.
          1,2 : have Hnbk : nb = k by apply (infinite_nser _ _ Hnb).
          3,4 : have Hnbk : nb = k by apply (infinite_ncon _ _ Hnb).
          all : lia.
Qed.

Definition alpha_convert_vis (a b : Names) (B : Visfun) :=
  {|
    dom := (alpha_convert_dom a b (dom B));
    B := alpha_convert_B a b B;
    _dom := alpha_convert_aux_dom a b B;
    dom_fin := (alpha_convert_dom_fin a b (dom B) (dom_fin B));
    codom_fin := alpha_convert_codom_fin a b B;
  |}.

Definition erase_B (x : Names) (B : Visfun) := minus_vis_B (names x) (minus_dom (dom B) (names x) ) (fun (y : Names_cur) => minus_V (B y) x ).

Lemma erase_dom_aux : forall (x : Names) (B : Visfun),
    let B1 :=  erase_B x B in
    forall (y : Names_cur) (z : Names), B1 y z -> (minus_dom (dom B) (names x)) y.
Proof.
  move=> x B B1 y z [[H1 _] H2]. split. by apply (_dom B y z H1). easy.
Qed.


Lemma erase_codom_fin : forall (x : Names) (B : Visfun), is_codom_fin (erase_B x B).
Proof.
  move=> x B. destruct (codom_fin B) as [n HB]. exists n.
  move=> k Hk y; split=> [H | H]; destruct H as [[HBk _] _].
  - apply (proj1 (HB k Hk y) HBk).
  - apply (proj2 (HB k Hk y) HBk).
Qed.

Definition erase_vis (x : Names) (B : Visfun) :=
  {|
    dom := minus_dom  (dom B) (names x);
    B := erase_B x B;
    _dom := erase_dom_aux x B;
    dom_fin := minus_dom_fin (dom B) (names x) (dom_fin B);
    codom_fin := erase_codom_fin x B; |}.

Definition is_fresh_vis (a : Names) (B : Visfun) := ~(dom B (names a)) /\ (forall (x : Names_cur), ~(B x a)).
  
Definition Visfun_weak (B1 : Visfun) (B2 : Visfun) : Prop := (dom_eq (dom B1) (dom B2)) /\ (forall (x : Names_cur), forall (y: Names), B1 x y -> B2 x y).

Lemma vis0_aux : is_V_fin (fun (y : Names) => False).
Proof.
  exists 0. easy.
Qed.
Definition vis0_ : Names_cur -> Vis := fun (x : Names_cur) => {| V:= fun (y : Names) => False; V_fin := vis0_aux; |}.
Lemma vis0_dom : forall (x : Names_cur) (y : Names), (vis0_ x y) -> (empty x).
Proof.
  trivial.
Qed.
Lemma vis0_codom_fin : is_codom_fin vis0_.
Proof.
  exists 0. intros k Hk x. split. by intro. by intro.
Qed.

Definition vis0 : Visfun :=
  {|
    dom := empty;
    B := vis0_;
    _dom := vis0_dom;
    dom_fin := empty_fin;
    codom_fin := vis0_codom_fin;
    |}.

Lemma vis_is_empty : forall (B : Visfun), dom_eq (dom B) empty -> Visfun_eq B vis0.
Proof.
  intros. split.
  - apply H.
  - intros. split.
    + intro. apply (proj1 (H x) (_dom B0 x y H0)).
    + intro. exfalso. apply ((_dom vis0 x y) H0).
Qed.
End visibility_function.

Section lemmas_visibility.

  (* a lemma linking equivalence of Vis and equality on Visfun *)
Lemma add_equiv_vis (x : Names_cur) (V1 V2 : Vis) (B1 B2 : Visfun) : (forall (x : Names), V1 x  <-> V2 x) -> Visfun_eq B1 B2 -> Visfun_eq (add_vis x V1 B1) (add_vis x V2 B2).
Proof.
  move=> HV Heq. split.
  - split=> Hd. all:  destruct Hd;[ left; by apply Heq| by right].
  - move=> y z. split=> Hadd; destruct Hadd.
    1,3 : left; by apply Heq.
    all: right; split. 1,3 : apply HV. all : by destruct H.
Qed.
(* Lemmas between alpha convesion and other operations on Visfun *)

Lemma alpha_convert_idem : forall (a : Names) (B : Visfun), Visfun_eq (alpha_convert_vis a a B) B.
Proof.
  move=> a B. split.
  - split=> Hd.
    + destruct Hd as [[Hxa HB] | [Hxa HB]]; by try rewrite Hxa. 
    + case (Names_cur_dec x (names a))=> Hxa; [left | right]. rewrite Hxa in Hd. all : by split.
  - move=> x y. split=> HB.
    + destruct HB as [[Hx HB] | [Hx HB]]; destruct HB as [[Hy HBy] | [Hy HBy]]; try rewrite Hx; by try rewrite Hy.
    + case (Names_cur_dec x (names a))=> Hxa.
      left. split; [easy|]. rewrite Hxa in HB.
      2 : right; split;[easy|].
      all: case (Names_dec y a)=> Hya; [right; rewrite Hya in HB | left]; by split.
Qed.
  
Lemma alpha_add_vis_replace : forall (a b : Names) (V : Vis) (B : Visfun), ~(dom B (names a)) -> Visfun_eq (alpha_convert_vis a b (add_vis (names a) V B)) (add_vis (names b) (alpha_convert_V a b V) (alpha_convert_vis a b B)).
Proof.
  move=> a b V B Hda. split.
  - split=> Hd.
    + destruct Hd as [[Hx Hd] | [Hx Hd]].
      * rewrite Hx; by right.
      * destruct Hd.
        ++ left; right; by split.
        ++ by symmetry in H. 
    + destruct Hd as [[[Hx Hd] | [Hx Hd]] |].
      * left; split; [assumption | by right]. 
      * right; split; [assumption | by left].
      * left; split;[ by symmetry| by right].
  - move=> x y. split=> Halph.
    + destruct Halph as [[Hx Halph] | [Hx Halph]].
      * destruct Halph as [[Hy Hadd] | [Hy Hadd]]; destruct Hadd as [HB | Hadd].
        1,3 : exfalso; apply (Hda (_dom B (names a) _ HB)).
        all : right; split; [destruct Hadd | easy]. left; by split. right; by split.
      * destruct Halph as [[Hy Hadd] | [Hy Hadd]] ;destruct Hadd as [ HB | [ _ Ha]].
        2,4 :exfalso; by symmetry in Ha.
        all : left;right;split;[ easy|]. left; by split. right; by split.
    + destruct Halph as [Halph | [Halph Hb]].
      * destruct Halph as [[Hx Halph] | [Hx Halph]].
        ++  destruct Halph as [[Hy HB] | [Hy HB]].
            all: exfalso; apply (Hda (_dom B (names a) _ HB)).
        ++ destruct Halph as [[Hy HB] | [Hy HB]].
           all : right;split;[easy|]. left. 2: right. all: split; by try left.
      * destruct Halph as [[Hy HV] | [Hy HV]].
        all: left;split;[easy |]. left. 2: right. all: split;[easy| right;by split].
Qed.
      
Lemma alpha_add_vis_leave : forall (a b : Names) (x : Names_cur ) (V : Vis) (B : Visfun), (x <> (names a)) -> Visfun_eq (alpha_convert_vis a b (add_vis x V B)) (add_vis x (alpha_convert_V a b V) (alpha_convert_vis a b B)).
Proof.
  move=> a b x V B Hxa. split.
  - split=> Hd.
    + destruct Hd as [[Hx Hd] | [Hx Hd]]; destruct Hd as [Hd | Hd]; try easy; try by right.
      all: left. left; by split. right; by split.
    + destruct Hd as [Hd | Hx].
      * destruct Hd as [[Hx Hd] | [Hx Hd]].
        ++  left. split; by try left. 
        ++  right. split; by try left. 
      * right; split;[ by rewrite Hx in Hxa | rewrite Hx; by right]. 
  - move=> y z. split=> Halph.
    + destruct Halph as [[Hy Halph] | [Hy Halph]].
      * destruct Halph as [[Hz HV] | [Hz HV]]; destruct HV as [HB | HV].
        2,4 : by destruct HV. all : left;left;split;[easy|]. left;by split. right; by split.
      * destruct Halph as [[Hz HV] | [Hz HV]]; destruct HV as [HB | HV].
        1,3 :left; right; split;[ easy|]. 3,4 :right; destruct HV; split; [|easy].
        1,3 : left;by split. all: right; by split.
    + destruct Halph as [Halph | Halph].
      * destruct Halph as [[Hy Halph] | [Hy Halph]]; destruct Halph as [[Hz HB] | [Hz HB]].
        1,2 : left. 3,4 : right. all: split;[easy|]. 1,3 : left. 3,4 : right. all: by split;[easy | by left].
      * destruct Halph as [[[Hz HV] | [Hz HV]] Hxy]. 
        all: right; split;[ by rewrite Hxy in Hxa|]. left.  2 : right.
        all: split;[ easy | right; by split].
Qed.
                     
Lemma alpha_rest_vis : forall (a b : Names) (d : Names_cur -> Prop) (dfin : is_dom_fin d) (B : Visfun),
    ~(d (names b)) -> is_fresh_vis b B -> Visfun_eq (alpha_convert_vis a b (rest_vis B d dfin)) (rest_vis (alpha_convert_vis a b B) (alpha_convert_dom a b d) (alpha_convert_dom_fin a b d dfin)).
Proof.
  move=> a b d dfin B Hdb [HdBb HcBb]. split.
  - apply dom_eq_refl.
  - move=> x y. split=> [Halph | Hrest].
    + destruct Halph as [[Hx Halph] | [Hx Halph]]; destruct Halph as [[Hy [HB Hd]] | [Hy [HB Hd]]]; split.
      1-4 : left. 5-8 : right.
      1,3,5,7 : split;[easy|].
      5-8 : by split. 1,3 : left; by split. all : right; by split.
    + destruct Hrest as [Halph Halphd]; destruct Halph as [[Hx Halph] | [Hx Halph]].
      all : destruct Halphd as [[Hxa Hd] | [Hxa Hd]].
      2 : by rewrite Hx in Hd.
      all : destruct Halph as [[Hy HB] | [Hy HB]].
      3,4 : rewrite Hxa in HB; exfalso; by apply (HdBb (_dom B _ _ HB)).
      1,2 : left. 3,4 : right. all: split;[easy|].
      1,3 :left. 3,4 : right. all: split;[easy|by split].
Qed.
  
Lemma alpha_union_vis : forall (a b : Names) (B1 B2 : Visfun),
    Visfun_eq (alpha_convert_vis a b (union_vis B1 B2)) (union_vis (alpha_convert_vis a b B1) (alpha_convert_vis a b B2)).
Proof.
  move=> a b B1 B2. split.
  - split=> Hd.
    + destruct Hd as [[Hx [Hd | Hd]] | [Hx [Hd | Hd]]].
      1,3 : left. 3,4 : right. 1,3 : left. 3,4 : right. all: by split.
    + destruct Hd as [[[Hx Hd] | [Hx Hd]] | [[Hx Hd] | [Hx Hd]]].
      1,3 : left. 3,4 : right. all : split;[easy | try by left]. all: by right.
  - move=> x y. split=> [Halph | Hunion].
    + destruct Halph as [[Hx Halph] | [Hx Halph]]; destruct Halph as [[Hy Hunion] | [Hy Hunion]]; destruct Hunion.
      1,3,5,7 : left. 5-8 : right.
      1,2,5,6 : left. 5-8 : right.
      all : split;[easy|].
      1,3,5,7 : left. 5-8 : right.
      all : by split.
    + destruct Hunion as [Halph | Halph]; destruct Halph as [[Hx Halph] | [Hx Halph]]; destruct Halph as [[Hy HB] | [Hy HB]].
      1,2,5,6 : left. 5-8: right. all : split;[easy|].
      1,3,5,7 : left. 5-8 : right. all: split;[easy|try by left]. all: by right.
Qed.

Lemma alpha_alpha_vis : forall (a b c : Names) (B : Visfun), is_fresh_vis b B -> Visfun_eq (alpha_convert_vis b c (alpha_convert_vis a b B)) (alpha_convert_vis a c B).
Proof.
  move=> a b c B HfB. split.
  - move=> x; split=> Halph.
    + destruct Halph as [[Hx Hd] | [Hx Hd]]; destruct Hd as [[Hb Hd] | [Hb Hd]].
      left; by split.
      by destruct HfB.
      all: right; by split.
    + destruct Halph as [[Hx Hd] | [Hx Hd]].
      left. split. assumption. left. by split.
      right. split. move=> Hxb. rewrite Hxb in Hd. by destruct HfB.
      right. by split.
  - move=> x y; split=> Halph.
    + destruct Halph as [[Hx Halph] | [Hx Halph]]; destruct Halph as [[Hy Halph] | [Hy Halph]]; destruct Halph as [[Hb Halph] | [Hb Halph]]; destruct Halph as [[Hy2 HB] | [Hy2 HB]]; try easy.
      2,5,6 : have Hd : dom B (names b) by apply (_dom B _ _ HB). 2,3,4 : by destruct HfB.
      2,5 : exfalso; destruct HfB; by apply (H0 _ HB).
      1,2 : left. 3,4 : right. all: split;[assumption|].
      1,3 : left. 3,4 : right. all: by split.
    + destruct Halph as [[Hx Halph] | [Hx Halph]]; destruct Halph as [[Hy HB] | [Hy HB]].
      3, 4 : have Hxb : x <> (names b).
      3,5 : move=> Hxb; rewrite Hxb in HB; have Hdb : dom B (names b) by apply (_dom B _ _ HB).
      3,4 : by destruct HfB.
      1,3 : have Hyb : y <> b. 1,3 : move=> Hyb; rewrite Hyb in HB; destruct HfB; by apply (H0 _ HB).
      1,3 : left. 3,4 : right. all : split;[easy|].
      1,3 : left. 3,4 : right. all: split;[easy|].
      1,3 : left. 3,4 : right. all : split;[easy|].
      1,3 : left. 3,4 : right. all: by split.
Qed.

Lemma alpha_vis_invol : forall (a b : Names) (B : Visfun), is_fresh_vis b B -> Visfun_eq (alpha_convert_vis b a (alpha_convert_vis a b B)) (B).
Proof.
  move=> a b B HfB.
  have Halph1 : Visfun_eq (alpha_convert_vis b a (alpha_convert_vis a b B)) (alpha_convert_vis a a B).
  by apply (alpha_alpha_vis a b a B HfB).
  have Halph2 : Visfun_eq (alpha_convert_vis a a B) B. apply alpha_convert_idem.
  apply (Visfun_eq_trans _ _ _ Halph1 Halph2).
Qed.
  
Lemma erase_add_vis1 : forall (a : Names) (V : Vis) (B : Visfun), Visfun_eq (erase_vis a (add_vis (names a) V B)) (erase_vis a B).
Proof.
  move=> a V B. split.
  - split=> Hd; destruct Hd as [Hd Hx].
    + destruct Hd; [by split | easy].
    + split; [by left | assumption].
  - move=> x y. split=> [Her|Her]; destruct Her as [[Hadd Hy] Hx].
    + destruct Hadd. split;[ by split| easy]. by destruct H.
    + split; [|assumption]. split; [by left | assumption].
Qed.

Lemma erase_add_vis2 : forall (a : Names) (b : Names_cur) (V : Vis) (B : Visfun),
    (b <> (names a)) -> Visfun_eq (erase_vis a (add_vis b V B)) (add_vis b (minus_V V a) (erase_vis a B)).
Proof.
  move=> a b V B Hba. split.
  - split=> [Hd | Hd]; destruct Hd.
    + destruct H. left. by split. by right.
    + destruct H. split. by left. easy.
    + split. by right. intro. rewrite (eq_sym H0) in H. by inversion H.
  - move=> x y; split=> [Her | Hadd].
    + destruct Her. destruct H. destruct H. left. split; try assumption. by split. destruct H.
      right. split; try assumption. by split.
    + destruct Hadd. destruct H. destruct H. split; [|assumption]. split; [|assumption]. by left.
      destruct H. destruct H. split. split;[|assumption].  right. by split. intro. rewrite (eq_sym H2) in H0. by inversion H0.
Qed.
(*
Lemma erase_trans_minus : forall (a b : Names) (B : Visfun),
    b <> a -> Visfun_eq (erase_vis a (trans_minus_vis b B)) (trans_minus_vis b (erase_vis a B)).
Proof.
  move=> a b B Hba. split.
  - split=> Hdom; destruct Hdom as [[Hdom Hb] Ha]. all: split; [by split | assumption].
  - move=> x y; split=> [Her | Htrans].
    + destruct Her as [[[Hl [Hxb Hyb]] Hya] Hax]. destruct Hl as [HB | [HB1 HB2]].
      all : split;[|by split].
      left. 2 : right. all: split;[split|]; try easy.
      split; [by split|].  symmetry. move=> Hab. by inversion Hab.
    + destruct Htrans as [[HB | [HB1 HB2]] [Hx Hy]].
      2 : destruct HB1 as [[HB1 Hya1] Hax1]; destruct HB2 as [[HB2 Hya2] Hax2].
      destruct HB as [[HB Hya] Hax].
      all : split; [split; [split; [| by split]| assumption] |assumption].                                              by left. by right.
Qed.*)

Lemma erase_union_vis : forall (a : Names) (B1 B2 : Visfun),
    Visfun_eq (erase_vis a (union_vis B1 B2)) (union_vis (erase_vis a B1) (erase_vis a B2)).
Proof.
  move=> a B1 B2. split.
  - split=> [Hmin | Hun].
    + destruct Hmin. destruct H. left. 2 : right. all : by split. 
    + destruct Hun. all : destruct H; split; [|assumption]. by left. by right.
  - move=> x y; split=> [Her | Hun].
    + destruct Her. destruct H. destruct H; [left |right]; by split; [split | assumption]. 
    + destruct Hun. all : destruct H; destruct H;  split; [| assumption]; split; [|assumption]. by left . by right.
Qed.

Lemma erase_idem : forall (a : Names) (B : Visfun), ~(dom B (names a))
                                               -> (forall (x : Names_cur), ~(B x a)) -> Visfun_eq B (erase_vis a B).
Proof.
  move=> a B Hf1 Hf2. split.
  - split=> [Hd | Herd].
    + split; [assumption|]. intro. by rewrite H in Hf1.
    + by destruct Herd.
  - move=> x y. split=> [HB | HerB].
    split. split. assumption. intro. rewrite H in HB. apply (Hf2 x HB). intro. rewrite H in Hf1.
    apply (Hf1 (_dom B x y HB)). destruct HerB. by destruct H.
Qed.

Lemma fresh_erase : forall (x : Names) (B : Visfun), is_fresh_vis x (erase_vis x B).
Proof.
  move=> x B. split. intro. by destruct H.
  move=> y Her. destruct Her. by destruct H.
Qed.
(* Definition and lemmas on weakening relation *)

Lemma Visfun_weak_refl : forall (B1 : Visfun), Visfun_weak B1 B1.
Proof.
  easy.
Qed.

Lemma Visfun_weak_trans : forall (B1 B2 B3 : Visfun), Visfun_weak B1 B2 -> Visfun_weak B2 B3 -> Visfun_weak B1 B3.
Proof.
  move=> B1 B2 B3 Hweak1 Hweak2. split.
  - apply (dom_eq_trans (dom B1) (dom B2) (dom B3) (proj1 Hweak1) (proj1 Hweak2)).
  - move=> x y HB. apply Hweak2. by apply Hweak1. 
Qed.

Lemma Visfun_weak_eq : forall (B1 B2 B3 B4 : Visfun), Visfun_eq B1 B2 -> Visfun_eq B3 B4 -> Visfun_weak B1 B3 -> Visfun_weak B2 B4.
  Proof.
  move=> B1 B2 B3 B4 Heq1 Heq2 Hweak. split.
  - apply (dom_eq_trans _ (dom B3)). apply (dom_eq_trans _ (dom B1)). apply dom_eq_sym. apply Heq1.
    apply Hweak. apply Heq2.
  - move=> x y HB2. apply Heq2. apply Hweak. by apply Heq1.
Qed.

Lemma Visfun_weak_empty : forall (B : Visfun), Visfun_weak vis0 B -> Visfun_eq B vis0.
Proof.
  move=> B Hweak. apply vis_is_empty. apply dom_eq_sym. by destruct Hweak.
Qed.



Lemma Visfun_weak_add : forall (B1 B2 : Visfun) (x : Names_cur) (V : Vis), (~dom B1 x) -> Visfun_weak (add_vis x V B1) B2 -> (exists (B3 : Visfun), Visfun_eq B2 ((add_vis x (B2 x) B3)) /\ Visfun_weak B1 B3).
Proof.
  move=> B1 B2 x V HdB1 Hweak. 
  exists (rest_vis B2 (minus_dom (dom B2) x) (minus_dom_fin (dom B2) x (dom_fin B2))).
  split.
  - split.
    + have Hdx : dom B2 x. apply (proj1 Hweak). by right.
      split=> Hd. destruct x; destruct x0.
      by right. left. by split. left. by split.
      case (Names_dec n n0)=> Hnn0. right. by rewrite Hnn0.
      left. split; [assumption|]. move=> Hnnn0. by inversion Hnnn0.
      destruct Hd. destruct H. assumption. by rewrite (eq_sym H).
    + move=> y z; split=> HB.
      * case (Names_cur_dec x y)=> Hxy.
        right. rewrite Hxy. by split.
        left; split;[ assumption| split;[ apply (_dom B2 _ _ HB)| easy]].
      * destruct HB. by destruct H.
        destruct H. by rewrite H0 in H.
  - split;[|split].
    + split=> Hd.
      * split. apply (proj1 Hweak). by left. move=> Hx; by rewrite Hx in HdB1.
      * destruct Hd. have HdB2 : dom (add_vis x V B1) x0 by apply (proj1 Hweak).
        by destruct HdB2.
    + apply Hweak. by left.
    + have Hd : dom B1 x0  by apply (_dom B1 _ _ H). split. apply Hweak. by left. intro. by rewrite H0 in HdB1.
Qed.

Lemma Visfun_weak_union : forall (B1 B2 B3 : Visfun), Visfun_weak (union_vis B1 B2) B3 -> (exists (B4 B5 : Visfun), Visfun_eq B3 (union_vis B4 B5) /\ Visfun_weak B1 B4 /\ Visfun_weak B2 B5).
  Proof.
    move=> B1 B2 B3 Hweak. exists (rest_vis B3 (dom B1) (dom_fin B1)). exists (rest_vis B3 (dom B2) (dom_fin B2)).
    split.
    - split.
      + split=> Hd.
        * have Hd12 : (dom (union_vis B1 B2) x) by apply (proj2 (proj1 Hweak x) Hd). destruct Hd12.
          by left. by right.
        * destruct Hd; apply (proj1 Hweak). by left. by right.
      + move=> x y; split=> HB.
        * have Hd12 : dom (union_vis B1 B2) x by apply (proj2 (proj1 Hweak x) (_dom B3 _ _ HB)).
          destruct Hd12; [left|right]; by split.
        * destruct HB; by destruct H.
    - split. all : split;[ easy| move=> x y HB; split;[ apply (proj2 Hweak) |]].
      by left. apply (_dom B1 _ _ HB). by right. apply (_dom B2 _ _ HB).
Qed.

Lemma alpha_empty_vis : forall (a b : Names), Visfun_eq (alpha_convert_vis a b vis0) vis0.
Proof.
  move=> a b. apply vis_is_empty. split.
  - intro. destruct H. all: by apply (proj2 H). 
  - intro. easy.
Qed.

Lemma erase_empty : forall (a : Names), Visfun_eq (erase_vis a vis0) vis0.
Proof.
  move=> a. apply vis_is_empty. split=> [H | H]; [by destruct H| easy].
Qed.

End lemmas_visibility.
