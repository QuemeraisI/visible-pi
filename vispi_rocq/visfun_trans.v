Require Import ssreflect.
Require Import ZArith Psatz.
Require Export Names visfun.

Definition is_trans (B : Visfun) : Prop := forall (x : Names_cur) (y z : Names), B x y -> B (names y) z -> B x z.

Structure Visfun_trans := {
    Btr :> Visfun;
    B_trans : (is_trans Btr); }.

Definition Visfun_trans_eq (B1 B2 : Visfun_trans) : Prop := Visfun_eq B1 B2.

Lemma is_trans_eq (B1 B2 : Visfun) : Visfun_eq B1 B2 -> is_trans B1 -> is_trans B2.
Proof.
  move=> Heq Htr x y z Hxy Hyz.
  apply Heq. apply (Htr x y z); by apply Heq.
Qed.

Lemma minus_vis_is_trans (x : Names_cur) (B : Visfun_trans) : is_trans (minus_vis x B).
Proof.
  move=> y z t HByz HBzt.
  destruct HByz as [HByz Hxy]. destruct HBzt as [HBzt _]. split. by apply (B_trans B y z t). assumption.
Qed.

Definition minus_vis_trans (x : Names_cur) (B : Visfun_trans) : Visfun_trans :=
  {| Btr := minus_vis x B;
    B_trans := minus_vis_is_trans x B;|}.

Lemma erase_trans_minus (x : Names) (B : Visfun_trans) : Visfun_eq (erase_vis x B) (trans_minus_vis x B).
Proof.
  split.
  - easy.
  - move=> y z; split=> HB.
    + destruct HB. destruct H. split. by left. split. by symmetry. assumption.
    + destruct HB. destruct H. 2 : destruct H; destruct H; [| by destruct H0]. 
      2 : destruct H1. 2 : have HB : B y z by apply (B_trans B _ x z H H1).
      3 : destruct H1; destruct H1; have HB : B (names x) z by apply (B_trans B (names x) _ z H1 H2).
      3 : have HB2 : B y z by apply (B_trans B _ x z H HB).
      all: destruct H0; split;[by split| easy].
Qed.

Lemma erase_is_trans (x : Names) (B : Visfun_trans) : is_trans (erase_vis x B).
Proof.
  move=> y z t Hyz Hzt. destruct Hyz. destruct H. destruct Hzt. destruct H2.
  split. split. apply (B_trans B y z t H H2). all : assumption.
Qed.

Definition erase_vis_trans (x : Names) (B : Visfun_trans) :=
  {| Btr := erase_vis x B;
    B_trans := erase_is_trans x B;|}.

Lemma add1_trans : forall (x : Names_cur) (V : Vis), is_trans (add_vis x V vis0).
Proof.
  move=> x V y z t Hyz Hzt. destruct Hyz. easy. destruct H. destruct Hzt. easy. destruct H1.
  right. split; assumption.
Qed.

Definition add1_vis_trans (x : Names_cur) (V : Vis) :=
  {| Btr := add_vis x V vis0;
    B_trans := add1_trans x V;|}.

Lemma add1_trans_weak (x : Names_cur) (V1 V2 : Vis) : (forall y, V1 y -> V2 y) -> Visfun_weak (add1_vis_trans x V1) (add1_vis_trans x V2).
Proof.
  move=> Hincl. split. easy. move=> y z HaddV1. destruct HaddV1. easy. destruct H. right. split. by apply Hincl.
  assumption.
Qed.

Lemma add2_trans : forall (x y : Names_cur) (V : Vis), is_trans (add_vis x V (add_vis y V vis0)).
Proof.
  move=> x y V a b c Hab Hbc.
  destruct Hab.
  - destruct H. easy. destruct H.
    + destruct Hbc. destruct H1;[easy|]. all : destruct H1; left;right; by split.
  - destruct H. right. split. 2 : assumption.
    destruct Hbc. destruct H1. easy. all : by destruct H1.
Qed.

Definition add2_vis_trans (x y : Names_cur) (V : Vis) :=
  {| Btr := add_vis x V (add_vis y V vis0);
    B_trans := add2_trans x y V; |}.

Lemma add2_trans_weak (x y : Names_cur) (V1 V2 : Vis) : (forall y, V1 y -> V2 y) -> Visfun_weak (add2_vis_trans x y V1) (add2_vis_trans x y V2).
Proof.
  move=> Hincl. split. easy. move=> a b HaddV1. destruct HaddV1. destruct H. easy. destruct H.
  left. right. split. by apply Hincl. assumption. destruct H. right. split. by apply Hincl. assumption.
Qed.

Lemma add_rec_is_trans (x : Names) (B : Visfun_trans) : dom B (names x) -> is_trans (add_rec_vis x B).
Proof.
  move=> Hd. move=> a b c HBab HBbc. destruct HBab.
  destruct HBbc. left. by apply (B_trans B _ b). destruct H0. destruct H0. easy. inversion H1.
  rewrite H3 in H0. rewrite H0. by left. destruct H. destruct H. easy.
  rewrite (eq_sym H0). by rewrite H in HBbc.
Qed.

Definition add_rec_trans (x : Names) (B : Visfun_trans) (Hdx : dom B (names x)) :=
  {| Btr := add_rec_vis x B; B_trans := add_rec_is_trans x B Hdx;|}.

Lemma union_trans : forall (B1 B2 B3 : Visfun), Visfun_eq B1 (union_vis B2 B3) -> is_trans B1 -> dom_disj (dom B2) (dom B3) -> (is_trans B2 /\ is_trans B3).
Proof.
  have Haux : forall B1 B2 B3 : Visfun, Visfun_eq B1 (union_vis B2 B3) -> is_trans B1 -> dom_disj (dom B2) (dom B3) -> is_trans B2.
  - move=> B1 B2 B3 Heq Htr Hdisj x y z Hxy Hyz. have HBu : union_vis B2 B3 x z.
    apply Heq. apply (Htr x y z). 1,2 : apply Heq; by left.
    destruct HBu. easy. have Hd2 : dom B2 x by apply (_dom B2 x y Hxy). have Hd3 : dom B3 x by apply (_dom B3 x z H). exfalso. by apply (Hdisj x Hd2 Hd3).
  - move=> B1 B2 B3 Heq Htr Hdisj. split.
    by apply (Haux B1 _ B3). apply (Haux B1 _ B2). apply (Visfun_eq_trans _ (union_vis B2 B3)).
    assumption. apply union_vis_comm. assumption. by apply dom_disj_comm.
Qed.

Inductive trans_close_V (B : Visfun) (x : Names_cur) (z : Names): Prop :=
| in_B : B x z -> trans_close_V B x z
| trans_step : forall (y : Names), (y <> z) -> trans_close_V B x y -> trans_close_V B (names y) z -> trans_close_V B x z.

Lemma trans_close_V_is_fin (B : Visfun) (x : Names_cur) : is_V_fin (trans_close_V B x).
Proof.
  destruct (codom_fin B) as [n Hn].
  exists n. move=> k Hk; split=> [Htrs| Htrc].
  - have Htr : forall (x : Names_cur) (y : Names), trans_close_V B x y -> y = (srv (give_nser k)) -> False.
    move=> y z Htr. induction Htr.
    + move=> Hz. rewrite Hz in H. by apply (proj1 (Hn k Hk _) H).
    + assumption.
    + by apply (Htr x (srv (give_nser k))).
  - have Htr : forall (x : Names_cur) (y : Names), trans_close_V B x y -> y = (cnt (give_ncon k)) -> False.
    move=> y z Htr. induction Htr.
    + move=> Hz. rewrite Hz in H. by apply (proj2 (Hn k Hk _) H).
    + assumption.
    + by apply (Htr x (cnt (give_ncon k))).
Qed.

Definition trans_close_B (B : Visfun) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V := trans_close_V B x; V_fin := trans_close_V_is_fin B x;|}.

Lemma trans_close_B_dom (B : Visfun) : forall (x : Names_cur) (y : Names), (trans_close_B B x y) -> dom B x.
Proof.
  move=> x y Htr. induction Htr. apply (_dom B _ _ H). assumption.
Qed.

Lemma trans_close_B_codom_fin (B : Visfun) : is_codom_fin (trans_close_B B).
Proof.
  destruct (codom_fin B) as [n Hn]. exists n.
  move=> k Hk x; split=> [Htr | Htr].
  - have Htr' : forall (x : Names_cur) (y : Names), trans_close_V B x y -> y = (srv (give_nser k)) -> False.
    move=> y z Htr'. induction Htr'=> Hz.
    rewrite Hz in H. apply (proj1 (Hn k Hk _) H). by apply IHHtr'1.
    by apply (Htr' x (srv (give_nser k))).
  -  have Htr' : forall (x : Names_cur) (y : Names), trans_close_V B x y -> y = (cnt (give_ncon k)) -> False.
    move=> y z Htr'. induction Htr'=> Hz.
    rewrite Hz in H. apply (proj2 (Hn k Hk _) H). by apply IHHtr'1.
    by apply (Htr' x (cnt (give_ncon k))).
Qed.

Definition trans_close_vis (B : Visfun) : Visfun :=
  {| dom := dom B;
    B := trans_close_B B;
    _dom := trans_close_B_dom B;
    dom_fin := dom_fin B;
    codom_fin := trans_close_B_codom_fin B;|}.

Lemma is_trans_trans_close_vis (B : Visfun) : is_trans (trans_close_vis B).
Proof.
  move=> x y z Hxy Hyz. case (Names_dec y z)=> Hyz2. by rewrite Hyz2 in  Hxy.
  by apply (trans_step B x z y).
Qed.

Definition trans_close_vis_trans (B : Visfun) : Visfun_trans :=
  {| Btr := trans_close_vis B;
    B_trans := is_trans_trans_close_vis B;|}.

(*Lemma trans_add_trans_minus (B : Visfun_trans) (x : Names) (V : Vis) : Visfun_eq B (trans_minus_vis x (add_vis (names x) V B)) -> Visfun_eq B (erase_vis_trans x (trans_close_vis_trans (add_vis (names x) V B))).
Proof.
  move=> Heq. split.
  - split=> Hd.
    + split. by left. move=> Hxx0. rewrite (eq_sym Hxx0) in Hd. have Hdx : (dom (trans_minus_vis x (add_vis (names x) V B)) (names x)) by apply Heq. by destruct Hdx.
    + destruct Hd. destruct H. assumption. easy.
  - move=> y z. split=> HB.
    split. split. apply in_B. by left. have Ht : trans_minus_vis x (add_vis (names x) V B) y z by apply Heq.
    destruct Ht. by destruct H0.
    have Ht : trans_minus_vis x (add_vis (names x) V B) y z by apply Heq. destruct Ht. symmetry. by destruct H0.
    destruct HB. destruct H. induction H. destruct H. assumption. by destruct H.
    case (Names_dec y x)=> Hyx.
    apply Heq. split. right.

    
  Lemma trans_trans_minus (B : Visfun) (Bt : Visfun_trans) (x : Names) : Visfun_eq Bt (trans_minus_vis x B) -> Visfun_eq Bt (erase_vis_trans x (trans_close_vis_trans B)).
Proof.
  move=> Heq. split. apply Heq.
  move=> y z; split=> HB.
  - have HBm : trans_minus_vis x B y z. by apply Heq.
    destruct HBm. destruct H0. destruct H.
    + split. split. by apply in_B. assumption. by symmetry.
    + destruct H. split. split. apply (trans_step _ _ _ x); by apply in_B. assumption. by symmetry.
  - destruct HB. destruct H. induction H.
    apply Heq. split. by left. split. by symmetry. assumption.
    case (Names_dec x y)=> Hxy. *)
(*
Definition Restore_trans_minus_V_ (B1 B2 : Visfun_trans) (p : Names) := fun x => B1 (names p) x \/ (exists t, ((incl_V (B1 (names t)) (B1 (names p)) \/ B1 (names p) t) /\ B2 (names t) x)).
Lemma Restore_trans_minus_V_fin (B1 B2 : Visfun_trans) (p : Names): is_V_fin (Restore_trans_minus_V_ B1 B2 p).
Proof.
  destruct (codom_fin B1) as [n Hn]. destruct (codom_fin B2) as [m Hm]. exists (max n m).
  move=> k Hk. split=> Hrest.
  - destruct Hrest as [HB1 | [t [_ HB2]]].
    + have Hkn : k >= n by lia. by apply (proj1 (Hn k Hkn _) HB1).
    + have Hkm : k >= m by lia. by apply (proj1 (Hm k Hkm _) HB2).
  - destruct Hrest as [HB1 | [t [_ HB2]]].
    + have Hkn : k >= n by lia. by apply (proj2 (Hn k Hkn _) HB1).
    + have Hkm : k >= m by lia. by apply (proj2 (Hm k Hkm _) HB2).
Qed.
Definition Restore_trans_minus_V (B1 B2 : Visfun_trans) (p : Names) : Vis :=
  {| V := Restore_trans_minus_V_ B1 B2 p;
    V_fin := Restore_trans_minus_V_fin B1 B2 p;|}.

Definition Restore_trans_minus_vis (B1 B2 : Visfun_trans) (x : Names) := (add_vis (names x) (Restore_trans_minus_V B1 B2 x) (union_vis B1 B2)).

Lemma restore_is_trans (B1 B2 : Visfun_trans) (x : Names) : is_fresh_vis x B2 -> B1 (names x) x -> Visfun_weak (erase_vis x B1) B2 -> is_trans (Restore_trans_minus_vis B1 B2 x).
Proof.
  move=> Hfx HrecBx Hweak a b c Bab Bbc.
  have HBleq : forall a b, a <> names x -> b <> x -> B1 a b -> B2 a b.
  move=> p q Hpx Hqx HB1. apply Hweak. split. split. 3 : symmetry. 1,2,3 : assumption.
  destruct Bab as [ [HB11 | HB21] | [Hr1 Hxa]]; destruct Bbc as [[HB12 | HB22] | [Hr2 Hxb]].
  - left. left. by apply (B_trans B1 _ b _).
  - case (Names_cur_dec a (names x))=> Hax. right. split. right. exists b. rewrite Hax in HB11. split;[by right| assumption].  by symmetry.
    left. right. apply (B_trans B2 _ b). apply HBleq;try assumption. intro. rewrite H in HB22. destruct Hfx.
    have Hdx : dom B2 (names x) by apply (_dom B2  _ _ HB22). contradiction. assumption.
  - destruct Hr2. left. left. apply (B_trans B1 _ b). assumption. by rewrite Hxb in H.
    case (Names_cur_dec a (names x))=> Hxa. right. split. by right. by symmetry.
    left. destruct H as [t [[Hincl | Hxt] Htc]].
    + have Hincl2 : incl_V (B1 (names t)) (B1 a). move=> y HBt. apply (B_trans B1 _ x). by inversion Hxb.
      by apply Hincl.
      have Hincl3 : incl_V (B2 (names t)) (B2 a). apply Hweak. apply Hweak. apply (_dom B2 _ _ Htc).
      split. apply (_dom B1 _ _ HB11). by symmetry. move=> y HB1. destruct HB1. destruct H. split. split.
      by apply Hincl2. assumption. by symmetry.
      right. by apply Hincl3.
    + rewrite Hxb in Hxt. have Hat : B1 a t by apply (B_trans B1 _ b). right.
      apply (B_trans B2 _ t). apply HBleq; try assumption. intro. rewrite H in Htc. have Hdx : dom B2 (names x) by apply (_dom B2 _ _ Htc). by destruct Hfx. assumption.
  - case (Names_dec x c)=> Hxc. left. 
    *)
(*  need B2 transitive and B1 reflexive on x (we should be able to show that typing is not affected by adding a reflexivity for one name) but it means that we need to show that B3 is transitive*)
Lemma Visfun_weak_trans_minus_rec : forall (B1 B2 : Visfun_trans) (x : Names), is_fresh_vis x B2 -> B1 (names x) x -> Visfun_weak (erase_vis x B1) B2 -> (exists (B3 : Visfun_trans), Visfun_eq B2 (erase_vis x B3) /\ Visfun_weak B1 B3).
Proof.
  move=> B1 B2 x Hfx HBx Hweak.
  pose B3 := trans_close_vis_trans (union_vis B1 B2).
  have HB1B2 : forall a b, (a<> (names x)) -> (b<> x) -> B1 a b -> B2 a b.
  move=> a b Hax Hbx HBab. apply Hweak. split;[split|]; try assumption. intro. symmetry in H. by inversion H.
  have HB2B1B2 : forall a b c d, B2 a b -> B1 (names b) c -> B2 (names c) d -> B2 a d.
  move=> a b c d HBab HBbc HBcd. apply (B_trans B2 _ b); try assumption. apply (B_trans B2 _ c); try assumption.
  apply HB1B2. intro. inversion H. rewrite H1 in HBab. destruct Hfx. by apply (H2 _ HBab). intro. rewrite H in HBcd.
  have Hd : dom B2 (names x) by apply (_dom B2 _ _ HBcd). by destruct Hfx. assumption.
  have HB1B2B1 : forall a b c d, B1 a b -> B2 (names b) c -> B1 (names c) d -> (B1 a d \/ (B1 a b /\ B2 (names b) d) \/ (B2 a c /\ B1 (names c) d)).
  move=> a b c d HB1 HB2 HB3. case (Names_cur_dec a (names x))=> Hax. case (Names_dec d x)=> Hdx.
  left. rewrite Hax. by rewrite Hdx.
  right;left. split. assumption. apply (B_trans B2 _ c). assumption. apply HB1B2; try assumption. intro.
  inversion H. rewrite H1 in HB2. destruct Hfx. by apply (H2 _ HB2).
  repeat right. split. apply (B_trans B2 _ b); try assumption. apply HB1B2; try assumption. intro.
  rewrite H in HB2. have Hd : dom B2 (names x) by apply (_dom _ _ _ HB2). by destruct Hfx. assumption.
  have HB3notx : forall a b, trans_close_vis (union_vis B1 B2) a b -> B1 a b \/ B2 a b \/ (exists t, B2 a t /\ B1 (names t) b) \/ (exists t, B1 a t /\ B2 (names t) b).
  move=> a b Htr. induction Htr.
  destruct H. by left. right; by left. destruct IHHtr1 as [ HB1 | [HB1 | [[t [HB1 HB2]] | [t [HB1 HB2]]]]]; destruct IHHtr2 as [ HB3 | [HB3 | [[u [HB3 HB4]] | [u [HB3 HB4]]]]].
  left. by apply (B_trans B1 _ y). repeat right. exists y. by split.
  destruct (HB1B2B1 _ _ _ _ HB1 HB3 HB4). by left. destruct H0. repeat right. exists y. assumption. right;right;left.
  exists u. assumption.
  repeat right. exists u. split. by apply (B_trans B1 _ y). assumption.
  right;right;left. exists y. by split. right;left. by apply (B_trans B2 _ y).
  right;right;left. exists u. split. by apply (B_trans B2 _ y). assumption.
  right;left. by apply (HB2B1B2 x0 y u z).
  right;right;left. exists t. split. assumption. by apply (B_trans B1 _ y).
  right;left. by apply (HB2B1B2 x0 t y z).
  right;right;left. exists u. split. by apply (HB2B1B2 x0 t y u). assumption.
  right;left. apply (HB2B1B2 x0 t u z); try assumption. by apply (B_trans B1 _ y).
  destruct (HB1B2B1 _ _ _ _ HB1 HB2 HB3). by left. destruct H0. repeat right. exists t. assumption. right;right;left.
  exists y. assumption.
  repeat right. exists t. split. assumption. by apply (B_trans B2 _ y).
  have HB5 : B2 (names t) u by apply (B_trans B2 _ y). destruct (HB1B2B1 _ _ _ _ HB1 HB5 HB4).
  by left. destruct H0. repeat right. exists t. assumption. right;right;left. exists u. assumption.
  repeat right. exists t. split;[assumption|]. apply (HB2B1B2 _ _ _ _ HB2 HB3 HB4).
  
  exists B3. split;split.
  - apply (dom_eq_trans _ (dom (trans_minus_vis x B1)) _ ). apply dom_eq_sym. by apply Hweak.
    split => Hd. destruct Hd. split; [|assumption]. by left.
    destruct Hd. split;[|assumption]. destruct H. assumption. have Hdt : dom (trans_minus_vis x B1) x0.
    by apply Hweak. by destruct Hdt.
  - move=> y z; split=> HByz.
    split. split. apply in_B. by right. intro. rewrite H in HByz. destruct Hfx.
    by apply (H1 _ HByz). intro. rewrite (eq_sym H) in HByz. have Hdx : dom B2 (names x) by apply (_dom B2 _ _ HByz). by destruct Hfx. 
    destruct HByz. destruct H. destruct (HB3notx y z H).
    apply HB1B2. by symmetry. assumption. assumption.
    destruct H2. assumption. destruct H2.
    destruct H2. destruct H2. apply (B_trans B2 _ x0). assumption. apply HB1B2. 
    intro. inversion H4. rewrite H6 in H2. destruct Hfx. by apply (H7 _ H2). assumption. assumption.
    destruct H2. destruct H2. apply (B_trans B2 _ x0). apply HB1B2. by symmetry.
    intro. rewrite H4 in H3. have Hd : dom B2 (names x) by apply (_dom B2 _ _ H3). by destruct Hfx.
    assumption. assumption.
  - split=> Hd. by left. destruct Hd. assumption. have Hd : dom (erase_vis x B1) x0 by apply Hweak.
    by destruct Hd.
  - move=> y z HByz. apply in_B. by left.
Qed.

Lemma Visfun_weak_trans_minus : forall (B1 B2 : Visfun_trans) (x : Names), is_fresh_vis x B2 -> dom B1 (names x) -> Visfun_weak (erase_vis x B1) B2 -> (exists (B3 : Visfun_trans), Visfun_eq B2 (erase_vis x B3) /\ Visfun_weak B1 B3).
  move=> B1 B2 x Hfx HdBx Hweak. pose B4 := add_rec_trans x B1 HdBx.
  have Hdeq : dom_eq (dom B1) (dom B4).
  split=> Hd. by left. destruct Hd. assumption. by rewrite (eq_sym H).
  have HB41 : Visfun_eq (erase_vis x B1) (erase_vis x B4).
  split. split=> Hd. destruct Hd. split. by apply Hdeq.  assumption. destruct Hd. destruct H.
  by split. easy. move=> y z; split=> Her. destruct Her. destruct H. split; try assumption. split; try assumption.
  by left. destruct Her. destruct H. destruct H. split; try assumption. split; try assumption. destruct H.
  easy.
  have Hweak2 : Visfun_weak (erase_vis x B4) B2. apply (Visfun_weak_eq (erase_vis x B1) _ B2). assumption. apply Visfun_eq_refl. assumption.
  have HBxx : B4 (names x) x. right. split. by right. reflexivity.
  destruct (Visfun_weak_trans_minus_rec B4 B2 x Hfx HBxx Hweak2) as [B3 [Heq Hweak3]].
  exists B3. split. assumption. apply (Visfun_weak_trans _ B4). 2 : assumption.
  split. assumption. move=> y z HByz. by left.
Qed.

Lemma Visfun_weak_trans_minus_bis : forall (B1 B2 : Visfun_trans) (x : Names), is_fresh_vis x B2 -> ~ dom B1 (names x) -> Visfun_weak (erase_vis x B1) B2 -> (exists (B3 : Visfun_trans), Visfun_eq B2 (erase_vis x B3) /\ Visfun_weak B1 B3).
Proof.
  move=> B1 B2 x Hfx Hdx Hweak.
  pose B3 := trans_close_vis_trans (union_vis B1 B2).
  have HB1B2_incl : forall a b, (b<> x) -> B1 a b -> B2 a b.
  move=> a b Hbx HBab. apply Hweak. split;[split|]; try assumption. intro. rewrite (eq_sym H) in HBab.
  by apply (Hdx (_dom B1 _ _ HBab)).
  have HB1B2 : forall a b c, B1 a b -> B2 (names b) c -> B2 a c.
  move=> a b c HBab HBbc. apply (B_trans B2 _ b); try assumption. apply Hweak. split. split. assumption.
  intro. rewrite H in HBbc. destruct Hfx. by apply (H0 (_dom B2 _ _ HBbc)). intro. rewrite (eq_sym H) in HBab.
  by apply (Hdx (_dom _ _ _ HBab)).
  have HB3notx : forall a b, trans_close_vis (union_vis B1 B2) a b -> B1 a b \/ B2 a b \/ (exists t, B2 a t /\ B1 (names t) b).
  move=> a b Htr. induction Htr. destruct H. by left. right; by left.
  destruct IHHtr1 as [HB1 | [HB1 | [t [ HB1 HB2]]]]; destruct IHHtr2 as [HB3 | [HB3 | [u [HB3 HB4]]]].
  left. by apply (B_trans B1 _ y). right;left. by apply (HB1B2 _ y). repeat right.
  exists u. split. by apply (HB1B2 _ y). assumption.
  repeat right. exists y. by split. right;left. by apply (B_trans B2 _ y).
  repeat right. exists u. split. by apply (B_trans B2 _ y). assumption.
  repeat right. exists t. split. assumption. by apply (B_trans B1 _ y).
  right;left. apply (B_trans B2 _ t). assumption. by apply (HB1B2 _ y).
  repeat right. exists u. split. apply (B_trans B2 _ t). assumption. by apply (HB1B2 _ y). assumption.
  exists B3. split.
  split.
  -  split=> Hd. split. by right. have Her : dom (erase_vis x B1) x0 by apply Hweak. by destruct Her.
     destruct Hd. destruct H. apply Hweak. by split. assumption.
  - move=> a b; split=> HB.
    + split. split. apply in_B. by right. intro. rewrite H in HB. destruct Hfx. by apply (H1 _ HB).
      intro. rewrite (eq_sym H) in HB. have Hd: dom B2 (names x) by apply  (_dom B2 _ _ HB). by destruct Hfx.
    + destruct HB. destruct H. destruct (HB3notx a b H).
      apply Hweak. split. split. 1-3 : assumption. destruct H2. assumption.
      destruct H2 as [t [HBat HBtb]]. apply (B_trans B2 _ t). assumption. by apply HB1B2_incl.
  - split.
    + split=> Hd. by left. destruct Hd. assumption. have Hd : dom (erase_vis x B1) x0 by apply Hweak.
      by destruct Hd.
    + move=> a b HBab. apply in_B. by left.
Qed.

Lemma Visfun_weak_union_trans : forall (B1 B2 B3 : Visfun_trans), dom_disj (dom B1) (dom B2) -> Visfun_weak (union_vis B1 B2) B3 -> (exists (B4 B5 : Visfun_trans), Visfun_eq B3 (union_vis B4 B5) /\ Visfun_weak B1 B4 /\ Visfun_weak B2 B5).

  move=> B1 B2 B3 Hdisj Hweak. destruct (Visfun_weak_union B1 B2 B3 Hweak) as [B4_ [B5_ [Heq [Hweak1 Hweak2]]]].
  have Htr : is_trans B4_ /\ is_trans B5_. apply (union_trans B3). assumption. apply B_trans.
  move=> x Hdx4 Hdx5. have Hdx1 : dom B1 x by apply Hweak1. have Hdx2 : dom B2 x by apply Hweak2. apply (Hdisj x Hdx1 Hdx2).
  pose B4 := {| Btr := B4_; B_trans := proj1 Htr;|}. pose B5 := {|Btr := B5_; B_trans := proj2 Htr;|}.
  exists B4,B5. easy.
Qed.

