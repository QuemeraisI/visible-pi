Require Import ssreflect. 
Require Import ZArith Psatz. 
Require Export Names.

Inductive proc : Type :=
| proc_nil : proc
| proc_out_ser : Nser -> Nfo -> Nser -> Ncon -> proc -> proc (* a^ <n>(b,p).P *)
| proc_out_con : Ncon -> Nfo -> Nser -> proc -> proc (* p^ <n>(b).P *)
| proc_out_loc : Nref -> Nfo -> proc (* h^ <n> *)
| proc_in_ser : Nser -> Nfo -> Nser -> Ncon -> proc -> proc (* !a(n,b,p).P *)
| proc_in_con : Ncon -> Nfo -> Nser -> proc -> proc (* p(n,b).Q *)
| proc_in_loc : Nref -> Nfo -> proc -> proc (* h(n).P *)
| proc_nu_ser : Nser -> proc -> proc (* nu a P *)
| proc_nu_con : Ncon -> proc -> proc (* nu p P *)
| proc_nu_loc : Nref -> proc -> proc (* nu h P *)
| proc_par : proc -> proc -> proc (* P|Q *)
| proc_ite : Nfo -> Nfo -> proc -> proc -> proc (* if x=n then P else Q *)
| proc_fwd_ser : Nser -> Nser -> proc (* a ▷ b *)
| proc_fwd_con : Ncon -> Ncon -> proc (* p ▷ q *)
.

Inductive notin_proc_con : Ncon -> proc -> Prop :=
| con_notin_proc_nil : forall (p : Ncon), notin_proc_con p proc_nil
| con_notin_proc_out_ser : forall (p : Ncon), forall (a b : Nser) (n : Nfo) (q : Ncon) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_out_ser a n b q P)
| con_notin_proc_out_con : forall (p : Ncon), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_out_con q n b P)
| con_notin_proc_out_loc : forall (p : Ncon), forall (h : Nref) (n : Nfo), notin_proc_con p (proc_out_loc h n)
| con_notin_proc_in_ser : forall (p : Ncon), forall (a b : Nser) (n : Nfo) (q : Ncon) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_in_ser a n b q P)
| con_notin_proc_in_con : forall (p : Ncon), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_in_con q n b P)
| con_notin_proc_in_loc : forall (p : Ncon), forall (h : Nref) (n : Nfo) (P : proc),
    notin_proc_con p P -> notin_proc_con p (proc_in_loc h n P)
| con_notin_proc_nu_ser : forall (p : Ncon), forall (a : Nser) (P : proc),
    notin_proc_con p P -> notin_proc_con p (proc_nu_ser a P)
| con_notin_proc_nu_con : forall (p : Ncon), forall (q : Ncon) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_nu_con q P)
| con_notin_proc_nu_loc : forall (p : Ncon),forall (h : Nref) (P : proc),
    notin_proc_con p P -> notin_proc_con p (proc_nu_loc h P)
| con_notin_proc_par : forall (p : Ncon), forall (P1 P2 : proc),
    notin_proc_con p P1 -> notin_proc_con p P2 -> notin_proc_con p (proc_par P1 P2)
| con_notin_proc_ite : forall (p : Ncon), forall (x n : Nfo) (P1 P2 : proc),
    notin_proc_con p P1 -> notin_proc_con p P2 -> notin_proc_con p (proc_ite x n P1 P2)
| con_notin_proc_fwd_ser : forall (p : Ncon), forall (a b : Nser), notin_proc_con p (proc_fwd_ser a b)
| con_notin_proc_fwd_con : forall (p : Ncon), forall (q r : Ncon),
    (q <> p) -> (r <> p) -> notin_proc_con p (proc_fwd_con q r).

Inductive notfree_con : Ncon -> proc -> Prop :=
| con_notfree_nil : forall (r : Ncon), notfree_con r proc_nil
| con_notfree_out_ser : forall (a b : Nser) (n : Nfo ) (p r : Ncon) (P : proc),
    ( r = p \/ notfree_con r P) -> notfree_con r (proc_out_ser a n b p P)
| con_notfree_out_con : forall (b : Nser) (n : Nfo) (p r : Ncon) (P : proc),
    p <> r -> notfree_con r P -> notfree_con r (proc_out_con p n b P)
| con_notfree_out_loc : forall (r : Ncon) (h : Nref) (n : Nfo), notfree_con r (proc_out_loc h n)
| con_notfree_in_ser : forall (a b : Nser) (n : Nfo ) (p r : Ncon) (P : proc),
    ( r = p \/ notfree_con r P) -> notfree_con r (proc_in_ser a n b p P)
| con_notfree_in_con : forall (b : Nser) (n : Nfo) (p r : Ncon) (P : proc),
    p <> r -> notfree_con r P -> notfree_con r (proc_in_con p n b P)
| con_notfree_in_loc : forall (r : Ncon) (h : Nref) (n : Nfo) (P : proc),
    notfree_con r P -> notfree_con r (proc_in_loc h n P)
| con_notfree_nu_ser : forall (a : Nser) (r : Ncon)  (P : proc),
    notfree_con r P -> notfree_con r (proc_nu_ser a P)
| con_notfree_nu_con : forall (p r : Ncon) (P : proc),
    (r = p \/ notfree_con r P) -> notfree_con r (proc_nu_con p P)
| con_notfree_nu_loc : forall (r : Ncon) (h : Nref) (P : proc),
    notfree_con r P -> notfree_con r (proc_nu_loc h P)
| con_notfree_par : forall (r : Ncon) (P1 P2 : proc),
    notfree_con r P1 -> notfree_con r P2 -> notfree_con r (proc_par P1 P2)
| con_notfree_ite : forall (r : Ncon) (x n : Nfo) (P1 P2 : proc),
    notfree_con r P1 -> notfree_con r P2 -> notfree_con r (proc_ite x n P1 P2)
| con_notfree_fwd_ser : forall (a b : Nser) (r : Ncon), notfree_con r (proc_fwd_ser a b)
| con_notfree_fwd_con : forall (p q r : Ncon), p <> r -> q <> r -> notfree_con r (proc_fwd_con p q)
.


Inductive notin_proc_ser : Nser -> proc -> Prop :=
| ser_notin_proc_nil : forall (a : Nser), notin_proc_ser a proc_nil
| ser_notin_proc_out_ser : forall (a : Nser), forall (b c : Nser) (n : Nfo) (p : Ncon) (P : proc),
    (b <> a) -> (c <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_out_ser b n c p P)
| ser_notin_proc_out_con : forall (a : Nser), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (b <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_out_con q n b P)
| ser_notin_proc_out_loc : forall (a : Nser), forall (h : Nref) (n : Nfo), notin_proc_ser a (proc_out_loc h n)
| ser_notin_proc_in_ser : forall (a : Nser), forall (b c : Nser) (n : Nfo) (q : Ncon) (P : proc),
    (b <> a) -> (c <> a) ->  notin_proc_ser a P -> notin_proc_ser a (proc_in_ser b n c q P)
| ser_notin_proc_in_con : forall (a : Nser), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (b <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_in_con q n b P)
| ser_notin_proc_in_loc : forall (a : Nser), forall (h : Nref) (n : Nfo) (P : proc),
    notin_proc_ser a P -> notin_proc_ser a (proc_in_loc h n P)
| ser_notin_proc_nu_ser : forall (a : Nser), forall (b : Nser) (P : proc),
    (b <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_nu_ser b P)
| ser_notin_proc_nu_con : forall (a : Nser), forall (q : Ncon) (P : proc),
    notin_proc_ser a P -> notin_proc_ser a (proc_nu_con q P)
| ser_notin_proc_nu_loc : forall (a : Nser),forall (h : Nref) (P : proc),
    notin_proc_ser a P -> notin_proc_ser a (proc_nu_loc h P)
| ser_notin_proc_par : forall (a : Nser), forall (P1 P2 : proc),
    notin_proc_ser a P1 -> notin_proc_ser a P2 -> notin_proc_ser a (proc_par P1 P2)
| ser_notin_proc_ite : forall (a : Nser), forall (x n : Nfo) (P1 P2 : proc),
    notin_proc_ser a P1 -> notin_proc_ser a P2 -> notin_proc_ser a (proc_ite x n P1 P2)
| ser_notin_proc_fwd_ser : forall (a : Nser), forall (b c : Nser),
    (b <> a) -> (c <> a) -> notin_proc_ser a (proc_fwd_ser b c)
| ser_notin_proc_fwd_con : forall (a : Nser), forall (q r : Ncon), notin_proc_ser a (proc_fwd_con q r).

Inductive notfree_ser : Nser -> proc -> Prop :=
| ser_notfree_nil : forall (c : Nser), notfree_ser c proc_nil
| ser_notfree_out_ser : forall (a b c: Nser) (n : Nfo ) (p : Ncon) (P : proc),
    c <> a -> ( c= b \/ notfree_ser c P) -> notfree_ser c (proc_out_ser a n b p P)
| ser_notfree_out_con : forall (b c : Nser) (n : Nfo) (p : Ncon) (P : proc),
    (c = b \/ notfree_ser c P) -> notfree_ser c (proc_out_con p n b P)
| ser_notfree_out_loc : forall (c : Nser) (h : Nref) (n : Nfo), notfree_ser c (proc_out_loc h n)
| ser_notfree_in_ser : forall (a b c: Nser) (n : Nfo ) (p : Ncon) (P : proc),
    c <> a -> ( c= b \/ notfree_ser c P) -> notfree_ser c (proc_in_ser a n b p P)
| ser_notfree_in_con : forall (b c : Nser) (n : Nfo) (p : Ncon) (P : proc),
    (c = b \/ notfree_ser c P) -> notfree_ser c (proc_in_con p n b P)
| ser_notfree_in_loc : forall (c : Nser) (h : Nref) (n : Nfo) (P : proc),
    notfree_ser c P -> notfree_ser c (proc_in_loc h n P)
| ser_notfree_nu_ser : forall (a c : Nser) (P : proc),
    (c = a \/ notfree_ser c P) -> notfree_ser c (proc_nu_ser a P)
| ser_notfree_nu_con : forall (c : Nser) (p : Ncon) (P : proc),
    notfree_ser c P -> notfree_ser c (proc_nu_con p P)
| ser_notfree_nu_loc : forall (c : Nser) (h : Nref) (P : proc),
    notfree_ser c P -> notfree_ser c (proc_nu_loc h P)
| ser_notfree_par : forall (c : Nser) (P1 P2 : proc),
    notfree_ser c P1 -> notfree_ser c P2 -> notfree_ser c (proc_par P1 P2)
| ser_notfree_ite : forall (c : Nser) (x n : Nfo) (P1 P2 : proc),
    notfree_ser c P1 -> notfree_ser c P2 -> notfree_ser c (proc_ite x n P1 P2)
| ser_notfree_fwd_ser : forall (a b c : Nser), a <> c -> b <> c -> notfree_ser c (proc_fwd_ser a b)
| ser_notfree_fwd_con : forall (c : Nser) (p q : Ncon), notfree_ser c (proc_fwd_con p q)
.

Inductive in_toplevel_proc : Names -> proc -> Prop :=
| in_ser_toplevel : forall (x : Names) (a b : Nser) (n : Nfo) (p : Ncon) (P : proc), (x= (srv a)) -> in_toplevel_proc x (proc_in_ser a n b p P)
| in_con_toplevel : forall (x : Names) (p : Ncon) (n : Nfo) (b : Nser) (P : proc), (x = (cnt p)) -> in_toplevel_proc x (proc_in_con p n b P)
| nu_ser_toplevel : forall (x : Names) (a : Nser) (P : proc), (x <> (srv a)) -> in_toplevel_proc x P -> in_toplevel_proc x (proc_nu_ser a P)
| nu_con_toplevel : forall (x : Names) (p : Ncon) (P : proc), (x <> (cnt p)) -> in_toplevel_proc x P -> in_toplevel_proc x (proc_nu_con p P)
| nu_loc_toplevel : forall (x : Names) (h : Nref) (P : proc), in_toplevel_proc x P -> in_toplevel_proc x (proc_nu_loc h P)
| par_toplevel1 : forall (x : Names) (P Q : proc), in_toplevel_proc x P -> in_toplevel_proc x (proc_par P Q)
| par_toplevel2 : forall (x : Names) (P Q : proc), in_toplevel_proc x Q -> in_toplevel_proc x (proc_par P Q)
| ite_toplevel : forall (x : Names) (y n : Nfo) (P Q : proc), in_toplevel_proc x P -> in_toplevel_proc x Q -> in_toplevel_proc x (proc_ite y n P Q)
| fwd_ser_toplevel : forall (x : Names) (a b : Nser), (x = (srv a)) -> in_toplevel_proc x (proc_fwd_ser a b)
| fwd_con_toplevel : forall (x : Names) (p q : Ncon), (x = (cnt p)) -> in_toplevel_proc x (proc_fwd_con p q).

Lemma proc_fin_con : forall ( P : proc), exists (n : nat), forall (k : nat), (k >= n) -> notin_proc_con (give_ncon k) P.
Proof.
  induction P as [ | a n b p P | p n b P | h n | a n b p P | p n b P | h n P | a P | p P | h P | Q | x n Q | a b | p q].
  - exists 0. intros k Hk. apply con_notin_proc_nil.
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_out_ser. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_out_ser.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_out_con. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_out_con.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - exists 0. intros k Hk. apply con_notin_proc_out_loc.
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_in_ser. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_in_ser.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_in_con. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_in_con.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply con_notin_proc_in_loc. apply (HP k Hk).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply con_notin_proc_nu_ser. apply (HP k Hk).
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_nu_con. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp]. exists (max nP (np +1)). intros k Hk.
      apply con_notin_proc_nu_con. intro. rewrite H in Hp. have Hnpk : np=k by apply infinite_ncon. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply con_notin_proc_nu_loc. apply (HP k Hk).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply con_notin_proc_par.
    + have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    + have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply con_notin_proc_ite.
    + have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    + have HknP : k >= nP by lia. apply (HP k HknP).
  - exists 0. intros k Hk. apply con_notin_proc_fwd_ser.
  - destruct (given_ncon p).
    + destruct (given_ncon q).
      * exists 0. intros k Hk. apply con_notin_proc_fwd_con. intro Hp. apply (H k (eq_sym Hp)).
        intro Hq. apply (H0 k (eq_sym Hq)).
      * destruct H0 as [nq Hq]. exists (nq +1). intros k Hk. apply con_notin_proc_fwd_con.
        intro Hp. apply (H k (eq_sym Hp)).
        intro Hq1. rewrite Hq1 in Hq. have Hnqk : nq =k by apply infinite_ncon. lia.
    + destruct H as [np Hp]. destruct (given_ncon q).
      * exists (np +1). intros k Hk. apply con_notin_proc_fwd_con.
        intro Hp1. rewrite Hp1 in Hp. have Hnpk : np =k by apply infinite_ncon. lia.
        intro Hq. apply (H k (eq_sym Hq)).
      * destruct H as [nq Hq]. exists (max (np +1) (nq +1)). intros k Hk. apply con_notin_proc_fwd_con.
        intro Hp1. rewrite Hp1 in Hp. have Hnpk : np =k by apply infinite_ncon. lia.
        intro Hq1. rewrite Hq1 in Hq. have Hnqk : nq =k by apply infinite_ncon. lia.
Qed.

Lemma proc_fin_ser : forall ( P : proc), exists (n : nat), forall (k : nat), (k >= n) -> notin_proc_ser (give_nser k) P.
Proof.
  induction P as [ | a n b p P | p n b P | h n | a n b p P | p n b P | h n P | a P | p P | h P | Q | x n Q | a b | p q].
  - exists 0. intros k Hk. apply ser_notin_proc_nil.
  - destruct IHP as [nP HP]. destruct (given_nser a).
    + destruct (given_nser b).
      * exists nP. intros k Hk. apply ser_notin_proc_out_ser. intro Ha. apply (H k (eq_sym Ha)).
        intro Hb. apply (H0 k (eq_sym Hb)).
        apply (HP k Hk).
      * destruct H0 as [nb Hb]. exists (max nP (nb+1)).  intros k Hk. apply ser_notin_proc_out_ser.
        intro Ha. apply (H k (eq_sym Ha)).
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
    + destruct H as [na Ha]. destruct (given_nser b).
      * exists (max nP (na +1)). intros k Hk. apply ser_notin_proc_out_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hb. apply (H k (eq_sym Hb)).
        have HknP : k >= nP by lia. apply (HP k HknP).
      * destruct H as [nb Hb]. exists (max nP (max (na +1) (nb +1))). intros k Hk. apply ser_notin_proc_out_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_nser b).
    + exists nP. intros k Hk. apply ser_notin_proc_out_con.
      intro Hb. apply (H k (eq_sym Hb)).
      apply (HP k Hk).
    + destruct H as [nb Hb]. exists (max nP (nb +1)). intros k Hk. apply ser_notin_proc_out_con.
      intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - exists 0. intros k Hk. apply ser_notin_proc_out_loc.
  - destruct IHP as [nP HP]. destruct (given_nser a).
    + destruct (given_nser b).
      * exists nP. intros k Hk. apply ser_notin_proc_in_ser. intro Ha. apply (H k (eq_sym Ha)).
        intro Hb. apply (H0 k (eq_sym Hb)).
        apply (HP k Hk).
      * destruct H0 as [nb Hb]. exists (max nP (nb+1)).  intros k Hk. apply ser_notin_proc_in_ser.
        intro Ha. apply (H k (eq_sym Ha)).
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
    + destruct H as [na Ha]. destruct (given_nser b).
      * exists (max nP (na +1)). intros k Hk. apply ser_notin_proc_in_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hb. apply (H k (eq_sym Hb)).
        have HknP : k >= nP by lia. apply (HP k HknP).
      * destruct H as [nb Hb]. exists (max nP (max (na +1) (nb +1))). intros k Hk. apply ser_notin_proc_in_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_nser b).
    + exists nP. intros k Hk. apply ser_notin_proc_in_con.
      intro Hb. apply (H k (eq_sym Hb)).
      apply (HP k Hk).
    + destruct H as [nb Hb]. exists (max nP (nb +1)). intros k Hk. apply ser_notin_proc_in_con.
      intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply ser_notin_proc_in_loc. apply (HP k Hk).
  - destruct IHP as [nP HP]. destruct (given_nser a).
    + exists nP. intros k Hk. apply ser_notin_proc_nu_ser. intro Ha. apply (H k (eq_sym Ha)).
      apply (HP k Hk).
    + destruct H as [na Ha]. exists (max nP (na +1)). intros k Hk. apply ser_notin_proc_nu_ser.
      intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply ser_notin_proc_nu_con. apply (HP k Hk).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply ser_notin_proc_nu_loc. apply (HP k Hk).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply ser_notin_proc_par. have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply ser_notin_proc_ite. have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct (given_nser a).
    + destruct (given_nser b).
      * exists 0. intros k Hk. apply ser_notin_proc_fwd_ser. intro Ha. apply (H k (eq_sym Ha)). intro Hb. apply (H0 k (eq_sym Hb)).
      * destruct H0 as [nb Hb]. exists (nb +1).
        intros k Hk. apply ser_notin_proc_fwd_ser. intro Ha. apply (H k (eq_sym Ha)). intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
    + destruct H as [na Ha]. destruct (given_nser b).
      * exists (na +1). intros k Hk. apply ser_notin_proc_fwd_ser. intro Hak. rewrite Hak in Ha.
        have Hnak : na = k by apply infinite_nser. lia. intro Hb. apply (H k (eq_sym Hb)).
      * destruct H as [nb Hb]. exists (max (na+1) (nb+1)). intros k Hk. apply ser_notin_proc_fwd_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
  - exists 0. intros k Hk. apply ser_notin_proc_fwd_con.
Qed.
        
Inductive alpha_convert_proc_con :  Ncon -> Ncon -> proc -> proc -> Prop :=
| con_alpha_proc_nil : forall (p q : Ncon), alpha_convert_proc_con p q proc_nil proc_nil
| con_alpha_proc_out_ser_replace : forall (p q : Ncon), forall (a : Nser) (n : Nfo) (b : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_ser a n b p P) (proc_out_ser a n b q Q)
| con_alpha_proc_out_ser_leave : forall (p q : Ncon), forall (a : Nser) (n : Nfo) (b : Nser) (r : Ncon) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_ser a n b r P) (proc_out_ser a n b r Q)
| con_alpha_proc_out_con_replace : forall (p q : Ncon), forall (n : Nfo) (b : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_con p n b P) (proc_out_con q n b Q)
| con_alpha_proc_out_con_leave : forall (p q : Ncon), forall (r : Ncon) (n : Nfo) (b : Nser) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_con r n b P) (proc_out_con r n b Q)
| con_alpha_proc_out_loc : forall (p q : Ncon), forall (h : Nref) (n : Nfo), alpha_convert_proc_con p q (proc_out_loc h n) (proc_out_loc h n)
| con_alpha_proc_in_ser_replace : forall (p q : Ncon), forall (a b : Nser) (n : Nfo) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_ser a n b p P) (proc_in_ser a n b q Q)
| con_alpha_proc_in_ser_leave : forall (p q : Ncon), forall (a b : Nser) (n : Nfo) (r : Ncon) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_ser a n b r P) (proc_in_ser a n b r Q)
| con_alpha_proc_in_con_replace : forall (p q : Ncon), forall (n : Nfo) (b : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_con p n b P) (proc_in_con q n b Q)
| con_alpha_proc_in_con_leave : forall (p q : Ncon), forall (r : Ncon) (n : Nfo) (b : Nser) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_con r n b P) (proc_in_con r n b Q)
| con_alpha_proc_in_loc : forall (p q : Ncon), forall (h : Nref) (n : Nfo) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_loc h n P) (proc_in_loc h n Q)
| con_alpha_proc_nu_ser : forall (p q : Ncon), forall (a : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_ser a P) (proc_nu_ser a Q)
| con_alpha_proc_nu_con_replace : forall (p q : Ncon), forall (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_con p P) (proc_nu_con q Q)
| con_alpha_proc_nu_con_leave : forall (p q : Ncon), forall (r : Ncon) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_con r P) (proc_nu_con r Q)
| con_alpha_proc_nu_loc : forall (p q : Ncon), forall (h : Nref) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_loc h P) (proc_nu_loc h Q)
| con_alpha_proc_par : forall (p q : Ncon), forall (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_con p q P1 Q1 -> alpha_convert_proc_con p q P2 Q2 -> alpha_convert_proc_con p q (proc_par P1 P2) (proc_par Q1 Q2)
| con_alpha_proc_ite : forall (p q : Ncon), forall (x n : Nfo) (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_con p q P1 Q1 -> alpha_convert_proc_con p q P2 Q2 -> alpha_convert_proc_con p q (proc_ite x n P1 P2) (proc_ite x n Q1 Q2)
| con_alpha_proc_fwd_ser : forall (p q : Ncon), forall (a b : Nser),
    alpha_convert_proc_con p q (proc_fwd_ser a b) (proc_fwd_ser a b)
| con_alpha_proc_fwd_con_replace1 : forall (p q : Ncon),
    alpha_convert_proc_con p q (proc_fwd_con p p) (proc_fwd_con q q)
| con_alpha_proc_fwd_con_replace2 : forall (p q : Ncon), forall (r : Ncon),
    (r <> p) -> alpha_convert_proc_con p q (proc_fwd_con p r) (proc_fwd_con q r)
| con_alpha_proc_fwd_con_replace3 : forall (p q : Ncon), forall (r : Ncon),
    (r <> p) -> alpha_convert_proc_con p q (proc_fwd_con r p) (proc_fwd_con r q)
| con_alpha_proc_fwd_con_leave : forall (p q : Ncon), forall (r s : Ncon),
    (r <> p) -> (s <> p) -> alpha_convert_proc_con p q (proc_fwd_con r s) (proc_fwd_con r s).

Lemma exists_alpha_convert_proc_con : forall (p q : Ncon) (P : proc), exists (Q : proc), alpha_convert_proc_con p q P Q.
Proof.
  intros p q.
  induction P as [ | a n b r P | r n b P | h n | a n b r P | r n b P | h n P | a P | r P | h P | Q | x n Q | a b | r s].
  - exists proc_nil. apply con_alpha_proc_nil.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_out_ser a n b q Q). rewrite Hr. by apply con_alpha_proc_out_ser_replace.
    + intro Hr. exists (proc_out_ser a n b r Q). by apply con_alpha_proc_out_ser_leave.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_out_con q n b Q). rewrite Hr. by apply con_alpha_proc_out_con_replace.
    + intro Hr. exists (proc_out_con r n b Q). by apply con_alpha_proc_out_con_leave.
  - exists (proc_out_loc h n). by apply con_alpha_proc_out_loc.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_in_ser a n b q Q). rewrite Hr. by apply con_alpha_proc_in_ser_replace.
    + intro Hr. exists (proc_in_ser a n b r Q). by apply con_alpha_proc_in_ser_leave.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_in_con q n b Q). rewrite Hr. by apply con_alpha_proc_in_con_replace.
    + intro Hr. exists (proc_in_con r n b Q). by apply con_alpha_proc_in_con_leave.
  - destruct IHP as [Q HQ]. exists (proc_in_loc h n Q). by apply con_alpha_proc_in_loc.
  - destruct IHP as [Q HQ]. exists (proc_nu_ser a Q). by apply con_alpha_proc_nu_ser.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_nu_con q Q). rewrite Hr. by apply con_alpha_proc_nu_con_replace.
    + intro Hr. exists (proc_nu_con r Q). by apply con_alpha_proc_nu_con_leave.
  - destruct IHP as [Q HQ]. exists (proc_nu_loc h Q). by apply con_alpha_proc_nu_loc.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_par R1 R2). by apply con_alpha_proc_par.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_ite x n R1 R2). by apply con_alpha_proc_ite.
  - exists (proc_fwd_ser a b). by apply con_alpha_proc_fwd_ser.
  - case (Ncon_dec r p).
    + intro Hr. rewrite Hr. case (Ncon_dec s p).
      * intro Hs. rewrite Hs. exists (proc_fwd_con q q). by apply con_alpha_proc_fwd_con_replace1.
      * intro Hs. exists (proc_fwd_con q s). by apply con_alpha_proc_fwd_con_replace2.
    + intro Hr. case (Ncon_dec s p).
      * intros Hs. rewrite Hs. exists (proc_fwd_con r q). by apply con_alpha_proc_fwd_con_replace3.
      * intros Hs. exists (proc_fwd_con r s). by apply con_alpha_proc_fwd_con_leave.
Qed.

Inductive alpha_convert_proc_ser : Nser -> Nser -> proc -> proc -> Prop :=
| ser_alpha_proc_nil : forall (a b : Nser), alpha_convert_proc_ser a b proc_nil proc_nil
| ser_alpha_proc_out_ser_replace1 : forall (a b : Nser), forall (n : Nfo) (p : Ncon) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_out_ser a n a p P) (proc_out_ser b n b p Q)
| ser_alpha_proc_out_ser_replace2 : forall (a b : Nser), forall (n : Nfo) (c : Nser) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_out_ser a n c p P) (proc_out_ser b n c p Q)
| ser_alpha_proc_out_ser_replace3 : forall (a b : Nser), forall (c : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_out_ser c n a p P) (proc_out_ser c n b p Q)
| ser_alpha_proc_out_ser_leave : forall (a b : Nser), forall (c d : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> (d <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_out_ser c n d p P) (proc_out_ser c n d p Q)
| ser_alpha_proc_out_con_replace : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_out_con p n a P) (proc_out_con p n b Q)
| ser_alpha_proc_out_con_leave : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (c : Nser) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
                alpha_convert_proc_ser a b (proc_out_con p n c P) (proc_out_con p n c Q)
| ser_alpha_proc_out_loc : forall (a b : Nser), forall (h : Nref) (n : Nfo), alpha_convert_proc_ser a b (proc_out_loc h n) (proc_out_loc h n)
| ser_alpha_proc_in_ser_replace1 : forall (a b : Nser), forall (n : Nfo) (p : Ncon) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_ser a n a p P) (proc_in_ser b n b p Q)
| ser_alpha_proc_in_ser_replace2 : forall (a b : Nser), forall (n : Nfo) (c : Nser) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_in_ser a n c p P) (proc_in_ser b n c p Q)
| ser_alpha_proc_in_ser_replace3 : forall (a b : Nser), forall (c : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_in_ser c n a p P) (proc_in_ser c n b p Q)
| ser_alpha_proc_in_ser_leave : forall (a b : Nser), forall (c d : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> (d <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_in_ser c n d p P) (proc_in_ser c n d p Q)
| ser_alpha_proc_in_con_replace : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_con p n a P) (proc_in_con p n b Q)
| ser_alpha_proc_in_con_leave : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (c : Nser) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_con p n c P) (proc_in_con p n c Q)
| ser_alpha_proc_in_loc : forall (a b : Nser), forall (h : Nref) (n : Nfo) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_loc h n P) (proc_in_loc h n Q)
| ser_alpha_proc_nu_ser_replace : forall (a b : Nser), forall (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_ser a P) (proc_nu_ser b Q)
| ser_alpha_proc_nu_ser_leave : forall (a b : Nser), forall (c : Nser) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_ser c P) (proc_nu_ser c Q)
| ser_alpha_proc_nu_con : forall (a b : Nser), forall (p : Ncon) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_con p P) (proc_nu_con p Q)
| ser_alpha_proc_nu_loc : forall (a b : Nser), forall (h : Nref) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_loc h P) (proc_nu_loc h Q)
| ser_alpha_proc_par : forall (a b : Nser), forall (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_ser a b P1 Q1 -> alpha_convert_proc_ser a b P2 Q2 -> alpha_convert_proc_ser a b (proc_par P1 P2) (proc_par Q1 Q2)
| ser_alpha_proc_ite : forall (a b : Nser), forall (x n : Nfo) (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_ser a b P1 Q1 -> alpha_convert_proc_ser a b P2 Q2 -> alpha_convert_proc_ser a b (proc_ite x n P1 P2) (proc_ite x n Q1 Q2)
| ser_alpha_proc_fwd_ser_replace1 : forall (a b : Nser), alpha_convert_proc_ser a b (proc_fwd_ser a a) (proc_fwd_ser b b)
| ser_alpha_proc_fwd_ser_replace2 : forall (a b : Nser), forall (c: Nser),
    (c <> a) -> alpha_convert_proc_ser a b (proc_fwd_ser a c) (proc_fwd_ser b c)
| ser_alpha_proc_fwd_ser_replace3 : forall (a b : Nser), forall (c: Nser),
    (c <> a) -> alpha_convert_proc_ser a b (proc_fwd_ser c a) (proc_fwd_ser c b)
| ser_alpha_proc_fwd_ser_leave : forall (a b : Nser), forall (c d: Nser),
    (c <> a) -> (d <> a) -> alpha_convert_proc_ser a b (proc_fwd_ser c d) (proc_fwd_ser c d)
| ser_alpha_proc_fwd_con : forall (a b : Nser), forall (p q : Ncon), alpha_convert_proc_ser a b (proc_fwd_con p q) (proc_fwd_con p q).

Lemma exists_alpha_convert_proc_ser : forall (a b : Nser) (P : proc), exists (Q : proc), alpha_convert_proc_ser a b P Q.
Proof.
  intros a b.
  induction P as [ | c n d p P | p n d P | h n | c n d p P | p n d P | h n P | c P | p P | h P | Q | x n Q | c d | p q].
  - exists proc_nil. by apply ser_alpha_proc_nil.
  - destruct IHP as [Q HQ]. case (Nser_dec c a).
    + intro Hc. rewrite Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_out_ser b n b p Q). by apply ser_alpha_proc_out_ser_replace1.
      * intro Hd. exists (proc_out_ser b n d p Q). by apply ser_alpha_proc_out_ser_replace2.
    + intro Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_out_ser c n b p Q). by apply ser_alpha_proc_out_ser_replace3.        
      * intro Hd. exists (proc_out_ser c n d p Q). by apply ser_alpha_proc_out_ser_leave.
  - destruct IHP as [Q HQ]. case (Nser_dec d a).
    + intro Hd. rewrite Hd. exists (proc_out_con p n b Q). by apply ser_alpha_proc_out_con_replace.
    + intro Hd. exists (proc_out_con p n d Q). by apply ser_alpha_proc_out_con_leave.
  - exists (proc_out_loc h n). by apply ser_alpha_proc_out_loc.
  - destruct IHP as [Q HQ]. case (Nser_dec c a).
    + intro Hc. rewrite Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_in_ser b n b p Q). by apply ser_alpha_proc_in_ser_replace1.
      * intro Hd. exists (proc_in_ser b n d p Q). by apply ser_alpha_proc_in_ser_replace2.
    + intro Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_in_ser c n b p Q). by apply ser_alpha_proc_in_ser_replace3.        
      * intro Hd. exists (proc_in_ser c n d p Q). by apply ser_alpha_proc_in_ser_leave.
  - destruct IHP as [Q HQ]. case (Nser_dec d a).
    + intro Hd. rewrite Hd. exists (proc_in_con p n b Q). by apply ser_alpha_proc_in_con_replace.
    + intro Hd. exists (proc_in_con p n d Q). by apply ser_alpha_proc_in_con_leave.
  - destruct IHP as [Q HQ]. exists (proc_in_loc h n Q). by apply ser_alpha_proc_in_loc.
  - destruct IHP as [Q HQ]. case (Nser_dec c a).
    + intro Hc. rewrite Hc. exists (proc_nu_ser b Q). by apply ser_alpha_proc_nu_ser_replace.
    + intro Hc. exists (proc_nu_ser c Q). by apply ser_alpha_proc_nu_ser_leave.
  - destruct IHP as [Q HQ]. exists (proc_nu_con p Q). by apply ser_alpha_proc_nu_con.
  - destruct IHP as [Q HQ]. exists (proc_nu_loc h Q). by apply ser_alpha_proc_nu_loc.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_par R1 R2). by apply ser_alpha_proc_par.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_ite x n R1 R2). by apply ser_alpha_proc_ite.
  - case (Nser_dec c a).
    + intro Hc. rewrite Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_fwd_ser b b). by apply ser_alpha_proc_fwd_ser_replace1.
      * intro Hs. exists (proc_fwd_ser b d). by apply ser_alpha_proc_fwd_ser_replace2.
    + intro Hc. case (Nser_dec d a).
      * intros Hd. rewrite Hd. exists (proc_fwd_ser c b). by apply ser_alpha_proc_fwd_ser_replace3.
      * intros Hd. exists (proc_fwd_ser c d). by apply ser_alpha_proc_fwd_ser_leave.
  - exists (proc_fwd_con p q). by apply ser_alpha_proc_fwd_con.
Qed.

Lemma alpha_alpha_proc_ser : forall (a b c : Nser) (P Q R: proc),
    notin_proc_ser b P -> alpha_convert_proc_ser a b P Q
    -> alpha_convert_proc_ser b c Q R
    -> alpha_convert_proc_ser a c P R.
Proof.
  move=> a b c P Q R HbP Halph1. move: R. induction Halph1; move=> R Halph2.
  - inversion Halph2. apply ser_alpha_proc_nil.
  - inversion Halph2; try easy.
    apply ser_alpha_proc_out_ser_replace1. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    + rewrite H4 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_ser_replace2. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    + rewrite H0 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_ser_replace3. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H6 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_ser_leave; try assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy. apply ser_alpha_proc_out_con_replace. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H5 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_con_leave. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_out_loc.
  - inversion Halph2; try easy.  apply ser_alpha_proc_in_ser_replace1. apply IHHalph1. by inversion HbP.
    assumption.
  - inversion Halph2; try easy.
    + rewrite H4 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_ser_replace2. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    + rewrite H0 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_ser_replace3. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H6 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_ser_leave. assumption. assumption. apply IHHalph1. by inversion HbP.
      assumption.
  - inversion Halph2; try easy.
    apply ser_alpha_proc_in_con_replace. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H5 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_con_leave. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_in_loc. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    apply ser_alpha_proc_nu_ser_replace. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H0 in HbP. by inversion HbP.
    + apply ser_alpha_proc_nu_ser_leave. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_nu_con. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_nu_loc. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_par.
    apply IHHalph1_1. by inversion HbP. assumption. apply IHHalph1_2. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_ite. apply IHHalph1_1. by inversion HbP. assumption.
    apply IHHalph1_2. by inversion HbP. assumption.
  - inversion Halph2; try easy. apply ser_alpha_proc_fwd_ser_replace1.
  - inversion Halph2; try easy.
    + rewrite H3 in HbP. by inversion HbP.
    + apply ser_alpha_proc_fwd_ser_replace2. assumption.
  - inversion Halph2; try easy.
    + rewrite H3 in HbP. by inversion HbP.
    + apply ser_alpha_proc_fwd_ser_replace3. assumption.
  - inversion Halph2.
    + rewrite H4 in HbP. by inversion HbP.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H5 in HbP. by inversion HbP.
    + apply ser_alpha_proc_fwd_ser_leave; assumption.
  - inversion Halph2. apply ser_alpha_proc_fwd_con.
Qed.

Lemma alpha_proc_ser_idem (a b : Nser) (P Q : proc) : (a=b) -> alpha_convert_proc_ser a b P Q -> P = Q.
Proof.
  move=> Hab Halph. induction Halph; try rewrite Hab; try reflexivity; try have HPQ : P= Q by apply (IHHalph Hab). all : try by rewrite HPQ.
  all : have HPQ1 : P1= Q1 by apply (IHHalph1 Hab). all : rewrite HPQ1.
  all : have HPQ2 : P2 = Q2 by apply (IHHalph2 Hab). all: by rewrite HPQ2.
Qed.

Lemma alpha_proc_ser_invol (a b : Nser) (P Q R : proc) : notin_proc_ser b P -> alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser b a Q R -> P=R.
Proof.
  move=> HbP HPQ HQR. apply (alpha_proc_ser_idem a a). reflexivity. by apply (alpha_alpha_proc_ser a b a P Q R).
Qed.

Lemma alpha_alpha_proc_con : forall (p q r : Ncon) (P Q R: proc),
    notin_proc_con q P -> alpha_convert_proc_con p q P Q
    -> alpha_convert_proc_con q r Q R
    -> alpha_convert_proc_con p r P R.
Proof.
  move=> p q r P Q R HqP Halph1. move: R. induction Halph1; move=> R Halph2. all: inversion Halph2; try easy.
  all : inversion HqP.
  - apply con_alpha_proc_nil.
  - apply con_alpha_proc_out_ser_replace. by apply IHHalph1. 
  - by rewrite H6 in H12.
  - apply con_alpha_proc_out_ser_leave. assumption. by apply IHHalph1. 
  - apply con_alpha_proc_out_con_replace. by apply IHHalph1.
  - by rewrite H0 in H11.
  - apply con_alpha_proc_out_con_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_out_loc.
  - apply con_alpha_proc_in_ser_replace. by apply IHHalph1.
  - by rewrite H6 in H12.
  - apply con_alpha_proc_in_ser_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_in_con_replace. by apply IHHalph1.
  - by rewrite H0 in H11.
  - apply con_alpha_proc_in_con_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_in_loc.  by apply IHHalph1.
  - apply con_alpha_proc_nu_ser. by apply IHHalph1.
  - apply con_alpha_proc_nu_con_replace. by apply IHHalph1.
  - by rewrite H0 in H9.
  - apply con_alpha_proc_nu_con_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_nu_loc. by apply IHHalph1.
  - apply con_alpha_proc_par. by apply IHHalph1_1. by apply IHHalph1_2.
  - apply con_alpha_proc_ite. by apply IHHalph1_1. by apply IHHalph1_2.
  - apply con_alpha_proc_fwd_ser.
  - apply con_alpha_proc_fwd_con_replace1.
  - by rewrite H3 in H8.
  - by apply con_alpha_proc_fwd_con_replace2. 
  - by rewrite H3 in H7.
  - by apply con_alpha_proc_fwd_con_replace3.
  - by rewrite H4 in H9.
  - by rewrite H1 in H10.
  - by rewrite H5 in H11.
  - by apply con_alpha_proc_fwd_con_leave.
Qed.

Lemma alpha_proc_con_idem (p q : Ncon) (P Q : proc) : (p=q) -> alpha_convert_proc_con p q P Q -> P = Q.
Proof.
  move=> Hpq Halph. induction Halph; try rewrite Hpq; try reflexivity; try have HPQ : P= Q by apply (IHHalph Hpq). all : try by rewrite HPQ.
  all : have HPQ1 : P1= Q1 by apply (IHHalph1 Hpq). all : rewrite HPQ1.
  all : have HPQ2 : P2 = Q2 by apply (IHHalph2 Hpq). all: by rewrite HPQ2.
Qed.

Lemma alpha_proc_con_invol (p q : Ncon) (P Q R : proc) : notin_proc_con q P -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con q p Q R -> P=R.
Proof.
  move=> HqP HPQ HQR. apply (alpha_proc_con_idem p p). reflexivity. by apply (alpha_alpha_proc_con p q p P Q R).
Qed.

Lemma alpha_proc_ser_notfree_con (a b : Nser) (p : Ncon) (P Q : proc) : notfree_con p P -> alpha_convert_proc_ser a b P Q -> notfree_con p Q.
  move=> Hnfp Halph. induction Halph; inversion Hnfp.
  1 : apply con_notfree_nil. 1-4 : apply con_notfree_out_ser. 5,6 : apply con_notfree_out_con.
  10-13 : apply con_notfree_in_ser. 14-15 : apply con_notfree_in_con. 18 : apply con_notfree_in_loc.
  19-20 : apply con_notfree_nu_ser. 21 : apply con_notfree_nu_con. 22 : apply con_notfree_nu_loc.
  23 : apply con_notfree_par. 25 : apply con_notfree_ite. 27-30 : apply con_notfree_fwd_ser.
  27 : apply con_notfree_fwd_con.
  all : try assumption. 18,20 : by apply IHHalph1. 18-19 : by apply IHHalph2.
  1,7,16 : destruct H1.
  7,8,12,13 : destruct H2.
  15,18: destruct H3.
  all : try by left. all : try right. all : by apply IHHalph.
Qed.

Lemma alpha_proc_con_notfree_ser (p q : Ncon) (a : Nser) (P Q : proc) : notfree_ser a P -> alpha_convert_proc_con p q P Q -> notfree_ser a Q.
  move=> Hnfa Halph. induction Halph; inversion Hnfa.
  1 : apply ser_notfree_nil. 1,2 : apply ser_notfree_out_ser. 5,6 : apply ser_notfree_out_con.
  7 : apply ser_notfree_out_loc. 7,8 : apply ser_notfree_in_ser. 11,12 : apply ser_notfree_in_con.
  13 : apply ser_notfree_in_loc. 14 : apply ser_notfree_nu_ser. 15-16 : apply ser_notfree_nu_con.
  17 : apply ser_notfree_nu_loc. 18 : apply ser_notfree_par. 20 : apply ser_notfree_ite.
  22 : apply ser_notfree_fwd_ser. 24-27 : apply ser_notfree_fwd_con.
  all : try assumption. 14,16 : by apply IHHalph1. 14,15 : by apply IHHalph2.
  1,5 : destruct H6. 5,8 : destruct H7. 9,11,14 : destruct H1. 15,16 : destruct H2.
  all : try by left. all : try right. all : by apply IHHalph.
Qed.

(* We work with processes modulo alpha-conversion of bounded names *)
Axiom eq_proc_ser : forall (P Q : proc) (a b : Nser), notin_proc_ser b P -> notfree_ser a P -> alpha_convert_proc_ser a b P Q -> P = Q.

Axiom eq_proc_con : forall (P Q : proc) (p q : Ncon), notin_proc_con q P -> notfree_con p P -> alpha_convert_proc_con p q P Q -> P = Q.

Fixpoint size (P : proc) : nat := match P with
                                | proc_nil => 0
                                | proc_out_ser _ _ _ _ Q => 1 + (size Q)
                                | proc_out_con _ _ _ Q => 1 + (size Q)
                                | proc_out_loc _ _ => 1
                                | proc_in_ser _ _ _ _ Q => 1 + (size Q)
                                | proc_in_con _ _ _ Q => 1 + (size Q)
                                | proc_in_loc _ _  Q => 1 + (size Q)
                                | proc_nu_ser _ Q => 1 + (size Q)
                                | proc_nu_con _ Q => 1 + (size Q) 
                                | proc_nu_loc _ Q => 1 + (size Q)
                                | proc_par Q R => 1+ size Q + size R
                                | proc_ite _ _ Q R => 1+ size Q + size R
                                | proc_fwd_ser _ _ => 1
                                | proc_fwd_con _ _ => 1
                                end.

Lemma size_alpha_con : forall (P Q : proc) (p q : Ncon), alpha_convert_proc_con p q P Q -> size P = size Q.
  induction P=> Q p q Halph; inversion Halph; try easy.
  1,5 : rewrite (eq_sym H5) in H7. 4,7 : rewrite (eq_sym H) in H6. 11 : rewrite (eq_sym H) in H4.
  all : simpl; apply eq_S; try by apply (IHP _ p q).
  all: have HPQ1 : size P1 = size Q1 by apply (IHP1 _ p q).
  all : have HPQ2 : size P2 = size Q2 by apply (IHP2 _ p q).
  all: lia.
Qed.

Lemma size_alpha_ser : forall (P Q : proc) (a b : Nser), alpha_convert_proc_ser a b P Q -> size P = size Q.
  induction P=> Q a b Halph; inversion Halph; try easy.
  1,7 :rewrite H4 in H; rewrite (eq_sym H) in H7. 3,8 : rewrite (eq_sym H) in H8.
  5,9 : rewrite (eq_sym H4) in H8.  8,11 : rewrite (eq_sym H4) in H6.
  14 : rewrite (eq_sym H) in H4.
  all : simpl; apply eq_S; try by apply (IHP _ a b).
  all: have HPQ1 : size P1 = size Q1 by apply (IHP1 _ a b).
  all : have HPQ2 : size P2 = size Q2 by apply (IHP2 _ a b).
  all: lia.
Qed.
