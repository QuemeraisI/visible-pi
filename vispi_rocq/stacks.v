Require Import ssreflect.
Require Import ZArith Psatz.
Require Export Names.


Inductive stack_ : Type :=
| Out : stackO -> stack_
| In : stackI -> stack_
with stackO : Type :=
| stack_out : Ncon -> stackI -> stackO
with stackI : Type :=
| stack_in : Ncon -> stackO -> stackI
| nil : stackI.

Scheme stack_ind_ := Induction for stack_ Sort Prop
    with stackO_ind_ := Induction for stackO Sort Prop
                       with stackI_ind_ :=Induction for stackI Sort Prop.

Combined Scheme stack_ind from stack_ind_, stackI_ind_, stackO_ind_.

Inductive not_in_stack (p : Ncon) : stack_ -> Prop :=
| notin_nil : not_in_stack p (In nil)
| notin_out : forall (s : stackO), forall (q : Ncon), p <> q -> not_in_stack p (Out s) -> not_in_stack p (In (stack_in q s))
| notin_in : forall (s : stackI), forall (q : Ncon), p <> q -> not_in_stack p (In s) -> not_in_stack p (Out (stack_out q s)).

Inductive is_stack_p (p : Ncon) : stack_ -> Prop :=
| is_stack_p_nil : is_stack_p p (In nil)
| is_stack_popi : forall (s : stackO), not_in_stack p (Out s) -> is_stack_p p (Out (stack_out p (stack_in p s)))
| is_stack_po : forall (s: stackI), not_in_stack p (In s) -> is_stack_p p (Out (stack_out p s))
| is_stack_pi : forall (s: stackO), not_in_stack p (Out s) -> is_stack_p p (In (stack_in p s))
| is_stack_qo : forall (q: Ncon), forall (s: stackI), q <> p -> is_stack_p p (In s) -> is_stack_p p (Out (stack_out q s))
| is_stack_qi : forall (q: Ncon), forall (s: stackO), q <> p -> is_stack_p p (Out s) -> is_stack_p p (In (stack_in q s))
.

Definition is_stack (s: stack_) :  Prop := forall (p : Ncon), is_stack_p p s.

Structure stack := {
    s :> stack_;
    well_defined : is_stack s;}.

Axiom eq_stack : forall (s1 s2 : stack), (s s1) = (s s2) -> s1 = s2.
Lemma stack_fin_aux : (forall (s : stack_), exists (n : nat), forall (k : nat), k >= n -> not_in_stack (give_ncon k) s) /\
                    (forall (s : stackI), exists (n : nat), forall (k : nat), k >= n -> not_in_stack (give_ncon k) (In s)) /\
                    (forall (s : stackO), exists (n : nat), forall (k : nat), k >= n -> not_in_stack (give_ncon k) (Out s)).
Proof.
  apply stack_ind.
  1,2 : by intros.
  1,2 : move=> p s Hs; destruct Hs as [ns Hs]; destruct (given_ncon p).
  1,3 : exists ns; move=> k Hk. 2 : apply notin_out. apply notin_in. 1,3 : apply H. 1,2 : by apply (Hs k Hk).
  1,2 : destruct H as [np Hp]; exists (max ns (np+1)); intros k Hk.
  2 : apply notin_out. apply notin_in.
  1,3 : move=>Hkp; rewrite (eq_sym Hkp) in Hp; have Hnpk : np = k by apply infinite_ncon. 1,2 : lia.
  1,2 : have Hnsk : k >= ns by lia. 1,2 : apply (Hs k Hnsk).
  exists 0; move=> k Hk; apply notin_nil.
Qed.

Lemma stack_fin_ : forall (s : stack_), exists (n: nat) , forall (k : nat), k >= n -> not_in_stack (give_ncon k) s.
Proof.
  by apply (proj1 stack_fin_aux).
Qed.

Lemma stack_fin : forall (s : stack), exists (n: nat) , forall (k : nat), k >= n -> not_in_stack (give_ncon k) s.
Proof.
  move=> s. by apply (stack_fin_ s).
Qed.

(* inter_ s1 s2 s3 <-> s3 ∈ inter( s1, s2) *)
Inductive inter_ : stack_ -> stack_ -> stack_ -> Prop :=
| inter_nil : inter_ (In nil) (In nil) (In nil)
| ins_OO1  : forall (p : Ncon), forall (s1 : stackO), forall (s2 : stackO), forall (s3: stackO), inter_ (Out s1) (Out s2) (Out s3) -> inter_ (In (stack_in p s1)) (Out s2) (In (stack_in p s3))
| ins_OO2 : forall (p : Ncon), forall (s1 : stackO), forall (s2 : stackO), forall (s3: stackO), inter_ (Out s1) (Out s2) (Out s3) -> inter_ (Out s1) (In (stack_in p s2)) (In (stack_in p s3))
| ins_OI1 : forall (p:Ncon), forall (s1 : stackO), forall (s2 : stackI), forall (s3: stackO), inter_ (Out s1) (In s2) (Out s3) -> inter_ (In (stack_in p s1)) (In s2) (In (stack_in p s3))
| ins_OI2 : forall (p:Ncon), forall (s1 : stackO), forall (s2 : stackI), forall (s3 : stackI), inter_ (Out s1) (In s2) (In s3) -> inter_ (Out s1) (Out (stack_out p s2)) (Out (stack_out p s3))
| ins_IO1 : forall (p:Ncon), forall (s1 : stackI), forall (s2 : stackO), forall (s3 : stackI), inter_ (In s1) (Out s2) (In s3) -> inter_ (Out (stack_out p s1)) (Out s2) (Out (stack_out p s3))
| ins_IO2 : forall (p:Ncon), forall (s1 : stackI), forall (s2 : stackO), forall (s3 : stackO), inter_ (In s1) (Out s2) (Out s3) -> inter_ (In s1) (In (stack_in p s2)) (In (stack_in p s3))
| ins_II1 : forall (p: Ncon), forall (s1 : stackI), forall (s2 : stackI), forall (s3 : stackI), inter_ (In s1) (In s2) (In s3) -> inter_ (Out (stack_out p s1)) (In s2) (Out (stack_out p s3))
| ins_II2 : forall (p: Ncon), forall (s1 : stackI), forall (s2 : stackI), forall (s3 : stackI), inter_ (In s1) (In s2) (In s3) -> inter_ (In s1) (Out (stack_out p s2)) (Out (stack_out p s3)).


Definition inter : stack -> stack -> stack -> Prop := inter_.

(* Definition of inserting p:I,p:O in stack s *)
Inductive insert_stack_p_ : Ncon -> stack_ -> stack_ -> Prop :=
| ins_stack_add_p : forall (p: Ncon) (s : stackO), not_in_stack p (Out s) -> insert_stack_p_ p (Out (stack_out p (stack_in p s))) (Out s)
| ins_stack_in : forall (p: Ncon) (q : Ncon) (s1 s2 : stackO) , (p <> q) -> insert_stack_p_ p (Out s1) (Out s2) -> insert_stack_p_ p (In (stack_in q s1)) (In (stack_in q s2))
| ins_stack_out : forall (p : Ncon) (q : Ncon) (s1 s2 : stackI) , (p <> q) -> insert_stack_p_ p (In s1) (In s2) -> insert_stack_p_ p (Out (stack_out q s1)) (Out (stack_out q s2)).

Definition insert_stack_p : Ncon -> stack -> stack -> Prop := insert_stack_p_.

(* Alpha conversion for stacks *)
Inductive alpha_convert_stack_ : Ncon -> Ncon -> stack_ -> stack_ -> Prop :=
| alpha_empty : forall (p q : Ncon), alpha_convert_stack_ p q (In nil) (In nil)
| alpha_replace_in : forall (p q : Ncon) (s1 s2 : stackO), alpha_convert_stack_ p q (Out s1) (Out s2) -> alpha_convert_stack_ p q (In (stack_in p s1)) (In (stack_in q s2))
| alpha_replace_out : forall (p q : Ncon) (s1 s2 : stackI), alpha_convert_stack_ p q (In s1) (In s2) -> alpha_convert_stack_ p q (Out (stack_out p s1)) (Out (stack_out q s2))
| alpha_leave_in : forall (p q r : Ncon) (s1 s2 : stackO), (r <> p) -> alpha_convert_stack_ p q (Out s1) (Out s2) -> alpha_convert_stack_ p q (In (stack_in r s1)) (In (stack_in r s2))
| alpha_leave_out : forall (p q r : Ncon) (s1 s2 : stackI), (r <> p) -> alpha_convert_stack_ p q (In s1) (In s2) -> alpha_convert_stack_ p q (Out (stack_out r s1)) (Out (stack_out r s2)).

Definition alpha_convert_stack : Ncon -> Ncon -> stack -> stack -> Prop := alpha_convert_stack_.

Lemma exists_alpha_convert_stack_aux (p q : Ncon) : ( forall (s : stack_), exists (s1 : stack_), alpha_convert_stack_ p q s s1)
                                                 /\ (forall (s : stackI), exists (s1 : stackI), alpha_convert_stack_ p q (In s) (In s1))
                                                 /\  (forall (s : stackO), exists (s1 : stackO), alpha_convert_stack_ p q (Out s) (Out s1)).
Proof.
  apply stack_ind.
  1,2 : intros s Hs; destruct Hs as [s1 Hs1].
  by exists (Out s1). by exists (In s1).
                      1,2 : intros r s Hs; destruct Hs as [s1 Hs1]; case (Ncon_dec r p)=> Hrp.
                      1,3 : rewrite Hrp. exists (stack_out q s1); by apply alpha_replace_out.
                      exists (stack_in q s1); by apply alpha_replace_in.
                      exists (stack_out r s1); by apply alpha_leave_out.
                      exists (stack_in r s1); by apply alpha_leave_in.
                      exists nil. by apply alpha_empty.
Qed.

Lemma exists_alpha_convert_stack_ (p q : Ncon) : forall (s : stack_), exists (s1 : stack_), alpha_convert_stack_ p q s s1.
Proof.
  apply (proj1 (exists_alpha_convert_stack_aux p q)).
Qed.

Lemma notin_is_stack (p : Ncon) : forall (s : stack_), not_in_stack p s -> is_stack_p p s.
Proof.
  move=> s Hns. induction Hns.
  apply is_stack_p_nil. apply is_stack_qi. by symmetry. assumption. apply is_stack_qo. by symmetry. assumption.
Qed.

Lemma alpha_stack_idem_ (p q : Ncon) (s1 s2 : stack_) : (p = q) -> alpha_convert_stack_ p q s1 s2 -> s1 = s2.
Proof.
  move=> Hpq Halph. induction Halph. reflexivity.
  1,3 : have Hs1s2 : Out s1 = Out s2 by apply (IHHalph Hpq).
  3,4 : have Hs1s2 : In s1 = In s2 by apply (IHHalph Hpq).
  1,3 : rewrite Hpq. all : by inversion Hs1s2.
Qed.

Lemma alpha_stack_idem (p q : Ncon) (s1 s2 : stack) : (p = q) -> alpha_convert_stack p q s1 s2 -> s1 = s2.
Proof.
  move=> Hpq Halph. apply eq_stack. by apply (alpha_stack_idem_ p q s1 s2 Hpq Halph).
Qed.

Lemma alpha_not_in_stack1 (p q r : Ncon) : forall (s1 s2 : stack_), r <> q -> alpha_convert_stack_ p q s1 s2 -> not_in_stack r s1 -> not_in_stack r s2.
Proof.
  move=> s1 s2 Hrq Halph. induction Halph=> Hrs1. apply notin_nil.
  all:  inversion Hrs1. 1,3 : apply notin_out. 5,6 : apply notin_in.
  all: try assumption; by apply IHHalph.
Qed.

Lemma alpha_not_in_stack2 (p q : Ncon) : forall (s1 s2 : stack_), p <> q -> alpha_convert_stack_ p q s1 s2 -> not_in_stack p s2.
Proof.
  move=> s1 s2 Hpq Halph. induction Halph; try apply notin_nil; try apply notin_out; try apply notin_in.
  1,3 : assumption. 3,5 : by symmetry. all: by apply IHHalph.
Qed.

Lemma alpha_not_in_stack3 (p q : Ncon) : forall (s1 s2 : stack_), not_in_stack p s1 -> not_in_stack q s1 -> alpha_convert_stack_ p q s1 s2 -> not_in_stack q s2.
Proof.
  move=> s1 s2 Hs1 Hs2 Halph. induction Halph; try apply notin_nil; try by inversion Hs1.
  2 : apply notin_in. apply notin_out.
  1,3 : by inversion Hs2. all : apply IHHalph. 1,3 : by inversion Hs1. all : by inversion Hs2.
Qed.

Lemma inverse_is_stack_out : forall (p  : Ncon) (s : stackI), is_stack (Out (stack_out p s)) -> is_stack (In s).
Proof.
  move=> p s Hs q. have Hsq : is_stack_p q (Out (stack_out p s)) by apply (Hs q). inversion Hsq.
  by apply is_stack_pi. by apply notin_is_stack. assumption.
Qed.

Lemma inverse_is_stack_in : forall (p  : Ncon) (s : stackO), is_stack (In (stack_in p s)) -> is_stack (Out s).
Proof.
  move=> p s Hs q. have Hsq : is_stack_p q (In (stack_in p s)) by apply (Hs q). inversion Hsq.
  by apply notin_is_stack. assumption.
Qed.

Lemma alpha_is_stack_aux (p q : Ncon) : (forall (s1 s2 : stack_), not_in_stack q s1 -> is_stack s1 -> alpha_convert_stack_ p q s1 s2 -> is_stack s2)
                                        /\ (forall (s1 s2 : stackI), not_in_stack q (In s1) -> is_stack (In s1) -> alpha_convert_stack_ p q (In s1) (In s2) -> is_stack (In s2))                                                                                                    /\ (forall (s1 s2 : stackO), not_in_stack q (Out s1) -> is_stack (Out s1) -> alpha_convert_stack_ p q (Out s1) (Out s2) -> is_stack (Out s2)).
Proof.
  apply stack_ind.
  - move=> s1 IHs s2 Hqs1 Hs1 Halph.
    inversion Halph. apply IHs; try assumption. by rewrite H3.
    apply IHs; try assumption. by rewrite H4.
  - move=> s1 IHs s2 Hqs1 Hs1 Halph.
    inversion Halph. move=> x. apply is_stack_p_nil. apply IHs; try assumption. by rewrite H3.
    apply IHs; try assumption. by rewrite H4.
  - move=> r s1 IHs s2 Hqs1 Hs1 Halph x. 
    inversion Halph. inversion H2.
    + case (Ncon_dec x q)=> Hxq. rewrite Hxq. apply is_stack_po. apply notin_nil.
      apply is_stack_qo. by symmetry. apply is_stack_p_nil.
    + case (Ncon_dec x q)=> Hxq. rewrite Hxq. apply is_stack_popi. apply (alpha_not_in_stack3 r q (Out s4)).
      rewrite (eq_sym H5) in Hs1. have Hs4 : is_stack_p r (Out (stack_out r (stack_in r s4))) by apply (Hs1 r).
      inversion Hs4. assumption. by inversion H11. easy.
      rewrite (eq_sym H5) in Hqs1. inversion Hqs1. by inversion H13.
      assumption. apply is_stack_qo. by symmetry. apply IHs. by inversion Hqs1. by apply (inverse_is_stack_out _ _ Hs1). rewrite H8. by rewrite H.
    + case (Ncon_dec x q)=> Hxq.
      * rewrite Hxq. apply is_stack_po. rewrite H6. apply (alpha_not_in_stack3 r q (In s1)).
        have Hs1r : is_stack_p r (Out (stack_out r s1)) by apply (Hs1 r). inversion Hs1r.
        rewrite (eq_sym H11) in H5. by inversion H5. assumption. easy. by inversion Hqs1.
        assumption.
      * apply is_stack_qo. by symmetry. apply IHs. by inversion Hqs1. by apply (inverse_is_stack_out r).
        rewrite H6. by rewrite H.
    + inversion H5.
      * case (Ncon_dec r x)=> Hxr. rewrite Hxr. apply is_stack_po. apply notin_nil.
        apply is_stack_qo. assumption. apply is_stack_p_nil.
      * case (Ncon_dec r x)=> Hxr. rewrite Hxr. apply is_stack_po.
        apply notin_out. rewrite (eq_sym Hxr). symmetry. by inversion Hqs1.
        apply (alpha_not_in_stack1 p q x (Out s4)). rewrite Hxr in Hqs1. symmetry. by inversion Hqs1.
        assumption.
        have Hxs1 : is_stack_p x (Out (stack_out r s1)) by apply (Hs1 x). inversion Hxs1. 
        rewrite (eq_sym H13) in H6. inversion H6. easy. rewrite (eq_sym H6) in H12. by inversion H12.
        easy.
        apply is_stack_qo. assumption. apply IHs. by inversion Hqs1. by apply (inverse_is_stack_out r).
        by rewrite H9.
      * have Hrs4 : not_in_stack r (Out s4).
        rewrite (eq_sym H6) in Hs1. have Hrs1 : is_stack_p r (Out (stack_out r (stack_in r1 s4))) by apply Hs1.
        inversion Hrs1. assumption. by inversion H13. easy.
        case (Ncon_dec r x)=> Hxr.
        ++ rewrite Hxr. case (Ncon_dec x r1)=> Hxr1.
           ** rewrite Hxr1. apply is_stack_popi. apply (alpha_not_in_stack1 p q r1 (Out s4)).
              rewrite (eq_sym Hxr1). rewrite (eq_sym Hxr). symmetry. by inversion Hqs1.
              assumption. rewrite (eq_sym Hxr1). by rewrite (eq_sym Hxr).
           ** apply is_stack_po. apply notin_out. assumption.  rewrite (eq_sym Hxr). apply (alpha_not_in_stack1 p q r (Out s4)). symmetry. by inversion Hqs1. assumption. assumption.
        ++ apply is_stack_qo. assumption. apply IHs. by inversion Hqs1.
           by apply (inverse_is_stack_out r). by rewrite H7.
  - move=> r s1 IHs s2 Hqs1 Hs1 Halph x. inversion Halph.
    + case (Ncon_dec x q)=> Hxq.
      * rewrite Hxq. apply is_stack_pi.
        apply (alpha_not_in_stack3 r q (Out s1)).
        have Hrs1 : is_stack_p r (In (stack_in r s1)) by apply Hs1. inversion Hrs1.
        assumption. easy. by inversion Hqs1. assumption.
      * apply is_stack_qi. by symmetry. apply IHs. by inversion Hqs1. by apply (inverse_is_stack_in r).
        by rewrite H.
    + case (Ncon_dec x r)=> Hxr.
      * rewrite Hxr. apply is_stack_pi.
        apply (alpha_not_in_stack1 p q r (Out s1)). symmetry. by inversion Hqs1. assumption.        
        have Hrs1 : is_stack_p r (In (stack_in r s1)) by apply Hs1. inversion Hrs1.
        assumption. easy.
      * apply is_stack_qi. by symmetry. apply IHs. by inversion Hqs1. by apply (inverse_is_stack_in r).
        assumption.
  - move=> s2 _ _ Halph x. inversion Halph.  apply is_stack_p_nil.
Qed.

Lemma alpha_is_stack (p q : Ncon) : forall (s1 s2 : stack_), not_in_stack q s1 -> is_stack s1 -> alpha_convert_stack_ p q s1 s2 -> is_stack s2.
Proof.
  apply (proj1 (alpha_is_stack_aux p q)).
Qed.

Lemma exists_alpha_convert_stack (p q : Ncon) : forall (s : stack), not_in_stack q s -> exists (s1 : stack), alpha_convert_stack p q s s1.
Proof.
  move=> s Hqs. destruct (exists_alpha_convert_stack_ p q s) as [s1 Hs1].
  exists {| s := s1 ; well_defined := (alpha_is_stack p q s s1 Hqs (well_defined s) Hs1) |}.
  assumption.
Qed.

Lemma unique_alpha_convert_stack_aux (p q : Ncon) : (forall (s1 s2 s3 : stack_),
    alpha_convert_stack_ p q s1 s2 -> alpha_convert_stack_ p q s1 s3 -> s2 = s3)
/\ (forall (s1 : stackI) (s2 s3 : stack_),
        alpha_convert_stack_ p q (In s1) s2 -> alpha_convert_stack_ p q (In s1) s3 -> s2 = s3)
/\ (forall (s1 : stackO) (s2 s3 : stack_),
        alpha_convert_stack_ p q (Out s1) s2 -> alpha_convert_stack_ p q (Out s1) s3 -> s2 = s3).
Proof.
  apply stack_ind.
  - move=> s1 Hs1 s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * apply Hs1.
        by rewrite H3. by rewrite H8.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_replace_out.
        rewrite (eq_sym H9) in Halph2. rewrite (eq_sym H) in Halph2. inversion Halph2.
        rewrite (eq_sym H). by apply alpha_replace_out. easy.
    + inversion Halph2.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_out.
        rewrite (eq_sym H5). by apply alpha_replace_out.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_out.
        rewrite (eq_sym H5). by apply alpha_leave_out.
  - move=> s1 Hs1 s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * easy.
      * rewrite (eq_sym H3) in H2. by inversion H2.
      * rewrite (eq_sym H3) in H2. by inversion H2.
    + inversion Halph2.
      * rewrite (eq_sym H7) in H. by inversion H.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_replace_in.
        rewrite (eq_sym H4). by apply alpha_replace_in.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_replace_in.
        rewrite (eq_sym H4). by apply alpha_leave_in.
    + inversion Halph2.
      * rewrite (eq_sym H8) in H. by inversion H.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_in.
        rewrite (eq_sym H5). by apply alpha_replace_in.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_in.
        rewrite (eq_sym H5). by apply alpha_leave_in.
  -  move=> r s1 Hs s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * have Hs46 : (In s4) = (In s6). apply Hs. by rewrite H. by rewrite H.
        inversion Hs46. reflexivity.
      * by rewrite H in H9.
    + inversion Halph2.
      * have Hs46 : (In s4) = (In s6). apply Hs. assumption. by rewrite H6. inversion Hs46.
        rewrite (eq_sym H9) in Halph2. inversion Halph2. by rewrite H12 in H3.
        reflexivity.
      * have Hs46 : (In s4) = (In s6). by apply Hs.  by inversion Hs46.
  - move=> r s1 Hs s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * have Hs46 : (Out s4) = (Out s6). by apply Hs; rewrite H.
        by inversion Hs46.
      * by rewrite H in H9.
    + inversion Halph2.
      * by rewrite H6 in H3.
      * have Hs46 : (Out s4) = (Out s6). by apply Hs. by inversion Hs46.
  - move=> s1 s2 Halph1 Halph2. inversion Halph1. by inversion Halph2.
Qed.
        
Lemma unique_alpha_convert_stack_ (p q : Ncon) : forall (s1 s2 s3 : stack_),
    alpha_convert_stack_ p q s1 s2 -> alpha_convert_stack_ p q s1 s3 -> s2 = s3.
Proof.
  apply (proj1 (unique_alpha_convert_stack_aux p q)).
Qed.

Lemma unique_alpha_convert_stack (p q : Ncon) : forall (s1 s2 s3 : stack),
    alpha_convert_stack p q s1 s2 -> alpha_convert_stack p q s1 s3 -> s2 = s3.
Proof.
  move=> s1 s2 s3 Halph1 Halph2.  apply eq_stack. by apply (unique_alpha_convert_stack_ p q s1 s2 s3 Halph1 Halph2).
Qed.

Lemma alpha_stack_idem2_ (p q : Ncon) (s1 s2 : stack_): not_in_stack p s1 -> alpha_convert_stack_ p q s1 s2 -> s1 = s2.
Proof.
  move=> Hp Halph. induction Halph.
  - trivial.
  - by inversion Hp.
  - by inversion Hp.
  - have Hs : Out s1 = Out s2. apply IHHalph. by inversion Hp. by inversion Hs.
  - have Hs : In s1 = In s2. apply IHHalph. by inversion Hp. by inversion Hs.
Qed.

Lemma alpha_stack_idem2 (p q : Ncon) (s1 s2 : stack) : not_in_stack p s1 -> alpha_convert_stack_ p q s1 s2 -> s1 = s2.
Proof.
  move=> Hps1 Halph. apply eq_stack. by apply (alpha_stack_idem2_ p q s1 s2 Hps1 Halph).
Qed.

Lemma alpha_insert_replace (p q : Ncon) (s1 s2 s3: stack_) : not_in_stack q s3 -> alpha_convert_stack_ p q s1 s2 ->insert_stack_p_ p s1 s3 -> insert_stack_p_ q s2 s3.
Proof.
  move=> Hq Halph Hins. move: s2 Halph; induction Hins.
  - move=> s2 Halph. inversion Halph. inversion H3. have Hs : Out s0 = Out s5. by apply (alpha_stack_idem2_ p q).
    rewrite Hs. apply ins_stack_add_p. by rewrite (eq_sym Hs).
    easy. easy.
  - move=> s Halph. inversion Halph. easy. apply ins_stack_in. intro. rewrite H7 in Hq. by inversion Hq.
    apply IHHins.  by inversion Hq. assumption.
  - move=> s Halph. inversion Halph. easy.
    apply ins_stack_out. intro. rewrite H7 in Hq. by inversion Hq.
    apply IHHins. by inversion Hq. assumption.
Qed.

Lemma alpha_alpha_stack (p q r : Ncon) (s1 s2 s3 : stack_) : not_in_stack q s1 -> alpha_convert_stack_ p q s1 s2 -> alpha_convert_stack_ q r s2 s3 -> alpha_convert_stack_ p r s1 s3.
Proof.
  move=> Hqs Halph1. move: s3. induction Halph1; move=> s3 Halph2.
  - inversion Halph2. apply alpha_empty.
  - inversion Halph2.
    + apply alpha_replace_in. apply IHHalph1. by inversion Hqs. assumption.
    + easy.
  - inversion Halph2.
    + apply alpha_replace_out.  apply IHHalph1. by inversion Hqs. assumption.
    + easy.
  - inversion Halph2.
    + rewrite H0 in Hqs. by inversion Hqs.
    + apply alpha_leave_in. assumption. apply IHHalph1. by inversion Hqs. assumption.
  - inversion Halph2.
    + rewrite H0 in Hqs. by inversion Hqs.
    + apply alpha_leave_out. assumption. apply IHHalph1. by inversion Hqs. assumption.
Qed.

Lemma alpha_stack_invol_ (p q : Ncon) (s1 s2 s3 : stack_) : not_in_stack q s1 -> alpha_convert_stack_ p q s1 s2 -> alpha_convert_stack_ q p s2 s3 -> s1=s3.
  move=> Hqs Halph1 Halph2.
  have Hs : alpha_convert_stack_ p p s1 s3. by apply (alpha_alpha_stack p q p s1 s2 s3).
  by apply (alpha_stack_idem_ p p).
Qed.

Lemma alpha_stack_invol (p q : Ncon) (s1 s2 s3 : stack) : not_in_stack q s1 -> alpha_convert_stack p q s1 s2 -> alpha_convert_stack q p s2 s3 -> s1=s3.
Proof.
  move=> Hqs1 Halph1 Halph2. apply eq_stack. by apply (alpha_stack_invol_ p q s1 s2).
Qed.

Lemma insert_new (p : Ncon) (s1 s2 : stack_) : insert_stack_p_ p s1 s2 -> not_in_stack p s2.
Proof.
  intro. induction H.
  - assumption.
  - by apply notin_out.
  - by apply notin_in.
Qed.

Lemma insert_not_in (p q : Ncon) (s1 s2 : stack_) : insert_stack_p_ p s1 s2 -> not_in_stack q s2 -> p<> q -> not_in_stack q s1.
Proof.
  move=> Hins Hnin Hpq. induction Hins.
  - apply notin_in. easy. apply notin_out. easy. assumption.
  - inversion Hnin. apply notin_out. assumption. by apply IHHins.
  - inversion Hnin. apply notin_in. assumption. by apply IHHins.
Qed.

Lemma alpha_insert_leave : forall (p q r : Ncon) (s1 s2 s3 s4 : stack_),
    (r <> p) -> (r <> q) -> not_in_stack q s1 -> insert_stack_p_ r s3 s1 -> alpha_convert_stack_ p q s1 s2
    -> alpha_convert_stack_ p q s3 s4 -> insert_stack_p_ r s4 s2.
Proof.
  move=> p q r s1 s2 s3 s4 Hrp Hrq Hqs1 Hins. move: s2 s4. induction Hins.
  - move=> s2 s4 Halph1 Halph2. inversion Halph2. 
    +  by rewrite H0 in Hrp. 
    + inversion H6. by rewrite H7 in Hrp. have Hs : s2 = (Out s6). by apply (unique_alpha_convert_stack_ p q (Out s0)). rewrite Hs. apply ins_stack_add_p. by apply (alpha_not_in_stack1 p q _ (Out s0)).
  - move=> s3 s4 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * apply ins_stack_in. easy. apply IHHins; try assumption.  by inversion Hqs1. by rewrite H0.
        by rewrite H0.
      * by rewrite H0 in H10.
    + inversion Halph2. by rewrite H7 in H4.
      apply ins_stack_in. assumption. apply IHHins; try assumption. by inversion Hqs1.
  - move=> s3 s4 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * apply ins_stack_out. easy. apply IHHins; try assumption.  by inversion Hqs1. by rewrite H0.
        by rewrite H0.
      * by rewrite H0 in H10.
    + inversion Halph2. by rewrite H7 in H4.
      apply ins_stack_out. assumption. apply IHHins; try assumption. by inversion Hqs1.
Qed.

Lemma alpha_inter_ : forall (p q : Ncon) (s1 s2 s3 s4 s5 s6 : stack_),
    inter_ s1 s2 s3 -> alpha_convert_stack_ p q s1 s4 -> alpha_convert_stack_ p q s2 s5
    -> alpha_convert_stack_ p q s3 s6 -> inter_ s4 s5 s6.
Proof.
  move=> p q s1 s2 s3 s4 s5 s6 Hints. move: s4 s5 s6.
  induction Hints; move=> s4 s5 s6 Halph1 Halph2 Halph3.
  - inversion Halph1. inversion Halph2. inversion Halph3. apply inter_nil.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OO1.
    + rewrite H9.  apply IHHints; try assumption; try by rewrite H. 
    + by rewrite H in H14.
    + apply IHHints; try assumption; try by rewrite H. rewrite (eq_sym H5). by apply alpha_leave_out.
    
    + by rewrite H in H15.
    + by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_out.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OO2.
    + rewrite H3. apply IHHints; try assumption; try by rewrite H10.
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_replace_out.
    + rewrite H4. apply IHHints; try assumption; try by rewrite H11.
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OI1.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H8). apply alpha_empty. 
    + by rewrite H in H13.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_replace_in.
    + by rewrite H in H14.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_leave_in. 
    + by rewrite H in H15.
    + by rewrite H10 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H9). by apply alpha_empty.
    + by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_in.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_in.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OI2.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H). by apply alpha_replace_out.
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try by assumption. rewrite (eq_sym H). by apply alpha_replace_out.
    + apply IHHints; try by rewrite H11. rewrite (eq_sym H). by apply alpha_leave_out.
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_IO1.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_replace_out. 
    + by rewrite H in H14.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_leave_out. 
    + by rewrite H in H15.
    +  by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_out.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_IO2.
    + apply IHHints; try by rewrite H9. rewrite (eq_sym H2). apply alpha_empty.
    + by rewrite H3 in H13.
    + by rewrite H10 in H7.
    + apply IHHints; try assumption. rewrite (eq_sym H2). apply alpha_empty.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H). by apply alpha_replace_in. 
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try assumption.  rewrite (eq_sym H). by apply alpha_replace_in.
    + apply IHHints; try by rewrite H5. rewrite (eq_sym H). by apply alpha_leave_in. 
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_in.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_II1.
    + apply IHHints; try by rewrite H9. rewrite (eq_sym H8).  by apply alpha_empty.
    +  by rewrite H in H13.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H5). by apply alpha_replace_in. 
    + by rewrite H in H14.
    + apply IHHints; try by rewrite H11. rewrite (eq_sym H5). by apply alpha_leave_in.
    + by rewrite H in H15.
    + by rewrite H10 in H3.
    + apply IHHints; try assumption.  rewrite (eq_sym H9). by apply alpha_empty.
    + by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_in.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_in.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_II2.
    + apply IHHints; try by rewrite H9. rewrite (eq_sym H2). by apply alpha_empty.
    + by rewrite H3 in H13.
    + by rewrite H10 in H7.
    + apply IHHints; try assumption. rewrite (eq_sym H2). by apply alpha_empty.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H). by apply alpha_replace_in. 
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_replace_in.
    + apply IHHints; try by rewrite H11. rewrite (eq_sym H). by apply alpha_leave_in. 
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_in.
Qed.

Lemma inter_notin1 : forall (p : Ncon) (s1 s2 s3 : stack_), inter_ s1 s2 s3 -> not_in_stack p s3
                                                       -> (not_in_stack p s1 /\ not_in_stack p s2).
Proof.
  move=> p s1 s2 s3 Hins. induction Hins; move=> Hps3.
  - split; apply notin_nil.
  - inversion Hps3. split. apply notin_out. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_out. assumption. by apply IHHins.
  - inversion Hps3. split. apply notin_out. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_in. assumption. by apply IHHins.
  - inversion Hps3. split. apply notin_in. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_out. assumption. by apply IHHins.
  - inversion Hps3. split. apply notin_in. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_in. assumption. by apply IHHins.
Qed.

Lemma inter_notin2 : forall (p : Ncon) (s1 s2 s3 : stack_), inter_ s1 s2 s3                                                       -> (not_in_stack p s1 /\ not_in_stack p s2) -> not_in_stack p s3.
Proof.
  move=> p s1 s2 s3 Hins. induction Hins; move=> [Hps1 Hps2].
  - apply notin_nil.
  - inversion Hps1. apply notin_out. assumption. by apply IHHins.
  - inversion Hps2. apply notin_out. assumption. by apply IHHins.
  - inversion Hps1. apply notin_out. assumption. by apply IHHins.
  - inversion Hps2. apply notin_in. assumption. by apply IHHins.
  - inversion Hps1. apply notin_in. assumption. by apply IHHins.
  - inversion Hps2. apply notin_out. assumption. by apply IHHins.
  - inversion Hps1. apply notin_in. assumption. by apply IHHins.
  - inversion Hps2. apply notin_in. assumption. by apply IHHins.
Qed.

Lemma is_stack_nil : is_stack (In (nil)).
Proof.
  move=> p; apply is_stack_p_nil.
Qed.

Definition stack_nil := {| s := In (nil); well_defined := is_stack_nil; |}.

Lemma is_stack_stack1 (p : Ncon) : is_stack (Out (stack_out p nil)).
Proof.
  move=> q. case (Ncon_dec p q)=> Hpq. rewrite Hpq. apply is_stack_po. apply notin_nil.
  apply is_stack_qo.  assumption. apply is_stack_p_nil.
Qed.
Definition stack1 (p : Ncon) := {| s := Out (stack_out p nil); well_defined := is_stack_stack1 p; |}.

Lemma is_stack_stack2 (p q : Ncon) : (p <> q) -> is_stack (In (stack_in p (stack_out q nil))).
Proof.
  move=> Hpq r. case (Ncon_dec p r) => Hpr.
  rewrite Hpr. apply is_stack_pi. apply notin_in. by rewrite (eq_sym Hpr). apply notin_nil.
  apply is_stack_qi. assumption. apply (is_stack_stack1 q r).
Qed.

Definition stack2 (p q : Ncon) (Hpq : p<> q) := {| s := In (stack_in p (stack_out q nil)); well_defined := is_stack_stack2 p q Hpq; |}.
