Require Import ssreflect. 
Require Import ZArith Psatz. 

Parameter Nser Ncon Nref Nfo : Type.
Axiom Nser_dec : forall (x y : Nser), (x=y) \/ (x<> y).
Axiom Ncon_dec : forall (x y : Ncon), (x=y) \/ (x <> y).


Inductive Names :=
| srv : Nser -> Names
| cnt : Ncon -> Names.

Inductive Names_cur :=
| cur : Names_cur
| names : Names -> Names_cur.

Variable give_nser : nat -> Nser.
Axiom infinite_nser : forall (n1 n2 : nat), (give_nser n1 = give_nser n2 -> n1 = n2).
Axiom given_nser : forall (x : Nser), (forall (n : nat), (give_nser n <> x)) \/ (exists (n : nat), (give_nser n = x)).
Variable give_ncon : nat -> Ncon.
Axiom infinite_ncon : forall (n1 n2 : nat), (give_ncon n1 = give_ncon n2 -> n1 = n2).
Axiom given_ncon : forall (x : Ncon), (forall (n : nat), (give_ncon n <> x)) \/ (exists (n : nat), (give_ncon n = x)).

Definition is_dom_fin (dom : Names_cur -> Prop) : Prop :=  exists (n : nat), forall (k :nat), k >= n -> ~(dom (names (srv (give_nser k)))) /\ ~(dom (names (cnt (give_ncon k)))).
Definition is_V_fin (V : Names -> Prop) : Prop := exists (n: nat), forall (k:nat), k>= n -> ~(V (srv (give_nser k))) /\ ~(V (cnt (give_ncon k))).

Structure Vis := {
    V :> Names -> Prop;
    V_fin : is_V_fin V;
  }.

Definition is_codom_fin (B : Names_cur -> Vis) : Prop :=  exists (n : nat), forall (k : nat), k >= n -> forall (x : Names_cur), ~(B x (srv (give_nser k))) /\ ~(B x (cnt (give_ncon k))).

Structure Visfun := {
    dom : Names_cur -> Prop;
    B :> Names_cur -> Vis;
    _dom : forall (x : Names_cur) (y : Names), (B x y) -> dom x;
    dom_fin : is_dom_fin dom; 
    codom_fin : is_codom_fin B;
  }.


(* Operation on V : Names -> Prop *)
Definition add_V_ (V : Names -> Prop) (y : Names) := fun (x : Names) => ( V x \/ x = y).
Lemma add_V_fin : forall (V : Names -> Prop) (y : Names), is_V_fin V -> is_V_fin (add_V_ V y).
Proof.
  intros. destruct H as [n HV]. destruct y as [a | p].
  - destruct (given_nser a).
    + exists n. intros k Hk. split.
      * intro HaddV. destruct HaddV as [HaddV | HaddV]. apply ((proj1 ( HV k Hk)) HaddV). inversion HaddV. apply (H k H1).
      * intro HaddV. destruct HaddV as [HaddV | HaddV]. apply ((proj2 ( HV k Hk)) HaddV). inversion HaddV. 
    +  destruct H as [n1 Hy]. exists (max n (n1+1)). intros k Hk. split.
       * intro HaddV. destruct HaddV as [HaddV | HaddV]. 
         ++  have Hkn : (k >= n) by lia. apply ((proj1 (HV k Hkn)) HaddV).
         ++ inversion HaddV. rewrite (eq_sym Hy) in H0. have Hkn1 : k = n1 by apply infinite_nser. lia.
       * intro HaddV. destruct HaddV as [HaddV | HaddV].
         ++ have Hkn : (k >= n) by lia. apply ((proj2 (HV k Hkn)) HaddV).
         ++ inversion HaddV.
  - destruct (given_ncon p).
    + exists n. intros k Hk. split.
      * intro HaddV. destruct HaddV as [HaddV | HaddV]. apply ((proj1 ( HV k Hk)) HaddV). inversion HaddV.
      * intro HaddV. destruct HaddV as [HaddV | HaddV]. apply ((proj2 ( HV k Hk)) HaddV). inversion HaddV. apply (H k H1).
    +  destruct H as [n1 Hy]. exists (max n (n1+1)). intros k Hk. split.
       * intro HaddV. destruct HaddV as [HaddV | HaddV].
         ++ have Hkn : (k >= n) by lia. apply ((proj1 (HV k Hkn)) HaddV).
         ++ inversion HaddV.
       * intro HaddV. destruct HaddV as [HaddV | HaddV]. 
         ++  have Hkn : (k >= n) by lia. apply ((proj2 (HV k Hkn)) HaddV).
         ++ inversion HaddV. rewrite (eq_sym Hy) in H0. have Hkn1 : k = n1 by apply infinite_ncon. lia.
Qed.

Definition add_V (V : Vis) (y : Names) := {| V := add_V_ V y; V_fin := add_V_fin V y (V_fin V); |}.
Definition minus_V_ (V : Names -> Prop) (y : Names) := fun (x : Names) => (V x /\ x <> y).
Lemma minus_V_fin : forall (V : Names -> Prop) (y : Names), is_V_fin V -> is_V_fin (minus_V_ V y).
Proof.
  intros V y HV. destruct HV as [n HV]. destruct y as [a | p].
  - exists n. intros k Hk.  split.
    + intro HmV. destruct HmV. apply ((proj1 (HV k Hk)) H).
    + intro HmV. destruct HmV. apply ((proj2 (HV k Hk)) H).
  - exists n. intros k Hk.  split.
    + intro HmV. destruct HmV. apply ((proj1 (HV k Hk)) H).
    + intro HmV. destruct HmV. apply ((proj2 (HV k Hk)) H).
Qed.

Definition minus_V (V : Vis) (y : Names) := {| V := minus_V_ V y; V_fin := minus_V_fin V y (V_fin V); |}.

Definition union_V_ (V1 : Names -> Prop) (V2 : Names -> Prop) := fun (x : Names) => (V1 x \/ V2 x).
Lemma union_V_fin : forall (V1 V2 : Names -> Prop), is_V_fin V1 -> is_V_fin V2 -> is_V_fin (union_V_ V1 V2).
Proof.
  intros V1 V2 HV1 HV2. destruct HV1 as [n1 HV1]. destruct HV2 as [n2 HV2].
  exists (max n1 n2). intros k Hk. split.
  - intro HV. destruct HV.
    + have Hkn1 : k >= n1 by lia. apply ((proj1 (HV1 k Hkn1)) H).
    + have Hkn2 : k >= n2 by lia. apply ((proj1 (HV2 k Hkn2)) H).
  - intro HV. destruct HV.
    + have Hkn1 : k >= n1 by lia. apply ((proj2 (HV1 k Hkn1)) H).
    + have Hkn2 : k >= n2 by lia. apply ((proj2 (HV2 k Hkn2)) H).
Qed.

Definition union_V (V1 V2 : Vis) := {| V:= union_V_ V1 V2; V_fin := union_V_fin V1 V2 (V_fin V1) (V_fin V2);|}.

Definition filter_V_ (V : Names -> Prop) (x : Names) := fun (y : Names) => (V y /\ y=x).
Lemma filter_V_fin : forall (V : Vis) (x : Names), is_V_fin (filter_V_ V x).
Proof.
  move=> V x. destruct (V_fin V) as [n Hn]. exists n. move=> k Hk.
  split.
  - intro. destruct H. apply (proj1 (Hn k Hk) H).
  - intro. destruct H. apply (proj2 (Hn k Hk) H).
Qed.

Definition filter_V (V : Vis) (x : Names) := {| V := filter_V_ V x; V_fin := filter_V_fin V x;|}.
                                                                              
Definition alpha_convert_V_ (a b : Names) (V : Vis) := fun (y : Names) => (y <> a /\ V y) \/ (y=b /\ V a).
Lemma alpha_convert_V_fin : forall (a b : Names) (V1 : Vis), is_V_fin (alpha_convert_V_ a b V1).
Proof.
  intros. destruct (V_fin V1) as [n HV].
  destruct b as [b | p].
  - destruct (given_nser b).
    + exists n. intros k Hk. split.
      * intro Halpha. destruct Halpha. apply ((proj1 (HV k Hk)) (proj2 H0)).
        destruct H0. inversion H0. apply (H k H3).
      * intro Halpha. destruct Halpha. apply ((proj2 (HV k Hk)) (proj2 H0)).
        destruct H0. inversion H0. 
    + destruct H as [nb Hb]. exists (max n (nb+1)). intros k Hk. split.
      * intro Halpha. destruct Halpha.
        have Hkn : k >= n by lia. apply (proj1 ((HV k Hkn)) (proj2 H)). destruct H. inversion H.
        rewrite (eq_sym H2) in Hb. have Hnbk : nb = k by apply (infinite_nser nb k). lia.
      * intro Halpha. destruct Halpha.
        have Hkn : k >= n by lia. apply (proj2 ((HV k Hkn)) (proj2 H)). destruct H. inversion H.
  - destruct (given_ncon p).
    + exists n. intros k Hk. split.
      * intro Halpha. destruct Halpha. apply ((proj1 (HV k Hk)) (proj2 H0)).
        destruct H0. inversion H0.
      * intro Halpha. destruct Halpha. apply ((proj2 (HV k Hk)) (proj2 H0)).
        destruct H0. inversion H0. apply (H k H3).
    + destruct H as [np Hp]. exists (max n (np+1)). intros k Hk. split.
      * intro Halpha. destruct Halpha.
        have Hkn : k >= n by lia. apply (proj1 ((HV k Hkn)) (proj2 H)). destruct H. inversion H.
      * intro Halpha. destruct Halpha.
        have Hkn : k >= n by lia. apply (proj2 ((HV k Hkn)) (proj2 H)). destruct H. inversion H.
        rewrite (eq_sym H2) in Hp. have Hnbk : np = k by apply (infinite_ncon np k). lia.
Qed.

Definition alpha_convert_V (a b : Names) (V : Vis) := {| V := alpha_convert_V_ a b V; V_fin := alpha_convert_V_fin a b V; |}.

Lemma alpha_add_V_replace : forall (a b : Names) (V : Vis), forall ( x : Names), alpha_convert_V a b (add_V V a) x <-> add_V (alpha_convert_V a b V) b x.
Proof.
  move=> a b V x. split=> [HV | HV].
  - destruct HV. left. left. destruct H. destruct H0. by split. easy.
    destruct H. by right.
  - destruct HV. destruct H. left. split. easy. destruct H. by left.
    right. destruct H. split. easy. by right.
    right. split. assumption. by right.
Qed.
(*Lemma alpha_add_V_replace : forall (a b : Names) (V : Vis), ~(V a) -> forall (x : Names), alpha_convert_V a b (add_V V a) x <-> add_V V b x.
Proof.
  intros a b V Ha x. split.
  - simpl. intro H. destruct H.
    + destruct H. destruct H0. by left. easy.
    + destruct H. by right.
  - simpl. intro H. destruct H.
    + left. split. intro. by rewrite H0 in H. by left.
    + right. split. easy. simpl. by right.
Qed.*)
Lemma alpha_add_V_leave : forall (a b c: Names) (V : Vis), c <> a -> forall (x : Names), alpha_convert_V a b (add_V V c) x <-> add_V (alpha_convert_V a b V) c x.
Proof.
  intros a b c V Hc x. split.
  - simpl. intro HV. destruct HV.
    + destruct H as [Hx HV]. destruct HV. left. by left. by right.
    + destruct H as [Hx HV]. destruct HV. rewrite Hx. left. by right.
      exfalso. by apply (Hc (eq_sym H)).
  - simpl. intro HV. destruct HV.
    + destruct H.  destruct H as [Hx HV].  left. split. easy. by left. right. split. easy. left. easy.
    + left. split. by rewrite H. by right.
Qed.

Lemma add_equiv_V : forall (a : Names) (V1 V2 : Vis), (forall (x : Names), V1 x <-> V2 x) -> (forall (x : Names), add_V V1 a x <-> add_V V2 a x).
Proof.
  move=> a V1 V2 HV x; split.
  - move=> HV1. destruct HV1. left. apply (proj1 (HV x) H). by right.
  - move=> HV2. destruct HV2. left. apply (proj2 (HV x) H). by right.
Qed.

Lemma alpha_minus_V_replace : forall (a b : Names) (V : Vis), ~(V b) -> forall (x : Names), alpha_convert_V a b (minus_V V a) x <-> (minus_V V a) x.
Proof.
  intros a b V Hb x. split.
  - intro HV. destruct HV. destruct H as [Hx HV]. by destruct HV. destruct H. destruct H0. easy.
  - intro. simpl. destruct H. left. split. easy. simpl. by split.
Qed.

Lemma alpha_minus_V_leave : forall (a b c : Names) (V : Vis), (c <> a) -> (c <> b) -> forall (x : Names), alpha_convert_V a b (minus_V V c) x <-> minus_V (alpha_convert_V a b V) c x.
Proof.
  intros a b c V Hca Hcb x. simpl. split.
  - intro HV. destruct HV. destruct H as [Hx HV]. split. left. split. easy. by destruct HV.  by destruct HV.
    destruct H as [Hx HV]. rewrite Hx. split. destruct HV. by right. easy.
  - intro HV. destruct HV as [HV Hx]. destruct HV. left. split. easy.  split. easy. easy.
    right. split. easy. split. easy. easy.
Qed.

Lemma alpha_union_V : forall (a b : Names) (V1 V2 : Vis), forall (x : Names), alpha_convert_V a b (union_V V1 V2) x <-> union_V (alpha_convert_V a b V1) (alpha_convert_V a b V2) x.
Proof.
  intros a b V1 V2 x. simpl. split.
  - intro HV. destruct HV. destruct H as [Hx HV]. destruct HV.
    left. by left. right. by left.
    destruct H as [Hx HV]. destruct HV.
    left. by right. right. by right.
  - intro HV. destruct HV. destruct H.
    left. split. easy. left. easy.
    right. split. easy. left. easy.
    destruct H.
    left. split. easy. right. easy.
    right. split. easy. right. easy.
Qed.

Lemma add_minus_V : forall (a b : Names) (V : Vis), (a <> b) -> (forall (x : Names), add_V (minus_V V a) b x <-> minus_V (add_V V b) a x).
Proof.
  move=> a b V Hab x. split=> [Hadd | Hmin].
  - destruct Hadd. destruct H. split; [|assumption]. by left. split. by right. intro. by rewrite H0 in H.
  - destruct Hmin. destruct H. left. by split. by right.
Qed.
(* Relations and operations on domains *)
Definition empty : Names_cur -> Prop := fun (x : Names_cur) => False.
Lemma empty_fin : is_dom_fin empty.
Proof.
  exists 0. intros k Hk. split. by intro. by intro. 
Qed.

Definition dom_eq (dom1 dom2 : Names_cur -> Prop) : Prop := forall (x : Names_cur), (dom1 x) <-> (dom2 x).

Lemma dom_eq_sym : forall (dom1 dom2 : Names_cur -> Prop), (dom_eq dom1 dom2) -> (dom_eq dom2 dom1).
Proof.
  intros. split. apply (proj2 (H x)). apply (proj1 (H x)).
Qed.
Lemma dom_eq_refl : forall (dom1 : Names_cur -> Prop), (dom_eq dom1 dom1).
Proof.
  intros. split. trivial. trivial.
Qed.

Lemma dom_eq_trans : forall (dom1 dom2 dom3 : Names_cur -> Prop), (dom_eq dom1 dom2) -> (dom_eq dom2 dom3) -> (dom_eq dom1 dom3).
Proof.
  intros. split.
  - intro. apply ((proj1 (H0 x)) ((proj1 (H x)) H1)).
  - intro. apply ((proj2 (H x)) ((proj2 (H0 x)) H1)).
Qed.

Definition dom_incl (dom1 dom2 : Names_cur -> Prop) : Prop := forall (x : Names_cur), (dom1 x) -> (dom2 x).

Lemma dom_incl_refl : forall (dom : Names_cur -> Prop), dom_incl dom dom.
Proof.
  easy.
Qed.

Lemma dom_incl_trans : forall (dom1 dom2 dom3 : Names_cur -> Prop), dom_incl dom1 dom2 -> dom_incl dom2 dom3 -> dom_incl dom1 dom3.
Proof.
  intros dom1 dom2 dom3 H1 H2 x Hdom1. apply (H2 x (H1 x Hdom1)).
Qed.

Definition dom_disj (dom1 dom2 : Names_cur -> Prop) : Prop := forall (x : Names_cur), ((dom1 x) -> ~(dom2 x)).

Lemma dom_disj_comm (dom1 dom2 : Names_cur -> Prop) : dom_disj dom1 dom2 -> dom_disj dom2 dom1.
Proof.
  move=> H x Hx1 Hx2. apply (H x Hx2 Hx1).
Qed.

Definition dom_union (dom1 dom2 : Names_cur -> Prop) : Names_cur -> Prop := fun (x : Names_cur) => (dom1 x \/ dom2 x).

Lemma dom_union_comm : forall (dom1 dom2 : Names_cur -> Prop), dom_eq (dom_union dom1 dom2) (dom_union dom2 dom1).
Proof.
  intros dom1 dom2 x. split.
  - unfold dom_union. intro. destruct H. by right. by left.
  - unfold dom_union. intro. destruct H. by right. by left.
Qed.

Lemma dom_union_fin : forall (dom1 dom2 : Names_cur -> Prop), is_dom_fin dom1 -> is_dom_fin dom2 -> is_dom_fin (dom_union dom1 dom2).
Proof.
  intros. destruct H. destruct H0. exists (max x x0). intros. split.
  -  intro. destruct H2. have Hk : x <= k by apply (Nat.max_lub_l x x0).  apply ((proj1 (H k Hk)) H2).
     have Hk : x0 <= k by apply (Nat.max_lub_r x x0). apply ((proj1 (H0 k Hk)) H2).
  -  intro. destruct H2. have Hk : x <= k by apply (Nat.max_lub_l x x0).  apply ((proj2 (H k Hk)) H2).
     have Hk : x0 <= k by apply (Nat.max_lub_r x x0). apply ((proj2 (H0 k Hk)) H2).
Qed.

Definition minus_dom (dom : Names_cur -> Prop) (x : Names_cur) : Names_cur -> Prop :=
  fun (y: Names_cur) => (dom y) /\ (x <> y).

Lemma minus_dom_fin : forall (dom :Names_cur -> Prop) (x : Names_cur), is_dom_fin dom -> is_dom_fin (minus_dom dom x).
Proof.
  intros. destruct H. exists x0. intros k Hk. unfold minus_dom. split.
  - intro. destruct H0. apply ((proj1 (H k Hk)) H0).
  - intro. destruct H0. apply ((proj2 (H k Hk)) H0).
Qed.

Definition add_dom (dom : Names_cur -> Prop) (x : Names_cur) : Names_cur -> Prop :=
  fun (y : Names_cur) => (dom y) \/ (x = y).

Lemma add_dom_fin : forall (dom : Names_cur -> Prop) (x: Names_cur), is_dom_fin dom -> is_dom_fin (add_dom dom x).
Proof.
  intros. destruct H.
  destruct x. exists x0. intros. unfold add_dom. split.
  - intro. destruct H1. apply ((proj1 (H k H0)) H1). easy.
  - intro. destruct H1. apply ((proj2 (H k H0)) H1). easy.
  - destruct n as [a | p].
    + destruct (given_nser a).
      * exists x0. intros. unfold add_dom. split.
        ++ intro.  destruct H2. apply ((proj1 (H k H1)) H2).
           inversion H2.  apply (H0 k (eq_sym H4)).
        ++ intro.  destruct H2. apply ((proj2 (H k H1)) H2).
           inversion H2.
      * destruct H0. exists (max (x+1) x0). intros.
        unfold add_dom. split.
        ++ intro. destruct H2.
           have Hk : (k >= x0) by apply (Nat.max_lub_r (x+1) x0). apply ((proj1 (H k Hk)) H2).
           inversion H2. rewrite H4 in H0. have Hxk : x=k by apply (infinite_nser _ _ H0). rewrite Hxk in H1.
           have Hk : k >= k+1 by apply (Nat.max_lub_l _ x0). lia.
        ++  intro. destruct H2.
           have Hk : (k >= x0) by apply (Nat.max_lub_r (x+1) x0). apply ((proj2 (H k Hk)) H2).
           inversion H2.
    + destruct (given_ncon p).
      * exists x0. intros. unfold add_dom. split.
        ++ intro.  destruct H2. apply ((proj1 (H k H1)) H2).
           inversion H2.
        ++ intro.  destruct H2. apply ((proj2 (H k H1)) H2).
           inversion H2. apply (H0 k (eq_sym H4)).
      * destruct H0. exists (max (x+1) x0). intros.
        unfold add_dom. split.
        ++  intro. destruct H2.
            have Hk : (k >= x0) by apply (Nat.max_lub_r (x+1) x0). apply ((proj1 (H k Hk)) H2).
            inversion H2.
        ++ intro. destruct H2.
           have Hk : (k >= x0) by apply (Nat.max_lub_r (x+1) x0). apply ((proj2 (H k Hk)) H2).
           inversion H2. rewrite H4 in H0. have Hxk : x=k by apply (infinite_ncon _ _ H0). rewrite Hxk in H1.
           have Hk : k >= k+1 by apply (Nat.max_lub_l _ x0). lia.
Qed.

Definition alpha_convert_dom (a b : Names) (dom : Names_cur -> Prop) := fun (x : Names_cur) => ((x = (names b)) /\ dom (names a)) \/ ((x <> (names a)) /\ dom x).
Lemma alpha_convert_dom_fin : forall (a b : Names) (dom : Names_cur -> Prop), is_dom_fin dom -> is_dom_fin (alpha_convert_dom a b dom).
Proof.
  intros a b dom Hdom. destruct Hdom as [n Hdom]. destruct b as [b | p].
  - destruct (given_nser b).
    + exists n. intros k Hk. split.
      * intro. destruct H0. destruct H0 as [Hb Ha].
        inversion Hb. by apply (H k). by apply (proj1 (Hdom k Hk) (proj2 H0)).
      * intro. destruct H0. destruct H0 as [Hb Ha]. inversion Hb.
        apply ((proj2 (Hdom k Hk)) (proj2 H0)).
    + destruct H as [nb Hb]. exists (max n (nb+1)).
      intros k Hk. split.
      * intro. destruct H. destruct H. inversion H. rewrite (eq_sym H2) in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have Hkn : k >=n by lia. apply (proj1 (Hdom k Hkn) (proj2 H)).
      * intro. destruct H. destruct H. inversion H. have Hkn : k >= n by lia. apply (proj2 (Hdom k Hkn) (proj2 H)).
  - destruct (given_ncon p).
    + exists n. intros k Hk. split.
      * intro. destruct H0. destruct H0 as [Hp Ha]. inversion Hp.
        apply ((proj1 (Hdom k Hk)) (proj2 H0)).
      * intro. destruct H0. destruct H0 as [Hp Ha].
        inversion Hp. by apply (H k). by apply (proj2 (Hdom k Hk) (proj2 H0)).
    + destruct H as [np Hb]. exists (max n (np+1)).
      intros k Hk. split.
      * intro. destruct H. destruct H. inversion H. have Hkn : k >= n by lia. apply (proj1 (Hdom k Hkn) (proj2 H)).
      * intro. destruct H. destruct H. inversion H. rewrite (eq_sym H2) in Hb. have Hnpk : np = k by apply infinite_ncon. lia.
        have Hkn : k >=n by lia. apply (proj2 (Hdom k Hkn) (proj2 H)).
Qed.

Lemma alpha_add_dom_replace : forall (a b : Names) (dom : Names_cur -> Prop), ~(dom (names a)) -> dom_eq (alpha_convert_dom a b (add_dom dom (names a))) (add_dom dom (names b)).
Proof.
  intros a b dom Ha. split.
  - intro. destruct H. destruct H. rewrite H. by right.
    destruct H. destruct H0. by left. exfalso. by apply (H (eq_sym H0)).
  - intro. destruct H. right. split. intro. rewrite H0 in H. easy. by left.
    left. split. easy. by right.
Qed.

Lemma alpha_add_dom_leave : forall (a b c : Names) (dom : Names_cur -> Prop), (c <> a) -> dom_eq (alpha_convert_dom a b (add_dom dom (names c))) (add_dom (alpha_convert_dom a b dom) (names c)).
Proof.
  intros a b c dom Hc. split.
  - intro. destruct H. destruct H. destruct H0. left. by left. by inversion H0.
    destruct H. destruct H0. left. right. easy.
    by right.
  - intro. destruct H. destruct H. destruct H. left. split. easy. by left.
    right. destruct H. split. easy. by left.
    right. split. intro. rewrite H0 in H. by inversion H. by right.
Qed.

Lemma alpha_union_dom : forall (a b : Names) (dom1 dom2 : Names_cur -> Prop), dom_eq (alpha_convert_dom a b (dom_union dom1 dom2)) (dom_union (alpha_convert_dom a b dom1) (alpha_convert_dom a b dom2)).
Proof.
  intros a b dom1 dom2. split.
  - intro. destruct H. destruct H. destruct H0. left. by left. right. by left.
    destruct H. destruct H0. left. by right. right. by right.
  -  intro. destruct H. destruct H. left. destruct H. split. easy. by left.
     destruct H. right. split. easy. by left.
     destruct H. destruct H. left. destruct H. split. easy. by right.
     destruct H. right. split. easy. by right.
Qed.
(* Relations and operations on visibility functions (under type Names_cur -> Vis *)

Lemma minus_vis_B_aux : forall (x : Names_cur) (B : Names_cur -> Vis) (y : Names_cur), is_V_fin (fun (z : Names) => (B y z /\ (x <> y))).
Proof.
  intros x B y. destruct (V_fin (B y)) as [n HBy]. exists n. intros k Hk. split.
  - intro HBxy. apply ((proj1 (HBy k Hk)) (proj1 HBxy)).
  - intro HBxy. apply ((proj2 (HBy k Hk)) (proj1 HBxy)).
Qed.

Definition minus_vis_B (x : Names_cur) (dom : Names_cur -> Prop) (B: Names_cur-> Vis) : Names_cur -> Vis :=
  fun (y : Names_cur) => {| V := (fun (z : Names) => (B y z /\ x <> y)); V_fin := minus_vis_B_aux x B y; |}.

Lemma minus_codom_fin : forall (x : Names_cur) (dom : Names_cur -> Prop) (B : Names_cur -> Vis), is_codom_fin B -> is_codom_fin (minus_vis_B x dom B).
Proof.
  intros x dom B HB. destruct HB as [n HB]. exists n. intros k Hk y. split.
  - intro HmB. destruct HmB as [HmB Hxy]. apply ((proj1 (HB k Hk y)) HmB).
  - intro HmB. destruct HmB as [HmB Hxy]. apply ((proj2 (HB k Hk y)) HmB).
Qed.

Lemma add_vis_B_aux : forall (x : Names_cur) (V : Vis) (y : Names_cur), is_V_fin (fun (z: Names) => V z /\ (x=y)).
Proof.
  intros x V y. destruct (V_fin V) as [n HV]. exists n. intros k Hk. split.
  - intro HVxy. destruct HVxy. apply ((proj1 (HV k Hk)) H).
  - intro HVxy. destruct HVxy. apply ((proj2 (HV k Hk)) H).
Qed. 
Definition add_vis_B (x : Names_cur) (V: Vis) (dom : Names_cur -> Prop) (B: Names_cur -> Vis) : Names_cur -> Vis :=
  fun (y : Names_cur) => union_V (B y) {| V:= (fun (z: Names) => V z /\ (x=y)); V_fin := add_vis_B_aux x V y; |}.

Lemma add_codom_fin :forall (x : Names_cur) (V: Vis) (dom : Names_cur -> Prop) (B : Names_cur -> Vis),
    is_codom_fin B -> is_codom_fin (add_vis_B x V dom B).
Proof.
  intros x V dom B HB. destruct HB as [n1 HB]. destruct (V_fin V) as [n2 HV].
  exists (max n1 n2). intros k Hk y. split.
  - intro HaddB. destruct HaddB.
    + have Hkn1 : (k >= n1) by lia. apply ((proj1 (HB k Hkn1 y)) H).
    + have Hkn2 : (k >= n2) by lia. apply ((proj1 (HV k Hkn2)) (proj1 H)).
  - intro HaddB. destruct HaddB.
    + have Hkn1 : (k >= n1) by lia. apply ((proj2 (HB k Hkn1 y)) H).
    + have Hkn2 : (k >= n2) by lia. apply ((proj2 (HV k Hkn2)) (proj1 H)).
Qed.

Lemma loc_trans_B_aux : forall (x : Names) (B : Names_cur -> Vis) (y : Names_cur), is_V_fin (fun (z: Names) => (B y z) \/ (B y x /\ B (names x) z)).
Proof.
  intros x B y. destruct (V_fin (B y)) as [ny HBy]. destruct (V_fin (B (names x))) as [nx HBx].
  exists (max ny nx). intros k Hk. split.
  - intro HByxz. destruct HByxz.
    + have Hky : k >= ny by lia. apply ((proj1 (HBy k Hky)) H).
    + have Hkx : k >= nx by lia. apply ((proj1 (HBx k Hkx)) (proj2 H)).
  - intro HByxz. destruct HByxz.
    + have Hky : k >= ny by lia. apply ((proj2 (HBy k Hky)) H).
    + have Hkx : k >= nx by lia. apply ((proj2 (HBx k Hkx)) (proj2 H)). 
Qed.

Definition loc_trans_B (x : Names) (dom : Names_cur -> Prop) (B: Names_cur-> Vis) : Names_cur -> Vis :=
  fun (y : Names_cur) => {| V := fun (z : Names) => (B y z) \/ (B y x /\ B (names x) z); V_fin := loc_trans_B_aux x B y;|}.

Lemma loc_trans_codom_fin  : forall (x : Names) (dom : Names_cur -> Prop) (B : Names_cur -> Vis), is_codom_fin B -> is_codom_fin (loc_trans_B x dom B).
Proof.
  intros x dom B HB. destruct HB as [n HB]. exists n. intros k Hk y. split.
  - intro HtrB. destruct HtrB. apply ((proj1 (HB k Hk y)) H). destruct H. apply ((proj1 (HB k Hk (names x))) H0).
  - intro HtrB. destruct HtrB. apply ((proj2 (HB k Hk y)) H). destruct H. apply ((proj2 (HB k Hk (names x))) H0).
Qed.

Lemma trans_minus_vis_B_aux : forall (x : Names) (B: Names_cur -> Vis) (y : Names_cur), is_V_fin (fun (z: Names) => (B y z) /\ (y <> (names x)) /\ (z <> x)).
Proof.
  intros x B y. destruct (V_fin (B y)) as [n HBy]. exists n. intros k Hk. split.
  - intro HBtm. apply ((proj1 (HBy k Hk)) (proj1 HBtm)).
  - intro HBtm. apply ((proj2 (HBy k Hk)) (proj1 HBtm)).
Qed.

Definition trans_minus_vis_B (x : Names) (dom : Names_cur -> Prop) (B: Names_cur -> Vis) : Names_cur -> Vis :=
  let B1 := loc_trans_B x dom B in
  fun (y : Names_cur) => {| V:= fun (z : Names) => (B1 y z) /\ (y <> (names x)) /\ (z <> x); V_fin := trans_minus_vis_B_aux x B1 y; |}.

Lemma trans_minus_codom_fin : forall (x : Names) (dom : Names_cur -> Prop) (B : Names_cur -> Vis), is_codom_fin B -> is_codom_fin (trans_minus_vis_B x dom B).
Proof.
  intros x dom B HB. destruct (loc_trans_codom_fin x dom B HB) as [n HtrB].
  exists n. intros k Hk y. split.
  - intro HtrmB. destruct HtrmB. apply ((proj1 (HtrB k Hk y)) H).
  - intro HtrmB. destruct HtrmB. apply ((proj2 (HtrB k Hk y)) H).
Qed.

Definition vis_union_B (B1 B2 : Names_cur -> Vis) : Names_cur -> Vis := fun (x : Names_cur) => union_V (B1 x) (B2 x).                                                                        
Lemma union_codom_fin : forall (B1 B2 : Names_cur -> Vis), is_codom_fin B1 -> is_codom_fin B2 -> is_codom_fin (vis_union_B B1 B2).
Proof.
  intros B1 B2 HB1 HB2. destruct HB1 as [n1 HB1]. destruct HB2 as [n2 HB2].
  exists (max n1 n2). intros k Hk x. split.
  - intro Hunion. destruct Hunion.
    + have Hkn1 : k >= n1 by lia. apply ((proj1 (HB1 k Hkn1 x)) H).
    + have Hkn2 : k >= n2 by lia. apply ((proj1 (HB2 k Hkn2 x)) H).
  - intro Hunion. destruct Hunion.
    + have Hkn1 : k >= n1 by lia. apply ((proj2 (HB1 k Hkn1 x)) H).
    + have Hkn2 : k >= n2 by lia. apply ((proj2 (HB2 k Hkn2 x)) H). 
Qed.

Definition vis_filter_B (B : Names_cur -> Vis) (x : Names) : Names_cur -> Vis := fun (y : Names_cur) => filter_V (B y) x.

Lemma filter_codom_fin : forall (B : Visfun) (x : Names), is_codom_fin (vis_filter_B B x).
Proof.
  move=> B x. destruct (codom_fin B) as [n Hn]. exists n. move=> k Hk y.
  split=> H. destruct H. by apply (proj1 (Hn k Hk y) H). destruct H. by apply (proj2 (Hn k Hk y) H).
Qed.

Lemma vis_rest_B_aux : forall (B : Names_cur -> Vis) (dom : Names_cur -> Prop) (x : Names_cur), is_V_fin (fun (y : Names) => (B x y /\ dom x)).
Proof.
  intros B dom x. destruct (V_fin (B x)) as [n HBx]. exists n. intros k Hk. split.
  - intro HB. apply ((proj1 (HBx k Hk)) (proj1 HB)).
  - intro HB. apply ((proj2 (HBx k Hk)) (proj1 HB)).
Qed.

Definition vis_rest_B (B : Names_cur -> Vis) (dom : Names_cur -> Prop) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V:= fun (y : Names) => (B x y /\ dom x); V_fin := vis_rest_B_aux B dom x ; |}.

Lemma rest_codom_fin : forall (B : Names_cur -> Vis) (dom : Names_cur -> Prop), is_codom_fin B -> is_codom_fin (vis_rest_B B dom).
Proof.
  intros B dom HB. destruct HB as [n HB]. exists n. intros k Hk x. split.
  - intro Hrest. destruct Hrest. apply ((proj1 (HB k Hk x)) H).
  - intro Hrest. destruct Hrest. apply ((proj2 (HB k Hk x)) H).
Qed.

Definition alpha_convert_B_aux1 (a b : Names) (B : Names_cur -> Vis) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V := alpha_convert_V a b (B x); V_fin := alpha_convert_V_fin a b (B x); |}.

Lemma alpha_convert_B_fin_aux2 : forall (a b : Names) (B : Names_cur -> Vis) (x : Names_cur), is_V_fin (fun (y : Names) => (x = (names b) /\ B (names a) y) \/ (x <> (names a) /\ B x y)).
Proof.
  intros a b B x. destruct (V_fin (B (names a))) as [na HBa]. destruct (V_fin (B x)) as [nx HBx].
  exists (max na nx). intros k Hk. split.
  -  intro. destruct H. have Hka : k >= na by lia. apply (proj1 (HBa k Hka) (proj2 H)).
        have Hkx : k >= nx by lia. apply (proj1 (HBx k Hkx) (proj2 H)).
  - intro. destruct H. have Hka : k >= na by lia. apply (proj2 (HBa k Hka) (proj2 H)).
        have Hkx : k >= nx by lia. apply (proj2 (HBx k Hkx) (proj2 H)).
Qed.
        
Definition alpha_convert_B_aux2 (a b : Names) (B : Names_cur -> Vis) : Names_cur -> Vis :=
  fun (x : Names_cur) => {| V := fun (y : Names) => (x = (names b) /\ B (names a) y) \/ (x <> (names a) /\ B x y);
                        V_fin := alpha_convert_B_fin_aux2 a b B x; |}.

Definition alpha_convert_B (a b : Names) (B : Names_cur -> Vis) : Names_cur -> Vis :=
  alpha_convert_B_aux2 a b (alpha_convert_B_aux1 a b B).
(* Relations and operations on visibility functions (under type Visfun) *)

Lemma minus_vis_dom (x : Names_cur) (B: Visfun) :
  let dom1 := minus_dom (dom B) x in
  let B1 := minus_vis_B x (dom B) B in
  forall x y, (B1 x y) -> dom1 x.
Proof.
  intros. split.
  - apply (_dom B x0 y). apply (proj1 H).
  - apply (proj2 H).
Qed.

Definition minus_vis (x : Names_cur) (B : Visfun) : Visfun :=
  {|
    dom := minus_dom (dom B) x;
    B := (minus_vis_B x (dom B) B);
    _dom := (minus_vis_dom x B);
    dom_fin := minus_dom_fin (dom B) x (dom_fin B);
    codom_fin := minus_codom_fin x (dom B) B (codom_fin B);
  |}.

Lemma add_vis_dom (x : Names_cur) (V: Vis) (B: Visfun) :
  let dom1 := add_dom (dom B) x in
  let B1 := add_vis_B x V (dom B) B in
  forall x y, (B1 x y) -> (dom1 x).

Proof.
  intros. case H.
  - intro. left. apply (_dom B x0 y). apply H0.
  - intro. right. apply (proj2 H0).
Qed.

Definition add_vis (x : Names_cur) (V: Vis) (B : Visfun) : Visfun:=
  {|
    dom := (add_dom (dom B) x);
    B := (add_vis_B x V  (dom B) B);
    _dom := (add_vis_dom x V B);
    dom_fin := add_dom_fin (dom B) x (dom_fin B);
    codom_fin := add_codom_fin x V (dom B) B (codom_fin B);
  |}.


Lemma trans_minus_vis_dom (x : Names) (B: Visfun) :
  let dom1 := minus_dom (dom B) (names x) in
  let B1 := trans_minus_vis_B x (dom B) B in
  forall x y, (B1 x y) -> (dom1 x).
Proof.
  intros. destruct H. split.
  - destruct H.
    + apply (_dom B x0 y H).
    + apply (_dom B x0 x (proj1 H)).
  - intro. apply (proj1 H0 (eq_sym H1)).
Qed.

Definition trans_minus_vis (x : Names) (B : Visfun) : Visfun:=
  {|
    dom := minus_dom (dom B) (names x);
    B:= (trans_minus_vis_B x (dom B) B);
    _dom := (trans_minus_vis_dom x B);
    dom_fin := minus_dom_fin (dom B) (names x) (dom_fin B);
    codom_fin := trans_minus_codom_fin x (dom B) B (codom_fin B);
  |}.

Definition Visfun_eq (B1 : Visfun) (B2 : Visfun) : Prop := (dom_eq (dom B1) (dom B2)) /\ (forall (x : Names_cur), forall (y: Names), B1 x y <-> B2 x y). (* ok *)

Lemma Visfun_eq_refl : forall (B : Visfun), Visfun_eq B B.
Proof.
  intros. split. apply (dom_eq_refl (dom B0)).
  intros. split. trivial. trivial.
Qed.

Lemma Visfun_eq_sym : forall (B1 B2 : Visfun), Visfun_eq B1 B2 -> Visfun_eq B2 B1.
Proof.
  intros. split. apply (dom_eq_sym (dom B1) (dom B2) (proj1 H)).
  intros. split. apply (proj2 ((proj2 H) x y)). apply (proj1 ((proj2 H) x y)).
Qed.

Lemma Visfun_eq_trans : forall (B1 B2 B3 : Visfun), Visfun_eq B1 B2 -> Visfun_eq B2 B3 -> Visfun_eq B1 B3.
Proof.
  intros. split. apply (dom_eq_trans (dom B1) (dom B2) (dom B3) (proj1 H) (proj1 H0)).
  intros. split. intro. apply (proj1 (proj2 H0 x y) (proj1 (proj2 H x y) H1)).
  intro. apply (proj2 (proj2 H x y) (proj2 (proj2 H0 x y) H1)).
Qed.
Axiom eq_visfun : forall (B1 B2 : Visfun), Visfun_eq B1 B2 -> B1 = B2.
  
Lemma vis_union_dom (B1 : Visfun) (B2 : Visfun) :
  let dom3 := dom_union (dom B1) (dom B2) in
  let B3 := vis_union_B B1 B2 in
  forall (x : Names_cur) (y : Names), (B3 x y) -> dom3 x.
Proof.
  intros. destruct H.
  - left. apply (_dom B1 x y H).
  - right. apply (_dom B2 x y H).
Qed.

Definition union_vis (B1 : Visfun) (B2 : Visfun) : Visfun :=
  {|
    dom := dom_union (dom B1) (dom B2);
    B := vis_union_B B1 B2;
    _dom := vis_union_dom B1 B2;
    dom_fin := dom_union_fin (dom B1) (dom B2) (dom_fin B1) (dom_fin B2);
    codom_fin := union_codom_fin B1 B2 (codom_fin B1) (codom_fin B2);
  |}.

Lemma vis_filter_dom (B : Visfun) (x : Names) :
  let B1 := vis_filter_B B x in
  forall (y : Names_cur) (z : Names), (B1 y z) -> dom B y.
Proof.
  move=> B1 y z Byz. destruct Byz. by apply (_dom B y z H).
Qed.


Definition filter_vis (B : Visfun) (x : Names) : Visfun :=
  {|
    dom := dom B;
    B := vis_filter_B B x;
    _dom := vis_filter_dom B x;
    dom_fin := dom_fin B;
    codom_fin := filter_codom_fin B x; |}.

Lemma union_vis_comm : forall (B1 B2 : Visfun), union_vis B1 B2 = union_vis B2 B1.
Proof.
  intros. apply eq_visfun. split. apply dom_union_comm.
  intros. simpl. unfold vis_union_B. split.
  - intro H. destruct H. by right. by left.
  - intro H. destruct H. by right. by left.
Qed.


Lemma vis_rest_dom (B : Visfun) (dom1 : Names_cur -> Prop) :
  let B1 := vis_rest_B B dom1 in
  forall (x : Names_cur) (y : Names), (B1 x y) -> dom1 x.
Proof.
  unfold vis_rest_B. intros. simpl in H. easy.
Qed.

Definition rest_vis (B : Visfun) (d : Names_cur -> Prop) (dfin : is_dom_fin d) : Visfun :=
  {|
    dom := d;
    B := vis_rest_B B d;
    _dom := vis_rest_dom B d;
    dom_fin := dfin;
    codom_fin := rest_codom_fin B d (codom_fin B);
  |}.

Lemma rest_union_vis : forall (B1 B2 : Visfun), dom_disj (dom B1) (dom B2) -> rest_vis (union_vis B1 B2) (dom B1) (dom_fin B1) = B1.
Proof.
  intros. apply eq_visfun. split. by apply dom_eq_refl.
  intros. split.
  - intro. simpl in H0. destruct H0. destruct H0. apply H0. have H2 : dom B2 x by apply (_dom B2 x y).
    exfalso. apply (H x H1 H2).
  - intro. simpl. unfold vis_rest_B. split. by left. apply (_dom B1 x y H0).
Qed.

Lemma alpha_convert_aux_dom (a b : Names) (B : Visfun) :
  let dom1 := alpha_convert_dom a b (dom B) in
  let B1 := alpha_convert_B a b B in
  forall (x : Names_cur) (y : Names), (B1 x y) -> dom1 x.
Proof.
  intros. unfold B1 in H. simpl in H. unfold dom1. destruct H.
  - destruct H. destruct H0.
    + destruct H0. have Ha : dom B (names a) by apply (_dom B (names a) y H1). by left.
    + destruct H0. have Ha : dom B (names a) by apply (_dom B (names a) a H1). by left.
  - destruct H. destruct H0.
    +  destruct H0. have Hx : dom B x by apply (_dom B x y H1). by right.
    +  destruct H0. have Hx : dom B x by apply (_dom B x a H1). by right.
Qed.


Lemma alpha_convert_codom_fin (a b : Names) (B : Visfun) : is_codom_fin (alpha_convert_B a b B).
Proof.
  destruct (codom_fin B) as [n HB]. destruct b as [b | p].
  - destruct (given_nser b).
    + exists n. intros k Hk x. split.
      * intro Halpha. destruct Halpha. destruct H0. destruct H1.
        apply ((proj1 (HB k Hk (names a))) (proj2 H1)). destruct H1. inversion H1. apply (H k H4).
        destruct H0. destruct H1. apply ((proj1 (HB k Hk x)) (proj2 H1)). destruct H1. inversion H1. apply (H k H4).
      * intro Halpha. destruct Halpha as [Halpha | Halpha]. destruct Halpha. destruct H1.
        apply ((proj2 (HB k Hk (names a))) (proj2 H1)). destruct H1. inversion H1.
        destruct Halpha. destruct H1. apply ((proj2 (HB k Hk x)) (proj2 H1)). destruct H1. inversion H1.
    + destruct H as [n1 Hn1b]. exists (max n (n1+1)). intros k Hk x.  split.
      * intro Halpha. destruct Halpha. 
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj1 (HB k Hkn (names a))) (proj2 H0)).
           destruct H0. inversion H0. rewrite (eq_sym H3) in Hn1b. have Hn1k : n1 = k by apply (infinite_nser n1 k). lia.
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj1 (HB k Hkn x)) (proj2 H0)).
           destruct H0. inversion H0. rewrite (eq_sym H3) in Hn1b. have Hn1k : n1 = k by apply (infinite_nser n1 k). lia.
      * intro Halpha. destruct Halpha.
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj2 (HB k Hkn (names a))) (proj2 H0)).
           destruct H0. inversion H0.
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj2 (HB k Hkn x)) (proj2 H0)).
           destruct H0. inversion H0.
  - destruct (given_ncon p).
    + exists n. intros k Hk x. split.
      * intro Halpha. destruct Halpha as [Halpha | Halpha]. destruct Halpha. destruct H1.
        apply ((proj1 (HB k Hk (names a))) (proj2 H1)). destruct H1. inversion H1.
        destruct Halpha. destruct H1. apply ((proj1 (HB k Hk x)) (proj2 H1)). destruct H1. inversion H1.
      * intro Halpha. destruct Halpha. destruct H0. destruct H1.
        apply ((proj2 (HB k Hk (names a))) (proj2 H1)). destruct H1. inversion H1. apply (H k H4).
        destruct H0. destruct H1. apply ((proj2 (HB k Hk x)) (proj2 H1)). destruct H1. inversion H1. apply (H k H4).
    + destruct H as [n1 Hn1b]. exists (max n (n1+1)). intros k Hk x.  split.
      * intro Halpha. destruct Halpha.
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj1 (HB k Hkn (names a))) (proj2 H0)).
           destruct H0. inversion H0.
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj1 (HB k Hkn x)) (proj2 H0)).
           destruct H0. inversion H0.
      * intro Halpha. destruct Halpha. 
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj2 (HB k Hkn (names a))) (proj2 H0)).
           destruct H0. inversion H0. rewrite (eq_sym H3) in Hn1b. have Hn1k : n1 = k by apply (infinite_ncon n1 k). lia.
        ++ have Hkn : k >=n by lia. destruct H. destruct H0. apply ((proj2 (HB k Hkn x)) (proj2 H0)).
           destruct H0. inversion H0. rewrite (eq_sym H3) in Hn1b. have Hn1k : n1 = k by apply (infinite_ncon n1 k). lia.
Qed.

Definition alpha_convert_vis (a b : Names) (B : Visfun) :=
  {|
    dom := (alpha_convert_dom a b (dom B));
    B := alpha_convert_B a b B;
    _dom := alpha_convert_aux_dom a b B;
    dom_fin := (alpha_convert_dom_fin a b (dom B) (dom_fin B));
    codom_fin := alpha_convert_codom_fin a b B;
  |}.

Definition erase_B (x : Names) (B : Visfun) := minus_vis_B (names x) (minus_dom (dom B) (names x) ) (fun (y : Names_cur) => minus_V (B y) x ).
Lemma erase_dom_aux : forall (x : Names) (B : Visfun),
    let B1 :=  erase_B x B in
    forall (y : Names_cur) (z : Names), B1 y z -> (minus_dom (dom B) (names x)) y.
Proof.
  move=> x B B1 y z [[H1 _] H2]. split. by apply (_dom B y z H1). easy.
Qed.


Lemma erase_codom_fin : forall (x : Names) (B : Visfun), is_codom_fin (erase_B x B).
Proof.
  move=> x B. destruct (codom_fin B) as [n HB]. exists n.
  move=> k Hk y; split=> [Hser | Hcon].
  - destruct Hser as [Hm _]. destruct Hm as [HBk _]. apply (proj1 (HB k Hk y) HBk).
  - destruct Hcon as [Hm _]. destruct Hm as [HBk _]. apply (proj2 (HB k Hk y) HBk).
Qed.

Definition erase_vis (x : Names) (B : Visfun) :=
  {|
    dom := minus_dom  (dom B) (names x);
    B := erase_B x B;
    _dom := erase_dom_aux x B;
    dom_fin := minus_dom_fin (dom B) (names x) (dom_fin B);
    codom_fin := erase_codom_fin x B; |}.

(* a lemma linking equivalence of Vis and equality on Visfun *)
Lemma add_equiv_vis (x : Names_cur) (V1 V2 : Vis) (B : Visfun) : (forall (x : Names), V1 x  <-> V2 x) -> add_vis x V1 B = add_vis x V2 B.
Proof.
  move=> HV. apply eq_visfun. split.
  - simpl. by apply dom_eq_refl.
  - move=> y z. split.
    + intro. destruct H. by left.  simpl in H. right. simpl. split. apply (proj1 (HV z) (proj1 H)). by apply (proj2 H).
    + intro. destruct H. by left.  simpl in H. right. simpl. split. apply (proj2 (HV z) (proj1 H)). by apply (proj2 H).
Qed.
(* Lemmas between alpha convesion and other operations on Visfun *)

Definition is_fresh_vis (a : Names) (B : Visfun) := ~(dom B (names a)) /\ (forall (x : Names_cur), ~(B x a)).
Lemma alpha_convert_idem : forall (a : Names) (B : Visfun), alpha_convert_vis a a B = B.
Proof.
  intros a B. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H. 
      * destruct H. by rewrite H. 
      * by apply (proj2 H).
    + intro. destruct x as [| x].
      *  right. by split.
      * destruct a as [a | p].
        ++ destruct x as [b | q]. case (Nser_dec a b).
           ** intro.  left. split. by rewrite H0.  by rewrite H0.
           ** intro. right. split. intro. inversion H1. by rewrite H3 in H0. assumption.
           ** right. by split.
        ++ destruct x as [b | q]. right. by split.  case (Ncon_dec p q).
           ** intro. rewrite H0. left. by split.
           ** intro. right. split. intro. inversion H1. by rewrite H3 in H0. easy.
  - intros x y. split.
    + intro. destruct H. destruct H. destruct H0. rewrite H. by destruct H0.
      destruct H0. rewrite H0. by rewrite H.
      destruct H. destruct H0. by destruct H0. destruct H0. by rewrite H0.
    + intro. destruct x as [|x].
      * right. split. easy.
        destruct a as [a | p]. 
        ** destruct y as [c | r].
           ++ case (Nser_dec c a). intro. right. rewrite (eq_sym H0). by split.
              intro. left. split. intro. by inversion H1. easy.
           ++ left. by split.
              ** destruct y as [c|r].
           ++ left. by split.
           ++ case (Ncon_dec r p). intro. right. rewrite (eq_sym H0). by split.
              intro. left. split. intro. by inversion H1. easy.
      * destruct x as [b | q].
        ++ destruct a as [a | p].
           ** case (Nser_dec b a).
              +++ intro. rewrite H0 in H. rewrite H0. left. split. easy.
                  destruct y as [c | r].
                  *** case (Nser_dec c a). intro. right. rewrite H1. split. easy. by rewrite H1 in H.
                      intro. left. split. intro. by inversion H2. easy.
                  *** left. by split.
              +++ intro. right. split. intro. by inversion H1.
                  destruct y as [c |r].
                  *** case (Nser_dec c a). intro. rewrite H1 in H. right. rewrite H1. by split.
                      intro. left. split. intro. by inversion H2. easy.
                  *** left. by split.
           ** right. split. easy. destruct y as [c|r].
              +++ left. by split.
              +++ case (Ncon_dec r p). intro. right. rewrite H0. rewrite H0 in H. by split.
                  intro. left. split. intro. by inversion H1. easy.
        ++ destruct a as [a |p].
           ** right. split. easy. destruct y as [c | r].
              +++ case (Nser_dec c a).
                  *** intro. rewrite H0 in H. rewrite H0. right. by split.
                  *** intro. left. split. intro. by inversion H1. easy.
              +++ left. by split.
           ** case (Ncon_dec q p).
              +++ intro. rewrite H0 in H. rewrite H0. left. split. easy.
                  destruct y as [c |r ].
                  ***  left. by split. 
                  *** case (Ncon_dec r p). intro. rewrite H1 in H. rewrite H1. right. by split.
                      intro. left. split. intro. by inversion H2. easy.
              +++ intro. right. split. intro. by inversion H1.
                  destruct y as [c|r].
                  *** left. by split.
                  *** case (Ncon_dec r p). intro. rewrite H1 in H. rewrite H1. right. by split.
                      intro. left. split. intro. by inversion H2. easy.
Qed.                      
        
  
Lemma alpha_add_vis_replace : forall (a b : Names) (V : Vis) (B : Visfun), ~(dom B (names a)) -> alpha_convert_vis a b (add_vis (names a) V B) = add_vis (names b) (alpha_convert_V a b V) (alpha_convert_vis a b B).
Proof.
  intros a b V B Hda. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H.
      *  destruct H. rewrite H. by right.
      * destruct H. destruct H0.
        ++ left. right. by split.
        ++ exfalso. apply (H (eq_sym H0)).
    + intro. destruct H.
      * destruct H.
        ++  destruct H. left. split. easy. by right.
        ++ destruct H. right. split. easy. by left.
      * left. split. easy. by right.
  - intros x y. split.
    + intro. destruct H.
      *  destruct H. destruct H0.
         ++ destruct H0.  destruct H1.
            ** exfalso. apply (Hda (_dom B (names a) y H1)).
            ** simpl in H1. right. simpl. split. left. destruct H1. by split. easy.
         ++ destruct H0. destruct H1.
            ** exfalso. apply (Hda (_dom B (names a) a H1)).
            ** simpl in H1. destruct H1. right. simpl. split. right. by split. easy.
      * destruct H. destruct H0.
        ++  destruct H0. destruct H1.
            ** left. right. split. easy. left. by split.
            ** simpl in H1. destruct H1. exfalso. apply (H (eq_sym H2)).
        ++ destruct H0. destruct H1.
           ** left. right. split. easy. right. by split.
           ** simpl in H1. destruct H1. exfalso. apply (H (eq_sym H2)).
    + intro. destruct H.
      * destruct H.
        ++  destruct H. destruct H0.
            ** destruct H0. exfalso. apply (Hda (_dom B (names a) y H1)).
            ** destruct H0. exfalso. apply (Hda (_dom B (names a) a H1)).
        ++ destruct H. destruct H0.
           ** destruct H0. right. split. easy. left. split. easy. by left.
           ** destruct H0. right. split. easy. right. split. easy. by left.
      * simpl in H. destruct H. destruct H.
        ++ destruct H. left. split. easy. left. split. easy. right. simpl. by split.
        ++ destruct H. left. split. easy. right. split. easy. right. simpl. by split.
Qed.
      
Lemma alpha_add_vis_leave : forall (a b : Names) (x : Names_cur ) (V : Vis) (B : Visfun), (x <> (names a)) -> alpha_convert_vis a b (add_vis x V B) = add_vis x (alpha_convert_V a b V) (alpha_convert_vis a b B).
Proof.
  intros a b x V B Hxa. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H.
      * destruct H. destruct H0.
        ++  left. left. by split.
        ++ contradiction.
      * destruct H. destruct H0.
        ++ left. right. by split.
        ++ by right.
    + intro. destruct H.
      * destruct H.
        ++  destruct H. left. split. easy. by left.
        ++ destruct H. right. split. easy. by left.
      *  right. split. rewrite H in Hxa. easy. rewrite H. by right. 
  - intros y z. split.
    + intro. destruct H.
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1.
           ** left. left. split. easy. left. by split.
           ** simpl in H1. destruct H1. easy.
        ++ destruct H0. destruct H1.
           ** left. left. split. easy. right. by split.
           ** simpl in H1. destruct H1. easy.
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1.
           ** left. right. split. easy. left. by split.
           ** simpl in H1. right. simpl. destruct H1. split. left. by split. easy.
        ++ destruct H0. destruct H1.
           ** left. right. split. easy. right. by split.
           ** simpl in H1. destruct H1. right. simpl. split. right. by split. easy.
    + intro. destruct H.
      * destruct H.
        ++ destruct H. destruct H0.
           ** destruct H0. left. split. easy. left. split. easy. by left.
           ** destruct H0. left. split. easy. right. split. easy. by left.
        ++ destruct H. destruct H0.
           ** destruct H0. right. split. easy. left. split. easy. by left.
           ** destruct H0. right. split. easy. right. split. easy. by left.
      * simpl in H. destruct H. destruct H.
        ++ destruct H. right. split. by rewrite H0 in Hxa. left. split. easy. right. simpl. by split.
        ++ destruct H. right. split. by rewrite H0 in Hxa. right. split. easy. right. simpl. by split.
Qed.
      


Lemma alpha_trans_minus_vis_replace : forall (a b : Names) (B : Visfun), is_fresh_vis b B -> alpha_convert_vis a b (trans_minus_vis a B) = trans_minus_vis b (alpha_convert_vis a b B).
Proof.
  intros a b B [Hdb Hcdb]. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H.
      * destruct H. destruct H0.  easy.
      * destruct H. destruct H0. split. right. by split. intro. by rewrite  H2 in Hdb.
    + intro. destruct H. destruct H.
      * destruct H. by rewrite H in H0.
      * destruct H. right. split. easy. split. easy. intro. by rewrite H2 in H.
  - intros x y . split.
    + intro. destruct H.
      * destruct H. destruct H0.
        ++  destruct H0. destruct H1. destruct H2. easy.
        ++ destruct H0. destruct H1. destruct H2. easy.
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1. destruct H1.
           ** split.
              +++ left. right. split. easy. left. by split.
              +++ split. intro. rewrite (eq_sym H3) in Hdb. apply (Hdb (_dom B x y H1)).
                  intro. rewrite H3 in H1. apply (Hcdb x H1).
           ** destruct H1. destruct H2. split.
              +++ right. split. right. split. easy. right. by split. left. split. easy. left. by split.
              +++ split. intro. rewrite H5 in H1. apply (Hdb (_dom B (names b) a H1)).
                  intro. rewrite H5 in H3. apply (Hcdb (names a) H3).
        ++ destruct H0. destruct H1. destruct H2. easy.
    + intro. destruct H. destruct H.
      * destruct H.
        ++ destruct H. destruct H0. easy.
        ++ destruct H. destruct H1.
           ** right. split. easy. left. split. easy. split.
              +++ left.  easy.
              +++ split. easy. easy.
           ** destruct H1. destruct H0. easy.
      * destruct H. destruct H.
        ++ destruct H. destruct H0. easy.
        ++ destruct H. destruct H2.
           **  destruct H2. exfalso. apply (Hcdb x H3).
           ** destruct H2. destruct H1.
              +++ destruct H1. destruct H4.
                  *** destruct H4. right. split. easy. left. split. easy. split.
                      ++++ right. by split.
                      ++++ by split.
                  *** destruct H4. destruct H0. easy.
              +++ destruct H1. destruct H0. destruct H4.
                  *** destruct H4. exfalso. apply (Hdb (_dom B _ _  H6)).
                  *** destruct H4. exfalso. apply (Hdb (_dom B _ _  H6)).
Qed.

Lemma alpha_trans_minus_vis_leave : forall (a b x : Names) (B : Visfun), (x <> b) -> (x <> a) -> alpha_convert_vis a b (trans_minus_vis x B) = trans_minus_vis x (alpha_convert_vis a b B).
Proof.
  intros a b x B Hxb Hxa. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H.
      *  destruct H. destruct H0. split.
         ++ left. by split.
         ++ rewrite H. intro. by inversion H2.
      * destruct H. destruct H0. split.
        ++ right. by split.
        ++ easy.
    + intro. destruct H. destruct H.
      * destruct H. left. split. easy. split. easy. intro. by inversion H2.
      * destruct H. right. split. easy. by split.
  - intros y z. split. 
    + intro. destruct H.
      *  destruct H. destruct H0.
         ++ destruct H0. destruct H1. destruct H1.
            ** split.
               +++ left. left. split. easy. by left.
               +++ split. intro. rewrite H3 in H. by inversion H.
                   by destruct H2.
            ** split.
               +++ right. destruct H1. split.
                   *** left. split. easy. left.  by split.
                   *** right. split. intro. by inversion H4. left. by split.
               +++ destruct H2. split. intro. rewrite H4 in H. by inversion H. easy.
         ++ destruct H0. destruct H1. destruct H1.
            ** split.
               +++ left. left. split. easy. right. by split.
               +++ split. intro. rewrite H3 in H. by inversion H. rewrite H0. easy.
            ** split.
               +++ right.  destruct H1. destruct H2. split.
                   *** left. split. easy. left. by split.
                   *** right. split. intro. by inversion H5. right. by split.
               +++ split. intro. rewrite H3 in H. by inversion H. intro. by rewrite H3 in H0.
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1. destruct H2. destruct H1.
           ** split.
              +++ left. right. split. easy. left. by split.
              +++ by split.
           ** split. destruct H1. 
              +++ right. split.
                  *** right. split. easy. left. by split.
                  *** right. split. intro. by inversion H5. left. by split.
              +++ by split.
        ++ destruct H0. destruct H1. destruct H2. destruct H1.
           ** split.
              +++ left. right. split. easy. right. by split.
              +++ split. easy. intro. by rewrite H4 in H0.
           ** destruct H1. split.
              +++ right. split. right. split. easy. left. by split. right. split. intro. by inversion H5.
                  right. by split.
              +++ split. easy. intro. by rewrite H5 in H0.
    + intro. destruct H. destruct H0. destruct H.
      * destruct H.
        ++ destruct H. destruct H2.
           ** destruct H2. left. split. easy. left. split.  easy. split.
              +++ by left.
              +++ split. intro. inversion H4. by rewrite H6 in Hxa. easy.
           ** destruct H2. left. split. easy. right. split. easy. split. by left.
              split. intro. inversion H4. apply (Hxa (eq_sym H6)). easy.
        ++ destruct H. destruct H2.
           ** destruct H2. right. split. easy. left. split. easy. split.
              by left. by split.
           ** destruct H2. right. split. easy. right. split. easy. split. by left. split. easy. easy.
      * destruct H. destruct H.
        ++ destruct H. destruct H3.
           ** destruct H3. destruct H2.
              +++ destruct H2. by inversion H2.
              +++ destruct H2. destruct H5.
                  *** destruct H5. left. split. easy. left. split. easy. split.
                      ++++ right. by split.
                      ++++ split. easy. easy.
                  *** destruct H5. left. split. easy. right. split. easy. split. right. by split. split. easy. easy.
           ** destruct H3. contradiction.
        ++ destruct H. destruct H3.
           **  destruct H3. destruct H2.
               +++  destruct H2. by inversion H2.
               +++ destruct H2. destruct H5.
                   *** destruct H5. right. split. easy. left. split. easy. split. right. by split.
                       by split.
                   *** destruct H5. right. split. easy. right. split. easy. split. right. by split. split.
                       easy. easy.
           ** destruct H3. easy.
Qed.
                     
Lemma alpha_rest_vis : forall (a b : Names) (d : Names_cur -> Prop) (dfin : is_dom_fin d) (B : Visfun),
    ~(d (names b)) -> is_fresh_vis b B -> alpha_convert_vis a b (rest_vis B d dfin) = rest_vis (alpha_convert_vis a b B) (alpha_convert_dom a b d) (alpha_convert_dom_fin a b d dfin).
Proof.
  intros a b d dfin B Hdb [HdBb HcBb]. apply eq_visfun. split.
  - simpl. apply dom_eq_refl.
  - intros. split.
    + intro. destruct H.
      *  destruct H. destruct H0.
         ++ destruct H0. destruct H1. split.
            ** left. split. easy. left. by split.
            ** left. by split.
         ++ destruct H0. destruct H1. split.
            ** left. split. easy. right. by split.
            ** left. by split.
      * destruct H. destruct H0.
        ++  destruct H0. destruct H1. split.
            ** right. split. easy. left. by split.
            ** right. by split.
        ++ destruct H0. destruct H1. split.
           **  right. split. easy. right. by split.
           ** right. by split.
    + intro. destruct H. destruct H.
      *  destruct H. destruct H1.
         ++  destruct H1. destruct H0.
             **  destruct H0. left. split. easy. left. split. easy. by split.
             ** destruct H0. by rewrite H in H3.
         ++ destruct H1. destruct H0.
            ** destruct H0. left. split. easy. right. split. easy. by split.
            ** destruct H0. by rewrite H in H3.
      * destruct H. destruct H1.
        ++ destruct H1. destruct H0.
           ** destruct H0. rewrite H0 in H2. exfalso. by apply (HdBb (_dom B _ _ H2)).
           ** destruct H0. right. split. easy. left. split. easy. by split.
        ++ destruct H1. destruct H0.
           ** destruct H0. rewrite H0 in H2. exfalso. apply (HdBb (_dom B _ _ H2)).
           ** destruct H0. right. split. easy. right. split. easy. by split.
Qed.
  
Lemma alpha_union_vis : forall (a b : Names) (B1 B2 : Visfun),
    alpha_convert_vis a b (union_vis B1 B2) = union_vis (alpha_convert_vis a b B1) (alpha_convert_vis a b B2).
Proof.
  intros a b B1 B2. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H.
      * destruct H. destruct H0.
        ++  left. left. by split.
        ++ right. left. by split.
      * destruct H. destruct H0.
        ++ left. right. by split.
        ++ right. right. by split.
    + intro. destruct H.
      * destruct H.
        ++ destruct H. left. split. easy. by left.
        ++ destruct H. right. split. easy. by left.
      * destruct H.
        ++ destruct H. left. split. easy. by right.
        ++ destruct H. right. split. easy. by right.
  - intros x y. split.
    + intro. destruct H.
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1.
           ** left. left. split. easy. left. by split.
           ** right. left. split. easy. left. by split.
        ++ destruct H0. destruct H1.
           ** left. left. split. easy. right. by split.
           ** right. left. split. easy. right. by split.
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1.
           ** left. right. split. easy. left. by split.
           ** right. right. split. easy. left. by split.
        ++ destruct H0. destruct H1.
           ** left. right. split. easy. right. by split.
           ** right. right. split. easy. right. by split.
    + intro. destruct H.
      * destruct H.
        ++  destruct H. destruct H0.
            ** destruct H0. left. split. easy. left. split. easy. by left.
            ** destruct H0. left. split. easy. right. split. easy. by left.
        ++  destruct H. destruct H0.
            ** destruct H0. right. split. easy. left. split. easy. by left.
            ** destruct H0. right. split. easy. right. split. easy. by left.
      * destruct H.
        ++  destruct H. destruct H0.
            ** destruct H0. left. split. easy. left. split. easy. by right.
            ** destruct H0. left. split. easy. right. split. easy. by right.
        ++  destruct H. destruct H0.
            ** destruct H0. right. split. easy. left. split. easy. by right.
            ** destruct H0. right. split. easy. right. split. easy. by right.
Qed.

Lemma alpha_alpha_vis : forall (a b c : Names) (B : Visfun), is_fresh_vis b B -> alpha_convert_vis b c (alpha_convert_vis a b B) = alpha_convert_vis a c B.
Proof.
  move=> a b c B HfB. apply eq_visfun. split.
  - simpl. move=> x; split; move=> Halph.
    + destruct Halph. destruct H. destruct H0. destruct H0. left. by split.
      destruct H0. by destruct HfB. destruct H. destruct H0. by destruct H0.
      destruct H0. right. by split.
    + destruct Halph. destruct H. left. split. assumption. left. by split.
      destruct H. right. split. intro. rewrite H1 in H0. by destruct HfB. right. by split.
  - move=> x y; split; move=> Halph.
    + destruct Halph.
      * destruct H. destruct H0. destruct H0. destruct H1. destruct H1. destruct H2. destruct H2.
        left. split; try assumption. left. by split.
        by destruct H2.  destruct H1. destruct H2. destruct H2. exfalso. apply (proj1 HfB). by apply (_dom B _ _ H3).
        destruct H2. exfalso. apply (proj1 HfB). by apply (_dom B _ _ H3).
        destruct H0. destruct H1. destruct H1. destruct H2. destruct H2. exfalso. by apply (proj2 HfB _ H3).
        destruct H2. left. split; try assumption. right. by split.
        destruct H1. destruct H2. destruct H2. exfalso. apply (proj1 HfB).  by apply (_dom B _ _ H3).
        destruct H2. exfalso. apply (proj1 HfB). by apply (_dom B _ _ H3).
      * destruct H. destruct H0.
        ++ destruct H0. destruct H1. by destruct H1. destruct H1. destruct H2. destruct H2.
           right. split; try assumption. left. by split. by destruct H2.
        ++ destruct H0. destruct H1. by destruct H1. destruct H1. destruct H2. destruct H2.
           exfalso. by apply (proj2 HfB _ H3). destruct H2. right. split; try assumption.
           right. by split.
    + destruct Halph.
      * destruct H. destruct H0.
        ++ destruct H0. left. split; try assumption. left. split.
           intro. rewrite H2 in H1. by apply (proj2 HfB _ H1). left. split; try easy.
           left. by split.
        ++ destruct H0. left. split; try assumption. right. split; try assumption. left. split; try easy.
           right. by split.
      * destruct H. destruct H0.
        ++ destruct H0. right. split.
           intro. rewrite H2 in H1. apply (proj1 HfB). apply (_dom B _ _ H1).
           left. split. intro. rewrite H2 in H1. apply (proj2 HfB _ H1). right. split. assumption.
           left. by split.
        ++ destruct H0. right. split. intro. rewrite H2 in H1. apply (proj1 HfB). apply (_dom B _ _ H1).
           right. split. assumption. right. split. assumption. right. by split.
Qed.

Lemma alpha_vis_invol : forall (a b : Names) (B : Visfun), is_fresh_vis b B -> alpha_convert_vis b a (alpha_convert_vis a b B) = B.
Proof.
  move=> a b B HfB. rewrite (alpha_alpha_vis a b a B HfB). apply alpha_convert_idem.
Qed.
  
Lemma erase_add_vis1 : forall (a : Names) (V : Vis) (B : Visfun), erase_vis a (add_vis (names a) V B) = erase_vis a B.
Proof.
  move=> a V B. apply eq_visfun. split.
  - simpl. split.
    + intro. destruct H. destruct H; [by split | easy].
    + intro. destruct H. split; [by left | assumption].
  - move=> x y. split=> [H|H].
    + destruct H. destruct H. destruct H. split. by split. easy. simpl in H. by destruct H.
    + destruct H. destruct H. split; [|assumption]. split; [by left | assumption].
Qed.

Lemma erase_add_vis2 : forall (a : Names) (b : Names_cur) (V : Vis) (B : Visfun),
    (b <> (names a)) -> erase_vis a (add_vis b V B) = add_vis b (minus_V V a) (erase_vis a B).
Proof.
  move=> a b V B Hba. apply eq_visfun. split.
  - split=> [Hd | Hd].
    + destruct Hd. destruct H. left. by split. by right.
    + destruct Hd. destruct H. split. by left. easy. split. by right. intro. rewrite (eq_sym H0) in H. by inversion H.
  - move=> x y; split=> [Her | Hadd].
    + destruct Her. destruct H. destruct H. left. split; try assumption. by split. destruct H.
      right. split; try assumption. by split.
    + destruct Hadd. destruct H. destruct H. split; [|assumption]. split; [|assumption]. by left.
      destruct H. destruct H. split. split;[|assumption].  right. by split. intro. rewrite (eq_sym H2) in H0. by inversion H0.
Qed.

Lemma erase_trans_minus : forall (a b : Names) (B : Visfun),
    b <> a -> erase_vis a (trans_minus_vis b B) = trans_minus_vis b (erase_vis a B).
Proof.
  move=> a b B Hba. apply eq_visfun. split.
  - split=> Hdom.
    + destruct Hdom. destruct H. split; [|assumption]. by split.
    + destruct Hdom. destruct H. split; [|assumption]. by split.
  - move=> x y; split=> [Her | Htrans].
    + destruct Her. destruct H. destruct H. destruct H.
      * destruct H2. split; [|by split].
        left. split; [|assumption]. by split.
      * destruct H. destruct H2. split; [| by split].
        right. split; split ; try assumption; try by split. intro. inversion H5. by rewrite H7 in Hba.
    + destruct Htrans. destruct H.
      * destruct H. destruct H. destruct H0. split; [|assumption].                                              
        split; [|assumption]. by split; [left | split].
      * destruct H. destruct H. destruct H. destruct H1. destruct H1. destruct H0. split; [|assumption].
        split;[|assumption]. by split; try right; split.
Qed.

Lemma erase_union_vis : forall (a : Names) (B1 B2 : Visfun),
    erase_vis a (union_vis B1 B2) = union_vis (erase_vis a B1) (erase_vis a B2).
Proof.
  move=> a B1 B2. apply eq_visfun. split.
  - simpl. split=> [Hmin | Hun].
    + destruct Hmin. destruct H. left. by split. right. by split.
    + destruct Hun. destruct H. split; [by left |assumption].
      destruct H. split; [by right |assumption].
  - move=> x y; split=> [Her | Hun].
    + destruct Her. destruct H. destruct H; [left |right]; by split; [split | assumption]. 
    + destruct Hun. by destruct H; destruct H;  split; [| assumption]; split; [|assumption]; left .
      by destruct H; destruct H;  split; [| assumption]; split; [|assumption]; right .
Qed.

Lemma erase_idem : forall (a : Names) (B : Visfun), ~(dom B (names a))
                                               -> (forall (x : Names_cur), ~(B x a)) -> B = erase_vis a B.
Proof.
  move=> a B Hf1 Hf2. apply eq_visfun. split.
  - split=> [Hd | Herd].
    + split; [assumption|]. intro. by rewrite H in Hf1.
    + by destruct Herd.
  - move=> x y. split=> [HB | HerB].
    split. split. assumption. intro. rewrite H in HB. apply (Hf2 x HB). intro. rewrite H in Hf1.
    apply (Hf1 (_dom B x y HB)). destruct HerB. by destruct H.
Qed.

Lemma fresh_erase : forall (x : Names) (B : Visfun), is_fresh_vis x (erase_vis x B).
Proof.
  move=> x B. split. intro. by destruct H.
  move=> y Her. destruct Her. by destruct H.
Qed.
(* Definition and lemmas on weakening relation *)
Definition Visfun_weak (B1 : Visfun) (B2 : Visfun) : Prop := (dom_eq (dom B1) (dom B2)) /\ (forall (x : Names_cur), forall (y: Names), B1 x y -> B2 x y).

Lemma weak_minus : forall (B1 B2 : Visfun) (x : Names_cur), Visfun_weak B1 B2 -> Visfun_weak (minus_vis x B1) (minus_vis x B2).
Proof. 
  unfold Visfun_weak. simpl. intros. destruct H.
  split.
  - unfold minus_dom. split.
    + intro. destruct H1. split. apply ((proj1 (H x0)) H1). easy.
    + intro. destruct H1. split. apply ((proj2 (H x0)) H1). easy.
  - unfold minus_vis_B. intros. destruct H1. split. apply (H0 x0 y H1). easy.
Qed.

Lemma weak_add : forall (B1 B2 : Visfun) (x : Names_cur) (V1 V2 : Vis) , Visfun_weak B1 B2 -> (forall (x :Names), V1 x -> V2 x) -> Visfun_weak (add_vis x V1 B1) (add_vis x V2 B2).
Proof.
  unfold Visfun_weak. simpl. intros. destruct H. split.
  - unfold add_dom. split.
    + intro. destruct H2. left. apply ((proj1 (H x0)) H2). by right. 
    + intro. destruct H2. left. apply ((proj2 (H x0)) H2). by right.
  - unfold add_vis_B. intros. destruct H2. left. apply (H1 x0 y H2).
    right. destruct H2. split. apply (H0 y H2). easy.
Qed.

Lemma weak_trans_minus : forall (B1 B2 : Visfun) (x : Names), Visfun_weak B1 B2 -> Visfun_weak (trans_minus_vis x B1) (trans_minus_vis x B2).
Proof.
  unfold Visfun_weak. simpl. intros. destruct H. split.
  - unfold minus_dom. split.
    + intro. destruct H1. split. apply ((proj1 (H x0)) H1). easy.
    + intro. destruct H1. split. apply ((proj2 (H x0)) H1). easy.
  - unfold trans_minus_vis_B. unfold loc_trans_B. intros. destruct H1.
    split. destruct H1. left. apply (H0 x0 y H1). right. destruct H1. split. apply (H0 x0 x H1).
    apply (H0 (names x) y H3). easy.
Qed.

Lemma weak_union : forall (B1 B2 B3 B4 : Visfun), Visfun_weak B1 B2 -> Visfun_weak B3 B4 -> Visfun_weak (union_vis B1 B3) (union_vis B2 B4).
  unfold Visfun_weak. simpl. intros. destruct H. destruct H0. split.
  - unfold dom_union. split.
    + intro. destruct H3. left. apply (proj1 (H x) H3). right. apply (proj1 (H0 x) H3).
    + intro. destruct H3. left. apply (proj2 (H x) H3). right. apply (proj2 (H0 x) H3).
  - unfold vis_union_B. intros. destruct H3. left. apply (H1 x y H3). right. apply (H2 x y H3).
Qed.

Lemma weak_rest : forall (B1 B2 : Visfun) (dom : Names_cur -> Prop) (domfin : is_dom_fin dom), Visfun_weak B1 B2 -> Visfun_weak (rest_vis B1 dom domfin) (rest_vis B2 dom domfin).
Proof.
  intros. split. simpl. apply dom_eq_refl.
  intros. destruct H0. split. apply ((proj2 H) x y H0). apply H1.
Qed.

Lemma weak_alpha_convert : forall (a b : Names) (B1 B2 : Visfun), Visfun_weak B1 B2 -> Visfun_weak (alpha_convert_vis a b B1) (alpha_convert_vis a b B2).
Proof.
  intros a b B1 B2 HB1B2. split.
  - simpl. split.
    + intro. destruct H.
      * destruct H. left. split. easy. apply ((proj1 ((proj1 HB1B2) _)) H0).
      * destruct H. right. split. easy. apply ((proj1 ((proj1 HB1B2) _)) H0).
    + intro. destruct H.
      * destruct H. left. split. easy. apply ((proj2 ((proj1 HB1B2) _)) H0).
      * destruct H. right. split. easy. apply ((proj2 ((proj1 HB1B2) _)) H0).
  - intros x y. intro. destruct H.
    + destruct H. left. split. easy. destruct H0.
      * destruct H0. left. split. easy. by apply (proj2 HB1B2).
      * destruct H0. right. split. easy. by apply (proj2 HB1B2).
    + destruct H. right. split. easy. destruct H0.
      * destruct H0. left. split. easy. by apply (proj2 HB1B2).
      * destruct H0. right. split. easy. by apply (proj2 HB1B2).
Qed.

Lemma Visfun_weak_refl : forall (B1 : Visfun), Visfun_weak B1 B1.
Proof.
  easy.
Qed.
Lemma Visfun_weak_trans : forall (B1 B2 B3 : Visfun), Visfun_weak B1 B2 -> Visfun_weak B2 B3 -> Visfun_weak B1 B3.
Proof.
  unfold Visfun_weak. intros. split.
  - apply (dom_eq_trans (dom B1) (dom B2) (dom B3) (proj1 H) (proj1 H0)).
  - intros. apply ((proj2 H0) x y). by apply ((proj2 H) x y).
Qed.

(* Definition of empty visibility *)
Lemma vis0_aux : is_V_fin (fun (y : Names) => False).
Proof.
  exists 0. easy.
Qed.
Definition vis0_ : Names_cur -> Vis := fun (x : Names_cur) => {| V:= fun (y : Names) => False; V_fin := vis0_aux; |}.
Lemma vis0_dom : forall (x : Names_cur) (y : Names), (vis0_ x y) -> (empty x).
Proof.
  trivial.
Qed.
Lemma vis0_codom_fin : is_codom_fin vis0_.
Proof.
  exists 0. intros k Hk x. split. by intro. by intro.
Qed.

Definition vis0 : Visfun :=
  {|
    dom := empty;
    B := vis0_;
    _dom := vis0_dom;
    dom_fin := empty_fin;
    codom_fin := vis0_codom_fin;
    |}.

Lemma vis_is_empty : forall (B : Visfun), dom_eq (dom B) empty -> B = vis0.
Proof.
  intros.
  apply eq_visfun. split.
  - apply H.
  - intros. split.
    + intro. apply (proj1 (H x) (_dom B0 x y H0)).
    + intro. exfalso. apply ((_dom vis0 x y) H0).
Qed.

Lemma alpha_empty_vis : forall (a b : Names), alpha_convert_vis a b vis0 = vis0.
Proof.
  intros a b. apply vis_is_empty. simpl. split.
  - intro. destruct H. by apply (proj2 H). by apply (proj2 H).
  - intro. easy.
Qed.

Lemma erase_empty : forall (a : Names), erase_vis a vis0 = vis0.
Proof.
  move=> a. apply vis_is_empty. simpl. split=> [H | H]; [by destruct H| easy].
Qed.

(* Definition and operations on stacks *)

Inductive stack_ : Type :=
| Out : stackO -> stack_
| In : stackI -> stack_
with stackO : Type :=
| stack_out : Ncon -> stackI -> stackO
with stackI : Type :=
| stack_in : Ncon -> stackO -> stackI
| nil : stackI.

Scheme stack_ind_ := Induction for stack_ Sort Prop
    with stackO_ind_ := Induction for stackO Sort Prop
                       with stackI_ind_ :=Induction for stackI Sort Prop.

Combined Scheme stack_ind from stack_ind_, stackI_ind_, stackO_ind_.

Inductive not_in_stack (p : Ncon) : stack_ -> Prop :=
| notin_nil : not_in_stack p (In nil)
| notin_out : forall (s : stackO), forall (q : Ncon), p <> q -> not_in_stack p (Out s) -> not_in_stack p (In (stack_in q s))
| notin_in : forall (s : stackI), forall (q : Ncon), p <> q -> not_in_stack p (In s) -> not_in_stack p (Out (stack_out q s)).

Inductive is_stack_p (p : Ncon) : stack_ -> Prop :=
| is_stack_p_nil : is_stack_p p (In nil)
| is_stack_popi : forall (s : stackO), not_in_stack p (Out s) -> is_stack_p p (Out (stack_out p (stack_in p s)))
| is_stack_po : forall (s: stackI), not_in_stack p (In s) -> is_stack_p p (Out (stack_out p s))
| is_stack_pi : forall (s: stackO), not_in_stack p (Out s) -> is_stack_p p (In (stack_in p s))
| is_stack_qo : forall (q: Ncon), forall (s: stackI), is_stack_p p (In s) -> is_stack_p p (Out (stack_out q s))
| is_stack_qi : forall (q: Ncon), forall (s: stackO), is_stack_p p (Out s) -> is_stack_p p (In (stack_in q s))
.

Lemma stack_fin_ : (forall (s : stack_), exists (n : nat), forall (k : nat), k >= n -> not_in_stack (give_ncon k) s) /\
                    (forall (s : stackI), exists (n : nat), forall (k : nat), k >= n -> not_in_stack (give_ncon k) (In s)) /\
                    (forall (s : stackO), exists (n : nat), forall (k : nat), k >= n -> not_in_stack (give_ncon k) (Out s)).
Proof.
  apply stack_ind.
  - by intros.
  - by intros.
  - intros p s Hs. destruct Hs as [ns Hs]. destruct (given_ncon p).
    + exists ns. intros k Hk. apply notin_in. apply H. by apply (Hs k Hk).
    + destruct H as [np Hp]. exists (max ns (np+1)). intros k Hk.
      apply notin_in. intro. rewrite (eq_sym H) in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      have Hnsk : k >= ns by lia. apply (Hs k Hnsk).
  - intros p s Hs. destruct Hs as [ns Hs]. destruct (given_ncon p).
    + exists ns. intros k Hk. apply notin_out. apply H. by apply (Hs k Hk).
    + destruct H as [np Hp]. exists (max ns (np+1)). intros k Hk.
      apply notin_out. intro. rewrite (eq_sym H) in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      have Hnsk : k >= ns by lia. apply (Hs k Hnsk).
  - exists 0. intros k Hk. apply notin_nil.
Qed.

Lemma stack_fin : forall (s : stack_), exists (n: nat) , forall (k : nat), k >= n -> not_in_stack (give_ncon k) s.
Proof.
  by apply (proj1 stack_fin_).
Qed.


Definition is_stack (s: stack_) :  Prop := forall (p : Ncon), is_stack_p p s.
Structure stack := {
    s :> stack_;
    well_defined : is_stack s;}.

(* inter_ s1 s2 s3 <-> s3 ∈ inter( s1, s2) *)
Inductive inter_ : stack_ -> stack_ -> stack_ -> Prop :=
| inter_nil : inter_ (In nil) (In nil) (In nil)
| ins_OO1  : forall (p : Ncon), forall (s1 : stackO), forall (s2 : stackO), forall (s3: stackO), inter_ (Out s1) (Out s2) (Out s3) -> inter_ (In (stack_in p s1)) (Out s2) (In (stack_in p s3))
| ins_OO2 : forall (p : Ncon), forall (s1 : stackO), forall (s2 : stackO), forall (s3: stackO), inter_ (Out s1) (Out s2) (Out s3) -> inter_ (Out s1) (In (stack_in p s2)) (In (stack_in p s3))
| ins_OI1 : forall (p:Ncon), forall (s1 : stackO), forall (s2 : stackI), forall (s3: stackO), inter_ (Out s1) (In s2) (Out s3) -> inter_ (In (stack_in p s1)) (In s2) (In (stack_in p s3))
| ins_OI2 : forall (p:Ncon), forall (s1 : stackO), forall (s2 : stackI), forall (s3 : stackI), inter_ (Out s1) (In s2) (In s3) -> inter_ (Out s1) (Out (stack_out p s2)) (Out (stack_out p s3))
| ins_IO1 : forall (p:Ncon), forall (s1 : stackI), forall (s2 : stackO), forall (s3 : stackI), inter_ (In s1) (Out s2) (In s3) -> inter_ (Out (stack_out p s1)) (Out s2) (Out (stack_out p s3))
| ins_IO2 : forall (p:Ncon), forall (s1 : stackI), forall (s2 : stackO), forall (s3 : stackO), inter_ (In s1) (Out s2) (Out s3) -> inter_ (In s1) (In (stack_in p s2)) (In (stack_in p s3))
| ins_II1 : forall (p: Ncon), forall (s1 : stackI), forall (s2 : stackI), forall (s3 : stackI), inter_ (In s1) (In s2) (In s3) -> inter_ (Out (stack_out p s1)) (In s2) (Out (stack_out p s3))
| ins_II2 : forall (p: Ncon), forall (s1 : stackI), forall (s2 : stackI), forall (s3 : stackI), inter_ (In s1) (In s2) (In s3) -> inter_ (In s1) (Out (stack_out p s2)) (Out (stack_out p s3)).


Definition inter : stack -> stack -> stack -> Prop := inter_.

(* try on stack_concatenation : UNECESSARY *)
(*Fixpoint stack_concatenation_I (s1 : stackI) (s2 : stackI) : stackI :=
  match s1 with
  | nil => s2
  | stack_in p s => stack_in p (stack_concatenation_O s s2)                                               
  end
with stack_concatenation_O (s1 : stackO) (s2 : stackI) : stackO :=
       match s1 with
       | stack_out p s => stack_out p (stack_concatenation_I s s2)
       end.

Definition stack_concatenation_ (s1 : stack_) (s2 : stackI) : stack_ :=
  match s1 with
  | In s => In (stack_concatenation_I s s2)
  | Out s => Out (stack_concatenation_O s s2)
  end.

Definition can_concat (s1 : stack_) (s2 : stackI) : Prop := (forall (p : Ncon), (not_in_stack p (In s2) /\ is_stack_p p s1) \/ (is_stack_p p (In s2) /\ not_in_stack p s1 )).

Lemma is_stack_p_concatenation1 (s1 : stack_) (s2 : stackI) (p : Ncon) : not_in_stack p (In s2) -> is_stack_p p s1 -> is_stack_p p (stack_concatenation_ s1 s2).
Proof.
  admit.
Admitted.

Lemma is_stack_p_concatenation2 (s1 : stack_) (s2 : stackI) (p : Ncon) : is_stack_p p (In s2) -> not_in_stack p s1 -> (is_stack_p p (stack_concatenation_ s1 s2)).
Proof.
  admit.
Admitted.

Lemma is_stack_concatenation (s1 : stack_) (s2 : stackI) : can_concat s1 s2 -> is_stack (stack_concatenation_ s1 s2).
Proof.
  intros H p. destruct (H p).
  apply (is_stack_p_concatenation1 s1 s2 p (proj1 H0) (proj2 H0)).
  apply (is_stack_p_concatenation2 s1 s2 p (proj1 H0) (proj2 H0)).
Qed.*)

(* Definition of inserting p:I,p:O in stack s *)
Inductive insert_stack_p : Ncon -> stack_ -> stack_ -> Prop :=
| ins_stack_add_p : forall (p: Ncon) (s : stackO), not_in_stack p (Out s) -> insert_stack_p p (Out (stack_out p (stack_in p s))) (Out s)
| ins_stack_in : forall (p: Ncon) (q : Ncon) (s1 s2 : stackO) , (p <> q) -> insert_stack_p p (Out s1) (Out s2) -> insert_stack_p p (In (stack_in q s1)) (In (stack_in q s2))
| ins_stack_out : forall (p : Ncon) (q : Ncon) (s1 s2 : stackI) , (p <> q) -> insert_stack_p p (In s1) (In s2) -> insert_stack_p p (Out (stack_out q s1)) (Out (stack_out q s2)).

(* Alpha conversion for stacks *)
Inductive alpha_convert_stack : Ncon -> Ncon -> stack_ -> stack_ -> Prop :=
| alpha_empty : forall (p q : Ncon), alpha_convert_stack p q (In nil) (In nil)
| alpha_replace_in : forall (p q : Ncon) (s1 s2 : stackO), alpha_convert_stack p q (Out s1) (Out s2) -> alpha_convert_stack p q (In (stack_in p s1)) (In (stack_in q s2))
| alpha_replace_out : forall (p q : Ncon) (s1 s2 : stackI), alpha_convert_stack p q (In s1) (In s2) -> alpha_convert_stack p q (Out (stack_out p s1)) (Out (stack_out q s2))
| alpha_leave_in : forall (p q r : Ncon) (s1 s2 : stackO), (r <> p) -> alpha_convert_stack p q (Out s1) (Out s2) -> alpha_convert_stack p q (In (stack_in r s1)) (In (stack_in r s2))
| alpha_leave_out : forall (p q r : Ncon) (s1 s2 : stackI), (r <> p) -> alpha_convert_stack p q (In s1) (In s2) -> alpha_convert_stack p q (Out (stack_out r s1)) (Out (stack_out r s2)).


Lemma exists_alpha_convert_stack_ (p q : Ncon) : ( forall (s : stack_), exists (s1 : stack_), alpha_convert_stack p q s s1)
                                                 /\ (forall (s : stackI), exists (s1 : stackI), alpha_convert_stack p q (In s) (In s1))
                                                 /\  (forall (s : stackO), exists (s1 : stackO), alpha_convert_stack p q (Out s) (Out s1)).
Proof.
  apply stack_ind.
  - intros s Hs. destruct Hs as [s1 Hs1]. exists (Out s1). assumption.
  - intros s Hs. destruct Hs as [s1 Hs1]. exists (In s1). assumption.
  - intros r s Hs. destruct Hs as [s1 Hs1]. case (Ncon_dec r p).
    + intro. rewrite H. exists (stack_out q s1). by apply alpha_replace_out.
    + intro. exists (stack_out r s1). by apply alpha_leave_out.
  - intros r s Hs. destruct Hs as [s1 Hs1]. case (Ncon_dec r p).
    + intro. rewrite H. exists (stack_in q s1). by apply alpha_replace_in.
    + intro. exists (stack_in r s1). by apply alpha_leave_in.
  - exists nil. by apply alpha_empty.
Qed.


Lemma exists_alpha_convert_stack (p q : Ncon) : forall (s : stack_), exists (s1 : stack_), alpha_convert_stack p q s s1.
Proof.
  apply (proj1 (exists_alpha_convert_stack_ p q)).
Qed.

Lemma unique_alpha_convert_stack_ (p q : Ncon) : (forall (s1 s2 s3 : stack_),
    alpha_convert_stack p q s1 s2 -> alpha_convert_stack p q s1 s3 -> s2 = s3)
/\ (forall (s1 : stackI) (s2 s3 : stack_),
        alpha_convert_stack p q (In s1) s2 -> alpha_convert_stack p q (In s1) s3 -> s2 = s3)
/\ (forall (s1 : stackO) (s2 s3 : stack_),
        alpha_convert_stack p q (Out s1) s2 -> alpha_convert_stack p q (Out s1) s3 -> s2 = s3).
Proof.
  apply stack_ind.
  - move=> s1 Hs1 s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * apply Hs1.
        by rewrite H3. by rewrite H8.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_replace_out.
        rewrite (eq_sym H9) in Halph2. rewrite (eq_sym H) in Halph2. inversion Halph2.
        rewrite (eq_sym H). by apply alpha_replace_out. easy.
    + inversion Halph2.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_out.
        rewrite (eq_sym H5). by apply alpha_replace_out.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_out.
        rewrite (eq_sym H5). by apply alpha_leave_out.
  - move=> s1 Hs1 s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * easy.
      * rewrite (eq_sym H3) in H2. by inversion H2.
      * rewrite (eq_sym H3) in H2. by inversion H2.
    + inversion Halph2.
      * rewrite (eq_sym H7) in H. by inversion H.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_replace_in.
        rewrite (eq_sym H4). by apply alpha_replace_in.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_replace_in.
        rewrite (eq_sym H4). by apply alpha_leave_in.
    + inversion Halph2.
      * rewrite (eq_sym H8) in H. by inversion H.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_in.
        rewrite (eq_sym H5). by apply alpha_replace_in.
      * apply Hs1. rewrite (eq_sym H). by apply alpha_leave_in.
        rewrite (eq_sym H5). by apply alpha_leave_in.
  -  move=> r s1 Hs s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * have Hs46 : (In s4) = (In s6). apply Hs. by rewrite H. by rewrite H.
        inversion Hs46. reflexivity.
      * by rewrite H in H9.
    + inversion Halph2.
      * have Hs46 : (In s4) = (In s6). apply Hs. assumption. by rewrite H6. inversion Hs46.
        rewrite (eq_sym H9) in Halph2. inversion Halph2. by rewrite H12 in H3.
        reflexivity.
      * have Hs46 : (In s4) = (In s6). by apply Hs.  by inversion Hs46.
  - move=> r s1 Hs s2 s3 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * have Hs46 : (Out s4) = (Out s6). by apply Hs; rewrite H.
        by inversion Hs46.
      * by rewrite H in H9.
    + inversion Halph2.
      * by rewrite H6 in H3.
      * have Hs46 : (Out s4) = (Out s6). by apply Hs. by inversion Hs46.
  - move=> s1 s2 Halph1 Halph2. inversion Halph1. by inversion Halph2.
Qed.
        
Lemma unique_alpha_convert_stack (p q : Ncon) : forall (s1 s2 s3 : stack_),
    alpha_convert_stack p q s1 s2 -> alpha_convert_stack p q s1 s3 -> s2 = s3.
Proof.
  apply (proj1 (unique_alpha_convert_stack_ p q)).
Qed.

Lemma alpha_stack_idem (p q : Ncon) (s1 s2 : stack_): not_in_stack p s1 -> alpha_convert_stack p q s1 s2 -> s1 = s2.
Proof.
  move=> Hp Halph. induction Halph.
  - trivial.
  - by inversion Hp.
  - by inversion Hp.
  - have Hs : Out s1 = Out s2. apply IHHalph. by inversion Hp. by inversion Hs.
  - have Hs : In s1 = In s2. apply IHHalph. by inversion Hp. by inversion Hs.
Qed.
Lemma alpha_insert_replace (p q : Ncon) (s1 s2 s3: stack_) : not_in_stack q s3 -> alpha_convert_stack p q s1 s2 ->insert_stack_p p s1 s3 -> insert_stack_p q s2 s3.
Proof.
  move=> Hq Halph Hins. move: s2 Halph; induction Hins.
  - move=> s2 Halph. inversion Halph. inversion H3. have Hs : Out s0 = Out s5. by apply (alpha_stack_idem p q).
    rewrite Hs. apply ins_stack_add_p. by rewrite (eq_sym Hs).
    easy. easy.
  - move=> s Halph. inversion Halph. easy. apply ins_stack_in. intro. rewrite H7 in Hq. by inversion Hq.
    apply IHHins.  by inversion Hq. assumption.
  - move=> s Halph. inversion Halph. easy.
    apply ins_stack_out. intro. rewrite H7 in Hq. by inversion Hq.
    apply IHHins. by inversion Hq. assumption.
Qed.

Lemma alpha_alpha_stack (p q r : Ncon) (s1 s2 s3 : stack_) : not_in_stack q s1 -> alpha_convert_stack p q s1 s2 -> alpha_convert_stack q r s2 s3 -> alpha_convert_stack p r s1 s3.
Proof.
  move=> Hqs Halph1. move: s3. induction Halph1; move=> s3 Halph2.
  - inversion Halph2. apply alpha_empty.
  - inversion Halph2.
    + apply alpha_replace_in. apply IHHalph1. by inversion Hqs. assumption.
    + easy.
  - inversion Halph2.
    + apply alpha_replace_out.  apply IHHalph1. by inversion Hqs. assumption.
    + easy.
  - inversion Halph2.
    + rewrite H0 in Hqs. by inversion Hqs.
    + apply alpha_leave_in. assumption. apply IHHalph1. by inversion Hqs. assumption.
  - inversion Halph2.
    + rewrite H0 in Hqs. by inversion Hqs.
    + apply alpha_leave_out. assumption. apply IHHalph1. by inversion Hqs. assumption.
Qed.

Lemma alpha_stack_idem2 (p q : Ncon) (s1 s2 : stack_) : (p = q) -> alpha_convert_stack p q s1 s2 -> s1 = s2.
Proof.
  move=> Hpq Halph. induction Halph.
  - reflexivity.
  - rewrite Hpq. have Hs1s2 : Out s1 = Out s2 by apply (IHHalph Hpq). by inversion Hs1s2.
  - have Hs1s2 : In s1 = In s2 by apply (IHHalph Hpq). rewrite Hpq. by inversion Hs1s2.
  - have Hs1s2 : Out s1 = Out s2 by apply (IHHalph Hpq). by inversion Hs1s2.
  - have Hs1s2 : In s1 = In s2 by apply (IHHalph Hpq). by inversion Hs1s2.
Qed.
Lemma alpha_stack_invol (p q : Ncon) (s1 s2 s3 : stack_) : not_in_stack q s1 -> alpha_convert_stack p q s1 s2 -> alpha_convert_stack q p s2 s3 -> s1=s3.
  move=> Hqs Halph1 Halph2.
  have Hs : alpha_convert_stack p p s1 s3. by apply (alpha_alpha_stack p q p s1 s2 s3).
  by apply (alpha_stack_idem2 p p).
Qed.

Lemma insert_new (p : Ncon) (s1 s2 : stack_) : insert_stack_p p s1 s2 -> not_in_stack p s2.
Proof.
  intro. induction H.
  - assumption.
  - by apply notin_out.
  - by apply notin_in.
Qed.

Lemma insert_not_in (p q : Ncon) (s1 s2 : stack_) : insert_stack_p p s1 s2 -> not_in_stack q s2 -> p<> q -> not_in_stack q s1.
Proof.
  move=> Hins Hnin Hpq. induction Hins.
  - apply notin_in. easy. apply notin_out. easy. assumption.
  - inversion Hnin. apply notin_out. assumption. by apply IHHins.
  - inversion Hnin. apply notin_in. assumption. by apply IHHins.
Qed.

Lemma alpha_not_in (p q : Ncon) (s1 s2 : stack_) : alpha_convert_stack p q s1 s2 -> not_in_stack p s1 -> not_in_stack q s1 -> not_in_stack q s2.
Proof.
  move=> Halph. induction Halph.
  - intros. apply notin_nil.
  - intros. by inversion H.
  - intros. by inversion H.
  - intros. apply notin_out. intro. rewrite H2 in H1. by inversion H1. apply IHHalph. by inversion H0. by inversion H1.
  - intros. apply notin_in. intro. rewrite H2 in H1. by inversion H1. apply IHHalph. by inversion H0. by inversion H1.
Qed.

Lemma alpha_not_in_other (p q r : Ncon) (s1 s2 : stack_) : (r <> p) -> (r <> q) -> alpha_convert_stack p q s1 s2 -> not_in_stack r s1 -> not_in_stack r s2.
Proof.
  move=> Hrp Hrq Halph Hrs. induction Halph.
  - apply notin_nil.
  - apply notin_out. easy. apply IHHalph. easy. easy. by inversion Hrs.
  - apply notin_in. easy. apply IHHalph. easy. easy. by inversion Hrs.
  - apply notin_out. by inversion Hrs. apply IHHalph. easy. easy. by inversion Hrs.
  - apply notin_in. by inversion Hrs. apply IHHalph. easy. easy. by inversion Hrs.
Qed.

Lemma alpha_insert_leave : forall (p q r : Ncon) (s1 s2 s3 s4 : stack_),
    (r <> p) -> (r <> q) -> not_in_stack q s1 -> insert_stack_p r s3 s1 -> alpha_convert_stack p q s1 s2
    -> alpha_convert_stack p q s3 s4 -> insert_stack_p r s4 s2.
Proof.
  move=> p q r s1 s2 s3 s4 Hrp Hrq Hqs1 Hins. move: s2 s4. induction Hins.
  - move=> s2 s4 Halph1 Halph2. inversion Halph2. 
    +  by rewrite H0 in Hrp. 
    + inversion H6. by rewrite H7 in Hrp. have Hs : s2 = (Out s6). by apply (unique_alpha_convert_stack p q (Out s0)). rewrite Hs. apply ins_stack_add_p. by apply (alpha_not_in_other p q _ (Out s0)).
  - move=> s3 s4 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * apply ins_stack_in. easy. apply IHHins; try assumption.  by inversion Hqs1. by rewrite H0.
        by rewrite H0.
      * by rewrite H0 in H10.
    + inversion Halph2. by rewrite H7 in H4.
      apply ins_stack_in. assumption. apply IHHins; try assumption. by inversion Hqs1.
  - move=> s3 s4 Halph1 Halph2. inversion Halph1.
    + inversion Halph2.
      * apply ins_stack_out. easy. apply IHHins; try assumption.  by inversion Hqs1. by rewrite H0.
        by rewrite H0.
      * by rewrite H0 in H10.
    + inversion Halph2. by rewrite H7 in H4.
      apply ins_stack_out. assumption. apply IHHins; try assumption. by inversion Hqs1.
Qed.

Lemma alpha_inter_ : forall (p q : Ncon) (s1 s2 s3 s4 s5 s6 : stack_),
    inter_ s1 s2 s3 -> alpha_convert_stack p q s1 s4 -> alpha_convert_stack p q s2 s5
    -> alpha_convert_stack p q s3 s6 -> inter_ s4 s5 s6.
Proof.
  move=> p q s1 s2 s3 s4 s5 s6 Hints. move: s4 s5 s6.
  induction Hints; move=> s4 s5 s6 Halph1 Halph2 Halph3.
  - inversion Halph1. inversion Halph2. inversion Halph3. apply inter_nil.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OO1.
    + rewrite H9.  apply IHHints; try assumption; try by rewrite H. 
    + by rewrite H in H14.
    + apply IHHints; try assumption; try by rewrite H. rewrite (eq_sym H5). by apply alpha_leave_out.
    
    + by rewrite H in H15.
    + by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_out.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OO2.
    + rewrite H3. apply IHHints; try assumption; try by rewrite H10.
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_replace_out.
    + rewrite H4. apply IHHints; try assumption; try by rewrite H11.
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OI1.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H8). apply alpha_empty. 
    + by rewrite H in H13.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_replace_in.
    + by rewrite H in H14.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_leave_in. 
    + by rewrite H in H15.
    + by rewrite H10 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H9). by apply alpha_empty.
    + by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_in.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_in.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_OI2.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H). by apply alpha_replace_out.
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try by assumption. rewrite (eq_sym H). by apply alpha_replace_out.
    + apply IHHints; try by rewrite H11. rewrite (eq_sym H). by apply alpha_leave_out.
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_IO1.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_replace_out. 
    + by rewrite H in H14.
    + apply IHHints; try by rewrite H. rewrite (eq_sym H5). by apply alpha_leave_out. 
    + by rewrite H in H15.
    +  by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_out.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_out.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_IO2.
    + apply IHHints; try by rewrite H9. rewrite (eq_sym H2). apply alpha_empty.
    + by rewrite H3 in H13.
    + by rewrite H10 in H7.
    + apply IHHints; try assumption. rewrite (eq_sym H2). apply alpha_empty.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H). by apply alpha_replace_in. 
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try assumption.  rewrite (eq_sym H). by apply alpha_replace_in.
    + apply IHHints; try by rewrite H5. rewrite (eq_sym H). by apply alpha_leave_in. 
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_in.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_II1.
    + apply IHHints; try by rewrite H9. rewrite (eq_sym H8).  by apply alpha_empty.
    +  by rewrite H in H13.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H5). by apply alpha_replace_in. 
    + by rewrite H in H14.
    + apply IHHints; try by rewrite H11. rewrite (eq_sym H5). by apply alpha_leave_in.
    + by rewrite H in H15.
    + by rewrite H10 in H3.
    + apply IHHints; try assumption.  rewrite (eq_sym H9). by apply alpha_empty.
    + by rewrite H11 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_replace_in.
    + by rewrite H12 in H3.
    + apply IHHints; try assumption. rewrite (eq_sym H6). by apply alpha_leave_in.
  - inversion Halph1; inversion Halph2; inversion Halph3; try apply ins_II2.
    + apply IHHints; try by rewrite H9. rewrite (eq_sym H2). by apply alpha_empty.
    + by rewrite H3 in H13.
    + by rewrite H10 in H7.
    + apply IHHints; try assumption. rewrite (eq_sym H2). by apply alpha_empty.
    + apply IHHints; try by rewrite H10. rewrite (eq_sym H). by apply alpha_replace_in. 
    + by rewrite H4 in H14.
    + by rewrite H11 in H8.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_replace_in.
    + apply IHHints; try by rewrite H11. rewrite (eq_sym H). by apply alpha_leave_in. 
    + by rewrite H5 in H15.
    + by rewrite H12 in H9.
    + apply IHHints; try assumption. rewrite (eq_sym H). by apply alpha_leave_in.
Qed.

Lemma inter_notin1 : forall (p : Ncon) (s1 s2 s3 : stack_), inter_ s1 s2 s3 -> not_in_stack p s3
                                                       -> (not_in_stack p s1 /\ not_in_stack p s2).
Proof.
  move=> p s1 s2 s3 Hins. induction Hins; move=> Hps3.
  - split; apply notin_nil.
  - inversion Hps3. split. apply notin_out. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_out. assumption. by apply IHHins.
  - inversion Hps3. split. apply notin_out. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_in. assumption. by apply IHHins.
  - inversion Hps3. split. apply notin_in. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_out. assumption. by apply IHHins.
  - inversion Hps3. split. apply notin_in. assumption. by apply IHHins. by apply IHHins.
  - inversion Hps3. split. by apply IHHins. apply notin_in. assumption. by apply IHHins.
Qed.

Lemma inter_notin2 : forall (p : Ncon) (s1 s2 s3 : stack_), inter_ s1 s2 s3                                                       -> (not_in_stack p s1 /\ not_in_stack p s2) -> not_in_stack p s3.
Proof.
  move=> p s1 s2 s3 Hins. induction Hins; move=> [Hps1 Hps2].
  - apply notin_nil.
  - inversion Hps1. apply notin_out. assumption. by apply IHHins.
  - inversion Hps2. apply notin_out. assumption. by apply IHHins.
  - inversion Hps1. apply notin_out. assumption. by apply IHHins.
  - inversion Hps2. apply notin_in. assumption. by apply IHHins.
  - inversion Hps1. apply notin_in. assumption. by apply IHHins.
  - inversion Hps2. apply notin_out. assumption. by apply IHHins.
  - inversion Hps1. apply notin_in. assumption. by apply IHHins.
  - inversion Hps2. apply notin_in. assumption. by apply IHHins.
Qed.

(* Definition of processes *)

Inductive proc : Type :=
| proc_nil : proc
| proc_out_ser : Nser -> Nfo -> Nser -> Ncon -> proc -> proc (* a^ <n>(b,p).P *)
| proc_out_con : Ncon -> Nfo -> Nser -> proc -> proc (* p^ <n>(b).P *)
| proc_out_loc : Nref -> Nfo -> proc (* h^ <n> *)
| proc_in_ser : Nser -> Nfo -> Nser -> Ncon -> proc -> proc (* !a(n,b,p).P *)
| proc_in_con : Ncon -> Nfo -> Nser -> proc -> proc (* p(n,b).Q *)
| proc_in_loc : Nref -> Nfo -> proc -> proc (* h(n).P *)
| proc_nu_ser : Nser -> proc -> proc (* nu a P *)
| proc_nu_con : Ncon -> proc -> proc (* nu p P *)
| proc_nu_loc : Nref -> proc -> proc (* nu h P *)
| proc_par : proc -> proc -> proc (* P|Q *)
| proc_ite : Nfo -> Nfo -> proc -> proc -> proc (* if x=n then P else Q *)
| proc_fwd_ser : Nser -> Nser -> proc (* a ▷ b *)
| proc_fwd_con : Ncon -> Ncon -> proc (* p ▷ q *)
.
Inductive notin_proc_con : Ncon -> proc -> Prop :=
| con_notin_proc_nil : forall (p : Ncon), notin_proc_con p proc_nil
| con_notin_proc_out_ser : forall (p : Ncon), forall (a b : Nser) (n : Nfo) (q : Ncon) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_out_ser a n b q P)
| con_notin_proc_out_con : forall (p : Ncon), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_out_con q n b P)
| con_notin_proc_out_loc : forall (p : Ncon), forall (h : Nref) (n : Nfo), notin_proc_con p (proc_out_loc h n)
| con_notin_proc_in_ser : forall (p : Ncon), forall (a b : Nser) (n : Nfo) (q : Ncon) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_in_ser a n b q P)
| con_notin_proc_in_con : forall (p : Ncon), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_in_con q n b P)
| con_notin_proc_in_loc : forall (p : Ncon), forall (h : Nref) (n : Nfo) (P : proc),
    notin_proc_con p P -> notin_proc_con p (proc_in_loc h n P)
| con_notin_proc_nu_ser : forall (p : Ncon), forall (a : Nser) (P : proc),
    notin_proc_con p P -> notin_proc_con p (proc_nu_ser a P)
| con_notin_proc_nu_con : forall (p : Ncon), forall (q : Ncon) (P : proc),
    (q <> p) -> notin_proc_con p P -> notin_proc_con p (proc_nu_con q P)
| con_notin_proc_nu_loc : forall (p : Ncon),forall (h : Nref) (P : proc),
    notin_proc_con p P -> notin_proc_con p (proc_nu_loc h P)
| con_notin_proc_par : forall (p : Ncon), forall (P1 P2 : proc),
    notin_proc_con p P1 -> notin_proc_con p P2 -> notin_proc_con p (proc_par P1 P2)
| con_notin_proc_ite : forall (p : Ncon), forall (x n : Nfo) (P1 P2 : proc),
    notin_proc_con p P1 -> notin_proc_con p P2 -> notin_proc_con p (proc_ite x n P1 P2)
| con_notin_proc_fwd_ser : forall (p : Ncon), forall (a b : Nser), notin_proc_con p (proc_fwd_ser a b)
| con_notin_proc_fwd_con : forall (p : Ncon), forall (q r : Ncon),
    (q <> p) -> (r <> p) -> notin_proc_con p (proc_fwd_con q r).

Inductive notin_proc_ser : Nser -> proc -> Prop :=
| ser_notin_proc_nil : forall (a : Nser), notin_proc_ser a proc_nil
| ser_notin_proc_out_ser : forall (a : Nser), forall (b c : Nser) (n : Nfo) (p : Ncon) (P : proc),
    (b <> a) -> (c <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_out_ser b n c p P)
| ser_notin_proc_out_con : forall (a : Nser), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (b <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_out_con q n b P)
| ser_notin_proc_out_loc : forall (a : Nser), forall (h : Nref) (n : Nfo), notin_proc_ser a (proc_out_loc h n)
| ser_notin_proc_in_ser : forall (a : Nser), forall (b c : Nser) (n : Nfo) (q : Ncon) (P : proc),
    (b <> a) -> (c <> a) ->  notin_proc_ser a P -> notin_proc_ser a (proc_in_ser b n c q P)
| ser_notin_proc_in_con : forall (a : Nser), forall (q : Ncon) (n : Nfo) (b : Nser) (P : proc),
    (b <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_in_con q n b P)
| ser_notin_proc_in_loc : forall (a : Nser), forall (h : Nref) (n : Nfo) (P : proc),
    notin_proc_ser a P -> notin_proc_ser a (proc_in_loc h n P)
| ser_notin_proc_nu_ser : forall (a : Nser), forall (b : Nser) (P : proc),
    (b <> a) -> notin_proc_ser a P -> notin_proc_ser a (proc_nu_ser b P)
| ser_notin_proc_nu_con : forall (a : Nser), forall (q : Ncon) (P : proc),
    notin_proc_ser a P -> notin_proc_ser a (proc_nu_con q P)
| ser_notin_proc_nu_loc : forall (a : Nser),forall (h : Nref) (P : proc),
    notin_proc_ser a P -> notin_proc_ser a (proc_nu_loc h P)
| ser_notin_proc_par : forall (a : Nser), forall (P1 P2 : proc),
    notin_proc_ser a P1 -> notin_proc_ser a P2 -> notin_proc_ser a (proc_par P1 P2)
| ser_notin_proc_ite : forall (a : Nser), forall (x n : Nfo) (P1 P2 : proc),
    notin_proc_ser a P1 -> notin_proc_ser a P2 -> notin_proc_ser a (proc_ite x n P1 P2)
| ser_notin_proc_fwd_ser : forall (a : Nser), forall (b c : Nser),
    (b <> a) -> (c <> a) -> notin_proc_ser a (proc_fwd_ser b c)
| ser_notin_proc_fwd_con : forall (a : Nser), forall (q r : Ncon), notin_proc_ser a (proc_fwd_con q r).

Lemma proc_fin_con : forall ( P : proc), exists (n : nat), forall (k : nat), (k >= n) -> notin_proc_con (give_ncon k) P.
Proof.
  induction P as [ | a n b p P | p n b P | h n | a n b p P | p n b P | h n P | a P | p P | h P | Q | x n Q | a b | p q].
  - exists 0. intros k Hk. apply con_notin_proc_nil.
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_out_ser. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_out_ser.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_out_con. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_out_con.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - exists 0. intros k Hk. apply con_notin_proc_out_loc.
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_in_ser. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_in_ser.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_in_con. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp].
      exists (max nP (np +1)). intros k Hk. apply con_notin_proc_in_con.
      * intro. rewrite H in Hp. have Hnpk : np = k by apply infinite_ncon. lia.
      * have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply con_notin_proc_in_loc. apply (HP k Hk).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply con_notin_proc_nu_ser. apply (HP k Hk).
  - destruct IHP as [nP HP]. destruct (given_ncon p).
    + exists nP. intros k Hk. apply con_notin_proc_nu_con. intro. apply (H k (eq_sym H0)).
      apply (HP k Hk).
    + destruct H as [np Hp]. exists (max nP (np +1)). intros k Hk.
      apply con_notin_proc_nu_con. intro. rewrite H in Hp. have Hnpk : np=k by apply infinite_ncon. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply con_notin_proc_nu_loc. apply (HP k Hk).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply con_notin_proc_par.
    + have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    + have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply con_notin_proc_ite.
    + have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    + have HknP : k >= nP by lia. apply (HP k HknP).
  - exists 0. intros k Hk. apply con_notin_proc_fwd_ser.
  - destruct (given_ncon p).
    + destruct (given_ncon q).
      * exists 0. intros k Hk. apply con_notin_proc_fwd_con. intro Hp. apply (H k (eq_sym Hp)).
        intro Hq. apply (H0 k (eq_sym Hq)).
      * destruct H0 as [nq Hq]. exists (nq +1). intros k Hk. apply con_notin_proc_fwd_con.
        intro Hp. apply (H k (eq_sym Hp)).
        intro Hq1. rewrite Hq1 in Hq. have Hnqk : nq =k by apply infinite_ncon. lia.
    + destruct H as [np Hp]. destruct (given_ncon q).
      * exists (np +1). intros k Hk. apply con_notin_proc_fwd_con.
        intro Hp1. rewrite Hp1 in Hp. have Hnpk : np =k by apply infinite_ncon. lia.
        intro Hq. apply (H k (eq_sym Hq)).
      * destruct H as [nq Hq]. exists (max (np +1) (nq +1)). intros k Hk. apply con_notin_proc_fwd_con.
        intro Hp1. rewrite Hp1 in Hp. have Hnpk : np =k by apply infinite_ncon. lia.
        intro Hq1. rewrite Hq1 in Hq. have Hnqk : nq =k by apply infinite_ncon. lia.
Qed.

Lemma proc_fin_ser : forall ( P : proc), exists (n : nat), forall (k : nat), (k >= n) -> notin_proc_ser (give_nser k) P.
Proof.
  induction P as [ | a n b p P | p n b P | h n | a n b p P | p n b P | h n P | a P | p P | h P | Q | x n Q | a b | p q].
  - exists 0. intros k Hk. apply ser_notin_proc_nil.
  - destruct IHP as [nP HP]. destruct (given_nser a).
    + destruct (given_nser b).
      * exists nP. intros k Hk. apply ser_notin_proc_out_ser. intro Ha. apply (H k (eq_sym Ha)).
        intro Hb. apply (H0 k (eq_sym Hb)).
        apply (HP k Hk).
      * destruct H0 as [nb Hb]. exists (max nP (nb+1)).  intros k Hk. apply ser_notin_proc_out_ser.
        intro Ha. apply (H k (eq_sym Ha)).
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
    + destruct H as [na Ha]. destruct (given_nser b).
      * exists (max nP (na +1)). intros k Hk. apply ser_notin_proc_out_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hb. apply (H k (eq_sym Hb)).
        have HknP : k >= nP by lia. apply (HP k HknP).
      * destruct H as [nb Hb]. exists (max nP (max (na +1) (nb +1))). intros k Hk. apply ser_notin_proc_out_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_nser b).
    + exists nP. intros k Hk. apply ser_notin_proc_out_con.
      intro Hb. apply (H k (eq_sym Hb)).
      apply (HP k Hk).
    + destruct H as [nb Hb]. exists (max nP (nb +1)). intros k Hk. apply ser_notin_proc_out_con.
      intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - exists 0. intros k Hk. apply ser_notin_proc_out_loc.
  - destruct IHP as [nP HP]. destruct (given_nser a).
    + destruct (given_nser b).
      * exists nP. intros k Hk. apply ser_notin_proc_in_ser. intro Ha. apply (H k (eq_sym Ha)).
        intro Hb. apply (H0 k (eq_sym Hb)).
        apply (HP k Hk).
      * destruct H0 as [nb Hb]. exists (max nP (nb+1)).  intros k Hk. apply ser_notin_proc_in_ser.
        intro Ha. apply (H k (eq_sym Ha)).
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
    + destruct H as [na Ha]. destruct (given_nser b).
      * exists (max nP (na +1)). intros k Hk. apply ser_notin_proc_in_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hb. apply (H k (eq_sym Hb)).
        have HknP : k >= nP by lia. apply (HP k HknP).
      * destruct H as [nb Hb]. exists (max nP (max (na +1) (nb +1))). intros k Hk. apply ser_notin_proc_in_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
        have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. destruct (given_nser b).
    + exists nP. intros k Hk. apply ser_notin_proc_in_con.
      intro Hb. apply (H k (eq_sym Hb)).
      apply (HP k Hk).
    + destruct H as [nb Hb]. exists (max nP (nb +1)). intros k Hk. apply ser_notin_proc_in_con.
      intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply ser_notin_proc_in_loc. apply (HP k Hk).
  - destruct IHP as [nP HP]. destruct (given_nser a).
    + exists nP. intros k Hk. apply ser_notin_proc_nu_ser. intro Ha. apply (H k (eq_sym Ha)).
      apply (HP k Hk).
    + destruct H as [na Ha]. exists (max nP (na +1)). intros k Hk. apply ser_notin_proc_nu_ser.
      intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
      have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply ser_notin_proc_nu_con. apply (HP k Hk).
  - destruct IHP as [nP HP]. exists nP. intros k Hk. apply ser_notin_proc_nu_loc. apply (HP k Hk).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply ser_notin_proc_par. have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct IHQ as [nQ HQ]. destruct IHP1 as [nP HP]. exists (max nQ nP).
    intros k Hk. apply ser_notin_proc_ite. have HknQ : k >= nQ by lia. apply (HQ k HknQ).
    have HknP : k >= nP by lia. apply (HP k HknP).
  - destruct (given_nser a).
    + destruct (given_nser b).
      * exists 0. intros k Hk. apply ser_notin_proc_fwd_ser. intro Ha. apply (H k (eq_sym Ha)). intro Hb. apply (H0 k (eq_sym Hb)).
      * destruct H0 as [nb Hb]. exists (nb +1).
        intros k Hk. apply ser_notin_proc_fwd_ser. intro Ha. apply (H k (eq_sym Ha)). intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
    + destruct H as [na Ha]. destruct (given_nser b).
      * exists (na +1). intros k Hk. apply ser_notin_proc_fwd_ser. intro Hak. rewrite Hak in Ha.
        have Hnak : na = k by apply infinite_nser. lia. intro Hb. apply (H k (eq_sym Hb)).
      * destruct H as [nb Hb]. exists (max (na+1) (nb+1)). intros k Hk. apply ser_notin_proc_fwd_ser.
        intro Hak. rewrite Hak in Ha. have Hnak : na = k by apply infinite_nser. lia.
        intro Hbk. rewrite Hbk in Hb. have Hnbk : nb = k by apply infinite_nser. lia.
  - exists 0. intros k Hk. apply ser_notin_proc_fwd_con.
Qed.
        
Inductive alpha_convert_proc_con :  Ncon -> Ncon -> proc -> proc -> Prop :=
| con_alpha_proc_nil : forall (p q : Ncon), alpha_convert_proc_con p q proc_nil proc_nil
| con_alpha_proc_out_ser_replace : forall (p q : Ncon), forall (a : Nser) (n : Nfo) (b : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_ser a n b p P) (proc_out_ser a n b q Q)
| con_alpha_proc_out_ser_leave : forall (p q : Ncon), forall (a : Nser) (n : Nfo) (b : Nser) (r : Ncon) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_ser a n b r P) (proc_out_ser a n b r Q)
| con_alpha_proc_out_con_replace : forall (p q : Ncon), forall (n : Nfo) (b : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_con p n b P) (proc_out_con q n b Q)
| con_alpha_proc_out_con_leave : forall (p q : Ncon), forall (r : Ncon) (n : Nfo) (b : Nser) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_out_con r n b P) (proc_out_con r n b Q)
| con_alpha_proc_out_loc : forall (p q : Ncon), forall (h : Nref) (n : Nfo), alpha_convert_proc_con p q (proc_out_loc h n) (proc_out_loc h n)
| con_alpha_proc_in_ser_replace : forall (p q : Ncon), forall (a b : Nser) (n : Nfo) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_ser a n b p P) (proc_in_ser a n b q Q)
| con_alpha_proc_in_ser_leave : forall (p q : Ncon), forall (a b : Nser) (n : Nfo) (r : Ncon) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_ser a n b r P) (proc_in_ser a n b r Q)
| con_alpha_proc_in_con_replace : forall (p q : Ncon), forall (n : Nfo) (b : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_con p n b P) (proc_in_con q n b Q)
| con_alpha_proc_in_con_leave : forall (p q : Ncon), forall (r : Ncon) (n : Nfo) (b : Nser) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_con r n b P) (proc_in_con r n b Q)
| con_alpha_proc_in_loc : forall (p q : Ncon), forall (h : Nref) (n : Nfo) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_in_loc h n P) (proc_in_loc h n Q)
| con_alpha_proc_nu_ser : forall (p q : Ncon), forall (a : Nser) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_ser a P) (proc_nu_ser a Q)
| con_alpha_proc_nu_con_replace : forall (p q : Ncon), forall (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_con p P) (proc_nu_con q Q)
| con_alpha_proc_nu_con_leave : forall (p q : Ncon), forall (r : Ncon) (P Q : proc),
    (r <> p) -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_con r P) (proc_nu_con r Q)
| con_alpha_proc_nu_loc : forall (p q : Ncon), forall (h : Nref) (P Q : proc),
    alpha_convert_proc_con p q P Q -> alpha_convert_proc_con p q (proc_nu_loc h P) (proc_nu_loc h Q)
| con_alpha_proc_par : forall (p q : Ncon), forall (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_con p q P1 Q1 -> alpha_convert_proc_con p q P2 Q2 -> alpha_convert_proc_con p q (proc_par P1 P2) (proc_par Q1 Q2)
| con_alpha_proc_ite : forall (p q : Ncon), forall (x n : Nfo) (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_con p q P1 Q1 -> alpha_convert_proc_con p q P2 Q2 -> alpha_convert_proc_con p q (proc_ite x n P1 P2) (proc_ite x n Q1 Q2)
| con_alpha_proc_fwd_ser : forall (p q : Ncon), forall (a b : Nser),
    alpha_convert_proc_con p q (proc_fwd_ser a b) (proc_fwd_ser a b)
| con_alpha_proc_fwd_con_replace1 : forall (p q : Ncon),
    alpha_convert_proc_con p q (proc_fwd_con p p) (proc_fwd_con q q)
| con_alpha_proc_fwd_con_replace2 : forall (p q : Ncon), forall (r : Ncon),
    (r <> p) -> alpha_convert_proc_con p q (proc_fwd_con p r) (proc_fwd_con q r)
| con_alpha_proc_fwd_con_replace3 : forall (p q : Ncon), forall (r : Ncon),
    (r <> p) -> alpha_convert_proc_con p q (proc_fwd_con r p) (proc_fwd_con r q)
| con_alpha_proc_fwd_con_leave : forall (p q : Ncon), forall (r s : Ncon),
    (r <> p) -> (s <> p) -> alpha_convert_proc_con p q (proc_fwd_con r s) (proc_fwd_con r s).

Lemma exists_alpha_convert_proc_con : forall (p q : Ncon) (P : proc), exists (Q : proc), alpha_convert_proc_con p q P Q.
Proof.
  intros p q.
  induction P as [ | a n b r P | r n b P | h n | a n b r P | r n b P | h n P | a P | r P | h P | Q | x n Q | a b | r s].
  - exists proc_nil. apply con_alpha_proc_nil.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_out_ser a n b q Q). rewrite Hr. by apply con_alpha_proc_out_ser_replace.
    + intro Hr. exists (proc_out_ser a n b r Q). by apply con_alpha_proc_out_ser_leave.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_out_con q n b Q). rewrite Hr. by apply con_alpha_proc_out_con_replace.
    + intro Hr. exists (proc_out_con r n b Q). by apply con_alpha_proc_out_con_leave.
  - exists (proc_out_loc h n). by apply con_alpha_proc_out_loc.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_in_ser a n b q Q). rewrite Hr. by apply con_alpha_proc_in_ser_replace.
    + intro Hr. exists (proc_in_ser a n b r Q). by apply con_alpha_proc_in_ser_leave.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_in_con q n b Q). rewrite Hr. by apply con_alpha_proc_in_con_replace.
    + intro Hr. exists (proc_in_con r n b Q). by apply con_alpha_proc_in_con_leave.
  - destruct IHP as [Q HQ]. exists (proc_in_loc h n Q). by apply con_alpha_proc_in_loc.
  - destruct IHP as [Q HQ]. exists (proc_nu_ser a Q). by apply con_alpha_proc_nu_ser.
  - destruct IHP as [Q HQ]. case (Ncon_dec r p).
    + intro Hr. exists (proc_nu_con q Q). rewrite Hr. by apply con_alpha_proc_nu_con_replace.
    + intro Hr. exists (proc_nu_con r Q). by apply con_alpha_proc_nu_con_leave.
  - destruct IHP as [Q HQ]. exists (proc_nu_loc h Q). by apply con_alpha_proc_nu_loc.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_par R1 R2). by apply con_alpha_proc_par.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_ite x n R1 R2). by apply con_alpha_proc_ite.
  - exists (proc_fwd_ser a b). by apply con_alpha_proc_fwd_ser.
  - case (Ncon_dec r p).
    + intro Hr. rewrite Hr. case (Ncon_dec s p).
      * intro Hs. rewrite Hs. exists (proc_fwd_con q q). by apply con_alpha_proc_fwd_con_replace1.
      * intro Hs. exists (proc_fwd_con q s). by apply con_alpha_proc_fwd_con_replace2.
    + intro Hr. case (Ncon_dec s p).
      * intros Hs. rewrite Hs. exists (proc_fwd_con r q). by apply con_alpha_proc_fwd_con_replace3.
      * intros Hs. exists (proc_fwd_con r s). by apply con_alpha_proc_fwd_con_leave.
Qed.

Inductive alpha_convert_proc_ser : Nser -> Nser -> proc -> proc -> Prop :=
| ser_alpha_proc_nil : forall (a b : Nser), alpha_convert_proc_ser a b proc_nil proc_nil
| ser_alpha_proc_out_ser_replace1 : forall (a b : Nser), forall (n : Nfo) (p : Ncon) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_out_ser a n a p P) (proc_out_ser b n b p Q)
| ser_alpha_proc_out_ser_replace2 : forall (a b : Nser), forall (n : Nfo) (c : Nser) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_out_ser a n c p P) (proc_out_ser b n c p Q)
| ser_alpha_proc_out_ser_replace3 : forall (a b : Nser), forall (c : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_out_ser c n a p P) (proc_out_ser c n b p Q)
| ser_alpha_proc_out_ser_leave : forall (a b : Nser), forall (c d : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> (d <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_out_ser c n d p P) (proc_out_ser c n d p Q)
| ser_alpha_proc_out_con_replace : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_out_con p n a P) (proc_out_con p n b Q)
| ser_alpha_proc_out_con_leave : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (c : Nser) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
                alpha_convert_proc_ser a b (proc_out_con p n c P) (proc_out_con p n c Q)
| ser_alpha_proc_out_loc : forall (a b : Nser), forall (h : Nref) (n : Nfo), alpha_convert_proc_ser a b (proc_out_loc h n) (proc_out_loc h n)
| ser_alpha_proc_in_ser_replace1 : forall (a b : Nser), forall (n : Nfo) (p : Ncon) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_ser a n a p P) (proc_in_ser b n b p Q)
| ser_alpha_proc_in_ser_replace2 : forall (a b : Nser), forall (n : Nfo) (c : Nser) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_in_ser a n c p P) (proc_in_ser b n c p Q)
| ser_alpha_proc_in_ser_replace3 : forall (a b : Nser), forall (c : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_in_ser c n a p P) (proc_in_ser c n b p Q)
| ser_alpha_proc_in_ser_leave : forall (a b : Nser), forall (c d : Nser) (n : Nfo) (p : Ncon) (P Q : proc),
    (c <> a) -> (d <> a) -> alpha_convert_proc_ser a b P Q ->
    alpha_convert_proc_ser a b (proc_in_ser c n d p P) (proc_in_ser c n d p Q)
| ser_alpha_proc_in_con_replace : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_con p n a P) (proc_in_con p n b Q)
| ser_alpha_proc_in_con_leave : forall (a b : Nser), forall (p : Ncon) (n : Nfo) (c : Nser) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_con p n c P) (proc_in_con p n c Q)
| ser_alpha_proc_in_loc : forall (a b : Nser), forall (h : Nref) (n : Nfo) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_in_loc h n P) (proc_in_loc h n Q)
| ser_alpha_proc_nu_ser_replace : forall (a b : Nser), forall (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_ser a P) (proc_nu_ser b Q)
| ser_alpha_proc_nu_ser_leave : forall (a b : Nser), forall (c : Nser) (P Q : proc),
    (c <> a) -> alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_ser c P) (proc_nu_ser c Q)
| ser_alpha_proc_nu_con : forall (a b : Nser), forall (p : Ncon) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_con p P) (proc_nu_con p Q)
| ser_alpha_proc_nu_loc : forall (a b : Nser), forall (h : Nref) (P Q : proc),
    alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser a b (proc_nu_loc h P) (proc_nu_loc h Q)
| ser_alpha_proc_par : forall (a b : Nser), forall (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_ser a b P1 Q1 -> alpha_convert_proc_ser a b P2 Q2 -> alpha_convert_proc_ser a b (proc_par P1 P2) (proc_par Q1 Q2)
| ser_alpha_proc_ite : forall (a b : Nser), forall (x n : Nfo) (P1 P2 Q1 Q2 : proc),
    alpha_convert_proc_ser a b P1 Q1 -> alpha_convert_proc_ser a b P2 Q2 -> alpha_convert_proc_ser a b (proc_ite x n P1 P2) (proc_ite x n Q1 Q2)
| ser_alpha_proc_fwd_ser_replace1 : forall (a b : Nser), alpha_convert_proc_ser a b (proc_fwd_ser a a) (proc_fwd_ser b b)
| ser_alpha_proc_fwd_ser_replace2 : forall (a b : Nser), forall (c: Nser),
    (c <> a) -> alpha_convert_proc_ser a b (proc_fwd_ser a c) (proc_fwd_ser b c)
| ser_alpha_proc_fwd_ser_replace3 : forall (a b : Nser), forall (c: Nser),
    (c <> a) -> alpha_convert_proc_ser a b (proc_fwd_ser c a) (proc_fwd_ser c b)
| ser_alpha_proc_fwd_ser_leave : forall (a b : Nser), forall (c d: Nser),
    (c <> a) -> (d <> a) -> alpha_convert_proc_ser a b (proc_fwd_ser c d) (proc_fwd_ser c d)
| ser_alpha_proc_fwd_con : forall (a b : Nser), forall (p q : Ncon), alpha_convert_proc_ser a b (proc_fwd_con p q) (proc_fwd_con p q).

Lemma exists_alpha_convert_proc_ser : forall (a b : Nser) (P : proc), exists (Q : proc), alpha_convert_proc_ser a b P Q.
Proof.
  intros a b.
  induction P as [ | c n d p P | p n d P | h n | c n d p P | p n d P | h n P | c P | p P | h P | Q | x n Q | c d | p q].
  - exists proc_nil. by apply ser_alpha_proc_nil.
  - destruct IHP as [Q HQ]. case (Nser_dec c a).
    + intro Hc. rewrite Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_out_ser b n b p Q). by apply ser_alpha_proc_out_ser_replace1.
      * intro Hd. exists (proc_out_ser b n d p Q). by apply ser_alpha_proc_out_ser_replace2.
    + intro Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_out_ser c n b p Q). by apply ser_alpha_proc_out_ser_replace3.        
      * intro Hd. exists (proc_out_ser c n d p Q). by apply ser_alpha_proc_out_ser_leave.
  - destruct IHP as [Q HQ]. case (Nser_dec d a).
    + intro Hd. rewrite Hd. exists (proc_out_con p n b Q). by apply ser_alpha_proc_out_con_replace.
    + intro Hd. exists (proc_out_con p n d Q). by apply ser_alpha_proc_out_con_leave.
  - exists (proc_out_loc h n). by apply ser_alpha_proc_out_loc.
  - destruct IHP as [Q HQ]. case (Nser_dec c a).
    + intro Hc. rewrite Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_in_ser b n b p Q). by apply ser_alpha_proc_in_ser_replace1.
      * intro Hd. exists (proc_in_ser b n d p Q). by apply ser_alpha_proc_in_ser_replace2.
    + intro Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_in_ser c n b p Q). by apply ser_alpha_proc_in_ser_replace3.        
      * intro Hd. exists (proc_in_ser c n d p Q). by apply ser_alpha_proc_in_ser_leave.
  - destruct IHP as [Q HQ]. case (Nser_dec d a).
    + intro Hd. rewrite Hd. exists (proc_in_con p n b Q). by apply ser_alpha_proc_in_con_replace.
    + intro Hd. exists (proc_in_con p n d Q). by apply ser_alpha_proc_in_con_leave.
  - destruct IHP as [Q HQ]. exists (proc_in_loc h n Q). by apply ser_alpha_proc_in_loc.
  - destruct IHP as [Q HQ]. case (Nser_dec c a).
    + intro Hc. rewrite Hc. exists (proc_nu_ser b Q). by apply ser_alpha_proc_nu_ser_replace.
    + intro Hc. exists (proc_nu_ser c Q). by apply ser_alpha_proc_nu_ser_leave.
  - destruct IHP as [Q HQ]. exists (proc_nu_con p Q). by apply ser_alpha_proc_nu_con.
  - destruct IHP as [Q HQ]. exists (proc_nu_loc h Q). by apply ser_alpha_proc_nu_loc.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_par R1 R2). by apply ser_alpha_proc_par.
  - destruct IHQ as [R1 HR1]. destruct IHP1 as [R2 HR2]. exists (proc_ite x n R1 R2). by apply ser_alpha_proc_ite.
  - case (Nser_dec c a).
    + intro Hc. rewrite Hc. case (Nser_dec d a).
      * intro Hd. rewrite Hd. exists (proc_fwd_ser b b). by apply ser_alpha_proc_fwd_ser_replace1.
      * intro Hs. exists (proc_fwd_ser b d). by apply ser_alpha_proc_fwd_ser_replace2.
    + intro Hc. case (Nser_dec d a).
      * intros Hd. rewrite Hd. exists (proc_fwd_ser c b). by apply ser_alpha_proc_fwd_ser_replace3.
      * intros Hd. exists (proc_fwd_ser c d). by apply ser_alpha_proc_fwd_ser_leave.
  - exists (proc_fwd_con p q). by apply ser_alpha_proc_fwd_con.
Qed.

Lemma alpha_alpha_proc_ser : forall (a b c : Nser) (P Q R: proc),
    notin_proc_ser b P -> alpha_convert_proc_ser a b P Q
    -> alpha_convert_proc_ser b c Q R
    -> alpha_convert_proc_ser a c P R.
Proof.
  move=> a b c P Q R HbP Halph1. move: R. induction Halph1; move=> R Halph2.
  - inversion Halph2. apply ser_alpha_proc_nil.
  - inversion Halph2; try easy.
    apply ser_alpha_proc_out_ser_replace1. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    + rewrite H4 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_ser_replace2. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    + rewrite H0 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_ser_replace3. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H6 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_ser_leave; try assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy. apply ser_alpha_proc_out_con_replace. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H5 in HbP. by inversion HbP.
    + apply ser_alpha_proc_out_con_leave. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_out_loc.
  - inversion Halph2; try easy.  apply ser_alpha_proc_in_ser_replace1. apply IHHalph1. by inversion HbP.
    assumption.
  - inversion Halph2; try easy.
    + rewrite H4 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_ser_replace2. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    + rewrite H0 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_ser_replace3. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H6 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_ser_leave. assumption. assumption. apply IHHalph1. by inversion HbP.
      assumption.
  - inversion Halph2; try easy.
    apply ser_alpha_proc_in_con_replace. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H5 in HbP. by inversion HbP.
    + apply ser_alpha_proc_in_con_leave. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_in_loc. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2; try easy.
    apply ser_alpha_proc_nu_ser_replace. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2.
    + rewrite H0 in HbP. by inversion HbP.
    + apply ser_alpha_proc_nu_ser_leave. assumption. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_nu_con. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_nu_loc. apply IHHalph1. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_par.
    apply IHHalph1_1. by inversion HbP. assumption. apply IHHalph1_2. by inversion HbP. assumption.
  - inversion Halph2. apply ser_alpha_proc_ite. apply IHHalph1_1. by inversion HbP. assumption.
    apply IHHalph1_2. by inversion HbP. assumption.
  - inversion Halph2; try easy. apply ser_alpha_proc_fwd_ser_replace1.
  - inversion Halph2; try easy.
    + rewrite H3 in HbP. by inversion HbP.
    + apply ser_alpha_proc_fwd_ser_replace2. assumption.
  - inversion Halph2; try easy.
    + rewrite H3 in HbP. by inversion HbP.
    + apply ser_alpha_proc_fwd_ser_replace3. assumption.
  - inversion Halph2.
    + rewrite H4 in HbP. by inversion HbP.
    + rewrite H1 in HbP. by inversion HbP.
    + rewrite H5 in HbP. by inversion HbP.
    + apply ser_alpha_proc_fwd_ser_leave; assumption.
  - inversion Halph2. apply ser_alpha_proc_fwd_con.
Qed.

Lemma alpha_proc_ser_idem (a b : Nser) (P Q : proc) : (a=b) -> alpha_convert_proc_ser a b P Q -> P = Q.
Proof.
  move=> Hab Halph. induction Halph; try rewrite Hab; try reflexivity; try have HPQ : P= Q by apply (IHHalph Hab). all : try by rewrite HPQ.
  all : have HPQ1 : P1= Q1 by apply (IHHalph1 Hab). all : rewrite HPQ1.
  all : have HPQ2 : P2 = Q2 by apply (IHHalph2 Hab). all: by rewrite HPQ2.
Qed.

Lemma alpha_proc_ser_invol (a b : Nser) (P Q R : proc) : notin_proc_ser b P -> alpha_convert_proc_ser a b P Q -> alpha_convert_proc_ser b a Q R -> P=R.
Proof.
  move=> HbP HPQ HQR. apply (alpha_proc_ser_idem a a). reflexivity. by apply (alpha_alpha_proc_ser a b a P Q R).
Qed.

Lemma alpha_alpha_proc_con : forall (p q r : Ncon) (P Q R: proc),
    notin_proc_con q P -> alpha_convert_proc_con p q P Q
    -> alpha_convert_proc_con q r Q R
    -> alpha_convert_proc_con p r P R.
Proof.
  move=> p q r P Q R HqP Halph1. move: R. induction Halph1; move=> R Halph2. all: inversion Halph2; try easy.
  all : inversion HqP.
  - apply con_alpha_proc_nil.
  - apply con_alpha_proc_out_ser_replace. by apply IHHalph1. 
  - by rewrite H6 in H12.
  - apply con_alpha_proc_out_ser_leave. assumption. by apply IHHalph1. 
  - apply con_alpha_proc_out_con_replace. by apply IHHalph1.
  - by rewrite H0 in H11.
  - apply con_alpha_proc_out_con_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_out_loc.
  - apply con_alpha_proc_in_ser_replace. by apply IHHalph1.
  - by rewrite H6 in H12.
  - apply con_alpha_proc_in_ser_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_in_con_replace. by apply IHHalph1.
  - by rewrite H0 in H11.
  - apply con_alpha_proc_in_con_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_in_loc.  by apply IHHalph1.
  - apply con_alpha_proc_nu_ser. by apply IHHalph1.
  - apply con_alpha_proc_nu_con_replace. by apply IHHalph1.
  - by rewrite H0 in H9.
  - apply con_alpha_proc_nu_con_leave. assumption. by apply IHHalph1.
  - apply con_alpha_proc_nu_loc. by apply IHHalph1.
  - apply con_alpha_proc_par. by apply IHHalph1_1. by apply IHHalph1_2.
  - apply con_alpha_proc_ite. by apply IHHalph1_1. by apply IHHalph1_2.
  - apply con_alpha_proc_fwd_ser.
  - apply con_alpha_proc_fwd_con_replace1.
  - by rewrite H3 in H8.
  - by apply con_alpha_proc_fwd_con_replace2. 
  - by rewrite H3 in H7.
  - by apply con_alpha_proc_fwd_con_replace3.
  - by rewrite H4 in H9.
  - by rewrite H1 in H10.
  - by rewrite H5 in H11.
  - by apply con_alpha_proc_fwd_con_leave.
Qed.

Lemma alpha_proc_con_idem (p q : Ncon) (P Q : proc) : (p=q) -> alpha_convert_proc_con p q P Q -> P = Q.
Proof.
  move=> Hpq Halph. induction Halph; try rewrite Hpq; try reflexivity; try have HPQ : P= Q by apply (IHHalph Hpq). all : try by rewrite HPQ.
  all : have HPQ1 : P1= Q1 by apply (IHHalph1 Hpq). all : rewrite HPQ1.
  all : have HPQ2 : P2 = Q2 by apply (IHHalph2 Hpq). all: by rewrite HPQ2.
Qed.

Lemma alpha_proc_con_invol (p q : Ncon) (P Q R : proc) : notin_proc_con q P -> alpha_convert_proc_con p q P Q -> alpha_convert_proc_con q p Q R -> P=R.
Proof.
  move=> HqP HPQ HQR. apply (alpha_proc_con_idem p p). reflexivity. by apply (alpha_alpha_proc_con p q p P Q R).
Qed.

Inductive preVisTyp : Visfun -> stack_ -> proc -> Prop :=
| previstyp_nil : preVisTyp vis0 (In nil) proc_nil
| previstyp_O_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (V : Vis) (s : stackO) (P : proc),
    let Vb := add_V V (srv b) in
    let B1 := add_vis (names (cnt p)) Vb (add_vis (names (srv b)) Vb vis0) in
    let B2 := add_vis cur V vis0 in
    V (srv a) -> preVisTyp B1 (In (stack_in p s)) P
    -> preVisTyp B2 (Out s) (proc_out_ser a n b p P)
| previstyp_O_CON : forall (p : Ncon) (n : Nfo) (b : Nser) (V : Vis) (P : proc),
    let Vb := add_V V (srv b) in
    let B1 := add_vis (names (cnt p)) Vb vis0 in
    let B2 := add_vis cur V vis0 in
    V (cnt p) -> preVisTyp B1 (In nil) P
    -> preVisTyp B2 (Out (stack_out p nil)) (proc_out_con p n b P)
| previstyp_O_LOC : forall (h: Nref) (n : Nfo), preVisTyp vis0 (In nil) (proc_out_loc h n)
| previstyp_I_SER : forall (a b : Nser) (n : Nfo) (p : Ncon) (V : Vis) (P : proc),
    let Vbp := add_V (add_V V (srv b)) (cnt p) in
    let B1 := add_vis cur Vbp vis0 in
    let B2 := add_vis (names (srv a)) V vis0 in
    preVisTyp B1 (Out (stack_out p nil)) P
    -> preVisTyp B2 (In nil) (proc_in_ser a n b p P)
| previstyp_I_CON : forall (p q : Ncon) (n : Nfo) (b : Nser) (V : Vis) (P : proc),
    let Vb := add_V V (srv b) in
    let B1 := add_vis cur Vb vis0 in
    let B2 := add_vis (names (cnt p)) V vis0 in
    preVisTyp B1 (Out (stack_out q nil)) P
    -> preVisTyp B2 (In (stack_in p (stack_out q nil))) (proc_in_con p n b P)
| previstyp_I_LOC : forall (h : Nref) (n : Nfo) (V : Vis) (s : stack_) (P : proc),
    let B := add_vis cur V vis0 in
    preVisTyp B s P -> preVisTyp B s (proc_in_loc h n P)
| previstyp_RES_SER : forall (a : Nser) (B : Visfun) (s : stack_) (P : proc),
    preVisTyp B s P -> preVisTyp (trans_minus_vis (srv a) B) s (proc_nu_ser a P)
| previstyp_RES_CON1 : forall (p : Ncon) (B : Visfun) (s1 : stack_) (s2 : stack_) (P : proc),
    insert_stack_p p s1 s2 -> preVisTyp B s1 P -> preVisTyp (trans_minus_vis (cnt p) B) s2 (proc_nu_con p P)
| previstyp_RES_CON2 : forall (p : Ncon) (B : Visfun) (s : stack_) (P : proc),
    not_in_stack p s -> preVisTyp B s P -> preVisTyp (trans_minus_vis (cnt p) B) s (proc_nu_con p P)
| previstyp_RES_LOC : forall (h : Nref) (B : Visfun) (s : stack_) (P : proc),
    preVisTyp B s P -> preVisTyp B s (proc_nu_loc h P)
| previstyp_PAR : forall (B1 B2 : Visfun) (s s1 s2 : stack_) (P1 P2 : proc),
    dom_disj (dom B1) (dom B2) -> inter_ s1 s2 s ->
    let B := union_vis B1 B2 in
    preVisTyp B1 s1 P1 -> preVisTyp B2 s2 P2 -> preVisTyp B s (proc_par P1 P2)
| previstyp_ITE : forall (x n : Nfo) (B : Visfun) (s : stack_) (P1 P2 : proc),
    preVisTyp B s P1 -> preVisTyp B s P2 -> preVisTyp B s (proc_ite x n P1 P2)
| previstyp_FwC : forall (p q : Ncon) (V : Vis),
    let B := add_vis (names (cnt p)) V vis0 in
    let s := In (stack_in p (stack_out q nil)) in
    V (cnt q) -> preVisTyp B s (proc_fwd_con p q)
| previstyp_FwF : forall (a b : Nser) (V : Vis) ,
    let B := add_vis (names (srv a)) V vis0 in
    V (srv b) -> preVisTyp B (In nil) (proc_fwd_ser a b).

Definition is_fresh_nser (a : Nser) (B : Visfun) (s : stack_) (P : proc) : Prop:=
  ~(dom B (names (srv a))) /\ (forall (x : Names_cur), ~(B x (srv a))) /\ notin_proc_ser a P.

Lemma exists_fresh_nser : forall (B : Visfun) (s : stack_) (P : proc), exists (a : Nser), is_fresh_nser a B s P.
Proof.
  intros B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_ser P) as [nP HP]. exists (give_nser (max nP (max ndomB ncodomB))).
  split.
  - have Hk : max nP (max ndomB ncodomB) >= ndomB by lia. apply (proj1 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB ncodomB) >= ncodomB by lia. intro x. apply (proj1 (HcodomB _ Hk x)).
    + have Hk : max nP (max ndomB ncodomB) >= nP by lia. apply (HP _ Hk).
Qed.

Definition is_fresh_ncon (p : Ncon) (B : Visfun) (s : stack_) (P : proc) : Prop:=
  ~(dom B (names (cnt p))) /\ (forall (x : Names_cur), ~(B x (cnt p))) /\ notin_proc_con p P /\ not_in_stack p s.

Lemma exists_fresh_ncon : forall (B : Visfun) (s : stack_) (P : proc), exists (p : Ncon), is_fresh_ncon p B s P.
Proof.
  intros B s P. destruct (dom_fin B) as [ndomB HdomB]. destruct (codom_fin B) as [ncodomB HcodomB].
  destruct (proc_fin_con P) as [nP HP]. destruct (stack_fin s) as [ns Hs]. exists (give_ncon (max nP (max ndomB (max ncodomB ns)))).
  split.
  - have Hk : max nP (max ndomB (max ncodomB ns)) >= ndomB by lia. apply (proj2 (HdomB _ Hk)).
  - split.
    + have Hk : max nP (max ndomB (max ncodomB ns)) >= ncodomB by lia. intro x. apply (proj2 (HcodomB _ Hk x)).
    + split.
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= nP by lia. apply (HP _ Hk).
      * have Hk : max nP (max ndomB (max ncodomB ns)) >= ns by lia. apply (Hs _ Hk).
Qed.

Lemma erase_ser_previstyp : forall (P : proc) (B : Visfun) (s : stack_) (a : Nser),
    notin_proc_ser a P -> preVisTyp B s P -> preVisTyp (erase_vis (srv a) B) s P.
Proof.
  induction P; move=> B s a Ha Htyp; inversion Htyp.
  - rewrite erase_empty. apply previstyp_nil.
  - unfold B2. rewrite erase_add_vis2. easy. rewrite erase_empty.
    apply previstyp_O_SER. split; try assumption. intro. inversion  H8. rewrite H10 in Ha. by inversion Ha.
    have Her_add : (add_vis (names (cnt n2)) (add_V (minus_V V0 (srv a)) (srv n1)) (add_vis (names (srv n1)) (add_V (minus_V V0 (srv a)) (srv n1)) vis0)) = erase_vis (srv a) B1.
    + unfold B1. rewrite erase_add_vis2. easy. rewrite erase_add_vis2. intro. inversion H8. rewrite H10 in Ha.
      by inversion Ha. rewrite erase_empty.
      have HV : forall x, (add_V (minus_V V0 (srv a)) (srv n1)) x <-> (minus_V Vb0 (srv a)) x.
      * unfold Vb0. move=> x. split=> [Had | Hmi].
        ++ destruct Had. destruct H8. split; [by left | assumption]. split. by right. intro. rewrite H8 in H9.
           inversion H9. rewrite H11 in Ha. by inversion Ha.
        ++ destruct Hmi. destruct H8. left. by split. by right. 
      *  by repeat rewrite (add_equiv_vis _ _ _ _ HV).
    + rewrite Her_add. apply IHP. by inversion Ha. assumption.
  - unfold B2. rewrite erase_add_vis2. easy. rewrite erase_empty.
    apply previstyp_O_CON. by split.
    have Her_add : (add_vis (names (cnt n)) (add_V (minus_V V0 (srv a)) (srv n1)) vis0) = erase_vis (srv a) B1.
    + unfold B1. rewrite erase_add_vis2. easy.  rewrite erase_empty. apply add_equiv_vis.
      move=> x; split=> [Hadd | Hmin].
      * destruct Hadd. destruct H7. split; [|assumption]. by left. split. right. by rewrite (eq_sym H4) in H7.
        intro. rewrite H8 in H7. inversion H7. rewrite H10 in Ha. by inversion Ha.
      * destruct Hmin. destruct H7. left. by split. right. rewrite H7. by rewrite H4.
    + rewrite Her_add. apply IHP. by inversion Ha. assumption.
  -  rewrite erase_empty. apply previstyp_O_LOC.
  - unfold B0. rewrite erase_add_vis2. intro. inversion H7.  rewrite H9 in Ha. by inversion Ha.
    rewrite erase_empty. apply previstyp_I_SER.
    have Her_add : (add_vis cur (add_V (add_V (minus_V V0 (srv a)) (srv n1)) (cnt n2)) vis0) = erase_vis (srv a) B1.
    + unfold B1. rewrite erase_add_vis2. easy. rewrite erase_empty. apply add_equiv_vis. move=> x; split=> [Hadd|Hmin].
      * destruct Hadd. destruct H7. destruct H7. split; [|assumption]. left. by left. split. left. right. by rewrite H4. rewrite H7. intro. inversion H8. rewrite H10 in Ha. by inversion Ha.
        split. right. by rewrite H5. rewrite H7. easy.
      * destruct Hmin. destruct H7. destruct H7. left. left. by split.
        left. right. by rewrite H4 in H7. right. by rewrite H5 in H7.
    + rewrite Her_add. apply IHP. by inversion Ha. assumption.
  - unfold B0. rewrite erase_add_vis2. easy. rewrite erase_empty. apply previstyp_I_CON.
    have Her_add : (add_vis cur (add_V (minus_V V0 (srv a)) (srv n1)) vis0) = erase_vis (srv a) B1.
    unfold B1. rewrite erase_add_vis2. easy. rewrite erase_empty. apply add_equiv_vis.
    + move=> x; split=> [Hadd | Hmin].
      * destruct Hadd. destruct H6. split; [by left|assumption].  split. right. by rewrite H4. rewrite H6.
        intro. inversion H7. rewrite H9 in Ha. by inversion Ha.
      * destruct Hmin. destruct H6. left. by split. right. by rewrite (eq_sym H4).
    + rewrite Her_add. apply IHP. by inversion Ha. assumption.
  - unfold B0. rewrite erase_add_vis2. easy. rewrite erase_empty. apply previstyp_I_LOC.
    have Her_add : (add_vis cur (minus_V V0 (srv a)) vis0) = erase_vis (srv a) B0.
    rewrite erase_add_vis2. easy. rewrite erase_empty. reflexivity.
    rewrite Her_add. apply IHP. by inversion Ha. assumption.
  - rewrite erase_trans_minus. intro. inversion H4. rewrite H6 in Ha. by inversion Ha.
    apply previstyp_RES_SER. apply IHP. by inversion Ha. assumption.
  - rewrite erase_trans_minus. easy. apply (previstyp_RES_CON1 _ _ s1 s _). assumption.
    apply IHP. by inversion Ha. assumption.
  - rewrite erase_trans_minus. easy. apply previstyp_RES_CON2. assumption. apply IHP.
    by inversion Ha. assumption.
  - apply previstyp_RES_LOC. apply IHP. by inversion Ha. assumption.
  - unfold B0. rewrite erase_union_vis. inversion Ha.
    apply (previstyp_PAR _ _ _ s1 s2);[|assumption| by apply IHP1 | by apply IHP2].
    move=> x Her1 Her2; destruct Her1 as [HB1 _]; destruct Her2 as [HB2 _]; apply (H1 x HB1 HB2).
  - inversion Ha. by apply previstyp_ITE; [apply IHP1 | apply IHP2].
  - unfold B1. rewrite erase_add_vis2. intro. inversion H4. rewrite H6 in Ha. by inversion Ha.
    rewrite erase_empty. apply previstyp_FwF. split. assumption. intro. inversion H4. rewrite H6 in Ha. by inversion Ha.
  - unfold B1. rewrite erase_add_vis2. easy. rewrite erase_empty. rewrite (eq_sym H3). apply previstyp_FwC.
    split. rewrite H3. assumption. easy.
Qed.

Lemma co_erase_ser_previstyp : forall (P : proc) (B : Visfun) (s : stack_) (a : Nser),
    notin_proc_ser a P -> ~dom B (names (srv a)) -> preVisTyp (erase_vis (srv a) B) s P -> preVisTyp B s P.
Proof.
  induction P;move=> B s a HaP Hdom Htyp; inversion Htyp. 
  - have HB : B = vis0. apply vis_is_empty. split. intro. rewrite H0. split. assumption. intro.
    by rewrite H3 in Hdom. by intro. rewrite HB.
    apply previstyp_nil.
  - have HB : B = add_vis cur (B cur) vis0.  apply eq_visfun.
    have Hmdom : forall x, minus_dom (dom B) (names (srv a)) x <-> dom B x.
    move=> x. split. intro. destruct H9. assumption. intro. split. assumption. intro. by rewrite (eq_sym H10) in H9.
    split.
    +  split. intro. simpl. rewrite H. by apply (Hmdom x).
       intro. destruct H9. easy. apply (proj1 (Hmdom x)). rewrite (eq_sym H). by right.
    + move=> x y. split.
      * intro. right. simpl. rewrite (eq_sym H) in Hmdom.
        have Hxcur : cur = x. have Hdx : dom B x by apply (_dom B x y H9). have Hdex : add_dom empty cur x by apply (proj2 (Hmdom x) Hdx). by destruct Hdex. split. by rewrite Hxcur. assumption.
      * intro. destruct H9. easy. destruct H9. by rewrite H10 in H9.
    + rewrite HB. apply previstyp_O_SER. have Hern : erase_B (srv a) B (cur) (srv n).
      rewrite (eq_sym H0). right. by split. destruct Hern. by destruct H9.
      apply (IHP _ _ a). by inversion HaP.
      simpl. intro. destruct H9. destruct H9. easy. inversion H9. rewrite H11 in HaP. by inversion HaP.
      by inversion H9.
      have HB1 : B1 = (erase_vis (srv a) (add_vis (names (cnt n2)) (add_V (B cur) (srv n1)) (add_vis (names (srv n1)) (add_V (B cur) (srv n1)) vis0))).
      * apply eq_visfun. split.
        ++  simpl. split; move=> Hd.
            **  split. assumption. intro. destruct Hd. destruct H10. easy. rewrite (eq_sym H10) in H9.
                inversion H9. rewrite H12 in HaP. by inversion HaP.
                rewrite (eq_sym H9) in H10. by inversion H10.
            ** by destruct Hd.
        ++ have Her_incl : forall x, V0 x <-> erase_B (srv a) B cur x. rewrite (eq_sym H0).
           move=> x; split=> [HVx | Herx]. right. by split.
           destruct Herx. easy. by destruct H9.
           have Hincl : forall x, V0 x -> B cur x.
           move=> x HVx. destruct (proj1 (Her_incl x) HVx). by destruct H9.                                   
           move=> x y. split=> [HB1 | Her].
           ** unfold B1 in HB1. destruct HB1. destruct H9. easy.
              destruct H9. destruct H9. 
              split. split. left. right. split.
              left. by apply Hincl. assumption. intro. destruct (proj1 (Her_incl y) H9). rewrite H11 in H12. by inversion H12. intro. rewrite (eq_sym H10) in H11. inversion H11. rewrite H13 in HaP. by inversion HaP.
              split. split. left. right. split. by right. assumption. intro. rewrite H11 in H9. inversion H9.
              rewrite H13 in HaP. by inversion HaP.
              intro. rewrite (eq_sym H11) in H10. inversion H10. rewrite H13 in HaP. by inversion HaP.
              destruct H9. destruct H9.
              split. split. right. split. left. by apply Hincl. assumption. intro. rewrite H11 in H9.
              destruct (proj1 (Her_incl _) H9). by destruct H12. intro. rewrite (eq_sym H11) in H10. by inversion H10.
              split. split. right. split. by right. assumption. intro. rewrite H11 in H9. inversion H9.
              rewrite H13 in HaP. by inversion HaP.
              intro. rewrite (eq_sym H11) in H10. by inversion H10.
           ** destruct Her. destruct H9. destruct H9. destruct H9. easy. destruct H9. destruct H9.
              left. right. split. left. apply (proj2 (Her_incl y)). split. by split. easy. assumption.
              left. right. split. by right. assumption. destruct H9. destruct H9.
              right. split. left. apply (Her_incl y). split. by split. easy. assumption.
              right. split. by right.  assumption.
      * by rewrite (eq_sym HB1). 
  - have HB : B = add_vis cur (B cur) vis0. apply eq_visfun.
    have Hmdom : dom_eq (dom B) (minus_dom (dom B) (names (srv a))).
    split=> Hd. split. assumption. intro. by rewrite (eq_sym H8) in Hd. by destruct Hd. 
    split. simpl. by rewrite H.
    move=> x y; split=> HB. right. have xcur : cur = x. rewrite (eq_sym H) in Hmdom. have domxcur : add_dom empty cur x. apply (Hmdom x). apply (_dom B x y HB). by destruct domxcur. split.  by rewrite xcur. assumption.
    destruct HB. easy. destruct H8. by rewrite H9 in H8.
    have Her_x : forall x, V0 x <-> erase_B (srv a) B cur x.
    move=> x; split=> Hx. rewrite (eq_sym H0). right. by split. rewrite (eq_sym H0) in Hx. destruct Hx.
    easy. by destruct H8.
    have HVB : forall x, V0 x -> B cur x. move=> x Hx. destruct (proj1 (Her_x x) Hx). by destruct H8.
    rewrite HB. apply previstyp_O_CON. by apply HVB.
    apply (IHP _ _ a). by inversion HaP. intro. destruct H8. easy. by inversion H8.
    have Hdom1 :   dom_eq (add_dom empty (names (cnt n))) (minus_dom (add_dom empty (names (cnt n))) (names (srv a))).
    simpl. split=> Hd. split. assumption. intro. rewrite (eq_sym H8) in Hd. by destruct Hd.
    by destruct Hd.
    have HB1 : B1 = (erase_vis (srv a) (add_vis (names (cnt n)) (add_V (B cur) (srv n1)) vis0)).
    + apply eq_visfun. split. easy.
      move=> x y; split=> HB1.
      * destruct HB1. easy. destruct H8. destruct H8.
        split. split. right. split. left. by apply HVB. assumption. intro. rewrite H10 in H8.
        destruct (proj1 (Her_x _) H8). by inversion H11. intro. rewrite (eq_sym H10) in H9. by inversion H9.
        split. split. right. split. rewrite H8. rewrite H5. by right. assumption. rewrite H8. rewrite H5.
        intro. inversion H10. rewrite H12 in HaP. by inversion HaP. rewrite (eq_sym H9). intro. by inversion H10.
      * destruct HB1. destruct H8. destruct H8. easy. destruct H8. destruct H8.
        right. split. left. apply Her_x. split. by split. easy. assumption. right. split.
        right. by rewrite H5. assumption.
    + by rewrite (eq_sym HB1).
  - inversion Htyp.
    have HB : B = vis0. apply vis_is_empty. rewrite H5. split=> HdomB.
    split. assumption. intro. by rewrite H7 in Hdom. by destruct HdomB. rewrite HB.
    apply previstyp_O_LOC.
  - have HB : B = add_vis (names (srv n)) (B (names (srv n))) vis0. apply eq_visfun.
    have HdB : dom_eq (dom B) (add_dom empty (names (srv a0))). rewrite H.
    split=> Hd. split. assumption. intro. by rewrite H8 in Hdom. by destruct Hd.
    split. simpl. rewrite H3 in HdB. assumption.
    move=>x y; split=> HBx. right. have Hnx : names (srv n) = x.  destruct (proj1 (HdB x) (_dom B x y HBx)).
    easy. by rewrite H3 in H8.
    split. by rewrite Hnx. assumption. destruct HBx. easy. destruct H8. by rewrite H9 in H8.
    rewrite HB. apply previstyp_I_SER. apply (IHP _ _ a). by inversion HaP.
    simpl. intro. by destruct H8.
    have Her_x : forall x, V0 x <-> erase_B (srv a) B (names (srv a0)) x.
    rewrite (eq_sym H1). move=> x; split=> Hd. right. by split. destruct Hd. easy. by destruct H8.
    have HVB : forall x , V0 x -> B (names (srv a0)) x. move=> x HVx. destruct (proj1 (Her_x x) HVx). by destruct H8.
    have HB1 : B1= (erase_vis (srv a) (add_vis cur (add_V (add_V (B (names (srv n))) (srv n1)) (cnt n2)) vis0)).
    + apply eq_visfun. split. simpl. split=> Hd. split. assumption. intro. rewrite (eq_sym H8) in Hd. by destruct Hd. by destruct Hd.
      move=> x y; split=> HB1.
      * destruct HB1. easy. destruct H8. destruct H8. destruct H8.
        rewrite (eq_sym H3). split. split. right. split. left. left. by apply HVB. assumption. intro.
        rewrite H10 in H8. destruct (proj1 (Her_x _) H8). by destruct H11. intro. by rewrite (eq_sym H10) in H9.
        split. split. right. split. left. right. by rewrite H5 in H8. assumption. rewrite H8.
        intro. inversion H10. rewrite H5 in H12. rewrite H12 in HaP. by inversion HaP.
        by rewrite (eq_sym H9).
        split. split. right. split. right. by rewrite H6 in H8. assumption. by rewrite H8. by rewrite (eq_sym H9).
      * destruct HB1. destruct H8. destruct H8. easy. destruct H8. destruct H8. destruct H8.
        right. split. left. left. apply Her_x. split. split. by rewrite H3. assumption. rewrite H3. intro.
        inversion H12. rewrite H14 in HaP. by inversion HaP. assumption.
        right. split. left. right. by rewrite H5. assumption. right. split. right. by rewrite H6. assumption.
    + by rewrite (eq_sym HB1).
  - have HB : B = add_vis (names (cnt p)) (B (names (cnt p))) vis0.
    have HdB : dom_eq (dom B) (add_dom empty (names (cnt p))).
    rewrite H. move=> x; split=> Hd. split. assumption. intro. by rewrite H7 in Hdom. by destruct Hd.
    apply eq_visfun. split. simpl. assumption. move=> x y; split=> HBx.
    right. have Hpx : (names (cnt p)) = x. by destruct (proj1 (HdB x) (_dom B x y HBx)).
    split. by rewrite Hpx. assumption. destruct HBx. easy. destruct H7. by rewrite H8 in H7.
    rewrite HB. rewrite H3. apply previstyp_I_CON. apply (IHP _ _ a). by inversion HaP.
    simpl. intro. by destruct H7.
    have Her_x : forall x, V0 x <-> erase_B (srv a) B (names (cnt p)) x.
    rewrite (eq_sym H1). move=> x; split=> Hd. right. by split. destruct Hd. easy. by destruct H7.
    have HVB : forall x , V0 x -> B (names (cnt p)) x. move=> x HVx. destruct (proj1 (Her_x x) HVx). by destruct H7.
    have HB1 : B1 = (erase_vis (srv a) (add_vis cur (add_V (B (names (cnt n))) (srv n1)) vis0)).
    rewrite erase_add_vis2. easy. rewrite erase_empty. apply add_equiv_vis.
    move=> x; split=> HV. destruct HV. split. left. rewrite (eq_sym H3). by apply HVB. intro.
    rewrite H8 in H7. destruct (proj1 (Her_x _) H7). by destruct H9.
    split. right. by rewrite (eq_sym H5). rewrite H7. intro. inversion H8. rewrite H5 in H10. rewrite H10 in HaP. by inversion HaP.
    destruct HV. destruct H7. left. apply Her_x. split. split. by rewrite H3. assumption. easy.
    right. by rewrite H5.
    by rewrite (eq_sym HB1).
  - have HB : B = add_vis cur (B cur) vis0.
    have HdB : dom_eq (dom B) (add_dom empty cur).
    rewrite H. move=> x; split=> Hd. split. assumption. intro. by rewrite H6 in Hdom. by destruct Hd.
    apply eq_visfun. split.  by simpl. move=> x y; split=> HB.
    have Hxcur : cur = x by destruct (proj1 (HdB x) (_dom B _ _ HB)). right. split. by rewrite Hxcur. assumption. destruct HB. easy. destruct H6. by rewrite H7 in H6.
    rewrite HB. apply previstyp_I_LOC. apply (IHP _ _ a). by inversion HaP. simpl. intro. by destruct H6.
    have HB0 : B0 = (erase_vis (srv a) (add_vis cur (B cur) vis0)).
    rewrite erase_add_vis2. easy. rewrite erase_empty.
    have Her_x : forall x, V0 x <-> erase_B (srv a) B cur x. rewrite (eq_sym H1). move=> x; split=> HV. 
    right. by split. destruct HV. easy. by destruct H6.
    have HVB : forall x, V0 x -> B cur x. move=> x HVx. destruct (proj1 (Her_x x) HVx). by destruct H6.
    apply add_equiv_vis. move=> x; split=> HV.  split. by apply HVB. intro. rewrite H6 in HV.
    destruct (proj1 (Her_x _) HV). by destruct H7. destruct HV. apply Her_x. split. by split. easy.
    by rewrite (eq_sym HB0).
  - pose B1 := union_vis B0 (filter_vis B (srv a)).
    have Haa01 : a <> a0. rewrite H3. move=>Haa0. rewrite Haa0 in HaP. by inversion HaP.
    have Haa02 : srv a <> srv a0. move=> Haa0. by inversion Haa0.
    have Haa03 : names (srv a) <> names (srv a0). move=> Haa0. by inversion Haa0.
    have Hdom2 : ~dom B (names (srv a0)).
    intro. have Hdom_ : minus_dom (dom B) (names (srv a)) (names (srv a0)). by split.
    rewrite (eq_sym H) in Hdom_. by destruct Hdom_.    
    have HB : B = trans_minus_vis (srv a0) B1.
    apply eq_visfun. split.
    + move=> x; split=> Hd. simpl. split. right. assumption. intro.
      have Hdx : minus_dom (dom B) (names (srv a)) x. split. assumption. rewrite (eq_sym H5). assumption.
      rewrite (eq_sym H) in Hdx. rewrite (eq_sym H5) in Hdx.
      by inversion Hdx.
      destruct Hd. destruct H5. have Hdx : minus_dom (dom B0) (names (srv a0)) x by split.
      rewrite H in Hdx. by destruct Hdx. assumption.
    + move=> x y; split=> Hxy.
      * destruct y as [c |r]. case (Nser_dec c a). move=> Hca.
        split. left. right. split. assumption. by rewrite Hca. split. intro. rewrite H5 in Hxy. by apply (Hdom2 (_dom B _ _ Hxy)). by rewrite Hca.
        move=> Hca. have Her : erase_B (srv a) B x (srv c). split. split. assumption.
        intro. by inversion H5. intro. rewrite H5 in Hdom. by apply (Hdom (_dom B _ _ Hxy)).
        rewrite (eq_sym H1) in Her. destruct Her. destruct H6.
        split. destruct H5. left. by left. right. destruct H5. split; by left. by split. 
        have Her : erase_B (srv a) B x (cnt r). split. split. assumption.
        intro. by inversion H5. intro. rewrite H5 in Hdom. by apply (Hdom (_dom B _ _ Hxy)).
        rewrite (eq_sym H1) in Her. destruct Her. destruct H6.
        split. destruct H5. left. by left. right. destruct H5. split; by left. by split. 
      * destruct Hxy. destruct H6. destruct H5.
        ++  destruct H5. have Htr : trans_minus_vis_B (srv a0) (dom B0) B0 x y.
            split. by left. by split. rewrite H1 in Htr. destruct Htr. by destruct H8. by destruct H5.
        ++ destruct H5. destruct H5. destruct H8. have Htr : trans_minus_vis_B (srv a0) (dom B0) B0 x y.
           split. right. by split. by split.
           rewrite H1 in Htr. destruct Htr. by destruct H9.
           destruct H8.
           have Ha0 : minus_dom (dom B) (names (srv a)) (names (srv a0)).
           split.  by apply (_dom B _ _ H8). assumption. rewrite (eq_sym H) in Ha0. by inversion Ha0.
           destruct H5. by rewrite H9 in Haa02.
    +  rewrite HB. rewrite H3. apply previstyp_RES_SER. apply (IHP _ _ a). by inversion HaP.
       intro. destruct H5.  have Hda : minus_dom (dom B0) (names (srv a0)) (names (srv a)).
       split. assumption. by symmetry. rewrite H in Hda. by destruct Hda. easy.
       rewrite erase_union_vis.
       have Hun : union_vis (erase_vis (srv a) B0) (erase_vis (srv a) (filter_vis B (srv a))) = erase_vis (srv a) B0.
       apply eq_visfun. split.
       ++ simpl. move=> x; split=> Hd.
          destruct Hd. assumption. have Hmd : minus_dom (dom B0) (names (srv a0)) x by rewrite H. destruct Hmd. split. assumption. by destruct H5. by left.
       ++ move=> x y; split=> HB0.
          ** destruct HB0. assumption. destruct H5. destruct H5. by destruct H5.
          ** by left.
       ++ rewrite Hun. apply erase_ser_previstyp. by inversion HaP. assumption.
  - 

       
             
             
          
Admitted.


Lemma erase_con_previstyp : forall (P : proc) (B : Visfun) (s : stack_) (p : Ncon),
    notin_proc_con p P -> preVisTyp B s P -> preVisTyp (erase_vis (cnt p) B) s P.
Proof.
  induction P; move=> B s p Hp Htyp; inversion Htyp.
  - rewrite erase_empty. by apply previstyp_nil.
  - unfold B2. rewrite erase_add_vis2. easy. rewrite erase_empty. apply previstyp_O_SER.
    split; easy.
    have HB : (add_vis (names (cnt n2)) (add_V (minus_V V0 (cnt p)) (srv n1))  (add_vis (names (srv n1)) (add_V (minus_V V0 (cnt p)) (srv n1)) vis0)) = erase_vis (cnt p) B1.
    unfold B1. rewrite erase_add_vis2.
    intro. inversion H8. rewrite H10 in Hp. by inversion Hp.
    rewrite erase_add_vis2. easy. rewrite erase_empty. unfold Vb0.
    have HB_aux : add_vis (names (srv n1)) (add_V (minus_V V0 (cnt p)) (srv n1)) vis0 = add_vis (names (srv n1)) (minus_V (add_V V0 (srv n1)) (cnt p)) vis0 .
    + apply add_equiv_vis. by apply add_minus_V.
    + rewrite HB_aux. apply add_equiv_vis. by apply add_minus_V.
    + rewrite HB. inversion Hp. by apply IHP.
  - unfold B2. rewrite erase_add_vis2. easy. rewrite erase_empty. apply previstyp_O_CON. split. assumption.
    intro. inversion H7. rewrite H9 in Hp. by inversion Hp.
    have HB : (add_vis (names (cnt n)) (add_V (minus_V V0 (cnt p)) (srv n1)) vis0) = erase_vis (cnt p) B1.
    + rewrite erase_add_vis2. intro. inversion H7. rewrite H9 in Hp. by inversion Hp.
      rewrite erase_empty. apply add_equiv_vis. unfold Vb. rewrite H4. by apply add_minus_V.
    + rewrite HB. inversion Hp. by apply IHP.
  - rewrite erase_empty. apply previstyp_O_LOC.
  - rewrite erase_add_vis2. easy. rewrite erase_empty. apply previstyp_I_SER.
    have HB : add_vis cur (add_V (add_V (minus_V V0 (cnt p)) (srv n1)) (cnt n2)) vis0 = erase_vis (cnt p) B1.
    + rewrite erase_add_vis2. easy. rewrite erase_empty. apply add_equiv_vis. unfold Vbp.
      have Hx1 : forall (x : Names), add_V (add_V (minus_V V0 (cnt p)) (srv n1)) (cnt n2) x <-> add_V (minus_V (add_V V0 (srv n1)) (cnt p)) (cnt n2) x. apply add_equiv_V. apply add_minus_V. easy.
      have Hx2 : forall (x : Names), add_V (minus_V (add_V V0 (srv n1)) (cnt p)) (cnt n2) x <-> minus_V (add_V (add_V V0 (srv b)) (cnt p0)) (cnt p) x. rewrite H5. rewrite H4. apply add_minus_V. intro. inversion H7. rewrite H9 in Hp. by inversion Hp.
      move=> x. split=> Hx. apply Hx2. by apply Hx1. apply Hx1. by apply Hx2.
    + rewrite HB. inversion Hp. by apply IHP.
  - rewrite erase_add_vis2. intro. inversion H6. rewrite H8 in Hp. by inversion Hp.
    rewrite erase_empty. apply previstyp_I_CON.
    have HB : (add_vis cur (add_V (minus_V V0 (cnt p)) (srv n1)) vis0) = erase_vis (cnt p) B1.
    + rewrite erase_add_vis2. easy. rewrite erase_empty. apply add_equiv_vis. unfold Vb. rewrite H4. by apply add_minus_V.
    + rewrite HB. inversion Hp. by apply IHP.
  - rewrite erase_add_vis2. easy. rewrite erase_empty. apply previstyp_I_LOC.
    have HB : (add_vis cur (minus_V V0 (cnt p)) vis0) = erase_vis (cnt p) B0.
    + rewrite erase_add_vis2. easy. by rewrite erase_empty.
    + rewrite HB. inversion Hp. by apply IHP.
  - rewrite erase_trans_minus. easy. apply previstyp_RES_SER. inversion Hp. by apply IHP.
  - rewrite erase_trans_minus. intro. inversion H5. rewrite H7 in Hp. by inversion Hp.
    apply (previstyp_RES_CON1 _ _ s1). assumption. inversion Hp. by apply IHP.
  - rewrite erase_trans_minus. intro. inversion H5. rewrite H7 in Hp. by inversion Hp.
    apply previstyp_RES_CON2. assumption. inversion Hp. by apply IHP.
  - apply previstyp_RES_LOC. inversion Hp. by apply IHP.
  - rewrite erase_union_vis.  inversion Hp. apply (previstyp_PAR _ _ _ s1 s2). 
    simpl. move=> x Hd1 Hd2. destruct Hd1. destruct Hd2.  apply (H1 x H12 H14). assumption. by apply IHP1. by apply IHP2.
  - by inversion Hp; apply previstyp_ITE; [apply IHP1 | apply IHP2]. 
  - rewrite erase_add_vis2.  easy. rewrite erase_empty. apply previstyp_FwF.
    by split. 
  - rewrite erase_add_vis2. intro. inversion H4. rewrite H6 in Hp. by inversion Hp. rewrite erase_empty. 
    rewrite (eq_sym H3). apply previstyp_FwC. split. rewrite H3. assumption. intro. inversion H4. rewrite  H6 in H3. rewrite H3 in Hp. by inversion Hp.
Qed.

Lemma alpha_ser_previstyp : forall (P1 P2 : proc) (B : Visfun) (s : stack_) (a b : Nser), is_fresh_nser b B s P1 -> alpha_convert_proc_ser a b P1 P2 -> preVisTyp B s P1 -> preVisTyp (alpha_convert_vis (srv a) (srv b) B) s P2.
Proof.
  move=> P1 P2 B s a b Hf Halph. move: B s Hf.
  induction Halph; move=> B s [Hf1 [Hf2 Hf3]] Htyp; inversion Htyp.
  - rewrite alpha_empty_vis. apply previstyp_nil. 
  - unfold B2. have Halphadd : alpha_convert_vis (srv a) (srv b) (add_vis cur V0 vis0) = add_vis cur (alpha_convert_V (srv a) (srv b) V0) (alpha_convert_vis (srv a) (srv b) vis0).
     apply alpha_add_vis_leave. easy. rewrite Halphadd. rewrite alpha_empty_vis. apply previstyp_O_SER.
     + right. by split.
     + have Hr1 : add_vis (names (srv b)) (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) vis0 = alpha_convert_vis (srv a) (srv b) (add_vis (names (srv a)) V0 vis0).
       * rewrite (alpha_add_vis_replace (srv a) (srv b) _ vis0). easy.
         rewrite alpha_empty_vis. apply eq_sym. apply add_equiv_vis. intro. split.
         ++ intro. destruct H8. left. by left.  right. apply (proj1 H8).
         ++ intro. destruct H8. assumption. right. by split.
       * rewrite Hr1. rewrite (add_equiv_vis _ _ (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a) ))).
         ++ intros. split; apply alpha_add_V_replace.
         ++ rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). move=> Hpa; inversion Hpa.
            apply IHHalph.
            ** split.
               +++ intro. destruct H8. destruct H8. easy. inversion H8. rewrite H10 in Hf3.  by inversion Hf3.
                   by inversion H8.
               +++ split. move=> x Hp. destruct Hp as [Hp | Hp]. destruct Hp. easy. simpl in H8.
                   have Hb : B cur (srv b). rewrite (eq_sym H1). unfold B2. simpl. right. split. easy. easy.
                   apply (Hf2 cur Hb). simpl in Hp.
                   destruct Hp. destruct H8.
                   have Hb : B cur (srv b). rewrite (eq_sym H1). unfold B2. simpl. right. split. easy. easy.                    apply (Hf2 cur Hb). inversion H8. rewrite H11 in Hf3. by inversion Hf3.
                   by inversion Hf3.
            ** have HB1 : B1 = (add_vis (names (cnt p)) (add_V V0 (srv a)) (add_vis (names (srv a)) V0 vis0)).
               unfold B1. unfold Vb0.
               have Haux : (add_vis (names (srv a)) (add_V V0 (srv a)) vis0) = (add_vis (names (srv a)) V0 vis0). apply (add_equiv_vis _ (add_V V0 (srv a)) V0). move=> x; split=> [HV | HV]. destruct HV. easy. by rewrite H8.
               by left. by rewrite Haux.
               rewrite (eq_sym HB1). apply H7.
  - unfold B2. rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply previstyp_O_SER.
    right. by split. have HV : forall (x :Names),alpha_convert_V (srv a) (srv b) (add_V V0 (srv c)) x <->  (add_V (alpha_convert_V (srv a) (srv b) V0) (srv c)) x.
    apply alpha_add_V_leave. intro. by inversion H9.
    rewrite (eq_sym (add_equiv_vis (names (cnt p)) _ _ _ HV)). rewrite (eq_sym (add_equiv_vis (names (srv c)) _ _ _ HV)). rewrite (eq_sym (alpha_empty_vis (srv a) (srv b) )). rewrite (eq_sym (alpha_add_vis_leave _ _ (names (srv c)) _ _ _)). intro. by inversion H9.
    rewrite (eq_sym (alpha_add_vis_leave _ _ (names (cnt p)) _ _ _)). intro. by inversion H9.
    apply IHHalph.
    + split.
      * simpl. intro. destruct H9. destruct H9. easy. inversion H9. rewrite H11 in Hf3. by inversion Hf3. by inversion H9.
      * split.
        ++ intros x Hx. destruct Hx. destruct H9. easy. simpl in H9. destruct H9. destruct H9.
           have Hb : B cur (srv b). rewrite (eq_sym H2). simpl. right. by split. 
           apply (Hf2 cur Hb). inversion H9. rewrite H12 in Hf3. by inversion Hf3. simpl in H9. destruct H9. destruct H9.
           have Hb : B cur (srv b). rewrite (eq_sym H2). simpl. right. by split. apply (Hf2 cur Hb). inversion H9. rewrite H12 in Hf3.  by inversion Hf3.
        ++ by inversion Hf3.
    + have HB1 : (add_vis (names (cnt p)) (add_V V0 (srv c)) (add_vis (names (srv c)) (add_V V0 (srv c)) vis0)) = B1. easy. by rewrite HB1.
  - unfold B2. rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply previstyp_O_SER.
    left. split. intro. by inversion H9. assumption.
    have HV : forall (x : Names), (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a) )) x <-> (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) x. apply alpha_add_V_replace.
    rewrite (eq_sym (add_equiv_vis (names (srv b)) _ _ _ HV)).     rewrite (eq_sym (add_equiv_vis (names (cnt p)) _ _ _ HV)). rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_replace  _ _ _  _ _ )). intro. inversion H9. 
    rewrite (eq_sym (alpha_add_vis_leave _ _ _ _  _ _ )). intro. by inversion H9.
    apply IHHalph.
    + split. intro. destruct H9.  destruct H9. easy. inversion H9. rewrite H11 in Hf3. by inversion Hf3.
      inversion H9. split. move=> x Hp. destruct Hp. destruct H9. easy. simpl in H9. destruct H9. destruct H9.
      have Hb : B cur (srv b). rewrite (eq_sym H2). simpl. right. by split. apply (Hf2 cur Hb).
      inversion H9. rewrite H12 in Hf3. inversion Hf3. easy. simpl in H9. destruct H9. destruct H9.
      have Hb : B cur (srv b). rewrite (eq_sym H2). simpl. right. by split. apply (Hf2 cur Hb).
      inversion H9. rewrite H12 in Hf3. inversion Hf3. easy.
      by inversion Hf3.
    + apply H8.
  - unfold B2. rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply previstyp_O_SER.
    left. split. intro. by inversion H10. easy.
    have HV : forall (x : Names), (alpha_convert_V (srv a) (srv b) (add_V V0 (srv d))) x <-> (add_V (alpha_convert_V (srv a) (srv b) V0) (srv d)) x. apply alpha_add_V_leave. intro. by inversion H10.
    rewrite (eq_sym (add_equiv_vis (names (srv d)) _ _ _ HV)). rewrite (eq_sym (add_equiv_vis (names (cnt p)) _ _ _ HV)). rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). intro. by inversion H10. rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). intro. by inversion H10.
    apply IHHalph.
    + split.
      * intro. destruct H10. destruct H10. easy. inversion H10. rewrite H12 in Hf3. by inversion Hf3.
        by inversion H10.
      * split.
        ++ move=> x Hx. destruct Hx. destruct H10. easy. simpl in H10. destruct H10. destruct H10.
           have Hb : B cur (srv b). rewrite (eq_sym H3). simpl. right. by split. apply (Hf2 cur Hb).
           inversion H10. rewrite H13 in Hf3. inversion Hf3. easy. simpl in H10. destruct H10.
           destruct H10. have Hb : B cur (srv b). rewrite (eq_sym H3). simpl. right. by split. apply (Hf2 cur Hb).
           inversion H10. rewrite H13 in Hf3. inversion Hf3. easy.
        ++ by inversion Hf3.
    + assumption.
  - unfold B2. rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply previstyp_O_CON.
    + left. split. intro. by inversion H7. assumption.
    + rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))).
      have HV : forall x, alpha_convert_V (srv a) (srv b) (add_V V0 (srv a)) x <-> (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) x. apply alpha_add_V_replace.
      rewrite (eq_sym (add_equiv_vis _ _ _ _ HV)).
      rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). easy.
      apply IHHalph.
      * split. intro. destruct H7. easy. by inversion H7. split.
        move=> x Hx. destruct Hx. easy. simpl in H7. destruct H7. destruct H7.
        have Hb : B cur (srv b). rewrite (eq_sym H1). right. simpl. by split. apply (Hf2 cur Hb).
        inversion H7. rewrite H10 in Hf3. by inversion Hf3.
        by inversion Hf3.
      * by rewrite (eq_sym H4).
  - unfold B2.
    rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply previstyp_O_CON.
    + left. by split.
    + rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))).
      rewrite (eq_sym (add_equiv_vis _ _ _ _ (alpha_add_V_leave _ _ _ _ _))).
      intro. by inversion H8.
      rewrite (eq_sym (alpha_add_vis_leave _ _ (names (cnt p)) _ _ _)). easy.
      apply IHHalph.
      * split. intro. destruct H8. easy. by inversion H8.
        split. move=> x Hx. destruct Hx. easy. simpl in H8. destruct H8. destruct H8.
        have Hb : B cur (srv b). rewrite (eq_sym H2). right. by split. apply (Hf2 cur Hb).
        inversion H8. rewrite H11 in Hf3. by inversion Hf3.
        by inversion Hf3.
      * rewrite (eq_sym H5).  easy.
  - rewrite alpha_empty_vis. apply previstyp_O_LOC.
  -  unfold B0.
    rewrite alpha_add_vis_replace. easy. rewrite alpha_empty_vis. apply previstyp_I_SER.
    have HV : forall x, (add_V (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) (cnt p)) x <-> alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv a)) (cnt p)) x.
    have HV1 : forall x, alpha_convert_V (srv a) (srv b) (add_V V0 (srv a)) x <-> add_V (alpha_convert_V (srv a) (srv b) V0) (srv b) x. apply alpha_add_V_replace.
    have HV2 : forall x, add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a))) (cnt p) x <-> add_V (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) (cnt p) x. apply (add_equiv_V _ _ _ HV1).
    have HV3 : forall x, alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv a)) (cnt p)) x <-> add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a))) (cnt p) x. apply alpha_add_V_leave. easy.
    move=> x; split. intro. apply (proj2 (HV3 x)). by apply (proj2 (HV2 x)).
    intro. apply (proj1 (HV2 x)). by apply (proj1 (HV3 x)).
    rewrite (add_equiv_vis _ _ _ _ HV). rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))).
    rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). easy. apply IHHalph.
    + split. intro. destruct H7. easy. easy.
      split. move=> x Hx. destruct Hx. easy. simpl in H7. destruct H7. destruct H7. destruct H7. 
      have Hb : B (names (srv a0)) (srv b). rewrite (eq_sym H0). right. by split. apply (Hf2 _ Hb).
      inversion H7. rewrite H10 in Hf3. inversion Hf3. easy. inversion H7.
      by inversion Hf3.
    + rewrite (eq_sym H4). rewrite (eq_sym H5).  by rewrite (eq_sym H5) in H2.
  - unfold B0. rewrite alpha_add_vis_replace.
    by intro. rewrite alpha_empty_vis. apply previstyp_I_SER.
    rewrite (add_equiv_vis _ _ (alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv c)) (cnt p))) _).
    have HV1 : forall x, alpha_convert_V (srv a) (srv b) (add_V V0 (srv c)) x <-> (add_V (alpha_convert_V (srv a) (srv b) V0) (srv c)) x. apply alpha_add_V_leave. intro. by inversion H8.
    have HV2 : forall x, alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv c)) (cnt p)) x <-> add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv c))) (cnt p) x. apply alpha_add_V_leave. easy.
    have HV3 : forall x, add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv c))) (cnt p) x <-> add_V (add_V (alpha_convert_V (srv a) (srv b) V0) (srv c)) (cnt p) x. by apply add_equiv_V.
    move=> x; split. intro. apply (proj2 (HV2 x)). by apply (proj2 (HV3 x)).
    intro. apply (proj1 (HV3 x)). by apply (proj1 (HV2 x)).
    rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)).
    easy. apply IHHalph.
    + split. intro. by destruct H8.
      split. move=> x Hx. destruct Hx. easy. destruct H8. destruct H8. destruct H8.
      have Hb : B (names (srv a0)) (srv b). rewrite (eq_sym H1). right. by split. apply (Hf2 _ Hb).
      inversion H8. rewrite H11 in Hf3. by inversion Hf3. by inversion H8.
      by inversion Hf3.
    + rewrite (eq_sym H5). rewrite (eq_sym H6). by rewrite (eq_sym H6) in H3.
  - unfold B0. rewrite alpha_add_vis_leave.
    intro. by inversion H8. rewrite alpha_empty_vis. apply previstyp_I_SER.
    rewrite (add_equiv_vis _ _ (alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv a)) (cnt p))) _).
    have HV1 : forall x, alpha_convert_V (srv a) (srv b) (add_V V0 (srv a)) x <-> (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) x. by apply alpha_add_V_replace. 
    have HV2 : forall x, alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv a)) (cnt p)) x <-> add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a))) (cnt p) x. apply alpha_add_V_leave. easy.
    have HV3 : forall x, add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a))) (cnt p) x <-> add_V (add_V (alpha_convert_V (srv a) (srv b) V0) (srv b)) (cnt p) x. by apply add_equiv_V.
    move=> x; split. intro. apply (proj2 (HV2 x)). by apply (proj2 (HV3 x)).
    intro. apply (proj1 (HV3 x)). by apply (proj1 (HV2 x)).
    rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)).
    easy. apply IHHalph.
    + split. intro. by destruct H8.
      split. move=> x Hx. destruct Hx. easy. destruct H8. destruct H8. destruct H8.
      have Hb : B (names (srv a0)) (srv b). rewrite (eq_sym H1). right. by split. apply (Hf2 _ Hb).
      inversion H8. rewrite H11 in Hf3. by inversion Hf3. by inversion H8.
      by inversion Hf3.
    + rewrite (eq_sym H5). rewrite (eq_sym H6). by rewrite (eq_sym H6) in H3.
  - unfold B0. rewrite alpha_add_vis_leave.
    intro. by inversion H9. rewrite alpha_empty_vis. apply previstyp_I_SER.
    rewrite (add_equiv_vis _ _ (alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv d)) (cnt p))) _).
    have HV1 : forall x, alpha_convert_V (srv a) (srv b) (add_V V0 (srv d)) x <-> (add_V (alpha_convert_V (srv a) (srv b) V0) (srv d)) x. apply alpha_add_V_leave. intro. by inversion H9.
    have HV2 : forall x, alpha_convert_V (srv a) (srv b) (add_V (add_V V0 (srv d)) (cnt p)) x <-> add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv d))) (cnt p) x. apply alpha_add_V_leave. easy.
    have HV3 : forall x, add_V (alpha_convert_V (srv a) (srv b) (add_V V0 (srv d))) (cnt p) x <-> add_V (add_V (alpha_convert_V (srv a) (srv b) V0) (srv d)) (cnt p) x. by apply add_equiv_V.
    move=> x; split. intro. apply (proj2 (HV2 x)). by apply (proj2 (HV3 x)).
    intro. apply (proj1 (HV3 x)). by apply (proj1 (HV2 x)).
    rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)).
    easy. apply IHHalph.
    + split. intro. by destruct H9.
      split. move=> x Hx. destruct Hx. easy. destruct H8. destruct H9.
      destruct H8. destruct H8.
      have Hb : B (names (srv a0)) (srv b). rewrite (eq_sym H2). right. by split. apply (Hf2 _ Hb).
      inversion H8. rewrite H11 in Hf3. by inversion Hf3. by inversion H8.
      by inversion Hf3.
    + rewrite (eq_sym H6). rewrite (eq_sym H7). by rewrite (eq_sym H7) in H4.
  - unfold B0. rewrite alpha_add_vis_leave. easy.
    rewrite alpha_empty_vis. apply previstyp_I_CON. rewrite (add_equiv_vis _ _ (alpha_convert_V (srv a) (srv b) (add_V V0 (srv a))) _ ). intro. apply iff_sym. apply alpha_add_V_replace.
    rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). easy. apply IHHalph.
    + split. intro. by destruct H6.
      split. move=> x Hx. destruct Hx. easy. destruct H6. destruct H6.
      have Hb : B (names (cnt p0)) (srv b). rewrite (eq_sym H0). right. by split. apply (Hf2 _ Hb).
      inversion H6. rewrite H9 in Hf3. by inversion Hf3. by inversion Hf3.
    + by rewrite (eq_sym H4).
  - unfold B0. rewrite alpha_add_vis_leave. easy.
    rewrite alpha_empty_vis. apply previstyp_I_CON. rewrite (add_equiv_vis _ _ (alpha_convert_V (srv a) (srv b) (add_V V0 (srv c))) _ ). intro. apply iff_sym. apply alpha_add_V_leave. intro. by inversion H7.
    rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). easy. apply IHHalph.
    + split. intro. by destruct H7.
      split. move=> x Hx. destruct Hx. easy. destruct H7. destruct H7.
      have Hb : B (names (cnt p0)) (srv b). rewrite (eq_sym H1). right. by split. apply (Hf2 _ Hb).
      inversion H7. rewrite H10 in Hf3. by inversion Hf3. by inversion Hf3.
    + by rewrite (eq_sym H5).
  - unfold B0. rewrite alpha_add_vis_leave.
    easy. rewrite alpha_empty_vis. apply previstyp_I_LOC. rewrite (eq_sym (alpha_empty_vis (srv a) (srv b))). rewrite (eq_sym (alpha_add_vis_leave _ _ _ _ _ _)). easy. apply IHHalph.
    + split. intro. by destruct H5. split. move=> x Hx. destruct Hx. easy. destruct H5.
      have Hb : B cur (srv b). rewrite (eq_sym H0). right. by split. apply (Hf2 _ Hb). 
      by inversion Hf3.
    + easy.
  - have Her_b : B = trans_minus_vis (srv a0) (erase_vis (srv b) B0).
    +  rewrite (eq_sym (erase_trans_minus _ _ _ _)). intro. inversion H4. rewrite H6 in H. rewrite H in Hf3. by inversion Hf3. rewrite H0. by apply erase_idem. 
    + rewrite (eq_sym H). rewrite H0. rewrite Her_b. 
    rewrite alpha_trans_minus_vis_replace. split.
      * intro. destruct H4. easy.
      * move=> x Hx. destruct Hx. by destruct H4.
      * apply previstyp_RES_SER. rewrite H. apply IHHalph.
        split.
        ++ intro. by destruct H4.
        ++ split. intros x Hx. destruct Hx. by destruct H4. by inversion Hf3.
        ++ apply erase_ser_previstyp. by inversion Hf3. apply H2. 
  - have Her_b :  B = trans_minus_vis (srv a0) (erase_vis (srv b) B0).
    + rewrite (eq_sym H1). rewrite (eq_sym (erase_trans_minus _ _ _ _)). intro. inversion H5. rewrite H7 in H0.
      rewrite H0 in Hf3. by inversion Hf3. rewrite H1.  by apply erase_idem.
    + rewrite (eq_sym H0). rewrite H1. rewrite Her_b. 
      rewrite alpha_trans_minus_vis_leave. intro. inversion H5. rewrite H0 in H7. rewrite H7 in Hf3. by inversion Hf3.
    intro. inversion H5. by rewrite H0 in H7. apply previstyp_RES_SER. 
    apply IHHalph. split. intro. by destruct H5. split. intros x Hx. destruct Hx. by destruct H5. by inversion Hf3.
    apply erase_ser_previstyp. by inversion Hf3. assumption.
  - have Her_b : B = trans_minus_vis (cnt p0) (erase_vis (srv b) B0).
    rewrite (eq_sym (erase_trans_minus _ _ _ _)). easy. rewrite H1. by apply erase_idem.
    rewrite (eq_sym H). rewrite H1. rewrite Her_b.
    rewrite alpha_trans_minus_vis_leave; try easy. apply (previstyp_RES_CON1 _ _ s1 _ _).
    by rewrite H. 
    apply IHHalph. split.  intro. by destruct H5. split. move=> x Hx. destruct Hx as [Hx _]. by destruct Hx.
    by inversion Hf3. apply erase_ser_previstyp. by inversion Hf3. assumption.
  - have Her_b : B = trans_minus_vis (cnt p0) (erase_vis (srv b) B0).
    rewrite (eq_sym (erase_trans_minus _ _ _ _)). easy. rewrite H1. by apply erase_idem.
    rewrite (eq_sym H). rewrite H1. rewrite Her_b.
    rewrite alpha_trans_minus_vis_leave; try easy. apply previstyp_RES_CON2.
    by rewrite H. 
    apply IHHalph. split.  intro. by destruct H5. split. move=> x Hx. destruct Hx as [Hx _]. by destruct Hx.
    by inversion Hf3. apply erase_ser_previstyp. by inversion Hf3. assumption.
  - apply previstyp_RES_LOC. apply IHHalph; [|assumption]. split; [assumption |]. split; [assumption |by inversion Hf3].
  - rewrite alpha_union_vis. apply (previstyp_PAR _ _ s s1 s2).
    move=> x Hd1 Hd2.  destruct Hd1. destruct Hd2. destruct H7. destruct H8. apply (H1 _ H9 H10).
    destruct H7. destruct H8. rewrite H7 in H10. apply Hf1. rewrite (eq_sym H3).
    unfold B0. by right.  destruct Hd2. destruct H7. destruct H8. rewrite H8 in H9.
    apply Hf1. rewrite (eq_sym H3). unfold B0. by left. 
    destruct H7. destruct H8. apply (H1 _ H9 H10). assumption.
    apply IHHalph1. split. intro. apply Hf1. rewrite (eq_sym H3). by left.
    split. move => x Hx. apply (Hf2 x). rewrite (eq_sym H3). by left. by inversion Hf3. assumption.
    apply IHHalph2. split. intro. apply Hf1. rewrite (eq_sym H3). by right.
    split. move => x Hx. apply (Hf2 x). rewrite (eq_sym H3). by right. by inversion Hf3. assumption.
  - inversion Hf3. apply previstyp_ITE. apply IHHalph1. split. assumption.  by split. assumption.
    apply IHHalph2. split. assumption.  by split. assumption.
  - unfold B1. rewrite alpha_add_vis_replace. easy. rewrite alpha_empty_vis.    
    apply previstyp_FwF. right. by split.
  - unfold B1. rewrite alpha_add_vis_replace. easy. rewrite alpha_empty_vis. apply previstyp_FwF. left.
    split. intro. by inversion H5. assumption.
  - unfold B1. rewrite alpha_add_vis_leave. intro. by inversion H5.
    rewrite alpha_empty_vis. apply previstyp_FwF. right. by split.
  - rewrite alpha_add_vis_leave. intro. by inversion H6. rewrite alpha_empty_vis.
    apply previstyp_FwF. left. split. intro. by inversion H6. assumption.
  - rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. rewrite (eq_sym H3). apply previstyp_FwC.
    rewrite H3. left. split; easy.
Qed.

Lemma alpha_con_previstyp : forall (P1 P2 : proc) (B : Visfun) (s1 s2 : stack_) (p q : Ncon), is_fresh_ncon q B s1 P1 -> alpha_convert_proc_con p q P1 P2 -> (alpha_convert_stack p q s1 s2) -> preVisTyp B s1 P1 -> preVisTyp (alpha_convert_vis (cnt p) (cnt q) B) s2 P2.
Proof.
   move=> P1 P2 B s1 s2 p q Hf Halph1 Halph2. move: B s1 s2 Halph2 Hf.
   induction Halph1;  move=> B s1 s2 Halph2 [Hf1 [Hf2 [Hf3 Hf4]]] Htyp; inversion Htyp.
   - rewrite alpha_empty_vis. rewrite (eq_sym H0) in Halph2. inversion Halph2. apply previstyp_nil.
   - rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. rewrite (eq_sym H2) in Halph2.
     inversion Halph2.  apply previstyp_O_SER.
     left. by split.
     have HB : (add_vis (names (cnt q)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b))
                  (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0)) = alpha_convert_vis (cnt p) (cnt q) B1.
     + rewrite alpha_add_vis_replace.  intro. destruct H13.  easy.  by inversion H13.
       rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis.
       have Haux : (add_vis (names (srv b)) (alpha_convert_V (cnt p) (cnt q) Vb0) vis0) = (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0).
       * apply add_equiv_vis. apply alpha_add_V_leave. easy.
       * rewrite Haux. apply add_equiv_vis. move=> x; apply iff_sym; move: x. apply alpha_add_V_leave. easy.
     + rewrite HB. apply (IHHalph1 _ (In (stack_in p s0))).
       * apply alpha_replace_in. rewrite (eq_sym H8). apply alpha_replace_out. apply H11.
       * split. intro. destruct H13. destruct H13. easy. by inversion H13. inversion H13. rewrite H15 in Hf3. by inversion Hf3.
         split. move=> x Hx. destruct Hx. destruct H13. easy. destruct H13. destruct H13.
         have Hq : B cur (cnt q). rewrite (eq_sym H1). right. by split.  apply (Hf2 _ Hq).
         by inversion H13. destruct H13. destruct H13.
         have Hq : B cur (cnt q). rewrite (eq_sym H1). right. by split.  apply (Hf2 _ Hq).
         by inversion H8.
         split. by inversion Hf3. apply notin_out. intro. rewrite H13 in Hf3. by inversion Hf3.
         by rewrite H2.
       * assumption.
     + apply previstyp_O_SER. left. by split. 
       have HB : (add_vis (names (cnt q)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b))
                    (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0)) = alpha_convert_vis (cnt p) (cnt q) B1.
       * rewrite alpha_add_vis_replace. intro. destruct H14. easy. by inversion H14.
         rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis.
         have Haux : (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0) = (add_vis (names (srv b)) (alpha_convert_V (cnt p) (cnt q) Vb0) vis0).
         apply add_equiv_vis. move=> x; apply iff_sym; move: x. by apply alpha_add_V_leave.
         rewrite Haux. apply add_equiv_vis. move=> x; apply iff_sym; move:x. by apply alpha_add_V_leave.
       * rewrite HB. apply (IHHalph1 _ (In (stack_in p s0))).
         apply alpha_replace_in. by rewrite H13.
         split. intro. destruct H14. destruct H14. easy. by inversion H14. inversion H14. rewrite H16 in Hf3. by inversion Hf3.
         split. move=> x Hq. destruct Hq. destruct H14. easy. destruct H14. destruct H14.
         have Hq : B cur (cnt q). rewrite (eq_sym H1). by right. apply (Hf2 _ Hq).
         by inversion H14. destruct H14. destruct H14.
         have Hq : B cur (cnt q). rewrite (eq_sym H1). by right. apply (Hf2 _ Hq).
         by inversion H14.
         split. by inversion Hf3. apply notin_out. intro. rewrite H14 in Hf3. by inversion Hf3. by rewrite H2.
         assumption.
   - rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. rewrite (eq_sym H3) in Halph2. inversion Halph2.
     + apply previstyp_O_SER. left. by split.
       have HB : (add_vis (names (cnt r)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0)) = alpha_convert_vis (cnt p) (cnt q) B1.
       * rewrite alpha_add_vis_leave. intro. by inversion H14.
         have Haux : (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0) = (alpha_convert_vis (cnt p) (cnt q) (add_vis (names (srv b)) Vb0 vis0)).
         rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
         move=> x; apply iff_sym. apply alpha_add_V_leave. easy.
         rewrite Haux. apply add_equiv_vis. move=>x; apply iff_sym. apply alpha_add_V_leave. easy.
       * rewrite HB. apply (IHHalph1 _ (In (stack_in r s0))). apply alpha_leave_in.  easy.
         by rewrite H13.
         split. intro. destruct H14. destruct H14. easy. by inversion H14. inversion H14. rewrite H16 in Hf3.
         by inversion Hf3.
         split. move=> x Hx. destruct Hx. destruct H14. easy. destruct H14. destruct H14.
         have Hq : B cur (cnt q). rewrite (eq_sym H2). by right. apply (Hf2 _ Hq). by inversion H14.
         destruct H14.  destruct H14.
         have Hq : B cur (cnt q). rewrite (eq_sym H2). by right. apply (Hf2 _ Hq). by inversion H14.
         split. by inversion Hf3. apply notin_out. intro. rewrite H14 in Hf3. by inversion Hf3.
         by rewrite H3. assumption.         
     + apply previstyp_O_SER. left. by split.
       have HB : (add_vis (names (cnt r)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0)) = alpha_convert_vis (cnt p) (cnt q) B1.
       * rewrite alpha_add_vis_leave. intro. by inversion H15.
         have Haux : (add_vis (names (srv b)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0) = (alpha_convert_vis (cnt p) (cnt q) (add_vis (names (srv b)) Vb0 vis0)).
         rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
         move=> x; apply iff_sym. apply alpha_add_V_leave. easy.
         rewrite Haux. apply add_equiv_vis. move=>x; apply iff_sym. apply alpha_add_V_leave. easy.
       * rewrite HB. apply (IHHalph1 _ (In (stack_in r s0))). apply alpha_leave_in.  easy.
         by rewrite H14.
         split. intro. destruct H15. destruct H15. easy. by inversion H15. inversion H15. rewrite H17 in Hf3.
         by inversion Hf3.
         split. move=> x Hx. destruct Hx. destruct H15. easy. destruct H15. destruct H15.
         have Hq : B cur (cnt q). rewrite (eq_sym H2). by right. apply (Hf2 _ Hq). by inversion H15.
         destruct H15.  destruct H15.
         have Hq : B cur (cnt q). rewrite (eq_sym H2). by right. apply (Hf2 _ Hq). by inversion H15.
         split. by inversion Hf3. apply notin_out. intro. rewrite H15 in Hf3. by inversion Hf3.
         by rewrite H3. assumption.
   - rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. rewrite (eq_sym H2) in Halph2. inversion Halph2.
     + inversion H12. apply previstyp_O_CON. rewrite (eq_sym H7). right. by split.
       have HB : (add_vis (names (cnt q)) (add_V (alpha_convert_V (cnt p0) (cnt q) V0) (srv b)) vis0)
                 = alpha_convert_vis (cnt p) (cnt q) B1.
       rewrite alpha_add_vis_replace. by intro. rewrite alpha_empty_vis. apply add_equiv_vis.
       move=>x; apply iff_sym. rewrite (eq_sym H4). rewrite H. apply alpha_add_V_leave. easy.
       rewrite HB. apply (IHHalph1 _ (In nil)). apply alpha_empty.
       split. intro HB1. destruct HB1 as[ | Hpq] ; [easy|]. inversion Hpq. rewrite H17 in Hf3. by inversion Hf3.
       split. move=> x HBx. destruct HBx as [| HBx]; [easy|].  destruct HBx as [HVq _]. destruct HVq as[HVq|]; [|easy].
       have Hq : B cur (cnt q). rewrite (eq_sym H1). right. by split. apply (Hf2 _ Hq).
       split. by inversion Hf3. apply notin_nil. assumption.
     + rewrite (eq_sym H12) in Halph2. rewrite H. inversion Halph2.
       * rewrite (eq_sym H19) in H14. rewrite H14 in Hf3. by inversion Hf3.
       * easy.
   - rewrite alpha_add_vis_leave. easy. rewrite (eq_sym H3) in Halph2. inversion Halph2.
     + rewrite H0 in H8. exfalso. by apply (H (eq_sym H8)).
     + rewrite alpha_empty_vis. rewrite H0. inversion H14. apply previstyp_O_CON.
       left. split. intro. by inversion H18. assumption.
       have HB : (add_vis (names (cnt r)) (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
       rewrite alpha_add_vis_leave. intro. by inversion H18.
       rewrite alpha_empty_vis. apply add_equiv_vis. move=> x; apply iff_sym. rewrite (eq_sym H5). apply alpha_add_V_leave. easy.
       rewrite HB. apply (IHHalph1 _ (In nil)). apply alpha_empty.
       split. move=> HBq. destruct HBq as [HBq | HBq]. easy. inversion HBq. rewrite H19 in Hf3. by inversion Hf3.
       split. move=> x HBq. destruct HBq as [HBq | HVq]. easy. destruct HVq as [HVq _]. destruct HVq as [HVq| Hqb].
       have HBq : B cur (cnt q). rewrite (eq_sym H2). by right. apply (Hf2 _ HBq).
       by inversion Hqb.
       split. by inversion Hf3. apply notin_nil. assumption.
   - rewrite alpha_empty_vis. rewrite (eq_sym H0) in Halph2. inversion Halph2. apply previstyp_O_LOC.
   - rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. rewrite (eq_sym H1) in Halph2. inversion Halph2. apply previstyp_I_SER.
     have HB : (add_vis cur (add_V (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) (cnt q)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
     rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
     unfold Vbp. rewrite H5. rewrite H4.
     have HV1 : forall x : Names, add_V (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) (cnt q) x <-> add_V (alpha_convert_V (cnt p) (cnt q) (add_V V0 (srv b))) (cnt q) x. apply add_equiv_V.
     move=> x; apply iff_sym. apply alpha_add_V_leave. easy.
     have HV2 : forall x : Names, add_V (alpha_convert_V (cnt p) (cnt q) (add_V V0 (srv b))) (cnt q) x <->
                               alpha_convert_V (cnt p) (cnt q) (add_V (add_V V0 (srv b)) (cnt p)) x.
     move=> x; apply iff_sym. apply alpha_add_V_replace.
     move=> x; split=> HV. by apply HV2; apply HV1. by apply HV1; apply HV2.
     rewrite HB. apply (IHHalph1 _ (Out (stack_out p nil))).
     apply alpha_replace_out. apply alpha_empty.
     split. intro. by destruct H9. split.
     move=> x Hxq. destruct Hxq. easy. destruct H9. destruct H9. destruct H9.
     have HBq : B (names (srv a0)) (cnt q). rewrite (eq_sym H0). right. by split. apply (Hf2 _ HBq).
     by inversion H9. inversion H9. rewrite H5 in H13. rewrite H13 in Hf3. by inversion Hf3.
     split. by inversion Hf3. apply notin_in. intro. rewrite H9 in Hf3. by inversion Hf3. apply notin_nil.
     assumption.
   - rewrite alpha_add_vis_leave. intro. by inversion H8. rewrite alpha_empty_vis.
     rewrite (eq_sym H2) in Halph2. inversion Halph2. apply previstyp_I_SER. 
     have HB : (add_vis cur (add_V (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) (cnt r)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
     + rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
       have HV1 : forall x : Names, add_V (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) (cnt r) x <->
                                 add_V (alpha_convert_V (cnt p) (cnt q) (add_V V0 (srv b))) (cnt r) x.
       apply add_equiv_V. move=> x; apply iff_sym. apply alpha_add_V_leave. easy.
       have HV2 : forall x : Names, add_V (alpha_convert_V (cnt p) (cnt q) (add_V V0 (srv b))) (cnt r) x <->
                                 alpha_convert_V (cnt p) (cnt q) Vbp x.
       move=> x; apply iff_sym. rewrite (eq_sym H6). rewrite (eq_sym H5). apply alpha_add_V_leave.
       intro. inversion H10. by rewrite H6 in H13.
       move=> x; split=> HV. apply HV2. by apply HV1. apply HV1. by apply HV2.
     + rewrite HB. apply (IHHalph1 _ (Out (stack_out r nil))).
       apply alpha_leave_out. assumption. apply alpha_empty. split.
       intro. by destruct H10. split.
       move=> x Hxq. destruct Hxq. easy. destruct H10. destruct H10. destruct H10.
       have HBq : B (names (srv a0)) (cnt q). rewrite (eq_sym H1). right. by split. apply (Hf2 _ HBq).
       by inversion H10. inversion H10. rewrite H6 in H14. rewrite H14 in Hf3. by inversion Hf3. split.
       by inversion Hf3. apply notin_in. intro. rewrite H10 in Hf3. by inversion Hf3. apply notin_nil.
       assumption.
   - rewrite alpha_add_vis_replace. by intro. rewrite alpha_empty_vis. rewrite (eq_sym H1) in Halph2.
     rewrite H in Halph2. inversion Halph2.
     + inversion H9.
       * inversion H14. apply previstyp_I_CON.
         have HB : (add_vis cur (add_V (alpha_convert_V (cnt q0) (cnt q) V0) (srv b)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
         ++ rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
            move=> x ; apply iff_sym. rewrite (eq_sym H4). rewrite H11. apply alpha_add_V_leave. easy.
         ++ rewrite HB. apply (IHHalph1 _ (Out (stack_out q0 nil))). rewrite (eq_sym H11). apply alpha_replace_out. apply alpha_empty. split.
            intro. by destruct H20. split.
            move=> x Hxq. destruct Hxq. easy. destruct H20. destruct H20.
            have HBq : B (names (cnt p0)) (cnt q). rewrite (eq_sym H0). by right. apply (Hf2 _ HBq).
            by inversion H20. split.
            by inversion Hf3. apply notin_in. rewrite (eq_sym H11). intro. rewrite H20 in Hf3. by inversion Hf3.
            apply notin_nil. assumption.
       * inversion H17. apply previstyp_I_CON.
         have HB : (add_vis cur (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
         ++ rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
            move=> x ; apply iff_sym. rewrite (eq_sym H4).  apply alpha_add_V_leave. easy.
         ++ rewrite HB. apply (IHHalph1 _ (Out (stack_out q0 nil))). rewrite (eq_sym H11). apply alpha_leave_out. by rewrite H11. apply alpha_empty. split.
            intro. by destruct H21. split.
            move=> x Hxq. destruct Hxq. easy. destruct H21. destruct H21.
            have HBq : B (names (cnt p0)) (cnt q). rewrite (eq_sym H0). by right. apply (Hf2 _ HBq).
            by inversion H21. split.
            by inversion Hf3. rewrite (eq_sym H1) in Hf4. by inversion Hf4. 
            assumption.
     + rewrite (eq_sym H11) in Halph2.  by inversion Halph2.
   - rewrite alpha_add_vis_leave. intro. by inversion H7. rewrite alpha_empty_vis.
     rewrite (eq_sym H2) in Halph2. inversion Halph2.
     + rewrite H0 in H7. by rewrite H7 in H.
     + inversion H13.
       * inversion H17.  rewrite H0. apply previstyp_I_CON.
         have HB : (add_vis cur (add_V (alpha_convert_V (cnt q0) (cnt q) V0) (srv b)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
         ++ rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
            move=> x ; apply iff_sym. rewrite (eq_sym H14). rewrite (eq_sym H5). apply alpha_add_V_leave. easy.
         ++ rewrite HB. apply (IHHalph1 _ (Out (stack_out q0 nil))). rewrite (eq_sym H14). apply alpha_replace_out. apply alpha_empty. split.
            intro. by destruct H23. split.
            move=> x Hxq. destruct Hxq. easy. destruct H23. destruct H23.
            have HBq : B (names (cnt p0)) (cnt q). rewrite (eq_sym H1). by right. apply (Hf2 _ HBq).
            by inversion H23. split.
            by inversion Hf3. apply notin_in. rewrite (eq_sym H2) in Hf4. inversion Hf4. by inversion H26.
            apply notin_nil. assumption.
       * rewrite H0. inversion H20. apply previstyp_I_CON.
         have HB : (add_vis cur (add_V (alpha_convert_V (cnt p) (cnt q) V0) (srv b)) vis0) = alpha_convert_vis (cnt p) (cnt q) B1.
         ++ rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply add_equiv_vis.
            move=> x ; apply iff_sym. rewrite (eq_sym H5).  apply alpha_add_V_leave. easy.
         ++ rewrite HB. apply (IHHalph1 _ (Out (stack_out q0 nil))). apply alpha_leave_out. assumption.
            apply alpha_empty. split.
            intro.  by destruct H24. split.
            move=> x Hxq. destruct Hxq. easy. destruct H24. destruct H24.
            have HBq : B (names (cnt p0)) (cnt q). rewrite (eq_sym H1). by right. apply (Hf2 _ HBq).
            by inversion H21. split.
            by inversion Hf3. rewrite (eq_sym H2) in Hf4. by inversion Hf4. 
            assumption.
   - rewrite alpha_add_vis_leave. easy. rewrite alpha_empty_vis. apply previstyp_I_LOC.
     have HB : (add_vis cur (alpha_convert_V (cnt p) (cnt q) V0) vis0) = alpha_convert_vis (cnt p) (cnt q) B.
     rewrite (eq_sym H0). rewrite alpha_add_vis_leave. easy. by rewrite alpha_empty_vis.
     rewrite HB. apply (IHHalph1 _ s1). assumption. split; [assumption |].  split;[ assumption|]. split.
     by inversion Hf3. assumption. by rewrite H0 in H2.
   - have Her_b : B = trans_minus_vis (srv a0) (erase_vis (cnt q) B0).
     rewrite (eq_sym  (erase_trans_minus _ _ _ _)). easy. rewrite H0. apply erase_idem; assumption.
     rewrite H in H0. rewrite H0. rewrite Her_b.
     rewrite alpha_trans_minus_vis_leave. easy. easy. rewrite H. apply previstyp_RES_SER. apply (IHHalph1 _ s1).
     assumption. split. intro. by destruct H4. split. move=> x HBq. destruct HBq. by destruct H4.
     split. by inversion Hf3. assumption.
     apply erase_con_previstyp. by inversion Hf3. assumption.
   - have Hpq : (cnt p) <> (cnt q). intro. inversion H5. rewrite H7 in Hf3. by inversion Hf3.
     have Her_b : B = trans_minus_vis (cnt p0) (erase_vis (cnt q) B0).
     rewrite (eq_sym  (erase_trans_minus _ _ _ _)). rewrite H. assumption. rewrite H1. apply erase_idem; assumption.
     rewrite H in H1. rewrite H1. rewrite Her_b.
     rewrite H. rewrite alpha_trans_minus_vis_replace. apply fresh_erase.
     have Hs : s1 = s2. apply (alpha_stack_idem p q). by apply (insert_new p s0 s1).  assumption.
     destruct (exists_alpha_convert_stack p q s0) as [s4 Hs4]. 
     apply (previstyp_RES_CON1 _ _ s4). apply (alpha_insert_replace p q s0).
     by rewrite (eq_sym Hs). assumption.  by rewrite (eq_sym Hs). apply (IHHalph1 _ s0).
     assumption. split. apply (proj1 (fresh_erase _ _)). split. apply (proj2 (fresh_erase _ _)).
     split. by inversion Hf3. apply (insert_not_in p q s0 s1). assumption. assumption. intro. rewrite H5 in Hf3. by inversion Hf3.
     apply erase_con_previstyp. by inversion Hf3. assumption.
   - have Hpq : (cnt p) <> (cnt q). intro. inversion H5. rewrite H7 in Hf3. by inversion Hf3.
     have Her_q : B = trans_minus_vis (cnt p) (erase_vis (cnt q) B0).
     rewrite (eq_sym (erase_trans_minus _ _ _ _)). assumption. rewrite (eq_sym H). rewrite H1. apply erase_idem.
     assumption. assumption.
     rewrite H in H1. rewrite H1. rewrite Her_q. rewrite alpha_trans_minus_vis_replace.
     apply fresh_erase. apply previstyp_RES_CON2. by apply (alpha_not_in p q s1 s2).
     apply (IHHalph1 _ s1). assumption. split. apply (proj1 (fresh_erase _ _)).
     split. apply (proj2 (fresh_erase _ _)). split. by inversion Hf3. assumption.
     apply erase_con_previstyp. by inversion Hf3. assumption.
   - have Hrq : (cnt r) <> (cnt q). intro. inversion H6. rewrite H8 in Hf3. by inversion Hf3.
     have Hrq2: r <> q. intro. by rewrite H6 in Hrq.
     have Her_q : B = trans_minus_vis (cnt r) (erase_vis (cnt q) B0).
     rewrite (eq_sym (erase_trans_minus _ _ _ _)). assumption. rewrite (eq_sym H2). rewrite H0. apply erase_idem.
     rewrite (eq_sym H0). rewrite H2. assumption. rewrite (eq_sym H0). rewrite H2. assumption.
     rewrite H0 in H2. rewrite H2. rewrite Her_q. rewrite alpha_trans_minus_vis_leave. assumption.
     intro. by inversion H6.
      destruct (exists_alpha_convert_stack p q s0) as [s4 Hs4].
     apply (previstyp_RES_CON1 _ _ s4). by apply (alpha_insert_leave p q r s1 _ s0).
     apply (IHHalph1 _ s0); try assumption.
     split. apply fresh_erase. split. apply fresh_erase. split. by inversion Hf3.
     by apply (insert_not_in r q s0 s1). apply erase_con_previstyp. by inversion Hf3. assumption.
   - have Hrq : (cnt r) <> (cnt q). intro. inversion H6. rewrite H8 in Hf3. by inversion Hf3.
     have Hrq2 : r <> q. intro. by rewrite H6 in Hrq.
     have Her_q : B = trans_minus_vis (cnt r) (erase_vis (cnt q) B0).
     rewrite (eq_sym (erase_trans_minus _ _ _ _)). assumption. rewrite (eq_sym H2). rewrite H0. apply erase_idem.
     rewrite H0 in H2. rewrite H2. assumption. rewrite H0 in H2. rewrite H2. assumption.
     rewrite H0 in H2. rewrite H2. rewrite Her_q. rewrite alpha_trans_minus_vis_leave; try assumption.
     intro. by inversion H6.
     apply previstyp_RES_CON2. by apply (alpha_not_in_other p q r s1 s2).
     apply (IHHalph1 _ s1). assumption. split. apply (proj1 (fresh_erase _ _)).
     split. apply (proj2 (fresh_erase _ _)). split. by inversion Hf3. assumption.
     apply erase_con_previstyp. by inversion Hf3. assumption.
   - apply previstyp_RES_LOC. apply (IHHalph1 _ s1). assumption.
     split. assumption. split. assumption. split. by inversion Hf3. assumption. assumption.
   - rewrite alpha_union_vis.
     destruct (exists_alpha_convert_stack p q s3) as [s5 Hs5].
     destruct (exists_alpha_convert_stack p q s4) as [s6 Hs6].
     apply (previstyp_PAR _ _ _ s5 s6).
     + simpl. move=> x HdB1 HdB2. inversion HdB1. destruct H7. inversion HdB2.
       destruct H9. apply (H1 _ H8 H10). destruct H9. rewrite H7 in H10.
       have HBq : dom B (names (cnt q)). rewrite (eq_sym H3). by right.  easy.
       destruct H7. inversion HdB2.  destruct H9. rewrite H9 in H8.
       have HBq : dom B (names (cnt q)). rewrite (eq_sym H3). by left.  easy.
       destruct H9. apply (H1 x H8 H10).
     + by apply (alpha_inter_ p q s3 s4 s1 s5 s6).
     + apply (IHHalph1_1 _ s3). assumption. split. intro. apply Hf1. rewrite (eq_sym H3). by left.
       split. move=> x HBxq. apply (Hf2 x). rewrite (eq_sym H3). by left. split. by inversion Hf3.
       by apply (inter_notin1 q s3 s4 s1). assumption.
     + apply (IHHalph1_2 _ s4). assumption. split. intro. apply Hf1. rewrite (eq_sym H3). by right.
       split. move=> x HBxq. apply (Hf2 x). rewrite (eq_sym H3). by right. split. by inversion Hf3.
       by apply (inter_notin1 q s3 s4 s1). assumption.
   - apply previstyp_ITE.
     + apply (IHHalph1_1 _ s1). assumption. split. assumption. split. assumption. split. by inversion Hf3.
       assumption. assumption.
     + apply (IHHalph1_2 _ s1). assumption. split. assumption. split. assumption. split. by inversion Hf3.
       assumption. assumption.
   - rewrite alpha_add_vis_leave; try easy.  rewrite alpha_empty_vis. rewrite (eq_sym H1) in Halph2. inversion Halph2. apply previstyp_FwF. left. by split.
   - rewrite alpha_add_vis_replace; try easy. rewrite alpha_empty_vis. rewrite (eq_sym H1) in Halph2.
     inversion Halph2. inversion H9. inversion H13. apply previstyp_FwC. rewrite H3. by right.
     by rewrite H3 in H15. easy.
   - rewrite alpha_add_vis_replace. by intro. rewrite alpha_empty_vis.
     rewrite (eq_sym H2) in Halph2. inversion Halph2. inversion H10. inversion H14. have Hrp : r=p. rewrite H5.
     rewrite H11. by rewrite H4. easy.
     rewrite H4. inversion H17. apply previstyp_FwC. left. rewrite (eq_sym H5). split. intro. by inversion H21.
     assumption. easy.
   - rewrite alpha_add_vis_leave. intro. by inversion H5.
     rewrite alpha_empty_vis. rewrite (eq_sym H2) in Halph2. inversion Halph2. rewrite H5 in H. by rewrite H0 in H. inversion H11. rewrite H0. inversion H15. apply previstyp_FwC. rewrite (eq_sym H12). by right. easy.
   - rewrite alpha_add_vis_leave. intro. by inversion H6. rewrite alpha_empty_vis.
     rewrite (eq_sym H3) in Halph2. inversion Halph2. rewrite (eq_sym H1) in H. by rewrite H6 in H.
     rewrite H1. inversion H12. rewrite H13 in H0. by rewrite H5 in H0.
     inversion H19. rewrite H5. apply previstyp_FwC. left. split. intro. by inversion H23. assumption.
Qed.




(*suggestion of new preweakening lemma *)
Inductive alpha_convert_proc : Names -> Names -> proc -> proc -> Prop :=
| alpha_ser : forall (a b : Nser) (P Q : proc), alpha_convert_proc_ser a b P Q -> alpha_convert_proc (srv a) (srv b) P Q
| alpha_con : forall (p q : Ncon) (P Q : proc), alpha_convert_proc_con p q P Q -> alpha_convert_proc (cnt p) (cnt q) P Q.

Lemma preWeakening_up_to_alpha : forall (x y : Names) (P Q : proc), alpha_convert_proc x y P Q ->  (forall (B1 B2 : Visfun) (s : stack_), preVisTyp B1 s Q -> Visfun_weak B1 B2 -> preVisTyp B2 s Q).
Proof.
Admitted.

Lemma preWeakening : forall (P : proc) (B1 B2 : Visfun) (s : stack_), preVisTyp B1 s P -> Visfun_weak B1 B2 -> preVisTyp B2 s P.
Proof.
  induction P.
  - intros. inversion H. have H4 : (dom_eq (dom B1) (dom B2)) by apply (proj1 H0).
    rewrite (eq_sym H1) in H4.
    have H5 : B2 = vis0. simpl in H4. apply (vis_is_empty B2 (dom_eq_sym empty (dom B2) H4)).
    rewrite H5. apply previstyp_nil.
  - intros. inversion H.
    have HdomB3 : dom_eq (dom B3) (add_dom empty cur). simpl. intro. easy.
    have HdomB21 : dom_eq (dom B2) (dom B1) by apply (dom_eq_sym (dom B1) (dom B2) (proj1 H0)).
    have HdomB2 : dom_eq (dom B2) (add_dom empty cur). rewrite (eq_sym H3) in HdomB21. apply (dom_eq_trans _ _ _ HdomB21 HdomB3).
    have HB2 : B2 = (add_vis cur (B2 cur) vis0).
    + apply eq_visfun. split. easy. intros. split.
      * intro. simpl. unfold add_vis_B. right. have Hxcur : (add_dom empty cur) x by apply ((proj1 (HdomB2 x)) (_dom B2 x y H10)). unfold add_dom in Hxcur. destruct Hxcur. easy. rewrite H11. by split.
      * intro. simpl in H10. unfold add_vis_B in H10. destruct H10. easy. destruct H10. rewrite H11 in H10. easy.
    + rewrite HB2. apply previstyp_O_SER. apply (proj2 H0 cur (srv n)). rewrite (eq_sym H3). simpl.
      unfold add_vis_B. right. by split.
      have HVb0 : forall (x : Names), Vb0 x -> (fun x : Names => B2 cur x \/ x = srv n1) x.
      intros. unfold Vb0 in H10. destruct H10. have HB3 : B3 cur x. simpl. unfold add_vis_B. right. easy.
      left. rewrite H3 in HB3. apply (proj2 H0 cur x HB3). by right.
      apply (IHP B0 _ _ H9). apply (weak_add _ _ (names (cnt n2)) _ _). apply (weak_add _ _ (names (srv n1)) _ _). apply Visfun_weak_refl. apply HVb0. apply HVb0.
  - intros. inversion H.
    rewrite (eq_sym H3) in H0.
    have HdomB3 : dom_eq (dom B3) (add_dom empty cur).
    simpl. apply dom_eq_refl.
    have HdomB2 : dom_eq (dom B2) (add_dom empty cur).
    apply (dom_eq_trans _ _ _  (dom_eq_sym _ _ (proj1 H0)) HdomB3).
    have HB2 : B2 = add_vis cur (B2 cur) vis0.
    + apply eq_visfun. split. easy.
      intros. split.
      * intro. simpl. right. have Hxcur : ((add_dom empty cur) x).
        apply (proj1 (HdomB2 x)). apply (_dom B2 x y H9). destruct Hxcur. easy. split. by rewrite H10. easy.
      * simpl. intro. destruct H9. easy. destruct H9. by rewrite H10 in H9.
    + rewrite HB2. apply previstyp_O_CON.
      * apply (proj2 H0 cur (cnt n)). simpl. right. by split.
      * apply (IHP B0 _ _ H8). apply (weak_add _ _ (names (cnt n)) Vb _). apply Visfun_weak_refl.
      intros. destruct H9. left. apply (proj2 H0 cur x). simpl. right. by split.
      right. by rewrite H6 in H9.
  - intros. inversion H.
    have HB2 : B2 = vis0. apply vis_is_empty. rewrite (eq_sym H1) in H0. apply dom_eq_sym. apply (proj1 H0).
    rewrite HB2. apply previstyp_O_LOC.
  -  intros. inversion H. rewrite (eq_sym H2) in H0.
     have HdomB2 : dom_eq (dom B2) (add_dom empty (names (srv a))).
     have HdomB3 : dom_eq (dom B3) (add_dom empty (names (srv a))) by easy.
     apply (dom_eq_trans _ _ _ (dom_eq_sym _ _ (proj1 H0)) HdomB3).
     have HB2 : B2 = add_vis (names (srv a)) (B2 (names (srv a))) vis0.
     + apply eq_visfun. split. by simpl. simpl. intros. split.
       * intro. have Hxa : (add_dom empty (names (srv a))) x by apply ((proj1 (HdomB2 x)) (_dom B2 x y H9)).
         destruct Hxa. easy. right. rewrite H10. by split.
       * intros. destruct H9. easy. destruct H9. by rewrite H10 in H9.
     + rewrite HB2. rewrite H1. apply previstyp_I_SER. apply (IHP B0 _ _ H4).
       apply weak_add. easy. intros. destruct H9.     
       * left. destruct H9. left. apply (proj2 H0). rewrite (eq_sym H1). simpl. right. easy.
         right. by rewrite H6 in H9.
       * right. by rewrite H7 in H9.
  -  intros. inversion H. rewrite (eq_sym H2) in H0.
     have HdomB2 : dom_eq (dom B2) (add_dom empty (names (cnt p))).
     have HdomB3 : dom_eq (dom B3) (add_dom empty (names (cnt p))) by easy.
     apply (dom_eq_trans _ _ _ (dom_eq_sym _ _ (proj1 H0)) HdomB3).
     have HB2 : B2 = add_vis (names (cnt p)) (B2 (names (cnt p))) vis0.
     + apply eq_visfun. split. by simpl. simpl. intros. split.
       * intro. have Hxa : (add_dom empty (names (cnt p))) x by apply ((proj1 (HdomB2 x)) (_dom B2 x y H8)).
         destruct Hxa. easy. right. rewrite H9. by split.
       * intros. destruct H8. easy. destruct H8. by rewrite H9 in H8.
     + rewrite HB2. rewrite H1. apply previstyp_I_CON. apply (IHP B0 _ _ H4).
       apply weak_add. easy. intros. destruct H8.
       * left. apply (proj2 H0 _ _). right. by rewrite H1.
       * right. by rewrite H6 in H8.
  - intros. inversion H. rewrite (eq_sym H2) in H0. 
    have HdomB0 : dom_eq (dom B0) (add_dom empty cur) by easy.
    have HdomB2 : dom_eq (dom B2) (add_dom empty cur) by apply (dom_eq_trans _ _ _ (dom_eq_sym _ _ (proj1 H0)) HdomB0).
    have HB2 : B2 = add_vis cur (B2 cur) vis0.
    + apply eq_visfun. split. easy. intros. split.
      * intro. right. have Hxcur : (add_dom empty cur) x by apply ((proj1 (HdomB2 x)) (_dom B2 x y H7)).
        destruct Hxcur. easy. by rewrite H8.
      * intro. destruct H7. easy. destruct H7. by rewrite H8 in H7.
    + rewrite HB2. apply previstyp_I_LOC. apply (IHP B0 _ _ H4). apply weak_add. easy.
      intros. apply (proj2 H0 cur x). right. easy.
(*  - intros. inversion H.
    have HB2 : B2 = trans_minus_vis (srv a) (add_vis (names (srv a)) (B1 (names (srv a))) B2).
    + apply eq_visfun. split.
      * simpl. split. intro. split. by left. intro. rewrite (eq_sym H7) in H6. have Ha : dom B1 (names (srv a)) by apply (proj1 H0 (names (srv a))). rewrite (eq_sym H2) in Ha. simpl in Ha. unfold minus_dom in Ha.
        easy.
        intro. unfold minus_dom in H6. unfold add_dom in H6. destruct H6. destruct H6. easy. exfalso. apply (H7 H6).
      * intros. split.
        ++ simpl. unfold trans_minus_vis_B. unfold loc_trans_B. unfold add_vis_B. intro.
           split. left. by left.*)
  - move=> B1 B2 s Htyp Hweak. destruct (exists_fresh_nser B2 s (proc_nu_ser n P)) as [a Ha].
    destruct (exists_alpha_convert_proc_ser n a (proc_nu_ser n P)) as [Q HQ].
    have Halph_typ2 : preVisTyp (alpha_convert_vis (srv n) (srv a) B2) s Q.
    have Halph_typ1 : preVisTyp (alpha_convert_vis (srv n) (srv a) B1) s Q.
    + apply (alpha_ser_previstyp (proc_nu_ser n P)); try assumption.
      destruct Ha. split. intro. apply (H (proj1 (proj1 Hweak (names (srv a))) H1)).
      destruct H0. split. move=> x Hx. apply ((H0 x) (proj2 Hweak x (srv a) Hx)). assumption.
    + admit.
    + admit.                                          (* NEEDS ALPHA-CONVERSION *)
  - admit.
  - intros. inversion H. apply previstyp_RES_LOC. apply (IHP B1 B2 s0 H4 H0).
  - intros. inversion H. rewrite (eq_sym H5) in H0.
    have HB0 : B0 = rest_vis B4 (dom B0) (dom_fin B0) by apply (eq_sym (rest_union_vis B0 B3 H3)).
    have HB4 : B4 = union_vis B3 B0 by apply (union_vis_comm B0 B3).
    have HB3 : B3 = rest_vis B4 (dom B3) (dom_fin B3). rewrite HB4. apply (eq_sym (rest_union_vis B3 B0 (dom_disj_comm (dom B0) (dom B3) H3))).    
    have HB2 : B2 = union_vis (rest_vis B2 (dom B0) (dom_fin B0)) (rest_vis B2 (dom B3) (dom_fin B3)).
    apply eq_visfun. split.
    + simpl. replace (dom_union (dom B0) (dom B3)) with (dom B4). apply (dom_eq_sym _ _ (proj1 H0)). easy.
    + intros. split.
      * intro. simpl. unfold vis_union_B. unfold vis_rest_B.
        have Hx : dom B4 x by apply ((proj2 ((proj1 H0) x )) (_dom B2 x y H9)).
        simpl in Hx. destruct Hx. left. easy. right. easy.
      * simpl. intro. destruct H9. by destruct H9. by destruct H9.
    + rewrite HB2. apply (previstyp_PAR _ _ _ s2 s3).
      * by simpl.
      * by apply H4.
      * rewrite HB0 in H7. apply (IHP1 (rest_vis B4 (dom B0) (dom_fin B0)) _ _ H7).
        by apply weak_rest.
      * rewrite HB3 in H8. apply (IHP2 (rest_vis B4 (dom B3) (dom_fin B3)) _ _ H8). by apply weak_rest.
  - intros. inversion H. apply previstyp_ITE. by apply (IHP1 B1). by apply (IHP2 B1).
  - intros. inversion H. rewrite (eq_sym H2) in H0.
    have HdomB2 : dom_eq (dom B2) (add_dom empty (names (srv a))) by apply (dom_eq_trans _ (dom B0) _ (dom_eq_sym _ _ (proj1 H0))). 
    have HB2 : B2 = add_vis (names (srv a)) (B2 (names (srv a))) vis0.
    + apply eq_visfun. split. easy.
      intros. simpl. split.
      * intro. right. have Hax : (add_dom empty (names (srv a))) x by apply ((proj1 (HdomB2 x)) (_dom B2 x y H6)). simpl in Hax. destruct Hax. easy. rewrite H7. by split.
      * intro. destruct H6. easy. destruct H6. by rewrite H7 in H6.
    + rewrite HB2. rewrite H1. apply previstyp_FwF. apply (proj2 H0 _ _). simpl. right. rewrite H1. by split.
  - intros. inversion H. rewrite (eq_sym H2) in H0.
    have HdomB2 : dom_eq (dom B2) (add_dom empty (names (cnt p))) by apply (dom_eq_trans _ (dom B0) _ (dom_eq_sym _ _ (proj1 H0))). 
    have HB2 : B2 = add_vis (names (cnt p)) (B2 (names (cnt p))) vis0.
    + apply eq_visfun. split. easy.
      intros. simpl. split.
      * intro. right. have Hpx : (add_dom empty (names (cnt p))) x by apply ((proj1 (HdomB2 x)) (_dom B2 x y H6)). simpl in Hpx. destruct Hpx. easy. rewrite H7. by split.
      * intro. destruct H6. easy. destruct H6. by rewrite H7 in H6.
    + rewrite HB2. rewrite H1. rewrite (eq_sym H5). apply previstyp_FwC. apply (proj2 H0 _ _). simpl. right. rewrite H1. rewrite H5. by split.
Admitted.
